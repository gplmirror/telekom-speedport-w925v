
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include "libArcComApi.h"
/*******************************************************************************
 * Description
 *		Sets a memory block to all 0's. pls refer to bzero()
 *
 ******************************************************************************/
void* osBZero(void* pMem, size_t ulCnt)
{
	if (pMem!=ARC_COM_NULL && ulCnt>0)
		return memset(pMem,0,ulCnt);

	return 0;
}

/*******************************************************************************
 * Description
 *		Execute shell command
 *
 * Parameters
 *		sShellCmd:	shell command to execute
 *
 * Returns
 *		* ARC_COM_OK(0):		successfully
 *		* ARC_COM_ERROR(-1):	failed
 *
 ******************************************************************************/
long osSystem( char* sShellCmd )
{
	int ret_value=0;

	if (sShellCmd == ARC_COM_NULL)
		return ARC_COM_ERROR;

	if ((ret_value=system( sShellCmd )) == -1){
		int err_number=errno;

		switch(err_number){
			case ECHILD: /* 10:ECHILD No Child Process, but the shell command was launched completely, ignore this case. */
				return ARC_COM_OK;
			default:
				break;
	  	}
		return ARC_COM_ERROR;
	}

	while ( waitpid( -1, 0, WNOHANG ) > 0 )
	{
		/* NOOP */;
	}

	return ARC_COM_OK;
}

int arc_system(char *cmd)
{
	int rt = 0;
	rt = osSystem(cmd);

	return rt;
}

/*******************************************************************************
 * Description
 *		Execute shell command and get the output
 *
 * Parameters
 *		sShellCmd:	shell command to execute
 *		sBuf:		buffer to be filled with execution output. It can be NULL.
 *		iBufSz:		length of sBuf.
 *
 * Returns
 *		* >=0:					output size
 *		* ARC_COM_ERROR(-1):	failed
 *
 * Note
 *		* only up to iBufSz-1 bytes are copied into sBuf if output size is larger than iBufSz
 *
 ******************************************************************************/
long osSystem_GetOutput ( char* sShellCmd, char* sBuf, int iBufSz )
{
	char	sCmd[512];
	char	sTmpFn[32];
	FILE*	pFile;
	long	lCnt;

	if (sShellCmd == ARC_COM_NULL)
		return ARC_COM_ERROR;

	if (strlen(sShellCmd) > (sizeof(sCmd)-32))
		return ARC_COM_ERROR;

	sprintf( sTmpFn, "/tmp/%ld.XXXXXX", (long)getpid() );
	close(mkstemp(sTmpFn));

	sprintf( sCmd, "%s > %s", sShellCmd, sTmpFn );

	if (osSystem( sCmd ) != ARC_COM_OK)
	{
		unlink( sTmpFn );
		printf("[osSystem_GetOutput] osSystem failed, cmd=%s\n", sCmd);
		return ARC_COM_ERROR;
	}

	if ( (pFile=fopen(sTmpFn,"r")) == NULL ){
	    printf("[osSystem_GetOutput] fopen failed, cmd=%s\n", sCmd);
		return ARC_COM_ERROR;
	}
	if (sBuf && iBufSz>0)
	{
		osBZero( sBuf, iBufSz);
		fread( sBuf, 1, iBufSz-1, pFile );
	}

	fclose( pFile );

	lCnt = osFileSize( sTmpFn );

	unlink( sTmpFn );

	return lCnt;
}

/*******************************************************************************
 * Description
 *		Get the size of a file.
 *
 * Parameters
 *		sFileName:	file name
 *
 * Returns
 *		* ARC_COM_ERROR(-1):	file does not exist or is not readable
 *		* others:				size of file
 *
 ******************************************************************************/
long osFileSize ( const char* sFileName )
{
	struct stat	stBuf;

	if (sFileName == ARC_COM_NULL)
		return ARC_COM_ERROR;

	if (stat(sFileName, &stBuf) < 0)
		return ARC_COM_ERROR;

	return (long)stBuf.st_size;
}
/*******************************************************************************
 * Description
 *		Print formatted output to CONSOLE
 *
 ******************************************************************************/
int osPrintfConsole(const char* sFmt, ...)
{
	va_list		vlVars;
	char		sBuf[512];
	int			iRet;
	FILE*		fnOut;

	if (sFmt == ARC_COM_NULL)
		return 0;

	fnOut = fopen( "/dev/console", "w" );
	if (fnOut == ARC_COM_NULL)
		return 0;

	sBuf[sizeof(sBuf)-1] = '\0';

	va_start(vlVars,sFmt);

	iRet = vsnprintf(sBuf,sizeof(sBuf),sFmt,vlVars);

	va_end(vlVars);

	if (sBuf[sizeof(sBuf)-1] != '\0')
	{
		fprintf( fnOut, "NOTE: osPrintfConsole() overflow!!!\n");
	}

	sBuf[sizeof(sBuf)-1] = '\0';

	fprintf( fnOut, "%s", sBuf );
	fflush( fnOut );
	fclose( fnOut );

	return iRet;
}

int debug_act = 1;
int arc_dprintf(const char* sFmt, ...)
{
	va_list		vlVars;
	char		sBuf[512];
	int			iRet;
	FILE*		fnOut;

	if (sFmt == ARC_COM_NULL || debug_act == 0)
		return 0;

	fnOut = fopen( "/dev/console", "w" );
	if (fnOut == ARC_COM_NULL)
		return 0;

	sBuf[sizeof(sBuf)-1] = '\0';

	va_start(vlVars,sFmt);

	iRet = vsnprintf(sBuf,sizeof(sBuf),sFmt,vlVars);

	va_end(vlVars);

	if (sBuf[sizeof(sBuf)-1] != '\0')
	{
		fprintf( fnOut, "NOTE: osPrintfConsole() overflow!!!\n");
	}

	sBuf[sizeof(sBuf)-1] = '\0';

	fprintf( fnOut, "%s", sBuf );
	fflush( fnOut );
	fclose( fnOut );

	return iRet;
}
