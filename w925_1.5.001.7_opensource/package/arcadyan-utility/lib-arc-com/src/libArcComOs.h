#ifndef _ARC_COM_OS_H_
#define _ARC_COM_OS_H_

#include <stdlib.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

extern void*	osBZero(void* pMem, size_t ulCnt);
extern long		osFileSize ( const char* sFileName );
extern int		osPrintfConsole(const char* sFmt, ...);
extern long		osSystem ( char* sShellCmd );
extern int 		arc_system(char *cmd);
extern long		osSystem_GetOutput ( char* sShellCmd, char* sBuf, int iBufSz );

extern int 		arc_dprintf(const char* sFmt, ...);
#ifdef __cplusplus
}
#endif


#endif /* _ARC_COM_OS_H_ */
