
struct timezone {
	   int tz_minuteswest;     /* minutes west of Greenwich */
	   int tz_dsttime;         /* type of DST correction */
};

struct timeZoneTable_ {
           unsigned char name[8];
           int           gmtOffset;
           char          dstFlag;
};

typedef struct timeZoneTable_ timeZoneTable;
 
struct dstTable_ {
           unsigned char  startDay[21];
           unsigned char  endDay[21];
           unsigned char  startMonth;
           unsigned char  endMonth;
           unsigned char  startHour;
           unsigned char  endHour;
           unsigned char  diffMonth;
           char           resv;
           int            dstBias;
};
typedef struct dstTable_ dstTable;

const timeZoneTable tzEntry[] = {
	{"tz0",			-720,	0}, //(GMT-12:00)Eniwetok, Kwajalein
	{"tz1",			-660,	0}, //(GMT-11:00)Midway Island, Samoa
	{"tz2",			-600,	0}, //(GMT-10:00)Hawaii  
	{"tz3",			-540,	0}, //(GMT-09:00)Alaska
	{"tz4",			-480,	0}, //(GMT-08:00)Pacific Time (US & Canada)   
	{"tz5",       	-480,	1}, //(GMT-08:00)Tijuana  
	{"tz6",        	-420,	0}, //(GMT-07:00)Arizona
	{"tz7",        	-420,	0}, //(GMT-07:00)Mountain Time (US & Canada)
	{"tz8",       	-360,	0}, //(GMT-06:00)Central Time (US & Canada)
	{"tz9",       	-360,	0}, //(GMT-06:00)Tegucigalpa
	{"tz10",       	-360,	6}, //(GMT-06:00)Mexico City
	{"tz11",       	-300,	0}, //(GMT-06:00)Saskatchewan
	{"tz12",       	-300,	0}, //(GMT-05:00)Bogota, Lima, Quito
	{"tz13",		-300,	0}, //(GMT-05:00)Eastern Time (US & Canada)
	{"tz14", 		-300,	1}, //(GMT-05:00)Indiana (East) 
	{"tz15", 		-240,	0}, //(GMT-04:00)Atlantic Time (Canada)
	{"tz16", 		-240,	0}, //(GMT-04:00)Caracas, La Paz
	{"tz17", 		-240,	0}, //(GMT-04:00)Santiago
	{"tz18",		-210,	0}, //(GMT-03:30)Newfoundland
	{"tz19",		-180,	0}, //(GMT-03:00)Brasilia
	{"tz20",		-180,	0}, //(GMT-03:00)Buenos Aires, Georgetown
	{"tz21", 		-120,	0}, //(GMT-02:00)Mid-Atlantic
	{"tz22",       	-60,	2}, //(GMT-01:00)Azores
	{"tz23",       	-60,	0}, //(GMT-01:00)Cape Verde Is.
	{"tz24",        0,		0}, //(GMT)Casablanca, Monrovia
	{"tz25",       	0,		0}, //(GMT)Greenwich Mean Time: Edinburgh	
	{"tz26",       	0,		2}, //(GMT)Greenwich Mean Time: Dublin 
	{"tz27",       	0,		2}, //(GMT)Greenwich Mean Time: Lisbon
	{"tz28",       	0,		2}, //(GMT)Greenwich Mean Time: London
	{"tz29",       	60,		0}, //(GMT+01:00)Bern 
	{"tz30",       	60,		2}, //(GMT+01:00)Amsterdam, Berlin, Rome, Stockholm, Vienna  
	{"tz31",       	60,		2}, //(GMT+01:00)Belgrade, Bratislava, Budapest, Ljubljana 
	{"tz32",       	60,		2}, //(GMT+01:00)Prague, Barcelona, Madrid
	{"tz33",       	60,		2}, //(GMT+01:00)Brussels, Copenhagen, Paris	
	{"tz34",       	60,		2}, //(GMT+01:00)Vilnius, Sofija
	{"tz36",       	60,		2}, //(GMT+01:00)Sarajevo, Skopje, Warsaw, Zagreb
	{"tz37",       	120,	2}, //(GMT+02:00)Athens, Istanbul, Bucharest
	{"tz38",		120,	2}, //(GMT+02:00)Minsk
	{"tz39", 		120,	0}, //(GMT+02:00)Cairo
	{"tz40", 		120,	0}, //(GMT+02:00)Harare, Pretoria
	{"tz41", 		120,	2}, //(GMT+02:00)Helsinki, Riga, Tallinn
	{"tz42", 		120,	0}, //(GMT+02:00)Jerusalem
	{"tz43", 		180,	0}, //(GMT+03:00)Baghdad, Kuwait, Riyadh
	{"tz44", 		180,	0}, //(GMT+03:00)Moscow, St. Petersburg	
	{"tz45", 		180,	2}, //(GMT+03:00)Volgograd
	{"tz46", 		180,	0}, //(GMT+03:00)Mairobi
	{"tz47", 		210,	0}, //(GMT+03:30)Tehran
	{"tz48", 		240,	0}, //(GMT+04:00)Abu Dhabi, Muscat
	{"tz49", 		240,	0}, //(GMT+04:00)Tbilisi	
	{"tz50", 		240,	2}, //(GMT+04:00)Baku
	{"tz51", 		270,	0}, //(GMT+04:30)Kabul
	{"tz52", 		300,	0}, //(GMT+05:00)Ekaterinburg
	{"tz53", 		300,	2}, //(GMT+05:00)Yekaterinburg	
	{"tz54", 		300,	0}, //(GMT+05:00)Islamabad, Karachi, Tashkent
	{"tz55", 		330,	0}, //(GMT+05:30)Bombay, Calcutta, Madras, New Delhi
	{"tz56",		360,	0}, //(GMT+06:00)Astana, Almaty, Dhaka
	{"tz57",		360,	0}, //(GMT+06:00)Colombo
	{"tz58",		420,	0}, //(GMT+07:00)Bangkok, Hanoi, Jakarta
	{"tz59",		480,	0}, //(GMT+08:00)Beijing, Chongqing, Hong Kong, Urumqi
	{"tz60",		480,	0}, //(GMT+08:00)Perth
	{"tz61",		480,	0}, //(GMT+08:00)Kuala Lumpur, Singapore
	{"tz62",		480,	0}, //(GMT+08:00)Taipei
	{"tz63",		540,	0}, //(GMT+09:00)Osaka, Sapporo, Tokyo
	{"tz64",		540,	0}, //(GMT+09:00)Seoul
	{"tz65",		540,	2}, //(GMT+09:00)Yakutsk
	{"tz66",		570,	4}, //(GMT+09:30)Adelaide
	{"tz67",		570,	0}, //(GMT+09:30)Darwin
	{"tz68",		600,	0}, //(GMT+10:00)Brisbane
	{"tz69",		600, 	4}, //(GMT+10:00)Canberra
	{"tz70",		600, 	4}, //(GMT+10:00)Melbourne, Sydney, Hobart
	{"tz71",		600, 	0}, //(GMT+10:00)Guam, Port Moresby
	{"tz72",		600, 	2}, //(GMT+10:00)Vladivostok
	{"tz73",		660, 	0}, //(GMT+11:00)Solomon Is., New Caledonia
	{"tz74",		660, 	2}, //(GMT+11:00)Magadan
	{"tz75",		720,	0}, //(GMT+12:00)Wllington
	{"tz76",		720,	3}, //(GMT+12:00)Auckland
	{"tz77",		720,	0}, //(GMT+12:00)Fiji, Marshall Is.
	{"tz78",		720,	2}, //(GMT+12:00)Kamchatka
};

const  dstTable dstEntry[] = {
	{{ },                                   { },                                    0, 0, 0, 0, 0},
		/** (1) US and Canada [from 2007, 2:00am, 2nd Sunday in March ~ 2:00am, 1st Sunday in November]*/
		  {{ 11,9,8,14,13,11,10,9,8,13,12,11,10,8,14,13,12,10,9,8,14 },
		    { 4,2,1,7,6,4,3,2,1,6,5,4,3,1,7,6,5,3,2,1,7 }, 3, 11, 2, 1, 0, 0, 3600}

		/** (2) European Union [1:ooam, last Sunday in March ~ 1:00am, last Sunday in October]*/
		, {{ 25,30,29,28,27,25,31,30,29,27,26,25,31,29,28,27,26,31,30,29,28 },
		    { 28,26,25,31,30,28,27,26,25,30,29,28,27,25,31,30,29,27,26,25,31 }, 3, 10, 1, 0, 0, 0, 3600}

		 /*John@2008.05.27,modify new daylight time */
                /** (3) New Zealand [2:00am, begin on the last Sunday in Sep ~ 3:00am,
                *end on the 1st  Sunday in April]*/
                , {{ 30,28,27,26,25,30,29,28,27,25,24,30,29,27,26,25,24,29,28,27,26},
                    {1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4}, 9, 4, 2, 2, 1, 0, 3600}
                                                                                                               
                /** (4) Australia [2:00am, begin on the 1st Sunday in October ~ 3:00am,
                 *end on the 1st  Sunday in April]*/
                , {{ 7,5,4,3,2,7,6,5,4,2,1,7,6,4,3,2,1,6,5,4,3},
                    { 1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4}, 10, 4, 2, 2, 1, 0, 3600}

		/** (5) Greenland [22:00, last Saturday in March ~ 23:00, last Saturday in October]*/
		, {{ 24,29,28,27,26,24,30,29,28,26,25,24,30,28,27,26,25,30,29,28,27 },
		    { 27,25,24,30,29,27,26,25,24,29,28,27,26,24,30,29,28,26,25,24,30 }, 3, 10, 22, 22, 0, 0, 3600}
		
		/** (6) Mexico [from 2007 2:00am, 1st Sunday in April ~ 2:00am, last Sunday in Octobera]*/
		//Jemmy Fix Mexico daylight saveing time issue 2010.08.30
		, {{ 1,6,5,4,3,1,7,6,5,3,2,1,7,5,4,3,2,7,6,5,4 },
		    { 28,26,25,31,30,28,27,26,25,30,29,28,27,25,31,30,29,27,26,25,31 }, 4, 10, 2, 1, 0, 0, 3600}
	};

const int tzEntrySize = sizeof(tzEntry) / sizeof(timeZoneTable) ;

