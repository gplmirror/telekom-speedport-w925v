/*
 * ntpclient.c - NTP client
 *
 * Copyright 1997, 1999, 2000, 2003, 2006, 2007  Larry Doolittle  <larry@doolittle.boa.org>
 * Last hack: December 30, 2007
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License (Version 2,
 *  June 1991) as published by the Free Software Foundation.  At the
 *  time of writing, that license was published by the FSF with the URL
 *  http://www.gnu.org/copyleft/gpl.html, and is incorporated herein by
 *  reference.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Possible future improvements:
 *      - Write more documentation  :-(
 *      - Support leap second processing
 *      - Support IPv6
 *      - Support multiple (interleaved) servers
 *
 *  Compile with -DPRECISION_SIOCGSTAMP if your machine really has it.
 *  There are patches floating around to add this to Linux, but
 *  usually you only get an answer to the nearest jiffy.
 *  Hint for Linux hacker wannabes: look at the usage of get_fast_time()
 *  in net/core/dev.c, and its definition in kernel/time.c .
 *
 *  If the compile gives you any flak, check below in the section
 *  labelled "XXX fixme - non-automatic build configuration".
 */

#define _POSIX_C_SOURCE 199309

#ifdef USE_OBSOLETE_GETTIMEOFDAY
#define _BSD_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>     /* gethostbyname */
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>

#ifdef PRECISION_SIOCGSTAMP
#include <sys/ioctl.h>
#endif
#ifdef USE_OBSOLETE_GETTIMEOFDAY
#include <sys/time.h>
#endif
#include <sys/stat.h>
#include <sys/wait.h>

#include "ntpclient.h"
#include "timezone.h"

/* Default to the RFC-4330 specified value */
#ifndef MIN_INTERVAL
#define MIN_INTERVAL 15
#endif

#ifdef ENABLE_DEBUG
#define DEBUG_OPTION "d"
int debug=0;
#else
#define DEBUG_OPTION
#endif

#ifdef ENABLE_REPLAY
#define  REPLAY_OPTION   "r"
#else
#define  REPLAY_OPTION
#endif

#if !defined(FALSE)  || (FALSE != 0)
#define FALSE  (0)
#endif

#if !defined(TRUE) || (TRUE != 1)
#define TRUE   (1)
#endif

extern char *optarg;  /* according to man 2 getopt */

#include <stdint.h>
typedef uint32_t u32;  /* universal for C99 */
/* typedef u_int32_t u32;   older Linux installs? */

/* XXX fixme - non-automatic build configuration */
#ifdef __linux__
#include <sys/utsname.h>
#include <sys/time.h>
#include <sys/timex.h>
#else
extern struct hostent *gethostbyname(const char *name);
extern int h_errno;
#define herror(hostname) \
	fprintf(stderr,"[ntpclient] Error %d looking up hostname %s\n", h_errno,hostname)
#endif
/* end configuration for host systems */

#if 0
#define cprintf(fmt, args...) do { \
	FILE *fp = fopen("/dev/console", "w"); \
	if (fp) { \
		char uptime[32]; \
		FILE *fp2 = NULL; \
		if((fp2 = fopen("/proc/uptime", "r"))) \
		{ \
			fscanf(fp2, "%s", uptime); \
			fclose(fp2); \
		} \
		fprintf(fp, "[%s][ntpclient][%s] "fmt, uptime, __func__, ## args); \
		fclose(fp); \
	} \
} while (0)
#else
#define cprintf(fmt, args...)
#endif

#define JAN_1970        0x83aa7e80      /* 2208988800 1970 - 1900 in seconds */
#define NTP_PORT (123)

/* How to multiply by 4294.967296 quickly (and not quite exactly)
 * without using floating point or greater than 32-bit integers.
 * If you want to fix the last 12 microseconds of error, add in
 * (2911*(x))>>28)
 */
#define NTPFRAC(x) ( 4294*(x) + ( (1981*(x))>>11 ) )

/* The reverse of the above, needed if we want to set our microsecond
 * clock (via clock_settime) based on the incoming time in NTP format.
 * Basically exact.
 */
#define USEC(x) ( ( (x) >> 12 ) - 759 * ( ( ( (x) >> 10 ) + 32768 ) >> 16 ) )

/* Converts NTP delay and dispersion, apparently in seconds scaled
 * by 65536, to microseconds.  RFC-1305 states this time is in seconds,
 * doesn't mention the scaling.
 * Should somehow be the same as 1000000 * x / 65536
 */
#define sec2u(x) ( (x) * 15.2587890625 )

struct ntptime {
	unsigned int coarse;
	unsigned int fine;
};

//Charles: new IPV4+IPV6 sockaddr, is better then struct sockaddr_storage
struct len_and_sockaddr {
	socklen_t len;
	union
	{
		struct sockaddr sa;
		struct sockaddr_in sin;
		struct sockaddr_in6 sin6;
	};
};

struct ntp_control {
	u32 time_of_send[2];
	int live;
	int set_clock;   /* non-zero presumably needs root privs */
	int probe_count;
	int cycle_time;
	int goodness;
	int cross_check;
	//char serv_addr[4];
	/* modify by Charles for support IPV6 address */
	struct sockaddr_storage serv_addr;
	int ipversion;
	short int udp_local_port;   /* default of 0 means kernel chooses */
	int interval_time;   /* interval for correcting time */
	/* End */
};

/* prototypes for some local routines */
static void send_packet(int usd, u32 time_sent[2]);
static int rfc1305print(u32 *data, struct ntptime *arrival, struct ntp_control *ntpc, int *error);
static void get_time_zone(struct timeval tv, struct timezone *p_timezone);

#define MAX_CMD_STR_LEN  200
// define syslog type
#define LOG_TYPE_OTHER              0               // other type

#define LOG_MESSAGE_TYPE_SERVICE        6

#define PRIVACY_PID "/tmp/privacy.pid"

struct timeval last_update_HS_time;

static void get_time_zone(struct timeval tv, struct timezone *p_timezone)
{
	struct tm tm;
	int TimeZone, i, dls;	
	char startMonth, endMonth, startHour, endHour, diffMonth;
	char buf[16];

	osSystem_GetOutput("mngcli get ARC_SYS_TimeZone", buf, sizeof(buf));
	TimeZone = atoi(buf);
	osSystem_GetOutput("mngcli get ARC_SYS_DaylightSaving", buf, sizeof(buf));
	dls = atoi(buf);

	p_timezone->tz_minuteswest = tzEntry[TimeZone].gmtOffset;

	if(dls == 0)
	{
		return;
	}

	memcpy(&tm, localtime(&tv.tv_sec), sizeof(struct tm));

	startMonth = dstEntry[(int)tzEntry[TimeZone].dstFlag].startMonth;
	endMonth = dstEntry[(int)tzEntry[TimeZone].dstFlag].endMonth;
	diffMonth = dstEntry[(int)tzEntry[TimeZone].dstFlag].diffMonth;
	startHour = dstEntry[(int)tzEntry[TimeZone].dstFlag].startHour;
	endHour = dstEntry[(int)tzEntry[TimeZone].dstFlag].endHour;

	i = tm.tm_year+1900-2007;
	if (tzEntry[TimeZone].dstFlag && (((diffMonth == 0) &&
		((tm.tm_mon+1 == startMonth && 
		((tm.tm_mday > dstEntry[(int)tzEntry[TimeZone].dstFlag].startDay[i]) || (tm.tm_mday == dstEntry[(int)tzEntry[TimeZone].dstFlag].startDay[i] && tm.tm_hour >= startHour))) || 
		(tm.tm_mon+1 == endMonth && 
		((tm.tm_mday < dstEntry[(int)tzEntry[TimeZone].dstFlag].endDay[i]) || (tm.tm_mday == dstEntry[(int)tzEntry[TimeZone].dstFlag].endDay[i] && tm.tm_hour < endHour))) || 
		(tm.tm_mon+1 > startMonth && tm.tm_mon+1 < endMonth))) || 
		((diffMonth == 1) && ((tm.tm_mon+1 == startMonth && 
		((tm.tm_mday > dstEntry[(int)tzEntry[TimeZone].dstFlag].startDay[i]) || (tm.tm_mday == dstEntry[(int)tzEntry[TimeZone].dstFlag].startDay[i] && tm.tm_hour >= startHour))) ||
		(tm.tm_mon+1 == endMonth && 
		((tm.tm_mday < dstEntry[(int)tzEntry[TimeZone].dstFlag].endDay[i]) || (tm.tm_mday == dstEntry[(int)tzEntry[TimeZone].dstFlag].endDay[i] && tm.tm_hour < endHour))) || 
		(tm.tm_mon+1 > startMonth || tm.tm_mon+1 < endMonth)))) )
	{	
		p_timezone->tz_minuteswest += dstEntry[(int)tzEntry[TimeZone].dstFlag].dstBias/60;
	}
	return;
}

static int get_current_freq(void)
{
	/* OS dependent routine to get the current value of clock frequency.
	 */
#ifdef __linux__
	struct timex txc;
	txc.modes=0;
	if (__adjtimex(&txc) < 0) {
		perror("adjtimex"); exit(1);
	}
	return txc.freq;
#else
	return 0;
#endif
}

static int set_freq(int new_freq)
{
	/* OS dependent routine to set a new value of clock frequency.
	 */
#ifdef __linux__
	struct timex txc;
	txc.modes = ADJ_FREQUENCY;
	txc.freq = new_freq;
	if (__adjtimex(&txc) < 0) {
		perror("adjtimex"); exit(1);
	}
	return txc.freq;
#else
	return 0;
#endif
}

static void set_time(struct ntptime *new)
{
#if 0//ndef USE_OBSOLETE_GETTIMEOFDAY
	/* POSIX 1003.1-2001 way to set the system clock
	 */
	struct timespec tv_set;
	/* it would be even better to subtract half the slop */
	tv_set.tv_sec  = new->coarse - JAN_1970;
	/* divide xmttime.fine by 4294.967296 */
	tv_set.tv_nsec = USEC(new->fine)*1000;
	if (clock_settime(CLOCK_REALTIME, &tv_set)<0) {
		perror("clock_settime");
		exit(1);
	}

	if (debug) {
		printf("set time to %lu.%.9lu\n", tv_set.tv_sec, tv_set.tv_nsec);
	}
#else
	/* Traditional Linux way to set the system clock
	 */
	struct timeval tv_set;
	struct timezone tz;
	tz.tz_dsttime = 0;
	tz.tz_minuteswest = 0;
	FILE *fp = NULL;
	
	/* it would be even better to subtract half the slop */
	tv_set.tv_sec  = new->coarse - JAN_1970;
	/* divide xmttime.fine by 4294.967296 */
	tv_set.tv_usec = USEC(new->fine);
	
	get_time_zone(tv_set, &tz);
	
	if (settimeofday(&tv_set,&tz)<0) {
		perror("settimeofday");
		exit(1);
	}

	//check wifi time rule
	if(fp = fopen("/tmp/ntp_status", "r"))
	{
		fclose(fp);
	}
	else
	{
		system("mapi_wlan_cli time_rule reset");
	}
	if (debug) {
		printf("set time to %lu.%.6lu\n", tv_set.tv_sec, tv_set.tv_usec);
	}
#endif
}

static void ntpc_gettime(u32 *time_coarse, u32 *time_fine)
{
#ifndef USE_OBSOLETE_GETTIMEOFDAY
	/* POSIX 1003.1-2001 way to get the system time
	 */
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	*time_coarse = now.tv_sec + JAN_1970;
	*time_fine   = NTPFRAC(now.tv_nsec/1000);
#else
	/* Traditional Linux way to get the system time
	 */
	struct timeval now;
	gettimeofday(&now, NULL);
	*time_coarse = now.tv_sec + JAN_1970;
	*time_fine   = NTPFRAC(now.tv_usec);
#endif
}

static void send_packet(int usd, u32 time_sent[2])
{
	u32 data[12];
#define LI 0
#define VN 3
#define MODE 3
#define STRATUM 0
#define POLL 4
#define PREC -6	

	if (debug) fprintf(stderr,"[ntpclient] Sending ...\n");
	if (sizeof data != 48) {
		fprintf(stderr,"[ntpclient] size error\n");
		return;
	}
	memset(data,0,sizeof data);
	data[0] = htonl (
		( LI << 30 ) | ( VN << 27 ) | ( MODE << 24 ) |
		( STRATUM << 16) | ( POLL << 8 ) | ( PREC & 0xff ) );
	data[1] = htonl(1<<16);  /* Root Delay (seconds) */
	data[2] = htonl(1<<16);  /* Root Dispersion (seconds) */
	ntpc_gettime(time_sent, time_sent+1);
	data[10] = htonl(time_sent[0]); /* Transmit Timestamp coarse */
	data[11] = htonl(time_sent[1]); /* Transmit Timestamp fine   */
	send(usd,data,48,0);
}

static void get_packet_timestamp(int usd, struct ntptime *udp_arrival_ntp)
{
#ifdef PRECISION_SIOCGSTAMP
	/* XXX broken */
	struct timeval udp_arrival;
	if ( ioctl(usd, SIOCGSTAMP, &udp_arrival) < 0 ) {
		perror("ioctl-SIOCGSTAMP");
		gettimeofday(&udp_arrival,NULL);
	}
	udp_arrival_ntp->coarse = udp_arrival.tv_sec + JAN_1970;
	udp_arrival_ntp->fine   = NTPFRAC(udp_arrival.tv_usec);
#else
	(void) usd;  /* not used */
	ntpc_gettime(&udp_arrival_ntp->coarse, &udp_arrival_ntp->fine);
#endif
}

//static int check_source(int data_len, struct sockaddr *sa_source, unsigned int sa_len, struct ntp_control *ntpc)
static int check_source(int data_len, struct sockaddr_storage *sa_source, unsigned int sa_len, struct ntp_control *ntpc)
{
	struct sockaddr_in *sa_in=(struct sockaddr_in *)sa_source;
	struct sockaddr_in6 *sa_in6=(struct sockaddr_in6 *)sa_source;
	(void) sa_len;  /* not used */
	if (debug) {
		printf("packet of length %d received\n",data_len);
		if (sa_source->ss_family==AF_INET) {
			printf("Source: INET Port %d host %s\n",
				ntohs(sa_in->sin_port),inet_ntoa(sa_in->sin_addr));
		} 
		else if (sa_source->ss_family==AF_INET6) {
			char ipbuf[INET6_ADDRSTRLEN];
			inet_ntop(AF_INET6, &sa_in6->sin6_addr, ipbuf, sizeof(ipbuf));
			printf("Source: INET Port %d host %s\n",
				ntohs(sa_in6->sin6_port), ipbuf);
		}
	}
	/* we could check that the source is the server we expect, but
	 * Denis Vlasenko recommends against it: multihomed hosts get it
	 * wrong too often. */
#if 0
	if (memcmp(ntpc->serv_addr, &(sa_in->sin_addr), 4)!=0) {
		return 1;  /* fault */
	}
#else
	(void) ntpc; /* not used */
#endif
	if (NTP_PORT != ntohs(sa_in->sin_port)) {
		if (sa_source->ss_family==AF_INET)
		{
			system("umng_syslog_cli addErrorCode T102");
		}
		else if(sa_source->ss_family==AF_INET6)
		{
			system("umng_syslog_cli addErrorCode NT102");
		}
		return 1;  /* fault */
	}
	return 0;
}

static double ntpdiff( struct ntptime *start, struct ntptime *stop)
{
	int a;
	unsigned int b;
	a = stop->coarse - start->coarse;
	if (stop->fine >= start->fine) {
		b = stop->fine - start->fine;
	} else {
		b = start->fine - stop->fine;
		b = ~b;
		a -= 1;
	}

	return a*1.e6 + b * (1.e6/4294967296.0);
}

/* Does more than print, so this name is bogus.
 * It also makes time adjustments, both sudden (-s)
 * and phase-locking (-l).
 * sets *error to the number of microseconds uncertainty in answer
 * returns 0 normally, 1 if the message fails sanity checks
 */
static int rfc1305print(u32 *data, struct ntptime *arrival, struct ntp_control *ntpc, int *error)
{
/* straight out of RFC-1305 Appendix A */
	int li, vn, mode, stratum, poll, prec;
	int delay, disp, refid;
	struct ntptime reftime, orgtime, rectime, xmttime;
	double el_time,st_time,skew1,skew2;
	int freq;
#ifdef ENABLE_DEBUG
	const char *drop_reason=NULL;
#endif

#define Data(i) ntohl(((u32 *)data)[i])
	li      = Data(0) >> 30 & 0x03;
	vn      = Data(0) >> 27 & 0x07;
	mode    = Data(0) >> 24 & 0x07;
	stratum = Data(0) >> 16 & 0xff;
	poll    = Data(0) >>  8 & 0xff;
	prec    = Data(0)       & 0xff;
	if (prec & 0x80) prec|=0xffffff00;
	delay   = Data(1);
	disp    = Data(2);
	refid   = Data(3);
	reftime.coarse = Data(4);
	reftime.fine   = Data(5);
	orgtime.coarse = Data(6);
	orgtime.fine   = Data(7);
	rectime.coarse = Data(8);
	rectime.fine   = Data(9);
	xmttime.coarse = Data(10);
	xmttime.fine   = Data(11);
#undef Data

	if (debug) {
	printf("LI=%d  VN=%d  Mode=%d  Stratum=%d  Poll=%d  Precision=%d\n",
		li, vn, mode, stratum, poll, prec);
	printf("Delay=%.1f  Dispersion=%.1f  Refid=%u.%u.%u.%u\n",
		sec2u(delay),sec2u(disp),
		refid>>24&0xff, refid>>16&0xff, refid>>8&0xff, refid&0xff);
	printf("Reference %u.%.6u\n", reftime.coarse, USEC(reftime.fine));
	printf("(sent)    %u.%.6u\n", ntpc->time_of_send[0], USEC(ntpc->time_of_send[1]));
	printf("Originate %u.%.6u\n", orgtime.coarse, USEC(orgtime.fine));
	printf("Receive   %u.%.6u\n", rectime.coarse, USEC(rectime.fine));
	printf("Transmit  %u.%.6u\n", xmttime.coarse, USEC(xmttime.fine));
	printf("Our recv  %u.%.6u\n", arrival->coarse, USEC(arrival->fine));
	}
	el_time=ntpdiff(&orgtime,arrival);   /* elapsed */
	st_time=ntpdiff(&rectime,&xmttime);  /* stall */
	skew1=ntpdiff(&orgtime,&rectime);
	skew2=ntpdiff(&xmttime,arrival);
	freq=get_current_freq();
	if (debug) {
	printf("Total elapsed: %9.2f\n"
	       "Server stall:  %9.2f\n"
	       "Slop:          %9.2f\n",
		el_time, st_time, el_time-st_time);
	printf("Skew:          %9.2f\n"
	       "Frequency:     %9d\n"
	       " day   second     elapsed    stall     skew  dispersion  freq\n",
		(skew1-skew2)/2, freq);
	}

	/* error checking, see RFC-4330 section 5 */
#ifdef ENABLE_DEBUG
#define FAIL(x) do { drop_reason=(x); goto fail;} while (0)
#else
#define FAIL(x) goto fail;
#endif
	if (ntpc->cross_check) {
		if (li == 3) FAIL("LI==3");  /* unsynchronized */
		if (vn < 3) FAIL("VN<3");   /* RFC-4330 documents SNTP v4, but we interoperate with NTP v3 */
		if (mode != 4) FAIL("MODE!=3");
		if (orgtime.coarse != ntpc->time_of_send[0] ||
		    orgtime.fine   != ntpc->time_of_send[1] ) FAIL("ORG!=sent");
		if (xmttime.coarse == 0 && xmttime.fine == 0) FAIL("XMT==0");
		if (delay > 65536 || delay < -65536) FAIL("abs(DELAY)>65536");
		if (disp  > 65536 || disp  < -65536) FAIL("abs(DISP)>65536");
		if (stratum == 0) FAIL("STRATUM==0");  /* kiss o' death */
#undef FAIL
	}

	/* XXX should I do this if debug flag is set? */
	if (ntpc->set_clock) { /* you'd better be root, or ntpclient will crash! */	
		struct timeval now,newnow;
		gettimeofday(&now, NULL);
		//cprintf("ntpclient before set_time\n");
		set_time(&xmttime);
		//cprintf("ntpclient after set_time\n");
		gettimeofday(&newnow, NULL);
		if ((newnow.tv_sec > now.tv_sec && newnow.tv_sec - now.tv_sec > 10) ||
			  (now.tv_sec > newnow.tv_sec && now.tv_sec - newnow.tv_sec > 10) )
		{
			//cprintf("ntpclient time updated changed more than 10\n");
			osSystem("sip_config DECT_SendDateTime &");
			gettimeofday(&last_update_HS_time, NULL);
			osSystem("echo 1 > /tmp/ntp2decths_10");
		}
		else
		if (last_update_HS_time.tv_sec==0) 
		{
			gettimeofday(&last_update_HS_time, NULL);
			osSystem("echo 1 > /tmp/ntp2decths_24");
		}
			
		if (ntpc->serv_addr.ss_family==AF_INET)
		{
			system("umng_syslog_cli addErrorCode T101");
		}
		else if(ntpc->serv_addr.ss_family==AF_INET6)
		{
			system("umng_syslog_cli addErrorCode NT101");
		}
	}

	/* Not the ideal order for printing, but we want to be sure
	 * to do all the time-sensitive thinking (and time setting)
	 * before we start the output, especially fflush() (which
	 * could be slow).  Of course, if debug is turned on, speed
	 * has gone down the drain anyway. */
	if (ntpc->live) {
		int new_freq;
		new_freq = contemplate_data(arrival->coarse, (skew1-skew2)/2,
			el_time+sec2u(disp), freq);
		if (!debug && new_freq != freq) set_freq(new_freq);
	}
	printf("%d %.5d.%.3d  %8.1f %8.1f  %8.1f %8.1f %9d\n",
		arrival->coarse/86400, arrival->coarse%86400,
		arrival->fine/4294967, el_time, st_time,
		(skew1-skew2)/2, sec2u(disp), freq);
	fflush(stdout);
	*error = el_time-st_time;

	return 0;
fail:
	if (ntpc->serv_addr.ss_family==AF_INET)
	{
		system("umng_syslog_cli addErrorCode T102");
	}
	else if(ntpc->serv_addr.ss_family==AF_INET6)
	{
		system("umng_syslog_cli addErrorCode T102");
	}
#ifdef ENABLE_DEBUG
	printf("%d %.5d.%.3d  rejected packet: %s\n",
		arrival->coarse/86400, arrival->coarse%86400,
		arrival->fine/4294967, drop_reason);
#else
	printf("%d %.5d.%.3d  rejected packet\n",
		arrival->coarse/86400, arrival->coarse%86400,
		arrival->fine/4294967);
#endif
	return 1;
}

static void stuff_net_addr(struct in_addr *p, char *hostname)
{
	struct hostent *ntpserver;
	ntpserver=gethostbyname(hostname);
	if (ntpserver == NULL) {
		herror(hostname);
		exit(1);
	}
	if (ntpserver->h_length != 4) {
		/* IPv4 only, until I get a chance to test IPv6 */
		fprintf(stderr,"[ntpclient] oops %d\n",ntpserver->h_length);
		exit(1);
	}
	memcpy(&(p->s_addr),ntpserver->h_addr_list[0],4);
}

#if 0
static void setup_receive(int usd, unsigned int interface, short port)
{
	struct sockaddr_in sa_rcvr;
	memset(&sa_rcvr,0,sizeof sa_rcvr);
	sa_rcvr.sin_family=AF_INET;
	sa_rcvr.sin_addr.s_addr=htonl(interface);
	sa_rcvr.sin_port=htons(port);
	if(bind(usd,(struct sockaddr *) &sa_rcvr,sizeof sa_rcvr) == -1) {
		perror("bind");
		fprintf(stderr,"could not bind to udp port %d\n",port);
		exit(1);
	}
	/* listen(usd,3); this isn't TCP; thanks Alexander! */
}
#endif

static void setup_transmit(int usd, char *host, short port, struct ntp_control *ntpc)
{
	struct sockaddr_in sa_dest;
	memset(&sa_dest,0,sizeof sa_dest);
	sa_dest.sin_family=AF_INET;
	stuff_net_addr(&(sa_dest.sin_addr),host);
	memcpy(&(ntpc->serv_addr),&(sa_dest.sin_addr),4); /* XXX asumes IPv4 */
	sa_dest.sin_port=htons(port);
	if (connect(usd,(struct sockaddr *)&sa_dest,sizeof sa_dest)==-1)
		{perror("connect");exit(1);}
}

static int socket_select(int *usd, int *ai_family, struct ntp_control *ntpc, struct addrinfo *res, int force_v4)
{
	//int error;
	int check=-1, ip4_ready=0, ip6_ready=0, i=0;
	char ip[INET6_ADDRSTRLEN];
	struct addrinfo *cur=NULL, *ip6_cur=NULL, *ip4_cur=NULL;
	char buf[16];

	// check if the previous socket needs to be closed.
	if(*usd != -1)
	{
		close(*usd);
		*usd=-1;
	}
	
	osSystem_GetOutput("mngcli tmp_get ARC_WAN_TMP_DefaultRoute_IPv4_WAN_Ready", buf, sizeof(buf));
	ip4_ready = atoi(buf);
	osSystem_GetOutput("mngcli tmp_get ARC_WAN_TMP_DefaultRoute_IPv6_WAN_Ready", buf, sizeof(buf));
	ip6_ready = atoi(buf);

	cprintf("[ntpclient] ip4_ready = %d, ip6_ready = %d, force_v4 = %d\n", ip4_ready, ip6_ready, force_v4);

	if((ip4_ready == 0) && (ip6_ready == 0))
	{
		fprintf(stderr, "[ntpclient] %s: IPv4 and IPv6 both not ready!!!\n", __func__);
		return check;
	}
		
	if(res)
	{
		for(cur = res; cur != NULL; cur = cur->ai_next)
		{
			if((cur->ai_family == AF_INET6) && (ip6_cur == NULL))
				ip6_cur=cur;

			if((cur->ai_family == AF_INET)  && (ip4_cur == NULL))
				ip4_cur=cur;
		}
	}
	
	if((ip4_cur != NULL) && (ip4_ready == 1))
	{
	
		struct sockaddr_storage local_addr;
		struct sockaddr_in *local_addr4;
	
		*usd=socket(ip4_cur->ai_family, ip4_cur->ai_socktype, ip4_cur->ai_protocol);
		if(*usd == -1)
		{
			perror ("socket");
			return check;
		}
		//backup the server address pointer
		memcpy(&(ntpc->serv_addr), ip4_cur->ai_addr, ip4_cur->ai_addrlen);
		memset(&local_addr, 0, sizeof(struct sockaddr_storage));
		local_addr4=(struct sockaddr_in *)&local_addr;
		local_addr4->sin_family=AF_INET;
		local_addr4->sin_port=htons(ntpc->udp_local_port);
		local_addr4->sin_addr.s_addr=htonl(INADDR_ANY);
		inet_ntop(AF_INET, &((struct sockaddr_in *) ip4_cur->ai_addr)->sin_addr, ip, INET_ADDRSTRLEN);		
		fprintf(stderr, "[ntpclient] %s: IPv4 address is %s.\n", __func__, ip);

	
		if(bind(*usd, (struct sockaddr *)&local_addr, sizeof(local_addr)) == -1)
		{
			perror("bind");
			fprintf(stderr, "[ntpclient] %s: could not bind to local udp port %d\n", __func__, ntpc->udp_local_port);
			close(*usd);
			*usd=-1;
			return check;
		}
	
		//setup_transmit(usd, hostname, NTP_PORT, &ntpc);
		//connect NTP server
		if(connect(*usd, ip4_cur->ai_addr , ip4_cur->ai_addrlen)  == -1)
		{
			perror("connect");
			fprintf(stderr, "[ntpclient] %s: could not connect to the NTP server.\n", __func__);
			close(*usd);
			*usd=-1;
			return check;
		}
		*ai_family=ip4_cur->ai_family;
		check=0;
	}

	//If WAN type only supports IPv6 stack, we need to set force_v4 as 0.
	if(*usd == -1)
		force_v4=0;

	if((ip6_cur != NULL)&& (ip6_ready == 1) && (force_v4 == 0))
	{
		struct sockaddr_storage local_addr;
		struct sockaddr_in6 *local_addr6;

		//Previous IPv4 socket needs to be closed first.
		if(*usd!=-1)
		{
			close(*usd);
			*usd=-1;
			check=-1;
		}
		
		*usd = socket(ip6_cur->ai_family, ip6_cur->ai_socktype, ip6_cur->ai_protocol);
		if(*usd == -1)
		{
			perror ("socket");
			return check;
		}
		//backup the server address pointer
		memcpy(&(ntpc->serv_addr), ip6_cur->ai_addr, ip6_cur->ai_addrlen);
		memset(&local_addr, 0, sizeof(struct sockaddr_storage));
		local_addr6 = (struct sockaddr_in6 *)&local_addr;
		local_addr6->sin6_family=AF_INET6;
		local_addr6->sin6_port=htons(ntpc->udp_local_port);
		local_addr6->sin6_addr=in6addr_any;
		inet_ntop(AF_INET6, &((struct sockaddr_in6 *) ip6_cur->ai_addr)->sin6_addr, ip, INET6_ADDRSTRLEN);
		fprintf(stderr, "[ntpclient] %s: IPv6 address is %s.\n", __func__, ip);

		if(bind(*usd, (struct sockaddr *)&local_addr, sizeof(local_addr)) == -1)
		{
			perror("bind");
			fprintf(stderr, "[ntpclient] %s: could not bind to local udp port %d\n", __func__, ntpc->udp_local_port);
			close(*usd);		
			*usd=-1;
			return check;
		}

		//setup_transmit(usd, hostname, NTP_PORT, &ntpc);
		//connect NTP server
		if(connect(*usd, ip6_cur->ai_addr , ip6_cur->ai_addrlen)  == -1)
		{
			perror("connect");
			fprintf(stderr, "[ntpclient] %s: could not connect to the NTP server.\n", __func__);
			close(*usd);		
			*usd=-1;
			return check;
		}
		*ai_family=ip6_cur->ai_family;
		check=0;
	}
	return check;
}

static int primary_loop(struct ntp_control *ntpc, struct addrinfo *res)
{
	int usd=-1; //socket id
	int ai_family;
	int force_ipv4=0;
	fd_set fds;
	struct sockaddr_storage sa_xmit;
	int i, pack_len=-1, probes_sent, error;
	unsigned int sa_xmit_len;
	struct timeval to;
	struct ntptime udp_arrival_ntp;
	u32 incoming_word[325];	
	int check=1;
#define incoming ((char *) incoming_word)
#define sizeof_incoming (sizeof incoming_word)

	if (debug) printf("Listening...\n");

	//probes_sent=0;
	sa_xmit_len=sizeof sa_xmit;
	if(socket_select(&usd,&ai_family,ntpc,res,force_ipv4)==0)
		send_packet(usd,ntpc->time_of_send);

	if (usd == -1){
		cprintf("Failed reopening NTP socket\n");
		return check;
	}
	
	probes_sent=1;
	to.tv_sec=0;
	to.tv_usec=0;
	for (;;) {
		FD_ZERO(&fds);
		FD_SET(usd,&fds);
		if (to.tv_sec == 0)
		{
			to.tv_sec=ntpc->cycle_time;
			to.tv_usec=0;
		}
		i=select(usd+1,&fds,NULL,NULL,&to);  /* Wait on read or error */

		if (i <= 0) {
			check=1;
			if (i < 0) {
				if (errno != EINTR) perror("select");
				break;
			}
			if (to.tv_sec == 0) {
				if (ai_family == AF_INET)
				{
					system("umng_syslog_cli addErrorCode T103");
				}
				else if(ai_family == AF_INET6)
				{
					system("umng_syslog_cli addErrorCode NT103");
				}

				if (probes_sent >= ntpc->probe_count && ntpc->probe_count != 0) 
				{		
						close(usd);
						usd=-1;
						break;
				}
				if(ai_family == AF_INET6)
					force_ipv4=1;
				else
					force_ipv4=0;
				close(usd);
				usd=-1;
				if(socket_select(&usd,&ai_family,ntpc,res,force_ipv4) == 0)
				{
					send_packet(usd,ntpc->time_of_send);
					++probes_sent;
				}
			}
			continue;
		}

		if (FD_ISSET(usd,&fds))
			pack_len=recvfrom(usd,incoming,sizeof_incoming,0,
			                  &sa_xmit,&sa_xmit_len);
		else
			continue;

		error=ntpc->goodness;
		if (pack_len <= 0) {
			perror("recvfrom");
		} else if (pack_len > 0 && (unsigned)pack_len < sizeof_incoming){
			get_packet_timestamp(usd, &udp_arrival_ntp);
			if (check_source(pack_len, &sa_xmit, sa_xmit_len, ntpc) != 0) continue;
			if (rfc1305print(incoming_word, &udp_arrival_ntp, ntpc, &error) != 0) continue;
			check=0;//check number 0:correct, 1-incorrect
			break; /* Get time successfully */
		} else {
			printf("Ooops.  pack_len=%d\n",pack_len);
			if (ai_family == AF_INET)
			{
				system("umng_syslog_cli addErrorCode T102");
			}
			else if(ai_family == AF_INET6)
			{
				system("umng_syslog_cli addErrorCode NT102");
			}
			fflush(stdout);
		}
		/* best rollover option: specify -g, -s, and -l.
		 * simpler rollover option: specify -s and -l, which
		 * triggers a magic -c 1 */
		if (( error < ntpc->goodness && ntpc->goodness != 0 ) ||
		    (probes_sent >= ntpc->probe_count && ntpc->probe_count != 0)) {
			if (!ntpc->live) break;
		}
	}
	
	close(usd);
	
	return check; // 0:correct, 1-incorrect
#undef incoming
#undef sizeof_incoming
}

#ifdef ENABLE_REPLAY
static void do_replay(void)
{
	char line[100];
	int n, day, freq, absolute;
	float sec, el_time, st_time, disp;
	double skew, errorbar;
	int simulated_freq = 0;
	unsigned int last_fake_time = 0;
	double fake_delta_time = 0.0;

	while (fgets(line,sizeof line,stdin)) {
		n=sscanf(line,"%d %f %f %f %lf %f %d",
			&day, &sec, &el_time, &st_time, &skew, &disp, &freq);
		if (n==7) {
			fputs(line,stdout);
			absolute=day*86400+(int)sec;
			errorbar=el_time+disp;
			if (debug) printf("contemplate %u %.1f %.1f %d\n",
				absolute,skew,errorbar,freq);
			if (last_fake_time==0) simulated_freq=freq;
			fake_delta_time += (absolute-last_fake_time)*((double)(freq-simulated_freq))/65536;
			if (debug) printf("fake %f %d \n", fake_delta_time, simulated_freq);
			skew += fake_delta_time;
			freq = simulated_freq;
			last_fake_time=absolute;
			simulated_freq = contemplate_data(absolute, skew, errorbar, freq);
		} else {
			fprintf(stderr,"[ntpclient] Replay input error\n");
			exit(2);
		}
	}
}
#endif

static void usage(char *argv0)
{
	fprintf(stderr,
	"Usage: %s [-c count]"
#ifdef ENABLE_DEBUG
	" [-d]"
#endif
	" [-f frequency] [-g goodness] [-v ipversion] -h1 hostname1 [-h2 hostname2]\n"
	"\t[-i interval] [-t interval for correcting time] [-l] [-p port] [-q min_delay]"
#ifdef ENABLE_REPLAY
	" [-r]"
#endif
	" [-s] [-x]\n",
	argv0);
}

static int ntp_status(int ntp_time_available, int status, int fail_num)
{
	FILE *fp = NULL;
	char status_file[64] = "/tmp/ntp_status";
	struct stat statbuff;
#if 0
	if((stat(status_file, &statbuff) == 0) && (status == 0) && (fail_num == 0))
	{
		if(fp = fopen(status_file, "r"))
		{
			fscanf(fp, "ntp_time_available=%d\nget_time_state=%d\nfail_num=%d\n", &ntp_time_available, &status, &fail_num);
			fclose(fp);
		}
		cprintf("read ntp_time_available=%d, status=%d, fail_num is %d.\n", ntp_time_available, status, fail_num);
	}
#endif
	if(fp = fopen(status_file, "w"))
	{
		fprintf(fp, "ntp_time_available=%d\nget_time_state=%d\nfail_num=%d\n", ntp_time_available, status, fail_num);
		fclose(fp);
		cprintf("write ntp_time_available=%d, status=%d, fail_num is %d.\n", ntp_time_available, status, fail_num);
	}
	
	return 0;
}

static int ntpc_main(struct ntp_control *ntpc, char *hostname)
{
	int error;
	int check=-1,i=0;
	struct addrinfo *res=NULL;
	struct addrinfo hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_flags = AI_ALL;
	if(ntpc->ipversion == 4)
		hints.ai_family = AF_INET;		//force to IPV4
	else if(ntpc->ipversion == 6)
		hints.ai_family = AF_INET6;		//force to IPV6
	else
		hints.ai_family = AF_UNSPEC;	//protocol independence, Allow IPv4 or IPv6 
	hints.ai_socktype = SOCK_DGRAM;	//UDP
	hints.ai_protocol = IPPROTO_UDP;

	while(error = getaddrinfo(hostname, "123", &hints, &res))
	{
		fprintf(stderr, "[ntpclient] getaddrinfo: %s for host %s\n", gai_strerror(error), hostname);
		if(++i < ntpc->cycle_time)
		{
			usleep(200000);
		}
		else
		{
			fprintf(stderr, "[ntpclient] getaddrinfo: timeout for host %s\n", hostname);
			break;
		}
	}

	if(error == 0)
	{
		fprintf(stderr, "[ntpclient] getaddrinfo: Ok for host %s\n", hostname);
	}

	check=primary_loop(ntpc,res);
	freeaddrinfo(res);
	return check;
}

void sigroutine(int sig) { 
	cprintf("Catch a signal %d.\n", sig);
}

int main(int argc, char *argv[]) {
	int c;
	int fail_num=0, check=0/* 0: success to get time, 1: fail to get time */;
	int status, ntp_time_available=0; /* 1: HG has NTP time. HG has ntp time even thought resync unsuccessful. 0: HG never has NTP time*/
	/* These parameters are settable from the command line
	   the initializations here provide default behavior */
	char *hostname1=NULL;          /* must be set */
	char *hostname2=NULL;          /* must be set */
	char *runfile=NULL;
	int initial_freq;             /* initial freq value to use */
	struct ntp_control ntpc;
	int enableCWMP=1, sleep_time=0;
	struct stat statbuff;
	FILE *fp;
	void *local_tid;

	printf("\n#[%s] %d, \n", __FUNCTION__, __LINE__); 

	memset(&last_update_HS_time, 0, sizeof(last_update_HS_time));
	memset(&ntpc, 0, sizeof(ntpc));
	ntpc.live=0;
	ntpc.set_clock=0;
	ntpc.probe_count=0;           /* default of 0 means loop forever */
	ntpc.cycle_time=600;          /* seconds */
	ntpc.goodness=0;
	ntpc.cross_check=0;
	ntpc.ipversion=0;
	ntpc.udp_local_port=0;

	if(fp = fopen("/tmp/ntp_status", "r"))
	{
		fscanf(fp, "ntp_time_available=%d\nget_time_state=%d\nfail_num=%d\n", &ntp_time_available, &status, &fail_num);
		fclose(fp);
		cprintf("***ntp_time_available=%d\n", ntp_time_available);
	}
	else
		cprintf("***cannot open ntp_status\n");
	

	for (;;) {
		c = getopt( argc, argv, "c:" DEBUG_OPTION "f:g:v:h:o:P:t:i:lp:q:" REPLAY_OPTION "sxD");
		if (c == EOF) break;
		switch (c) {
			case 'c':
				ntpc.probe_count = atoi(optarg);
				break;
#ifdef ENABLE_DEBUG
			case 'd':
				++debug;
				break;
#endif
			case 'f':
				initial_freq = atoi(optarg);
				if (debug) printf("initial frequency %d\n",
						initial_freq);
				set_freq(initial_freq);
				break;
			case 'g':
				ntpc.goodness = atoi(optarg);
				break;
			case 'v':
				ntpc.ipversion = atoi(optarg);
				break;
			case 'h':
				hostname1 = optarg;
				break;
			case 'o':
				hostname2 = optarg;
				break;
			case 'i':
				ntpc.cycle_time = atoi(optarg);
				break;
			case 't':
				ntpc.interval_time = atoi(optarg);
				break;
			case 'l':
				(ntpc.live)++;
				break;
			case 'p':
				ntpc.udp_local_port = atoi(optarg);
				break;
			case 'q':
				min_delay = atof(optarg);
				break;
#ifdef ENABLE_REPLAY
			case 'r':
				do_replay();
				exit(0);
				break;
#endif
			case 's':
				(ntpc.set_clock)++;
				break;

			case 'x':
				(ntpc.cross_check)=0;
				break;

			case 'D':
				daemon();
				break;

			case 'P':
				runfile = optarg;
				break;

			default:
				usage(argv[0]);
				exit(1);
		}
	}
	if (hostname1 == NULL && hostname2 == NULL ) {
		usage(argv[0]);
		exit(1);
	}

	if (ntpc.set_clock && !ntpc.live && !ntpc.goodness && !ntpc.probe_count) {
		ntpc.probe_count = 1;
	}

	/* respect only applicable MUST of RFC-4330 */
	if (ntpc.probe_count != 1 && ntpc.cycle_time < MIN_INTERVAL) {
		ntpc.cycle_time=MIN_INTERVAL;
	}

	if (debug) {
		printf("Configuration:\n"
		"  -c probe_count %d\n"
		"  -d (debug)     %d\n"
		"  -g goodness    %d\n"
		"  -v ipversion   %d\n"
		"  -h hostname1    %s\n"
		"  -o hostname2    %s\n"
		"  -i interval    %d\n"
		"  -l live        %d\n"
		"  -p local_port  %d\n"
		"  -q min_delay   %f\n"
		"  -s set_clock   %d\n"
		"  -x cross_check %d\n",
		ntpc.probe_count, debug, ntpc.goodness, ntpc.ipversion,
		hostname1, hostname2, ntpc.cycle_time, ntpc.live, ntpc.udp_local_port, min_delay,
		ntpc.set_clock, ntpc.cross_check );
	}

	if(runfile)
	{
		FILE *pidfile;
		if((pidfile = fopen(runfile, "w")))
		{
			fprintf(pidfile, "%d\n", (int) getpid());
			fclose(pidfile);
		}
	}

	if(fail_num == 0)
	{
		cprintf("Waiting signal SIGUSR1.\n");
		signal(SIGUSR1, sigroutine);
		pause(); //wait signal from ppp
		signal(SIGUSR1, SIG_IGN);
		cprintf("Got signal SIGUSR1.\n");
	}
	else
	{
		cprintf("Set signal SIGUSR1 handler.\n");
		//signal(SIGUSR1, sigroutine);
		signal(SIGUSR1, SIG_IGN);
	}

	while(1)
	{
		struct timeval current_time;
		sleep_time = 0;

		if((check = ntpc_main(&ntpc, hostname1)) != 0)
		{
			check = ntpc_main(&ntpc, hostname2);
		}

		if(check != 0)
		{
			int wan_type;
			char buf[64];
			char username[64];

			fprintf(stderr, "[ntpclient] %s: Fail to get time.\n", __func__);
			fail_num = fail_num + 1;
			ntp_status(ntp_time_available, 0, fail_num);

			osSystem_GetOutput("mngcli get ARC_WAN_Type", buf, sizeof(buf));
			wan_type = atoi(buf);
			snprintf(buf, sizeof(buf), "mngcli get ARC_WAN_%d00_PPP_Username", wan_type);
			osSystem_GetOutput(buf, username, sizeof(username));
			
			osSystem_GetOutput("mngcli get ARC_TR69_EnableCWMP", buf, sizeof(buf));
			enableCWMP = atoi(buf);
			cprintf("%s():%d: username= %s, default_username_suffix = @setup.t-online.de, enableCWMP = %d\n", __FUNCTION__, __LINE__, username, enableCWMP);

			/* Not wait time if the HG is configuf with Manufacturing default credentials and 'EasySupport' is enabled in Web-UI. */
			if((strstr(username, "@setup.t-online.de") != NULL) && (enableCWMP == 1))
			{
				sleep_time = 0;
			}
			else
			{
				sleep_time = 285; 
			}
		}
		else
		{
			fprintf(stderr, "[ntpclient] %s: Get time successfully, sleep_time is %d.\n", __func__, ntpc.interval_time);
			ntp_time_available = 1;
			ntp_status(ntp_time_available, 1, fail_num);
			system("/usr/sbin/tr69_trigger findntp");
			if(stat(PRIVACY_PID, &statbuff))
			{
				cprintf("Execute arc_privacy_reconnect ..\n", __LINE__);
				osSystem("/usr/sbin/arc_privacy_reconnect &");	//it is symbolic link of /usr/sbin/arc-ipv6 
			}
			sleep_time = ntpc.interval_time;
		}

		if(sleep_time > 0)
		{
			fprintf(stderr, "[ntpclient] %s: sleep %d secs.\n", __func__, sleep_time);
			sleep(sleep_time);
		}

		gettimeofday(&current_time, NULL);
		if (last_update_HS_time.tv_sec!=0) 
		{
			if (current_time.tv_sec - last_update_HS_time.tv_sec >= 86400) //more than 24hr.
			{
				osSystem("sip_config DECT_SendDateTime &");
				gettimeofday(&last_update_HS_time, NULL);
				osSystem("echo 1 > /tmp/ntp2decths_24");
			}
		}

		cprintf("check=%d, sleep_time=%d, interval_time=%d....\n", check, sleep_time, ntpc.interval_time);
		if(check != 0)
		{
			cprintf(" ********Fail to get time, Restart NTP client.\n");
			osSystem("( /usr/sbin/agent/ntpclient-agent restart ) &");
			exit(1);
		}
	}
	/* End */

	return check;
}
