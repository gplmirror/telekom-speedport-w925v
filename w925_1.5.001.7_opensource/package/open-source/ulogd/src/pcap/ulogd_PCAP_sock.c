/*
 * ulogd output target for writing pcap-style files (like tcpdump)
 *
 * Copyright (C) 2012, Arcadyan Corporation
 * All Rights Reserved.
 * 
 * THIS SOFTWARE IS OFFERED "AS IS", AND ARCADYAN GRANTS NO WARRANTIES OF ANY
 * KIND, EXPRESS OR IMPLIED, BY STATUTE, COMMUNICATION OR OTHERWISE. ARCADYAN
 * SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A SPECIFIC PURPOSE OR NONINFRINGEMENT CONCERNING THIS SOFTWARE.
 *
 * $Id: ulogd_PCAP_noFON_sock.c 0 2012-08-17 11:49:00 terry_lin $
 */
/********************** Include Files ****************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <pcap.h>
#include <ulogd/ulogd.h>
#include <ulogd/conffile.h>

#define		ARC_PCAP_FON_FILTER
#define		ARC_PCAP_FON_FILTER_DEBUG
#define		ARC_PCAP_FON_FILTER_USE_CACHE
#define		ARC_PCAP_FON_FILTER_CACHE_SECONDS	2	/* Need 768K bytes memory */

/*
 * This is a timeval as stored in disk in a dumpfile.
 * It has to use the same types everywhere, independent of the actual
 * `struct timeval'
 */

struct pcap_timeval {
    int32_t tv_sec;		/* seconds */
    int32_t tv_usec;		/* microseconds */
};

/*
 * How a `pcap_pkthdr' is actually stored in the dumpfile.
 *
 * Do not change the format of this structure, in any way (this includes
 * changes that only affect the length of fields in this structure),
 * and do not make the time stamp anything other than seconds and
 * microseconds (e.g., seconds and nanoseconds).  Instead:
 *
 *	introduce a new structure for the new format;
 *
 *	send mail to "tcpdump-workers@tcpdump.org", requesting a new
 *	magic number for your new capture file format, and, when
 *	you get the new magic number, put it in "savefile.c";
 *
 *	use that magic number for save files with the changed record
 *	header;
 *
 *	make the code in "savefile.c" capable of reading files with
 *	the old record header as well as files with the new record header
 *	(using the magic number to determine the header format).
 *
 * Then supply the changes to "patches@tcpdump.org", so that future
 * versions of libpcap and programs that use it (such as tcpdump) will
 * be able to read your new capture file format.
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>

struct pcap_sf_pkthdr {
    struct pcap_timeval ts;	/* time stamp */
    uint32_t caplen;		/* length of portion present */
    uint32_t len;		/* length this packet (off wire) */
};

#ifndef ULOGD_PCAP_DEFAULT
#define ULOGD_PCAP_DEFAULT	"/var/log/ulogd.sock"
#endif

#ifndef ULOGD_PCAP_SYNC_DEFAULT
#define ULOGD_PCAP_SYNC_DEFAULT	0
#endif

#define NIPQUAD(addr) \
	((unsigned char *)&addr)[0], \
	((unsigned char *)&addr)[1], \
        ((unsigned char *)&addr)[2], \
        ((unsigned char *)&addr)[3]

static config_entry_t pcapsock_ce = { 
	.key = "sock", 
	.type = CONFIG_TYPE_STRING, 
	.options = CONFIG_OPT_NONE,
	.u = { .string = ULOGD_PCAP_DEFAULT }
};

static int					sock = -1;
static struct sockaddr_un	sock_addr;

struct intr_id {
	char* name;
	unsigned int id;		
};

#ifdef ARC_PCAP_FON_FILTER
#define INTR_IDS 	6
#else
#define INTR_IDS 	4
#endif
static struct intr_id intr_ids[INTR_IDS] = {
	{ "raw.pkt", 0 },
	{ "raw.pktlen", 0 },
	{ "oob.time.sec", 0 },
	{ "oob.time.usec", 0 },
/* Terry 20120817, Pkt snooping to filter FON related packets */
#ifdef ARC_PCAP_FON_FILTER
	{ "oob.in", 0 },
	{ "oob.out", 0 },
#endif
};

#define GET_VALUE(x)	ulogd_keyh[intr_ids[x].id].interp->result[ulogd_keyh[intr_ids[x].id].offset].value
#define GET_FLAGS(x)	ulogd_keyh[intr_ids[x].id].interp->result[ulogd_keyh[intr_ids[x].id].offset].flags

/*
   Terry 20120822, WTF, ntohl and noths should not be re-defined here!
   Actually pcap file reader can detect byte order automatically.  We
   don't need to swap the content of fields in a pcap file generation.
*/
#if 1
#define ntohl_local		ntohl
#define ntohs_local		ntohs
#else
#define ntohl_local(x)	((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >>  8) | \
					(((x) & 0x0000ff00) <<  8) | (((x) & 0x000000ff) << 24))
#define ntohs_local(x)	((((x) >> 8) & 0xff) | (((x) & 0xff) << 8))
#endif

/* Terry 20120817, Pkt snooping to filter FON related packets */
#ifdef ARC_PCAP_FON_FILTER
#include <arpa/inet.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#ifdef ARC_PCAP_FON_FILTER_DEBUG
#include <stdarg.h>
#endif

#define MAX_BUF_LEN					256
#define MAX_NAT_ENTRIES_NUM			256
#define MAX_FON_ENTRIES_NUM			256

typedef struct nat_entry_s {
	uint8_t				protocol;
	in_addr_t			out_saddr;
	in_addr_t			out_daddr;
	union {
		uint16_t		out_sport;
		uint16_t		out_id;
	};
	uint16_t			out_dport;
	in_addr_t			in_saddr;
	in_addr_t			in_daddr;
	union {
		uint16_t		in_sport;
		uint16_t		in_id;
	};
	uint16_t		in_dport;
} nat_entry_t;

static in_addr_t		g_fon_domain = 0;
static in_addr_t		g_fon_mask = 0;
static nat_entry_t		g_nat_table[MAX_NAT_ENTRIES_NUM];
static int				g_nat_table_len;
static int				g_fon_domain_initled = 0;

#ifdef ARC_PCAP_FON_FILTER_USE_CACHE
/* Terry 20130113, these tables are used to cache non-fon packets port info. */
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
static time_t			g_nat_tcp[64 * 1024] = {0};
static time_t			g_nat_udp[64 * 1024] = {0};
static time_t			g_nat_icmp[64 * 1024] = {0};
#else
static uint8_t			g_nat_tcp[8 * 1024] = {0};
static uint8_t			g_nat_udp[8 * 1024] = {0};
static uint8_t			g_nat_icmp[8 * 1024] = {0};
#endif
#endif

#ifdef ARC_PCAP_FON_FILTER_DEBUG
static int				g_fon_filter_debug = 0;
#endif
static char 			out_buf[MAX_BUF_LEN];

int iprintf(const char* fmt_ptr, ...)
{
#ifdef ARC_PCAP_FON_FILTER_DEBUG
	va_list		vlist;
	char		buf[512];
	int			ret;
	FILE*		fd;

	if (g_fon_filter_debug == 0)
		return 0;
	
	if ((fd = fopen( "/dev/console", "w" )) == NULL)
		return 0;

	va_start(vlist, fmt_ptr);
	ret = vsnprintf(buf, sizeof(buf) - 1, fmt_ptr, vlist);
	va_end(vlist);
	buf[sizeof(buf) - 1] = '\0';
	fprintf(fd, "%s", buf);
	fflush(fd);
	fclose(fd);

	return ret;
	
#else
	
	return 0;
	
#endif
}

#ifdef ARC_PCAP_FON_FILTER_DEBUG
static int chk_file_exists(char * fileName)
{
	struct stat buf;
	/* File found */
	if (stat(fileName, &buf) == 0) {
		return 1;
	}
	return 0;
}
#endif

/*
 ip4addr@fon = 172.17.2.1
 ip4mask@fon = 255.255.255.240
*/
static int system_output(const char *cmd_buf_ptr, char *out_buf_ptr, int out_buf_len)
{
	FILE *pf;
	
	if ((pf = popen(cmd_buf_ptr, "r")) == NULL) {
		printf("popen() error!\n");
		return -1;
	}
	fgets(out_buf_ptr, out_buf_len, pf);
	pclose(pf);
	
	return 0;
}

static int parse_nat_table(uint16_t proto_type)
{
	FILE *		fd;
	char		line[256];
	char		line2[256];
	
	/* Parse NAT table */
	if (proto_type == IPPROTO_ICMP) {
		if ((fd = popen("cat /proc/net/ip_conntrack|grep ^icmp", "r")) == 0)
			return -1;
	} else {
		if ((fd = popen("cat /proc/net/ip_conntrack|grep -v ^unknown|grep -v ^icmp", "r")) == 0)
			return -1;
	}
	
	g_nat_table_len = 0;
	while (fgets(line, sizeof(line), fd) != NULL && g_nat_table_len < MAX_NAT_ENTRIES_NUM) {
		int 		i;
		char *		line_ptr;
		int			protocol;
		in_addr_t	out_saddr, out_daddr, in_saddr, in_daddr;
		uint16_t	out_sport, out_dport, in_sport, in_dport;
		uint16_t	out_id, in_id;
		
#ifdef ARC_PCAP_FON_FILTER_DEBUG
		if (g_fon_filter_debug != 0)
			strcpy(line2, line);
#endif
		
		/* Protocol */
		line_ptr = strtok(&line[9], " ");	/* Skip protocol string */
		if (proto_type != IPPROTO_ICMP) {
			sscanf(line_ptr, "%d", &protocol);
		}

		/* Out saddr */
		if ((line_ptr = strstr(line_ptr + strlen(line_ptr) + 1, "src")) == NULL) {
			iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
			continue;
		}
		line_ptr = strtok(line_ptr + 4, " ");	/* Skip 'src=' */
		if ((out_saddr = inet_addr(line_ptr)) == -1) {
			iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
			continue;
		}
		
		/* Check FON client domain */
		if ((out_saddr & g_fon_mask) != g_fon_domain) {
			/*
			iprintf("[%s] (%d) out_saddr=%s\n", __FUNCTION__, __LINE__, inet_ntoa(*(struct in_addr *)&out_saddr));
			*/
			continue;
		}
		
		/* Out daddr */
		line_ptr = strtok(NULL, "=");	/* 'dst' */
		if (strcmp(line_ptr, "dst")) {	/* Unexpected format */
			iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
			continue;
		}
		line_ptr = strtok(NULL, " ");
		out_daddr = inet_addr(line_ptr);

		if (proto_type == IPPROTO_ICMP) {
			/* Out id */
			if ((line_ptr = strstr(line_ptr + strlen(line_ptr) + 1, "id")) == NULL) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			line_ptr = strtok(line_ptr + 3, " ");	/* Skip 'id=' */
			if ((out_id = atoi(line_ptr)) == 0) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
		} else {
			/* Out sport */
			line_ptr = strtok(NULL, "=");
			if (strcmp(line_ptr, "sport")) {	/* Unexpected format */
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			line_ptr = strtok(NULL, " ");
			if ((out_sport = atoi(line_ptr)) == 0) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}

			/* Out dport */
			line_ptr = strtok(NULL, "=");
			if (strcmp(line_ptr, "dport")) {	/* Unexpected format */
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			line_ptr = strtok(NULL, " ");
			if ((out_dport = atoi(line_ptr)) == 0) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
		}
		
		/* In packets */
		if ((line_ptr = strstr(line_ptr + strlen(line_ptr) + 1, "src")) == NULL) {
			iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
			continue;
		}
		
		/* In saddr */
		line_ptr = strtok(line_ptr + 4, " ");	/* Skip 'src=' */
		in_saddr = inet_addr(line_ptr);
		
		/* In daddr */
		line_ptr = strtok(NULL, "=");	/* 'dst' */
		if (strcmp(line_ptr, "dst")) {	/* Unexpected format */
			iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
			continue;
		}
		line_ptr = strtok(NULL, " ");
		in_daddr = inet_addr(line_ptr);
		
		if (proto_type == IPPROTO_ICMP) {
			if ((line_ptr = strstr(line_ptr + strlen(line_ptr) + 1, "id")) == NULL) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			line_ptr = strtok(line_ptr + 3, " ");	/* Skip 'id=' */
			if ((in_id = atoi(line_ptr)) == 0) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
		} else {
			/* In sport */
			line_ptr = strtok(NULL, "=");
			if (strcmp(line_ptr, "sport")) {	/* Unexpected format */
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			line_ptr = strtok(NULL, " ");
			if ((in_sport = atoi(line_ptr)) == 0) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			
			/* In dport */
			line_ptr = strtok(NULL, "=");
			if (strcmp(line_ptr, "dport")) {	/* Unexpected format */
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
			line_ptr = strtok(NULL, " ");
			if ((in_dport = atoi(line_ptr)) == 0) {
				iprintf("[%s] (%d) l=%s\n", __FUNCTION__, __LINE__, line2);
				continue;
			}
		}
		
		/* Parse done, fill the table */
		if (proto_type != IPPROTO_ICMP)
			g_nat_table[g_nat_table_len].protocol = protocol;
		g_nat_table[g_nat_table_len].out_saddr = out_saddr;
		g_nat_table[g_nat_table_len].out_daddr = out_daddr;
		if (proto_type == IPPROTO_ICMP) {
			g_nat_table[g_nat_table_len].out_id = out_id;
		} else {
			g_nat_table[g_nat_table_len].out_sport = out_sport;
			g_nat_table[g_nat_table_len].out_dport = out_dport;
		}
		g_nat_table[g_nat_table_len].in_saddr = in_saddr;
		g_nat_table[g_nat_table_len].in_daddr = in_daddr;
		if (proto_type == IPPROTO_ICMP) {
			g_nat_table[g_nat_table_len].in_id = in_id;
		} else {
			g_nat_table[g_nat_table_len].in_sport = in_sport;
			g_nat_table[g_nat_table_len].in_dport = in_dport;
		}
		
		g_nat_table_len++;
	}
	pclose(fd);
	
	iprintf("[%s] g_nat_table_len=%d\n", __FUNCTION__, g_nat_table_len);
	
	return 0;
}

static int is_fon_pkt(void *pkt, void *dir_ptr)
{
	register uint8_t 	*buf_ptr;
	struct iphdr 		*ip_ptr;
	int					off = 0;
	int					i;
	int					dir;
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
	time_t				cur_time;
#endif
	
	buf_ptr = (char *)pkt;
	
	/* Check L2TP packets */
	/* Terry 20120824, in this interface, we could have with/without VLAN tag cases */
	if (ntohs(*((uint16_t *)&buf_ptr[0x0c])) == 0x8100) {
		/* 802.1Q VLAN tag enabled packets */
		if ((ntohs(*((uint16_t *)&buf_ptr[0x0e])) & 0x0f) == 8) {
			/* Skip VLAN 8 packets checking */
			return 0;
		}
		off = 4;
	}
	
	if (ntohs(*((uint16_t *)&buf_ptr[0x0c + off])) != 0x8864) {
		/* Not PPPoE packets */
		return 0;
	}

	/* Skip non-IP packets checking */
	if (ntohs(*(uint16_t *)(pkt + sizeof(struct ethhdr) + off + 6)) != 0x0021)
		return 0;

#ifdef ARC_PCAP_FON_FILTER_DEBUG /* Terry 20130111, could impact performance */
	if (chk_file_exists("/tmp/wan_util_fon_filter-off"))
		return 0;
	
	if (chk_file_exists("/tmp/wan_util_fon_filter-debug"))
		g_fon_filter_debug = 1;
#endif
	
	/* Check tunnel packets */
	if ((buf_ptr[0x1f + off] == 0x11) &&								/* UDP (IP over UDP) */
			(ntohs(*((uint16_t *)&buf_ptr[0x2a + off])) == 0x06a5) &&	/* IP over UDP */
			(ntohs(*((uint16_t *)&buf_ptr[0x2c + off])) == 0x06a5))		/* IP over UDP */
	{
		return 1;
	}
	
	/* Check Normal packets */
	ip_ptr = (struct iphdr *)(pkt + sizeof(struct ethhdr) + off + (6 + 2));	/* PPPoE + PPP */
	iprintf("[%s] ip_ptr=0x%08x\n", __FUNCTION__, ip_ptr);
	iprintf("[%s] ip_ptr->protocol=%d\n", __FUNCTION__, ip_ptr->protocol);
	iprintf("[%s] ip_ptr->saddr=%s\n", __FUNCTION__, inet_ntoa(*(struct in_addr *)&ip_ptr->saddr));
	iprintf("[%s] ip_ptr->daddr=%s\n", __FUNCTION__, inet_ntoa(*(struct in_addr *)&ip_ptr->daddr));
	iprintf("[%s] IP header len=%d\n", __FUNCTION__, ip_ptr->ihl * 4);
	
	/* Get FON domain */
	if (g_fon_domain_initled == 0) {
		g_fon_domain_initled = 1;
		osSystem_GetOutput("mngcli get ARC_FON_IP4Mask", out_buf, sizeof(out_buf));
		if ((g_fon_mask = inet_addr(out_buf)) == -1) {
			return 0;
		}

		osSystem_GetOutput("mngcli get ARC_FON_IP4Addr", out_buf, sizeof(out_buf));
		if ((g_fon_domain = inet_addr(out_buf)) == -1)
			return 0;
		
		g_fon_domain &= g_fon_mask;
		
		iprintf("[%s] g_fon_domain=%s\n", __FUNCTION__, inet_ntoa(*(struct in_addr *)&g_fon_domain));
		iprintf("[%s] g_fon_mask=%s\n", __FUNCTION__, inet_ntoa(*(struct in_addr *)&g_fon_mask));
	}
	
	dir = ((*((char *)dir_ptr) != '\0') ? 0 : 1);
	/* iprintf("[%s] dir=%s\n", __FUNCTION__, ((dir == 0) ? "IN" : "OUT")); */

	/* Terry 20120827, workaround.  Filter src=172.17.2.x packets on WAN interface */
	if (dir == 1) {
		if ((ip_ptr->saddr & g_fon_mask) == g_fon_domain) {
			return 1;
		}
	} else {
		if ((ip_ptr->daddr & g_fon_mask) == g_fon_domain) {
			return 1;
		}
	}
	
	if (ip_ptr->protocol == IPPROTO_ICMP) {
		/* Check ICMP */
		struct icmphdr	*icmp_ptr = (struct icmphdr	*)((uint8_t *)ip_ptr + (ip_ptr->ihl * 4));
		uint16_t		id = ntohs(icmp_ptr->un.echo.id);
		
		iprintf("[%s] ICMP id=0x%04x\n", __FUNCTION__, id);
		
#ifdef ARC_PCAP_FON_FILTER_USE_CACHE
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
		cur_time = time(0);
		iprintf("[%s] cur_time=%d\n", __FUNCTION__, cur_time);
		if (g_nat_icmp[id] != 0 && ((cur_time - g_nat_icmp[id]) < ARC_PCAP_FON_FILTER_CACHE_SECONDS)) {
			g_nat_icmp[id] = cur_time;
			iprintf("[%s] g_nat_icmp cached. id=0x%04x\n", __FUNCTION__, id);
			return 0;
		}
#else
		if ((g_nat_icmp[id >> 3] & (1 << (id & 0x7))) != 0) {
			iprintf("[%s] g_nat_icmp cached. id=0x%04x\n", __FUNCTION__, id);
			return 0;
		}
#endif
#endif
		
		parse_nat_table(ip_ptr->protocol);
		
		if (dir == 0) {
			/* Incoming packets */
			for (i = 0; i < g_nat_table_len; i++) {
				if (ip_ptr->saddr == g_nat_table[i].in_saddr &&
						id == g_nat_table[i].in_id)
				{
					return 1;
				}
			}
		} else {
			/* Outgoing packets */
			for (i = 0; i < g_nat_table_len; i++) {
				if (ip_ptr->daddr == g_nat_table[i].out_daddr &&
						id == g_nat_table[i].in_id)					/* Should be transformed */
				{
					return 1;
				}
			}
		}
#ifdef ARC_PCAP_FON_FILTER_USE_CACHE
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
		g_nat_icmp[id] = time(0);
#else
		g_nat_icmp[id >> 3] |= (1 << (id & 0x7));
#endif
		iprintf("[%s] g_nat_icmp add id=0x%04x, cur_time=%d\n", __FUNCTION__, id, cur_time);
#endif
	} else {
		/* Check TCP/UDP */
		struct udphdr	*udp_ptr = (struct udphdr *)((uint8_t *)ip_ptr + (ip_ptr->ihl * 4));
		uint16_t		sport = ntohs(udp_ptr->source);
		uint16_t		dport = ntohs(udp_ptr->dest);
#if ARC_PCAP_FON_FILTER_CACHE_SECONDS
		time_t			*g_nat_ptr = ((ip_ptr->protocol == IPPROTO_TCP) ? &g_nat_tcp[0] : &g_nat_udp[0]);
#else
		uint8_t			*g_nat_ptr = ((ip_ptr->protocol == IPPROTO_TCP) ? &g_nat_tcp[0] : &g_nat_udp[0]);
#endif
		
		iprintf("[%s] TCP/UDP sport=%d\n", __FUNCTION__, sport);
		iprintf("[%s] TCP/UDP dport=%d\n", __FUNCTION__, dport);

#ifdef ARC_PCAP_FON_FILTER_USE_CACHE
		if (dir == 0) {
			/* Incoming packets */
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
			cur_time = time(0);
			iprintf("[%s] cur_time=%d, g_nat_ptr[%d]=%d (dport)\n", __FUNCTION__, cur_time, dport, g_nat_ptr[dport]);
			if (g_nat_ptr[dport] != 0 && ((cur_time - g_nat_ptr[dport]) < ARC_PCAP_FON_FILTER_CACHE_SECONDS)) {
				g_nat_ptr[dport] = cur_time;
				iprintf("[%s] g_nat_(tcp/udp) cached. dport=%d\n", __FUNCTION__, dport);
				return 0;
			}
#else
			if ((g_nat_ptr[dport >> 3] & (1 << (dport & 0x7))) != 0) {
				iprintf("[%s] g_nat_(tcp/udp) cached. dport=%d\n", __FUNCTION__, dport);
				return 0;
			}
#endif
		} else {
			/* Outgoing packets */
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
			cur_time = time(0);
			iprintf("[%s] cur_time=%d, g_nat_ptr[%d]=%d (sport)\n", __FUNCTION__, cur_time, sport, g_nat_ptr[sport]);
			if (g_nat_ptr[sport] != 0 && ((cur_time - g_nat_ptr[sport]) < ARC_PCAP_FON_FILTER_CACHE_SECONDS)) {
				g_nat_ptr[sport] = cur_time;
				iprintf("[%s] g_nat_(tcp/udp) cached. sport=%d\n", __FUNCTION__, sport);
				return 0;
			}
#else
			if ((g_nat_ptr[sport >> 3] & (1 << (sport & 0x7))) != 0) {
				iprintf("[%s] g_nat_(tcp/udp) cached. sport=%d\n", __FUNCTION__, sport);
				return 0;
			}
#endif
		}
#endif
		
		parse_nat_table(ip_ptr->protocol);
		
		if (dir == 0) {
			/* Incoming packets */
			for (i = 0; i < g_nat_table_len; i++) {
				if (ip_ptr->protocol == g_nat_table[i].protocol &&
						ip_ptr->saddr == g_nat_table[i].in_saddr &&
						sport == g_nat_table[i].in_sport &&
						dport == g_nat_table[i].in_dport)
				{
					return 1;
				}
			}
#ifdef ARC_PCAP_FON_FILTER_USE_CACHE
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
			g_nat_ptr[dport] = time(0);
#else
			g_nat_ptr[dport >> 3] |= (1 << (dport & 0x7));
#endif
			iprintf("[%s] g_nat_(tcp/udp) add dport=%d\n", __FUNCTION__, dport);
#endif
		} else {
			/* Outgoing packets */
			for (i = 0; i < g_nat_table_len; i++) {
				if (ip_ptr->protocol == g_nat_table[i].protocol &&
						ip_ptr->daddr == g_nat_table[i].out_daddr &&
						sport == g_nat_table[i].in_dport && /* Should be transformed */
						dport == g_nat_table[i].out_dport)
				{
					return 1;
				}
			}
#ifdef ARC_PCAP_FON_FILTER_USE_CACHE
#ifdef ARC_PCAP_FON_FILTER_CACHE_SECONDS
			g_nat_ptr[sport] = time(0);
#else
			g_nat_ptr[sport >> 3] |= (1 << (sport & 0x7));
#endif
			iprintf("[%s] g_nat_(tcp/udp) add sport=%d\n", __FUNCTION__, sport);
#endif
		}
	}
	
	return 0;
}
#endif

static int pcap_output(ulog_iret_t *res)
{
	int						send_len, slen, total_sent;
	char*					p;
	struct pcap_sf_pkthdr	pchdr;

	if ( sock < 0 )
		return 1;

/* Terry 20120817, Pkt snooping to filter FON related packets */
#ifdef ARC_PCAP_FON_FILTER
	do {
		//iprintf("GET_VALUE(4).ptr=%s\n", GET_VALUE(4).ptr);
		//iprintf("GET_VALUE(5).ptr=%s\n", GET_VALUE(5).ptr);

		if (is_fon_pkt(GET_VALUE(0).ptr, GET_VALUE(4).ptr)) {
	#ifdef ARC_PCAP_FON_FILTER_DEBUG
			if (g_fon_filter_debug != 0)
				*((uint32_t *)GET_VALUE(0).ptr) = htonl(0xdeadbeef); /* Fill out 0xdeadbeef to first 4 bytes */
			else
				return 0;
	#else
			return 0;
	#endif
		} 
	} while (0);
#endif
	
	pchdr.len = ntohl_local(GET_VALUE(1).ui32);
	pchdr.caplen = pchdr.len;

	if (GET_FLAGS(2) & ULOGD_RETF_VALID
	    && GET_FLAGS(3) & ULOGD_RETF_VALID) {
		pchdr.ts.tv_sec = GET_VALUE(2).ui32;
		pchdr.ts.tv_usec = GET_VALUE(3).ui32;
	} else {
		/* use current system time */
		struct timeval tv;
		gettimeofday(&tv, NULL);

		pchdr.ts.tv_sec = tv.tv_sec;
		pchdr.ts.tv_usec = tv.tv_usec;
	}

	pchdr.ts.tv_sec = ntohl_local(pchdr.ts.tv_sec);
	pchdr.ts.tv_usec = ntohl_local(pchdr.ts.tv_usec);

	p = (char*)&pchdr;
	send_len = sizeof(pchdr);
	total_sent = 0;
	while ( total_sent < send_len )
	{
		slen = send(sock, (p+total_sent), (send_len-total_sent), 0);
		if ( slen < 0 )
			return 1;
		total_sent += slen;
	}

	p = (char*)GET_VALUE(0).ptr;
	send_len = (int)GET_VALUE(1).ui32;
	
	total_sent = 0;
	while ( total_sent < send_len )
	{
		slen = send(sock, (p+total_sent), (send_len-total_sent), 0);
		if ( slen < 0 )
			return 1;
		total_sent += slen;
	}

	return 0;
}

/* stolen from libpcap savefile.c */
#define LINKTYPE_RAW		101
#define	LINKTYPE_ETHERNET	1
#define TCPDUMP_MAGIC		0xa1b2c3d4

static int write_pcap_header(void)
{
	int						send_len, slen, total_sent;
	char*					p;
	struct pcap_file_header pcfh;

	if ( sock < 0 )
		return 0;

	pcfh.magic = ntohl_local(TCPDUMP_MAGIC);
	pcfh.version_major = ntohs_local(PCAP_VERSION_MAJOR);
	pcfh.version_minor = ntohs_local(PCAP_VERSION_MINOR);
	pcfh.thiszone = ntohl_local(timezone);
	pcfh.sigfigs = ntohl_local(0);
	pcfh.snaplen = ntohl_local(65535); /* we don't know the length in advance */
	pcfh.linktype = ntohl_local(LINKTYPE_ETHERNET);

	p = (char*)&pcfh;
	send_len = sizeof(pcfh);
	total_sent = 0;
	while ( total_sent < send_len )
	{
		slen = send(sock, (p+total_sent), (send_len-total_sent), 0);
		if ( slen < 0 )
			return 0;
		total_sent += slen;
	}

	return send_len;
}
	
/* get all key id's for the keys we are intrested in */
static int get_ids(void)
{
	int i;
	struct intr_id *cur_id;

	for (i = 0; i < INTR_IDS; i++) {
		cur_id = &intr_ids[i];
		cur_id->id = keyh_getid(cur_id->name);
		if (!cur_id->id) {
			ulogd_log(ULOGD_ERROR, 
				"Cannot resolve keyhash id for %s\n", 
				cur_id->name);
			return 1;
		}
	}	
	return 0;
}

void append_create_sock(void)
{
	//create a UNIX tcp socket for the transaction
	if ((sock=socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
		return;

	memset( &sock_addr, 0, sizeof(sock_addr) );
	sock_addr.sun_family = AF_UNIX;
	strcpy( sock_addr.sun_path, pcapsock_ce.u.string );
        
	if ( connect( sock, (struct sockaddr*)&sock_addr, sizeof(sock_addr) ) < 0 )
	{
		close(sock);
		sock = -1;
		return;
	}

	if (!write_pcap_header())
	{
		close(sock);
		sock = -1;
		return;
	}
}

static void pcap_signal_handler(int signal)
{
	switch (signal) {
	case SIGHUP:
		ulogd_log(ULOGD_NOTICE, "pcap: reopening capture file\n");
		if (sock >= 0)
		{
			close(sock);
			sock = -1;
		}
		append_create_sock();
		break;
	default:
		break;
	}
}

static int pcap_init(void)
{
	/* FIXME: error handling */
	config_parse_file("PCAPSOCK", &pcapsock_ce);

	append_create_sock();
	
	return 0;
}

static void pcap_fini(void)
{
	if (sock >= 0)
	{
		close( sock );
		sock = -1;
	}
}

static ulog_output_t pcap_op = {
	.name = "pcap", 
	.init = &pcap_init,
	.fini = &pcap_fini,
	.output = &pcap_output,
	.signal = &pcap_signal_handler,
};

void _init(void)
{
	if (get_ids()) {
		ulogd_log(ULOGD_ERROR, "can't resolve all keyhash id's\n");
	}

	register_output(&pcap_op);
}
