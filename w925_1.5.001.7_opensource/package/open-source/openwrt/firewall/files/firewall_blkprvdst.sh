#!/bin/sh

#echo > /dev/console;  echo "@@@@@@@@@@@@@@@@@@ $0 $* @@@@@@@@@@@@@@@@@@" > /dev/console; echo > /dev/console

usage() {
	echo "Usage: $0 add <interface> <interface ip>" > /dev/console
	echo "       $0 del <interface>" > /dev/console
}

ACTION="$1"
IF_NAME="$2"
IF_ADDR="$3"

#FNAME="/tmp/firewall_blkprvdst.restore"
#FNAME_TEMP="/tmp/firewall_blkprvdst.restore.local"
FNAME="/tmp/firewallExt/M1_99_firewall_blkprvdst.user"
FNAME_TEMP="/tmp/M1_99_firewall_blkprvdst.user.local"

add_ipt() {
    local ipt_string="$@"
    echo ${ipt_string} >> ${FNAME}
}
execute_ipt_rules() {
    local IPT="$1"
    local chain_name="$2"

    ${IPT} -F ${chain_name}
    sh ${FNAME}
}

remove_ipt_with_special_key_word() {
    local wan_string="$1"
    cat ${FNAME} | sed "/${wan_string}/dw ${FNAME_TEMP}"
    cp -f ${FNAME_TEMP} ${FNAME}
}

if [ "$ACTION" == "add" ]; then
	if [ $# != 3 ]; then
		usage
		exit
	fi
elif [ "$ACTION" == "del" ]; then
	if [ $# != 2 ]; then
		usage
		exit
	fi
else
	usage
	exit
fi

if [ "$ACTION" == "add" ] && [ ${IF_ADDR} == "0.0.0.0" ]; then
	exit
fi

. /lib/functions.sh
. /usr/sbin/iptables_utility.sh
. /usr/sbin/ip_utility.sh

wanifs=`cat /etc/config/network | grep wan | cut -d" " -f3 | tr -d \'`
config_load network
is_wan_device=0
for wanif in $wanifs; do
	config_get realifname ${wanif} ifname
	config_get devicename ${wanif} device
	config_get up ${wanif} up
	[ "${devicename}" = "${IF_NAME}" ] && is_wan_device=1
	[ "${realifname}" = "${IF_NAME}" ] && is_wan_device=1
	if [ "${devicename}" = "${IF_NAME}" ] && [ $up -ne 0 ]; then
		config_get proto ${wanif} proto
		if [ "${proto:0:3}" = "ppp" ]; then
            IF_NAME="${realifname}"
            break
		fi
	fi
done

if [ ${is_wan_device} -eq 0 ]; then
	exit
fi

config_get LAST_LAN_IP lan ipaddr
config_get LAST_LAN_MASK lan netmask

ipt_lock_res

IPT="iptables"

$IPT -L BLK_PRV_DST_PKT > /dev/null 2>&1
if [ $? != 0 ]; then
	echo "BLK_PRV_DST_PKT chain does not exist !!" > /dev/console
	echo "$0 firewall_blkprvdst.sh: Create BLK_PRV_DST_PKT chain .." > /dev/console
	$IPT -N BLK_PRV_DST_PKT
	#ipt_unlock_res
	#exit
fi

### Delete old rules anyway.
#[ -r /tmp/delvaildip.sh ] && sh /tmp/delvaildip.sh && rm /tmp/delvaildip.sh
#$IPT -D BLK_PRV_DST_PKT -o $IF_NAME -d 10.0.0.0/8 -j DROP 2> /dev/null
#$IPT -D BLK_PRV_DST_PKT -o $IF_NAME -d 172.16.0.0/12 -j DROP 2> /dev/null
#$IPT -D BLK_PRV_DST_PKT -o $IF_NAME -d 192.168.0.0/16 -j DROP 2> /dev/null
remove_ipt_with_special_key_word $IF_NAME

if [ "$ACTION" == "add" ]; then
	IP_NUM_1=`echo ${IF_ADDR} | awk '{FS="."} {print $1}'`
	IP_NUM_2=`echo ${IF_ADDR} | awk '{FS="."} {print $2}'`

	if [ $IP_NUM_1 -eq 10 ]; then
		is_pub_v4_addr=0
	elif [ $IP_NUM_1 -eq 172 ] && [ $IP_NUM_2 -ge 16 -a $IP_NUM_2 -le 31 ]; then
		is_pub_v4_addr=0
	elif [ $IP_NUM_1 -eq 192 ] && [ $IP_NUM_2 -eq 168 ]; then
		is_pub_v4_addr=0
	else
		is_pub_v4_addr=1
	fi

    remove_ipt_with_special_key_word "RELATED,ESTABLISHED" 

    # Must except any session which is established .
    add_ipt "$IPT -I BLK_PRV_DST_PKT -m conntrack --ctstate RELATED,ESTABLISHED -j RETURN"

	if [ $is_pub_v4_addr == 1 ]; then
        add_ipt "$IPT -A BLK_PRV_DST_PKT -o $IF_NAME -d 10.0.0.0/8 -j DROP"
        add_ipt "$IPT -A BLK_PRV_DST_PKT -o $IF_NAME -d 172.16.0.0/12 -j DROP"
        add_ipt "$IPT -A BLK_PRV_DST_PKT -o $IF_NAME -d 192.168.0.0/16 -j DROP"
	fi

    #add_ipt "$IPT -A BLK_PRV_DST_PKT -i $IF_NAME -d 192.168.2.0/24 -j DROP"
    add_ipt "$IPT -A BLK_PRV_DST_PKT -i $IF_NAME -d ${LAST_LAN_IP}/${LAST_LAN_MASK} -j DROP"

	### add ip addr with subnet mask
	IF_ADDR_BROADCAST=`getdirectedbroadcastIP "${IF_ADDR}" "255.255.255.0"`
	echo IF_ADDR:${IF_ADDR} IF_ADDR_BROADCAST:${IF_ADDR_BROADCAST} > /dev/console
    add_ipt "$IPT -A BLK_PRV_DST_PKT -o $IF_NAME -d ${IF_ADDR_BROADCAST} -j DROP"
	#echo "$IPT -D BLK_PRV_DST_PKT -o $IF_NAME -d ${IF_ADDR_BROADCAST} -j DROP" > /tmp/delvaildip.sh

fi
execute_ipt_rules ${IPT} "BLK_PRV_DST_PKT"

#echo "=============" > /dev/console
#iptables-save | grep BLK_PRV_DST_PKT > /dev/console
#echo "=============" > /dev/console

ipt_unlock_res

