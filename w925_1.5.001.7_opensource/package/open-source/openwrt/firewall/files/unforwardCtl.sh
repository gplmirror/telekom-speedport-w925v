#!/bin/sh

[ -f /tmp/br_modem_enabled ] && { 
	echo "### unforwardCtl.sh is disabled in bridge modem mode .." > /dev/console 
	exit 0
}

. /usr/sbin/iptables_utility.sh
# [ "$ACTION" = ifup -o "$ACTION" = ifupdate ] || exit 0
# [ "$ACTION" = ifupdate -a -z "$IFUPDATE_ADDRESSES" -a -z "$IFUPDATE_DATA" ] && exit 0

ACTION=$1
INTERFACE=$2
DEVICE=$3

case "$ACTION" in
	ifup)
		#		turn on forward
		if [ "${INTERFACE:0:3}" = "wan" ] || [ "${INTERFACE:0:3}" = "fon" ]; then
			ipt_lock_res
			#ipt_log "turn on forward INTERFACE:${INTERFACE} DEVICE:${DEVICE}"
			(iptables -D FORWARD -o ${DEVICE} -j DROP >/dev/null 2>&1)
			ipt_unlock_res
		fi
	;;
	ifdown)
	;;
esac


# logger -t firewall "Unblocking firewall forwarding due to $ACTION of $INTERFACE ($DEVICE)"

