#!/bin/sh
#[ -z "$1" ] && echo "Error: should be run by udhcpc" && exit 1

. /lib/functions.sh
. /lib/netifd/netifd-proto.sh

echo "[dhclient.scrpt] \$@=$@" > /dev/console
INTERFACE=$1
# RELEASE, NBI, PREINIT6, PREINIT, BOUND, RENEW, REBOOT, REBIND, STOP, DECLINE, EXPIRE, MEDIUM, SWITCHNOIP, TIMEOUT, FAIL
echo "reason=$reason" > /dev/console
echo "INTERFACE=$INTERFACE" > /dev/console
echo "interface=$interface" > /dev/console
echo "new_broadcast_address=$new_broadcast_address" > /dev/console
echo "new_dhcp_lease_time=$new_dhcp_lease_time" > /dev/console
echo "new_dhcp_message_type=$new_dhcp_message_type" > /dev/console
echo "new_dhcp_server_identifier=$new_dhcp_server_identifier" > /dev/console
echo "new_domain_name_servers=$new_domain_name_servers" > /dev/console
echo "new_expiry=$new_expiry" > /dev/console
echo "new_ip_address=$new_ip_address" > /dev/console
echo "new_network_number=$new_network_number" > /dev/console
echo "new_routers=$new_routers" > /dev/console
echo "new_server_address=$new_server_address" > /dev/console
echo "new_subnet_mask=$new_subnet_mask" > /dev/console
echo "pid=$pid" > /dev/console

ip="$new_ip_address"
subnet="$new_subnet_mask"
router="$new_routers"
dns="$new_domain_name_servers"
lease="$new_dhcp_lease_time"
hostname="$new_dhcp_server_identifier"

set_classless_routes() {
	local max=128
	while [ -n "$1" -a -n "$2" -a $max -gt 0 ]; do
		proto_add_ipv4_route "${1%%/*}" "${1##*/}" "$2" "$ip"
		max=$(($max-1))
		shift 2
	done
}

setup_interface () {
	proto_init_update "*" 1
	proto_add_ipv4_address "$ip" "${subnet:-255.255.255.0}"
	# TODO: apply $broadcast

	for i in $router; do
		proto_add_ipv4_route "$i" 32 "" "$ip"
		proto_add_ipv4_route 0.0.0.0 0 "$i" "$ip"

		for r in $CUSTOMROUTES; do
			proto_add_ipv4_route "${r%%/*}" "${r##*/}" "$i" "$ip"
		done
	done

	# CIDR STATIC ROUTES (rfc3442)
	[ -n "$staticroutes" ] && set_classless_routes $staticroutes
	[ -n "$msstaticroutes" ] && set_classless_routes $msstaticroutes

#Sean: This will make there are only VLAN8 DNS serves in resolv.conf.auto. Move to udhcpc.user
	# static DNS is higher priority than DNS from server.+++
#	DNS_STATIC1="`mng_cli get ARC_WAN_0_IP4_DNS_Static1`"
#	DNS_STATIC2="`mng_cli get ARC_WAN_0_IP4_DNS_Static2`"
#	if [ "$DNS_STATIC1" != "0.0.0.0" -o "$DNS_STATIC2" != "0.0.0.0" ]; then
#		if [ "$DNS_STATIC1" != "0.0.0.0" ]; then
#			proto_add_dns_server "$DNS_STATIC1"
#		fi
#		if [ "$DNS_STATIC2" != "0.0.0.0" ]; then
#			proto_add_dns_server "$DNS_STATIC2"
#		fi
#	else
#		for subdns in $dns; do
#			proto_add_dns_server "$subdns"
#		done
#	fi
	# static DNS is higher priority than DNS from server.---
#	for domain in $domain; do
#		proto_add_dns_search "$domain"
#	done

	proto_add_data
	[ -n "$ZONE" ]     && json_add_string zone "$ZONE"
	[ -n "$ntpsrv" ]   && json_add_string ntpserver "$ntpsrv"
	[ -n "$timesvr" ]  && json_add_string timeserver "$timesvr"
	[ -n "$hostname" ] && json_add_string hostname "$hostname"
	[ -n "$message" ]  && json_add_string message "$message"
	[ -n "$timezone" ] && json_add_int timezone "$timezone"
	[ -n "$lease" ]    && json_add_int leasetime "$lease"
	proto_close_data

	proto_send_update "$INTERFACE"


	if [ "$IFACE6RD" != 0 -a -n "$ip6rd" ]; then
		local v4mask="${ip6rd%% *}"
		ip6rd="${ip6rd#* }"
		local ip6rdprefixlen="${ip6rd%% *}"
		ip6rd="${ip6rd#* }"
		local ip6rdprefix="${ip6rd%% *}"
		ip6rd="${ip6rd#* }"
		local ip6rdbr="${ip6rd%% *}"

		[ -n "$ZONE" ] || ZONE=$(fw3 -q network $INTERFACE)
		[ -z "$IFACE6RD" -o "$IFACE6RD" = 1 ] && IFACE6RD=${INTERFACE}_6

		json_init
		json_add_string name "$IFACE6RD"
		json_add_string ifname "@$INTERFACE"
		json_add_string proto "6rd"
		json_add_string peeraddr "$ip6rdbr"
		json_add_int ip4prefixlen "$v4mask"
		json_add_string ip6prefix "$ip6rdprefix"
		json_add_int ip6prefixlen "$ip6rdprefixlen"
		json_add_string tunlink "$INTERFACE"
		[ -n "$IFACE6RD_DELEGATE" ] && json_add_boolean delegate "$IFACE6RD_DELEGATE"
		[ -n "$ZONE6RD" ] || ZONE6RD=$ZONE
		[ -n "$ZONE6RD" ] && json_add_string zone "$ZONE6RD"
		[ -n "$MTU6RD" ] && json_add_string mtu "$MTU6RD"
		json_close_object

		ubus call network add_dynamic "$(json_dump)"
	fi
}

deconfig_interface() {
	proto_init_update "*" 0
	proto_send_update "$INTERFACE"
}

tmp_reason=""
case "$reason" in
	RELEASE)
		tmp_reason="deconfig"
	;;
	RENEW)
		tmp_reason="renew"
	;;
	BOUND)
		tmp_reason="bound"
	;;
	REBOOT)
		tmp_reason="bound"
	;;
esac

case "$tmp_reason" in
	deconfig)
		deconfig_interface
	;;
	renew|bound)
		setup_interface
	;;
esac

# user rules
echo "[dhcp.script] call udhcpc.user, dns is \"$dns\"" > /dev/console
echo "\$@:$@" > /dev/console
#[ -f /etc/udhcpc.user ] && . /etc/udhcpc.user "$@"
[ -f /etc/udhcpc.user ] && . /etc/udhcpc.user "$tmp_reason"

exit 0

