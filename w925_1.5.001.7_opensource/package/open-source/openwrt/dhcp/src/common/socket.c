/* socket.c

   BSD socket interface code... */

/*
 * Copyright (c) 2004-2012 by Internet Systems Consortium, Inc. ("ISC")
 * Copyright (c) 1995-2003 by Internet Software Consortium
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL ISC BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *   Internet Systems Consortium, Inc.
 *   950 Charter Street
 *   Redwood City, CA 94063
 *   <info@isc.org>
 *   https://www.isc.org/
 *
 * This software has been written for Internet Systems Consortium
 * by Ted Lemon in cooperation with Vixie Enterprises and Nominum, Inc.
 * To learn more about Internet Systems Consortium, see
 * ``https://www.isc.org/''.  To learn more about Vixie Enterprises,
 * see ``http://www.vix.com''.   To learn more about Nominum, Inc., see
 * ``http://www.nominum.com''.
 */

/* SO_BINDTODEVICE support added by Elliot Poger (poger@leland.stanford.edu).
 * This sockopt allows a socket to be bound to a particular interface,
 * thus enabling the use of DHCPD on a multihomed host.
 * If SO_BINDTODEVICE is defined in your system header files, the use of
 * this sockopt will be automatically enabled. 
 * I have implemented it under Linux; other systems should be doable also.
 */

#include "dhcpd.h"
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <sys/uio.h>

#if defined(sun) && defined(USE_V4_PKTINFO)
#include <sys/sysmacros.h>
#include <net/if.h>
#include <sys/sockio.h>
#include <net/if_dl.h>
#include <sys/dlpi.h>
#endif
/* tlhhh 2010-12-17. add to support raw socket */
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/filter.h>

#ifndef ETHERTYPE_IP
#define ETHERTYPE_IP	0x0800	/* IP Protocol */
#endif

#ifdef USE_SOCKET_FALLBACK
# if !defined (USE_SOCKET_SEND)
#  define if_register_send if_register_fallback
#  define send_packet send_fallback
#  define if_reinitialize_send if_reinitialize_fallback
# endif
#endif

#if defined(DHCPv6)
/*
 * XXX: this is gross.  we need to go back and overhaul the API for socket
 * handling.
 */
static unsigned int global_v6_socket_references = 0;
static int global_v6_socket = -1;

static void if_register_multicast(struct interface_info *info);
#endif

/*
 * We can use a single socket for AF_INET (similar to AF_INET6) on all
 * interfaces configured for DHCP if the system has support for IP_PKTINFO
 * and IP_RECVPKTINFO (for example Solaris 11).
 */
#if defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && defined(USE_V4_PKTINFO)
static unsigned int global_v4_socket_references = 0;
static int global_v4_socket = -1;
#endif

/*
 * If we can't bind() to a specific interface, then we can only have
 * a single socket. This variable insures that we don't try to listen
 * on two sockets.
 */
#if !defined(SO_BINDTODEVICE) && !defined(USE_FALLBACK)
static int once = 0;
#endif /* !defined(SO_BINDTODEVICE) && !defined(USE_FALLBACK) */

/* Reinitializes the specified interface after an address change.   This
   is not required for packet-filter APIs. */

#if defined (USE_SOCKET_SEND) || defined (USE_SOCKET_FALLBACK)
void if_reinitialize_send (info)
	struct interface_info *info;
{
#if 0
#ifndef USE_SOCKET_RECEIVE
	once = 0;
	close (info -> wfdesc);
#endif
	if_register_send (info);
#endif
}
#endif

int get_ifdex(char *name, int *ifindex)
{
    int fd;
    struct ifreq ifr;
    struct sockaddr_in *our_ip;

    memset(&ifr, 0, sizeof(struct ifreq));

    if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) >= 0) {
        ifr.ifr_addr.sa_family = AF_INET;
        strcpy(ifr.ifr_name, name);

        if (ioctl(fd, SIOCGIFINDEX, &ifr) == 0) {
            log_info("adapter index %d", ifr.ifr_ifindex);
            *ifindex = ifr.ifr_ifindex;
        } else {
			log_error("SIOCGIFINDEX failed!: %s", strerror(errno));
            return -1;
        }
    } else {
        log_error("socket failed!: %s", strerror(errno));
        return -1;
    }
    close(fd);
    return 0;
}

#ifdef USE_SOCKET_RECEIVE
void
get_hw_addr(const char *name, struct hardware *hw) {
	int sock;
	struct ifreq tmp;
	struct sockaddr *sa;

	if (strlen(name) >= sizeof(tmp.ifr_name)) {
		log_fatal("Device name too long: \"%s\"", name);
	}

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		log_fatal("Can't create socket for \"%s\": %m", name);
	}

	memset(&tmp, 0, sizeof(tmp));
	strcpy(tmp.ifr_name, name);
	if (ioctl(sock, SIOCGIFHWADDR, &tmp) < 0) {
		log_fatal("Error getting hardware address for \"%s\": %m", 
			  name);
	}

	sa = &tmp.ifr_hwaddr;
	switch (sa->sa_family) {
		case ARPHRD_ETHER:
			hw->hlen = 7;
			hw->hbuf[0] = HTYPE_ETHER;
			memcpy(&hw->hbuf[1], sa->sa_data, 6);
			break;
		case ARPHRD_IEEE802:
#ifdef ARPHRD_IEEE802_TR
		case ARPHRD_IEEE802_TR:
#endif /* ARPHRD_IEEE802_TR */
			hw->hlen = 7;
			hw->hbuf[0] = HTYPE_IEEE802;
			memcpy(&hw->hbuf[1], sa->sa_data, 6);
			break;
		case ARPHRD_FDDI:
			hw->hlen = 17;
			hw->hbuf[0] = HTYPE_FDDI;
			memcpy(&hw->hbuf[1], sa->sa_data, 16);
			break;
#if defined(ARPHRD_PPP)
                case ARPHRD_PPP:
			if (local_family != AF_INET6)
				log_fatal("Unsupported device type %d for \"%s\"",
				           sa->sa_family, name);
			hw->hlen = 0;
			hw->hbuf[0] = HTYPE_RESERVED;
			/* 0xdeadbeef should never occur on the wire, and is a signature that
			 * something went wrong.
			 */
			hw->hbuf[1] = 0xde;
			hw->hbuf[2] = 0xad;
			hw->hbuf[3] = 0xbe;
			hw->hbuf[4] = 0xef;
			break;
#endif
		default:
			log_fatal("Unsupported device type %ld for \"%s\"",
				  (long int)sa->sa_family, name);
	}

	close(sock);
}

void if_reinitialize_receive (info)
	struct interface_info *info;
{
#if 0
	once = 0;
	close (info -> rfdesc);
	if_register_receive (info);
#endif
}
#endif

#if defined (USE_SOCKET_SEND) || \
	defined (USE_SOCKET_RECEIVE) || \
		defined (USE_SOCKET_FALLBACK)
/* Generic interface registration routine... */
int
if_register_socket(struct interface_info *info, int family,
		   int *do_multicast)
{
	struct sockaddr_storage name;
	int name_len;
	int sock;
	int flag;
	int domain;
#ifdef DHCPv6
	struct sockaddr_in6 *addr6;
#endif
	struct sockaddr_in *addr;

	/* INSIST((family == AF_INET) || (family == AF_INET6)); */

#if !defined(SO_BINDTODEVICE) && !defined(USE_FALLBACK)
	/* Make sure only one interface is registered. */
	if (once) {
		log_fatal ("The standard socket API can only support %s",
		       "hosts with a single network interface.");
	}
	once = 1;
#endif

	/* 
	 * Set up the address we're going to bind to, depending on the
	 * address family. 
	 */ 
	memset(&name, 0, sizeof(name));
	switch (family) {
#ifdef DHCPv6
	case AF_INET6:
		addr6 = (struct sockaddr_in6 *)&name; 
		addr6->sin6_family = AF_INET6;
		addr6->sin6_port = local_port;
		/* XXX: What will happen to multicasts if this is nonzero? */
		memcpy(&addr6->sin6_addr,
		       &local_address6, 
		       sizeof(addr6->sin6_addr));
#ifdef HAVE_SA_LEN
		addr6->sin6_len = sizeof(*addr6);
#endif
		name_len = sizeof(*addr6);
		domain = PF_INET6;
		if ((info->flags & INTERFACE_STREAMS) == INTERFACE_UPSTREAM) {
			*do_multicast = 0;
		}
		break;
#endif /* DHCPv6 */

	case AF_INET:
	default:
		addr = (struct sockaddr_in *)&name; 
		addr->sin_family = AF_INET;
		addr->sin_port = local_port;
		memcpy(&addr->sin_addr,
		       &local_address,
		       sizeof(addr->sin_addr));
#ifdef HAVE_SA_LEN
		addr->sin_len = sizeof(*addr);
#endif
		name_len = sizeof(*addr);
		domain = PF_INET;
		break;
	}

	/* Make a socket... */
	sock = socket(domain, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0) {
		log_fatal("Can't create dhcp socket: %m");
	}

	/* Set the REUSEADDR option so that we don't fail to start if
	   we're being restarted. */
	flag = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
			(char *)&flag, sizeof(flag)) < 0) {
		log_fatal("Can't set SO_REUSEADDR option on dhcp socket: %m");
	}

	/* Set the BROADCAST option so that we can broadcast DHCP responses.
	   We shouldn't do this for fallback devices, and we can detect that
	   a device is a fallback because it has no ifp structure. */
	if (info->ifp &&
	    (setsockopt(sock, SOL_SOCKET, SO_BROADCAST,
			 (char *)&flag, sizeof(flag)) < 0)) {
		log_fatal("Can't set SO_BROADCAST option on dhcp socket: %m");
	}

#if defined(DHCPv6) && defined(SO_REUSEPORT)
	/*
	 * We only set SO_REUSEPORT on AF_INET6 sockets, so that multiple
	 * daemons can bind to their own sockets and get data for their
	 * respective interfaces.  This does not (and should not) affect
	 * DHCPv4 sockets; we can't yet support BSD sockets well, much
	 * less multiple sockets.
	 */
	if (local_family == AF_INET6) {
		flag = 1;
		if (setsockopt(sock, SOL_SOCKET, SO_REUSEPORT,
			       (char *)&flag, sizeof(flag)) < 0) {
			log_fatal("Can't set SO_REUSEPORT option on dhcp "
				  "socket: %m");
		}
	}
#endif

	/* Bind the socket to this interface's IP address. */
	if (bind(sock, (struct sockaddr *)&name, name_len) < 0) {
		log_error("Can't bind to dhcp address: %m");
		log_error("Please make sure there is no other dhcp server");
		log_error("running and that there's no entry for dhcp or");
		log_error("bootp in /etc/inetd.conf.   Also make sure you");
		log_error("are not running HP JetAdmin software, which");
		log_fatal("includes a bootp server.");
	}

#if defined(SO_BINDTODEVICE)
	/* Bind this socket to this interface. */
	if ((local_family != AF_INET6) && (info->ifp != NULL) &&
	    setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE,
			(char *)(info -> ifp), sizeof(*(info -> ifp))) < 0) {
		log_fatal("setsockopt: SO_BINDTODEVICE: %m");
	}
#endif

	/* IP_BROADCAST_IF instructs the kernel which interface to send
	 * IP packets whose destination address is 255.255.255.255.  These
	 * will be treated as subnet broadcasts on the interface identified
	 * by ip address (info -> primary_address).  This is only known to
	 * be defined in SCO system headers, and may not be defined in all
	 * releases.
	 */
#if defined(SCO) && defined(IP_BROADCAST_IF)
        if (info->address_count &&
	    setsockopt(sock, IPPROTO_IP, IP_BROADCAST_IF, &info->addresses[0],
		       sizeof(info->addresses[0])) < 0)
		log_fatal("Can't set IP_BROADCAST_IF on dhcp socket: %m");
#endif

#if defined(IP_PKTINFO) && defined(IP_RECVPKTINFO)  && defined(USE_V4_PKTINFO)
	/*
	 * If we turn on IP_RECVPKTINFO we will be able to receive
	 * the interface index information of the received packet.
	 */
	if (family == AF_INET) {
		int on = 1;
		if (setsockopt(sock, IPPROTO_IP, IP_RECVPKTINFO, 
		               &on, sizeof(on)) != 0) {
			log_fatal("setsockopt: IPV_RECVPKTINFO: %m");
		}
	}
#endif

#ifdef DHCPv6
	/*
	 * If we turn on IPV6_PKTINFO, we will be able to receive 
	 * additional information, such as the destination IP address.
	 * We need this to spot unicast packets.
	 */
	if (family == AF_INET6) {
		int on = 1;
#ifdef IPV6_RECVPKTINFO
		/* RFC3542 */
		if (setsockopt(sock, IPPROTO_IPV6, IPV6_RECVPKTINFO, 
		               &on, sizeof(on)) != 0) {
			log_fatal("setsockopt: IPV6_RECVPKTINFO: %m");
		}
#else
		/* RFC2292 */
		if (setsockopt(sock, IPPROTO_IPV6, IPV6_PKTINFO, 
		               &on, sizeof(on)) != 0) {
			log_fatal("setsockopt: IPV6_PKTINFO: %m");
		}
#endif
	}

	if ((family == AF_INET6) &&
	    ((info->flags & INTERFACE_UPSTREAM) != 0)) {
		int hop_limit = 32;
		if (setsockopt(sock, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
			       &hop_limit, sizeof(int)) < 0) {
			log_fatal("setsockopt: IPV6_MULTICAST_HOPS: %m");
		}
	}
#endif /* DHCPv6 */

	return sock;
}

int if_register_rawsocket(struct interface_info *info)
{
	int fd;
	struct sockaddr_ll sock;
	int ifindex = 0;

    /* linghong_tan. 2013-01-23. 
	 * Ugly hack, should use libpcap built-in function to export.
	 *
	 * FIXME: tcpdump -i wan_iface "udp port 67 or udp port 68" -dd 
	 */
    struct sock_filter BPF_code [] = {
#if 0
        { 0x28, 0, 0, 0x0000000c },
        { 0x15, 0, 7, 0x000086dd },
        { 0x30, 0, 0, 0x00000014 },
        { 0x15, 0, 18, 0x00000011 },
        { 0x28, 0, 0, 0x00000036 },
        { 0x15, 15, 0, 0x00000043 },
        { 0x15, 14, 0, 0x00000044 },
        { 0x28, 0, 0, 0x00000038 },
        { 0x15, 12, 11, 0x00000043 },
        { 0x15, 0, 12, 0x00000800 },

        { 0x30, 0, 0, 0x00000017 },
        { 0x15, 0, 10, 0x00000011 },
        { 0x28, 0, 0, 0x00000014 },
        { 0x45, 8, 0, 0x00001fff },
		{ 0xb1, 0, 0, 0x0000000e },
		{ 0x48, 0, 0, 0x0000000e },
		{ 0x15, 4, 0, 0x00000043 },
		{ 0x15, 3, 0, 0x00000044 },
		{ 0x48, 0, 0, 0x00000010 },
		{ 0x15, 1, 0, 0x00000043 },
		{ 0x15, 0, 1, 0x00000044 },
		{ 0x6, 0, 0, 0x0000ffff },
		{ 0x6, 0, 0, 0x00000000 },
#else
		/* Make sure this is an IP packet... */
		BPF_STMT (BPF_LD + BPF_H + BPF_ABS, 12),
		BPF_JUMP (BPF_JMP + BPF_JEQ + BPF_K, ETHERTYPE_IP, 0, 8),

		/* Make sure it's a UDP packet... */
		BPF_STMT (BPF_LD + BPF_B + BPF_ABS, 23),
		BPF_JUMP (BPF_JMP + BPF_JEQ + BPF_K, IPPROTO_UDP, 0, 6),

		/* Make sure this isn't a fragment... */
		BPF_STMT(BPF_LD + BPF_H + BPF_ABS, 20),
		BPF_JUMP(BPF_JMP + BPF_JSET + BPF_K, 0x1fff, 4, 0),

		/* Get the IP header length... */
		BPF_STMT (BPF_LDX + BPF_B + BPF_MSH, 14),

		/* Make sure it's to the right port... */
		BPF_STMT (BPF_LD + BPF_H + BPF_IND, 16),
		BPF_JUMP (BPF_JMP + BPF_JEQ + BPF_K, 67, 0, 1),             /* patch */

		/* If we passed all the tests, ask for the whole packet. */
		BPF_STMT(BPF_RET+BPF_K, (u_int)-1),

		/* Otherwise, drop it. */
		BPF_STMT(BPF_RET+BPF_K, 0),
#endif
	};

	struct sock_fprog sk_filter;
	int BPF_code_len = sizeof(BPF_code)/sizeof(struct sock_filter);

	sk_filter.len = BPF_code_len;
	sk_filter.filter = BPF_code;

	/* Patch the server port into the LPF  program...
	 * XXX changes to filter program may require changes
	 * to the insn number(s) used below! XXX 
	 */
	BPF_code[8].k = ntohs ((short)local_port);	/* Pass port 68 */

	if ((fd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
		log_error("socket call failed: %s", strerror(errno));
		return -1;
	}

	/* Attach the filter to the socket */
	if (setsockopt(fd, SOL_SOCKET, SO_ATTACH_FILTER, &sk_filter, sizeof(sk_filter)) < 0) {
		cprintf("[WARNING]: Failed to attach the filter!\n");
		close(fd);
		return -1;
	}

	memset(&sock, 0, sizeof(sock));
	
	sock.sll_family = AF_PACKET;
	sock.sll_protocol = htons(ETH_P_ALL);
	get_ifdex(info->name, &ifindex);

	if (ifindex <= 0) {
		close(fd);
		return -1;
	}
	sock.sll_ifindex = ifindex;
	if (bind(fd, (struct sockaddr *) &sock, sizeof(sock)) < 0) {
		log_error("bind call failed: %s", strerror(errno));
		close(fd);
		return -1;
	}

	return fd;
}
#endif /* USE_SOCKET_SEND || USE_SOCKET_RECEIVE || USE_SOCKET_FALLBACK */

#if defined (USE_SOCKET_SEND) || defined (USE_SOCKET_FALLBACK)
void if_register_send (info)
	struct interface_info *info;
{
#ifndef USE_SOCKET_RECEIVE
	info->wfdesc = if_register_socket(info, AF_INET, 0);
	/* If this is a normal IPv4 address, get the hardware address. */
	if (strcmp(info->name, "fallback") != 0)
		get_hw_addr(info->name, &info->hw_address);
#if defined (USE_SOCKET_FALLBACK)
	/* Fallback only registers for send, but may need to receive as
	   well. */
	info->rfdesc = info->wfdesc;
#endif
#else
	info->wfdesc = info->rfdesc;
#endif
	if (!quiet_interface_discovery)
		log_info ("Sending on   Socket/%s%s%s",
		      info->name,
		      (info->shared_network ? "/" : ""),
		      (info->shared_network ?
		       info->shared_network->name : ""));
}

#if defined (USE_SOCKET_SEND)
void if_deregister_send (info)
	struct interface_info *info;
{
#ifndef USE_SOCKET_RECEIVE
	close (info -> wfdesc);
#endif
	info -> wfdesc = -1;

	if (!quiet_interface_discovery)
		log_info ("Disabling output on Socket/%s%s%s",
		      info -> name,
		      (info -> shared_network ? "/" : ""),
		      (info -> shared_network ?
		       info -> shared_network -> name : ""));
}
#endif /* USE_SOCKET_SEND */
#endif /* USE_SOCKET_SEND || USE_SOCKET_FALLBACK */

#ifdef USE_SOCKET_RECEIVE
void if_register_receive (info)
	struct interface_info *info;
{

#if defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && defined(USE_V4_PKTINFO)
	if (global_v4_socket_references == 0) {
		global_v4_socket = if_register_socket(info, AF_INET, 0);
		if (global_v4_socket < 0) {
			/*
			 * if_register_socket() fatally logs if it fails to
			 * create a socket, this is just a sanity check.
			 */
			log_fatal("Failed to create AF_INET socket %s:%d",
				  MDL);
		}
	}
		
	info->rfdesc = global_v4_socket;
	global_v4_socket_references++;
#else
	/* If we're using the socket API for sending and receiving,
	   we don't need to register this interface twice. */
	info->rfdesc = if_register_socket(info, AF_INET, 0);
#endif /* IP_PKTINFO... */
	info -> recv_mode = LISTEN_KERNEL;	//tlhhh. use regular socket 
	
	/* If this is a normal IPv4 address, get the hardware address. */
	if (strcmp(info->name, "fallback") != 0)
		get_hw_addr(info->name, &info->hw_address);

	if (!quiet_interface_discovery)
		log_info ("Listening on Socket/%s%s%s",
		      info->name,
		      (info->shared_network ? "/" : ""),
		      (info->shared_network ?
		       info->shared_network->name : ""));
}

void if_deregister_receive (info)
	struct interface_info *info;
{
#if defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && defined(USE_V4_PKTINFO)
	/* Dereference the global v4 socket. */
	if ((info->rfdesc == global_v4_socket) &&
	    (info->wfdesc == global_v4_socket) &&
	    (global_v4_socket_references > 0)) {
		global_v4_socket_references--;
		info->rfdesc = -1;
	} else {
		log_fatal("Impossible condition at %s:%d", MDL);
	}

	if (global_v4_socket_references == 0) {
		close(global_v4_socket);
		global_v4_socket = -1;
	}
#else
	close(info->rfdesc);
	info->rfdesc = -1;
#endif /* IP_PKTINFO... */
	if (!quiet_interface_discovery)
		log_info ("Disabling input on Socket/%s%s%s",
		      info -> name,
		      (info -> shared_network ? "/" : ""),
		      (info -> shared_network ?
		       info -> shared_network -> name : ""));
}

void if_change_receive_mode (struct interface_info *info, int mode)
{
	cprintf("Enter: current mode=[%d], mode=[%d]\n", info->recv_mode, mode);
	if_deregister_receive(info);

	if (mode == LISTEN_RAW) {
		info->rfdesc = if_register_rawsocket(info);
		if (info->rfdesc < 0)
			return;
	}
	else {
		if_register_receive(info);	//LISTEN_KERNEL
	}

	info->recv_mode = mode;
	cprintf("Leave: current mode=[%d], mode=[%d]\n", info->recv_mode, mode);
}
#endif /* USE_SOCKET_RECEIVE */


#ifdef DHCPv6 
/*
 * This function joins the interface to DHCPv6 multicast groups so we will
 * receive multicast messages.
 */
static void
if_register_multicast(struct interface_info *info) {
	int sock = info->rfdesc;
	struct ipv6_mreq mreq;

	if (inet_pton(AF_INET6, All_DHCP_Relay_Agents_and_Servers,
		      &mreq.ipv6mr_multiaddr) <= 0) {
		log_fatal("inet_pton: unable to convert '%s'", 
			  All_DHCP_Relay_Agents_and_Servers);
	}
	mreq.ipv6mr_interface = if_nametoindex(info->name);
	if (setsockopt(sock, IPPROTO_IPV6, IPV6_JOIN_GROUP, 
		       &mreq, sizeof(mreq)) < 0) {
		log_fatal("setsockopt: IPV6_JOIN_GROUP: %m");
	}

	/*
	 * The relay agent code sets the streams so you know which way
	 * is up and down.  But a relay agent shouldn't join to the
	 * Server address, or else you get fun loops.  So up or down
	 * doesn't matter, we're just using that config to sense this is
	 * a relay agent.
	 */
	if ((info->flags & INTERFACE_STREAMS) == 0) {
		if (inet_pton(AF_INET6, All_DHCP_Servers,
			      &mreq.ipv6mr_multiaddr) <= 0) {
			log_fatal("inet_pton: unable to convert '%s'", 
				  All_DHCP_Servers);
		}
		mreq.ipv6mr_interface = if_nametoindex(info->name);
		if (setsockopt(sock, IPPROTO_IPV6, IPV6_JOIN_GROUP, 
			       &mreq, sizeof(mreq)) < 0) {
			log_fatal("setsockopt: IPV6_JOIN_GROUP: %m");
		}
	}
}

void
if_register6(struct interface_info *info, int do_multicast) {
	/* Bounce do_multicast to a stack variable because we may change it. */
	int req_multi = do_multicast;

	if (global_v6_socket_references == 0) {
		global_v6_socket = if_register_socket(info, AF_INET6,
						      &req_multi);
		if (global_v6_socket < 0) {
			/*
			 * if_register_socket() fatally logs if it fails to
			 * create a socket, this is just a sanity check.
			 */
			log_fatal("Impossible condition at %s:%d", MDL);
		} else {
			log_info("Bound to *:%d", ntohs(local_port));
		}
	}
		
	info->rfdesc = global_v6_socket;
	info->wfdesc = global_v6_socket;
	global_v6_socket_references++;

	if (req_multi)
		if_register_multicast(info);

	get_hw_addr(info->name, &info->hw_address);

	if (!quiet_interface_discovery) {
		if (info->shared_network != NULL) {
			log_info("Listening on Socket/%d/%s/%s",
				 global_v6_socket, info->name, 
				 info->shared_network->name);
			log_info("Sending on   Socket/%d/%s/%s",
				 global_v6_socket, info->name,
				 info->shared_network->name);
		} else {
			log_info("Listening on Socket/%s", info->name);
			log_info("Sending on   Socket/%s", info->name);
		}
	}
}

void 
if_deregister6(struct interface_info *info) {
	/* Dereference the global v6 socket. */
	if ((info->rfdesc == global_v6_socket) &&
	    (info->wfdesc == global_v6_socket) &&
	    (global_v6_socket_references > 0)) {
		global_v6_socket_references--;
		info->rfdesc = -1;
		info->wfdesc = -1;
	} else {
		log_fatal("Impossible condition at %s:%d", MDL);
	}

	if (!quiet_interface_discovery) {
		if (info->shared_network != NULL) {
			log_info("Disabling input on  Socket/%s/%s", info->name,
		       		 info->shared_network->name);
			log_info("Disabling output on Socket/%s/%s", info->name,
		       		 info->shared_network->name);
		} else {
			log_info("Disabling input on  Socket/%s", info->name);
			log_info("Disabling output on Socket/%s", info->name);
		}
	}

	if (global_v6_socket_references == 0) {
		close(global_v6_socket);
		global_v6_socket = -1;

		log_info("Unbound from *:%d", ntohs(local_port));
	}
}
#endif /* DHCPv6 */

#if defined (USE_SOCKET_SEND) || defined (USE_SOCKET_FALLBACK)
ssize_t send_packet (interface, packet, raw, len, from, to, hto)
	struct interface_info *interface;
	struct packet *packet;
	struct dhcp_packet *raw;
	size_t len;
	struct in_addr from;
	struct sockaddr_in *to;
	struct hardware *hto;
{
	int result;

	if (interface->send_mode == LISTEN_RAW)
		return send_raw_packet(interface, packet, raw, len, from, to, hto);

#ifdef IGNORE_HOSTUNREACH
	int retry = 0;
	do {
#endif
#if defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && defined(USE_V4_PKTINFO)
		struct in_pktinfo pktinfo;

		if (interface->ifp != NULL) {
			memset(&pktinfo, 0, sizeof (pktinfo));
			pktinfo.ipi_ifindex = interface->ifp->ifr_index;
			if (setsockopt(interface->wfdesc, IPPROTO_IP,
				       IP_PKTINFO, (char *)&pktinfo,
				       sizeof(pktinfo)) < 0) 
				log_fatal("setsockopt: IP_PKTINFO: %m");
		}
#endif
		result = sendto (interface -> wfdesc, (char *)raw, len, 0,
				 (struct sockaddr *)to, sizeof *to);
#ifdef IGNORE_HOSTUNREACH
	} while (to -> sin_addr.s_addr == htonl (INADDR_BROADCAST) &&
		 result < 0 &&
		 (errno == EHOSTUNREACH ||
		  errno == ECONNREFUSED) &&
		 retry++ < 10);
#endif
	if (result < 0) {
		log_error ("send_packet: %m");
		if (errno == ENETUNREACH)
			log_error ("send_packet: please consult README file%s",
				   " regarding broadcast address.");
	}
	return result;
}

/*  tlhhh 2010-12-17. use raw socket to send packets for dhclient */
ssize_t send_raw_packet (interface, packet, raw, len, from, to, hto)
	struct interface_info *interface;
	struct packet *packet;
	struct dhcp_packet *raw;
	size_t len;
	struct in_addr from;
	struct sockaddr_in *to;
	struct hardware *hto;
{
	unsigned hbufp = 0, ibufp = 0;
	double ih [1536 / sizeof (double)];
	unsigned char *buf = (unsigned char *)ih;
	int result;
	
	int fd;
	int ifindex;
	struct sockaddr_ll dest;

	if (!strcmp (interface -> name, "fallback"))
		return 0;

	if ((fd = socket(PF_PACKET, SOCK_RAW,  htons(ETH_P_ALL))) < 0) {
		log_error("socket call failed: %s", strerror(errno));
		return -1;
	}
	
	memset(&dest, 0, sizeof(dest));
	
	dest.sll_family = AF_PACKET;
	dest.sll_protocol = htons(ETH_P_ALL);
	get_ifdex(interface->name, &ifindex);
	if (ifindex <= 0 ) {
		close(fd);
		return -1;
	}
		
	dest.sll_ifindex = ifindex;

	/* fill in dest mac address */
	dest.sll_halen = 6;
	if (hto && hto -> hlen == 7) /* XXX */
		memcpy (dest.sll_addr, &hto -> hbuf [1], 6);
	else
		memset(dest.sll_addr, 0xff, 6);

	if (bind(fd, (struct sockaddr *)&dest, sizeof(struct sockaddr_ll)) < 0) {
		log_error( "bind call failed: %s", strerror(errno));
		close(fd);
		return -1;
	}

	/* temperary socket for send */
	interface->wfdesc = fd;

	cprintf("%s[%d]: interface->name=%s,  interface->wfdesc=%d\n", __func__, __LINE__, interface->name,  interface->wfdesc);

	/* Assemble the headers... */
	assemble_hw_header (interface, (unsigned char *)buf, &hbufp, hto);
	ibufp = hbufp;

	assemble_udp_ip_header (interface, buf, &ibufp, from.s_addr,
				to -> sin_addr.s_addr, to -> sin_port,
				(unsigned char *)raw, len);
	memcpy (buf + ibufp, raw, len);

	result = sendto (interface -> wfdesc, buf, ibufp + len, 0,  (struct sockaddr *) &dest, sizeof(dest));
	if (result < 0)
		log_error ("send_packet: %m");

	close(interface->wfdesc);
	interface->wfdesc = -1;

	return result;
}
#endif /* USE_SOCKET_SEND || USE_SOCKET_FALLBACK */

#ifdef DHCPv6
/*
 * Solaris 9 is missing the CMSG_LEN and CMSG_SPACE macros, so we will 
 * synthesize them (based on the BIND 9 technique).
 */

#ifndef CMSG_LEN
static size_t CMSG_LEN(size_t len) {
	size_t hdrlen;
	/*
	 * Cast NULL so that any pointer arithmetic performed by CMSG_DATA
	 * is correct.
	 */
	hdrlen = (size_t)CMSG_DATA(((struct cmsghdr *)NULL));
	return hdrlen + len;
}
#endif /* !CMSG_LEN */

#ifndef CMSG_SPACE
static size_t CMSG_SPACE(size_t len) {
	struct msghdr msg;
	struct cmsghdr *cmsgp;

	/*
	 * XXX: The buffer length is an ad-hoc value, but should be enough
	 * in a practical sense.
	 */
	union {
		struct cmsghdr cmsg_sizer;
		u_int8_t pktinfo_sizer[sizeof(struct cmsghdr) + 1024];
	} dummybuf;

	memset(&msg, 0, sizeof(msg));
	msg.msg_control = &dummybuf;
	msg.msg_controllen = sizeof(dummybuf);

	cmsgp = (struct cmsghdr *)&dummybuf;
	cmsgp->cmsg_len = CMSG_LEN(len);

	cmsgp = CMSG_NXTHDR(&msg, cmsgp);
	if (cmsgp != NULL) {
		return (char *)cmsgp - (char *)msg.msg_control;
	} else {
		return 0;
	}
}
#endif /* !CMSG_SPACE */

#endif /* DHCPv6 */

#if defined(DHCPv6) || \
	(defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && \
	 defined(USE_V4_PKTINFO))
/*
 * For both send_packet6() and receive_packet6() we need to allocate
 * space for the cmsg header information.  We do this once and reuse
 * the buffer.  We also need the control buf for send_packet() and
 * receive_packet() when we use a single socket and IP_PKTINFO to
 * send the packet out the correct interface.
 */
static void   *control_buf = NULL;
static size_t  control_buf_len = 0;

static void
allocate_cmsg_cbuf(void) {
	control_buf_len = CMSG_SPACE(sizeof(struct in6_pktinfo));
	control_buf = dmalloc(control_buf_len, MDL);
	return;
}
#endif /* DHCPv6, IP_PKTINFO ... */

#ifdef DHCPv6
/* 
 * For both send_packet6() and receive_packet6() we need to use the 
 * sendmsg()/recvmsg() functions rather than the simpler send()/recv()
 * functions.
 *
 * In the case of send_packet6(), we need to do this in order to insure
 * that the reply packet leaves on the same interface that it arrived 
 * on. 
 *
 * In the case of receive_packet6(), we need to do this in order to 
 * get the IP address the packet was sent to. This is used to identify
 * whether a packet is multicast or unicast.
 *
 * Helpful man pages: recvmsg, readv (talks about the iovec stuff), cmsg.
 *
 * Also see the sections in RFC 3542 about IPV6_PKTINFO.
 */

/* Send an IPv6 packet */
ssize_t send_packet6(struct interface_info *interface,
		     const unsigned char *raw, size_t len,
		     struct sockaddr_in6 *to) {
	struct msghdr m;
	struct iovec v;
	int result;
	struct in6_pktinfo *pktinfo;
	struct cmsghdr *cmsg;

	/*
	 * If necessary allocate space for the control message header.
	 * The space is common between send and receive.
	 */

	if (control_buf == NULL) {
		allocate_cmsg_cbuf();
		if (control_buf == NULL) {
			log_error("send_packet6: unable to allocate cmsg header");
			return(ENOMEM);
		}
	}
	memset(control_buf, 0, control_buf_len);

	/*
	 * Initialize our message header structure.
	 */
	memset(&m, 0, sizeof(m));

	/*
	 * Set the target address we're sending to.
	 */
	m.msg_name = to;
	m.msg_namelen = sizeof(*to);

	/*
	 * Set the data buffer we're sending. (Using this wacky 
	 * "scatter-gather" stuff... we only have a single chunk 
	 * of data to send, so we declare a single vector entry.)
	 */
	v.iov_base = (char *)raw;
	v.iov_len = len;
	m.msg_iov = &v;
	m.msg_iovlen = 1;

	/*
	 * Setting the interface is a bit more involved.
	 * 
	 * We have to create a "control message", and set that to 
	 * define the IPv6 packet information. We could set the
	 * source address if we wanted, but we can safely let the
	 * kernel decide what that should be. 
	 */
	m.msg_control = control_buf;
	m.msg_controllen = control_buf_len;
	cmsg = CMSG_FIRSTHDR(&m);
	cmsg->cmsg_level = IPPROTO_IPV6;
	cmsg->cmsg_type = IPV6_PKTINFO;
	cmsg->cmsg_len = CMSG_LEN(sizeof(*pktinfo));
	pktinfo = (struct in6_pktinfo *)CMSG_DATA(cmsg);
	memset(pktinfo, 0, sizeof(*pktinfo));
	pktinfo->ipi6_ifindex = if_nametoindex(interface->name);
	m.msg_controllen = cmsg->cmsg_len;

	result = sendmsg(interface->wfdesc, &m, 0);
	if (result < 0) {
		log_error("send_packet6: %m");
	}
	return result;
}
#endif /* DHCPv6 */

#ifdef USE_SOCKET_RECEIVE
ssize_t receive_packet (interface, buf, len, from, hfrom)
	struct interface_info *interface;
	unsigned char *buf;
	size_t len;
	struct sockaddr_in *from;
	struct hardware *hfrom;
{
#if !(defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && defined(USE_V4_PKTINFO))
	SOCKLEN_T flen = sizeof *from;
#endif
	int result;

	if ( interface->recv_mode == LISTEN_RAW ) 
		return receive_raw_packet(interface, buf, len, from, hfrom);

	/*
	 * The normal Berkeley socket interface doesn't give us any way
	 * to know what hardware interface we received the message on,
	 * but we should at least make sure the structure is emptied.
	 */
	memset(hfrom, 0, sizeof(*hfrom));

#ifdef IGNORE_HOSTUNREACH
	int retry = 0;
	do {
#endif

#if defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && defined(USE_V4_PKTINFO)
	struct msghdr m;
	struct iovec v;
	struct cmsghdr *cmsg;
	struct in_pktinfo *pktinfo;
	unsigned int ifindex;

	/*
	 * If necessary allocate space for the control message header.
	 * The space is common between send and receive.
	 */
	if (control_buf == NULL) {
		allocate_cmsg_cbuf();
		if (control_buf == NULL) {
			log_error("receive_packet: unable to allocate cmsg "
				  "header");
			return(ENOMEM);
		}
	}
	memset(control_buf, 0, control_buf_len);

	/*
	 * Initialize our message header structure.
	 */
	memset(&m, 0, sizeof(m));

	/*
	 * Point so we can get the from address.
	 */
	m.msg_name = from;
	m.msg_namelen = sizeof(*from);

	/*
	 * Set the data buffer we're receiving. (Using this wacky 
	 * "scatter-gather" stuff... but we that doesn't really make
	 * sense for us, so we use a single vector entry.)
	 */
	v.iov_base = buf;
	v.iov_len = len;
	m.msg_iov = &v;
	m.msg_iovlen = 1;

	/*
	 * Getting the interface is a bit more involved.
	 *
	 * We set up some space for a "control message". We have 
	 * previously asked the kernel to give us packet 
	 * information (when we initialized the interface), so we
	 * should get the interface index from that.
	 */
	m.msg_control = control_buf;
	m.msg_controllen = control_buf_len;

	result = recvmsg(interface->rfdesc, &m, 0);

	if (result >= 0) {
		/*
		 * If we did read successfully, then we need to loop
		 * through the control messages we received and 
		 * find the one with our inteface index.
		 */
		cmsg = CMSG_FIRSTHDR(&m);
		while (cmsg != NULL) {
			if ((cmsg->cmsg_level == IPPROTO_IP) && 
			    (cmsg->cmsg_type == IP_PKTINFO)) {
				pktinfo = (struct in_pktinfo *)CMSG_DATA(cmsg);
				ifindex = pktinfo->ipi_ifindex;
				/*
				 * We pass the ifindex back to the caller 
				 * using the unused hfrom parameter avoiding
				 * interface changes between sockets and 
				 * the discover code.
				 */
				memcpy(hfrom->hbuf, &ifindex, sizeof(ifindex));
				return (result);
			}
			cmsg = CMSG_NXTHDR(&m, cmsg);
		}

		/*
		 * We didn't find the necessary control message
		 * flag it as an error
		 */
		result = -1;
		errno = EIO;
	}
#else
		result = recvfrom(interface -> rfdesc, (char *)buf, len, 0,
				  (struct sockaddr *)from, &flen);
#endif /* IP_PKTINFO ... */
#ifdef IGNORE_HOSTUNREACH
	} while (result < 0 &&
		 (errno == EHOSTUNREACH ||
		  errno == ECONNREFUSED) &&
		 retry++ < 10);
#endif
	return (result);
}

/*  tlhhh 2010-12-17. use raw socket to receive packets for dhclient */
ssize_t receive_raw_packet (interface, buf, len, from, hfrom)
	struct interface_info *interface;
	unsigned char *buf;
	size_t len;
	struct sockaddr_in *from;
	struct hardware *hfrom;
{
	int length = 0;
	int offset = 0;
	unsigned char ibuf [1536];
	unsigned bufix = 0;
	unsigned paylen;

	length = read (interface -> rfdesc, ibuf, sizeof ibuf);
	if (length <= 0)
		return length;

	bufix = 0;

	/* Decode the physical header... */
	offset = decode_hw_header (interface, ibuf, bufix, hfrom);

	/* If the packet send by itself, then just drop it. */	
	if (0 == memcmp (interface -> hw_address.hbuf,
		     hfrom -> hbuf, hfrom -> hlen)) {
		log_info("The packet send by itself, then just drop it.\n");
		return 0;
	}

	/* If a physical layer checksum failed (dunno of any
	   physical layer that supports this, but WTH), skip this
	   packet. */
	if (offset < 0) {
		return 0;
	}

	bufix += offset;
	length -= offset;
	
	/* check if UDP packet? */

	/* Decode the IP and UDP headers... */
	offset = decode_udp_ip_header (interface, ibuf, bufix, from,
				       (unsigned)length, &paylen);

	/* If the IP or UDP checksum was bad, skip the packet... */
	if (offset < 0)
		return 0;

	bufix += offset;
	length -= offset;

	if (length < paylen)
		log_fatal("Internal inconsistency at %s:%d.", MDL);

	/* Copy out the data in the packet... */
	memcpy(buf, &ibuf[bufix], paylen);
	return paylen;
}

#endif /* USE_SOCKET_RECEIVE */

#ifdef DHCPv6
ssize_t 
receive_packet6(struct interface_info *interface, 
		unsigned char *buf, size_t len, 
		struct sockaddr_in6 *from, struct in6_addr *to_addr,
		unsigned int *if_idx)
{
	struct msghdr m;
	struct iovec v;
	int result;
	struct cmsghdr *cmsg;
	struct in6_pktinfo *pktinfo;

	/*
	 * If necessary allocate space for the control message header.
	 * The space is common between send and receive.
	 */
	if (control_buf == NULL) {
		allocate_cmsg_cbuf();
		if (control_buf == NULL) {
			log_error("receive_packet6: unable to allocate cmsg "
				  "header");
			return(ENOMEM);
		}
	}
	memset(control_buf, 0, control_buf_len);

	/*
	 * Initialize our message header structure.
	 */
	memset(&m, 0, sizeof(m));

	/*
	 * Point so we can get the from address.
	 */
	m.msg_name = from;
	m.msg_namelen = sizeof(*from);

	/*
	 * Set the data buffer we're receiving. (Using this wacky 
	 * "scatter-gather" stuff... but we that doesn't really make
	 * sense for us, so we use a single vector entry.)
	 */
	v.iov_base = buf;
	v.iov_len = len;
	m.msg_iov = &v;
	m.msg_iovlen = 1;

	/*
	 * Getting the interface is a bit more involved.
	 *
	 * We set up some space for a "control message". We have 
	 * previously asked the kernel to give us packet 
	 * information (when we initialized the interface), so we
	 * should get the destination address from that.
	 */
	m.msg_control = control_buf;
	m.msg_controllen = control_buf_len;

	result = recvmsg(interface->rfdesc, &m, 0);

	if (result >= 0) {
		/*
		 * If we did read successfully, then we need to loop
		 * through the control messages we received and 
		 * find the one with our destination address.
		 */
		cmsg = CMSG_FIRSTHDR(&m);
		while (cmsg != NULL) {
			if ((cmsg->cmsg_level == IPPROTO_IPV6) && 
			    (cmsg->cmsg_type == IPV6_PKTINFO)) {
				pktinfo = (struct in6_pktinfo *)CMSG_DATA(cmsg);
				*to_addr = pktinfo->ipi6_addr;
				*if_idx = pktinfo->ipi6_ifindex;

				return (result);
			}
			cmsg = CMSG_NXTHDR(&m, cmsg);
		}

		/*
		 * We didn't find the necessary control message
		 * flag is as an error
		 */
		result = -1;
		errno = EIO;
	}

	return (result);
}
#endif /* DHCPv6 */

#if defined (USE_SOCKET_FALLBACK)
/* This just reads in a packet and silently discards it. */

isc_result_t fallback_discard (object)
	omapi_object_t *object;
{
	char buf [1540];
	struct sockaddr_in from;
	SOCKLEN_T flen = sizeof from;
	int status;
	struct interface_info *interface;

	if (object -> type != dhcp_type_interface)
		return DHCP_R_INVALIDARG;
	interface = (struct interface_info *)object;

	status = recvfrom (interface -> wfdesc, buf, sizeof buf, 0,
			   (struct sockaddr *)&from, &flen);
#if defined (DEBUG)
	/* Only report fallback discard errors if we're debugging. */
	if (status < 0) {
		log_error ("fallback_discard: %m");
		return ISC_R_UNEXPECTED;
	}
#else
        /* ignore the fact that status value is never used */
        IGNORE_UNUSED(status);
#endif
	return ISC_R_SUCCESS;
}
#endif /* USE_SOCKET_FALLBACK */

#if defined (USE_SOCKET_RECEIVE)
int can_unicast_without_arp (ip)
	struct interface_info *ip;
{
	return 1;
}

int can_receive_unicast_unconfigured (ip)
	struct interface_info *ip;
{
#if 1 //defined (SOCKET_CAN_RECEIVE_UNICAST_UNCONFIGURED)
	return 1;
#else
	return 0;
#endif
}

int supports_multiple_interfaces (ip)
	struct interface_info *ip;
{
#if defined(SO_BINDTODEVICE) || \
	(defined(IP_PKTINFO) && defined(IP_RECVPKTINFO) && \
	 defined(USE_V4_PKTINFO))
	return(1);
#else
	return(0);
#endif
}

/* If we have SO_BINDTODEVICE, set up a fallback interface; otherwise,
   do not. */

void maybe_setup_fallback ()
{
#if defined (USE_SOCKET_FALLBACK)
	isc_result_t status;
	struct interface_info *fbi = (struct interface_info *)0;
	if (setup_fallback (&fbi, MDL)) {
		fbi -> wfdesc = if_register_socket (fbi, AF_INET, 0);
		fbi -> rfdesc = fbi -> wfdesc;
		log_info ("Sending on   Socket/%s%s%s",
		      fbi -> name,
		      (fbi -> shared_network ? "/" : ""),
		      (fbi -> shared_network ?
		       fbi -> shared_network -> name : ""));
	
		status = omapi_register_io_object ((omapi_object_t *)fbi,
						   if_readsocket, 0,
						   fallback_discard, 0, 0);
		if (status != ISC_R_SUCCESS)
			log_fatal ("Can't register I/O handle for %s: %s",
				   fbi -> name, isc_result_totext (status));
		interface_dereference (&fbi, MDL);
	}
#endif
}


#if defined(sun) && defined(USE_V4_PKTINFO)
/* This code assumes the existence of SIOCGLIFHWADDR */
void
get_hw_addr(const char *name, struct hardware *hw) {
	struct sockaddr_dl *dladdrp;
	int sock, i;
	struct lifreq lifr;

	memset(&lifr, 0, sizeof (lifr));
	(void) strlcpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/*
	 * Check if the interface is a virtual or IPMP interface - in those
	 * cases it has no hw address, so generate a random one.
	 */
	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ||
	    ioctl(sock, SIOCGLIFFLAGS, &lifr) < 0) {
		if (sock != -1)
			(void) close(sock);

#ifdef DHCPv6
		/*
		 * If approrpriate try this with an IPv6 socket
		 */
		if ((sock = socket(AF_INET6, SOCK_DGRAM, 0)) >= 0 &&
		    ioctl(sock, SIOCGLIFFLAGS, &lifr) >= 0) {
			goto flag_check;
		}
		if (sock != -1)
			(void) close(sock);
#endif
		log_fatal("Couldn't get interface flags for %s: %m", name);

	}

 flag_check:
	if (lifr.lifr_flags & (IFF_VIRTUAL|IFF_IPMP)) {
		hw->hlen = sizeof (hw->hbuf);
		srandom((long)gethrtime());

		hw->hbuf[0] = HTYPE_IPMP;
		for (i = 1; i < hw->hlen; ++i) {
			hw->hbuf[i] = random() % 256;
		}

		if (sock != -1)
			(void) close(sock);
		return;
	}

	if (ioctl(sock, SIOCGLIFHWADDR, &lifr) < 0)
		log_fatal("Couldn't get interface hardware address for %s: %m",
			  name);
	dladdrp = (struct sockaddr_dl *)&lifr.lifr_addr;
	hw->hlen = dladdrp->sdl_alen+1;
	switch (dladdrp->sdl_type) {
		case DL_CSMACD: /* IEEE 802.3 */
		case DL_ETHER:
			hw->hbuf[0] = HTYPE_ETHER;
			break;
		case DL_TPR:
			hw->hbuf[0] = HTYPE_IEEE802;
			break;
		case DL_FDDI:
			hw->hbuf[0] = HTYPE_FDDI;
			break;
		case DL_IB:
			hw->hbuf[0] = HTYPE_INFINIBAND;
			break;
		default:
			log_fatal("%s: unsupported DLPI MAC type %lu", name,
				  (unsigned long)dladdrp->sdl_type);
	}

	memcpy(hw->hbuf+1, LLADDR(dladdrp), hw->hlen-1);

	if (sock != -1)
		(void) close(sock);
}
#endif /* defined(sun) */

#endif /* USE_SOCKET_SEND */

int set_ipv4_addr(char *if_name, char *if4_addr, char *if4_mask, int add_or_del)
{
	int sockfd;
	struct ifreq ifr;
	struct sockaddr_in* addr = (struct sockaddr_in*)&ifr.ifr_addr;
	cprintf_10114_3("[%s][%s][%s][%d]\n", if_name, if4_addr, if4_mask, add_or_del);

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("socket");
		return -1;
	}

	strncpy(ifr.ifr_name, if_name, IFNAMSIZ);

	ifr.ifr_addr.sa_family = AF_INET;
	if(add_or_del == 1)
	{
		inet_pton(AF_INET, if4_addr, &addr->sin_addr);
		if(ioctl(sockfd, SIOCSIFADDR, &ifr) < 0)
		{
			perror("SIOCSIFADDR");
			close(sockfd);
			return -1;
		}

		inet_pton(AF_INET, if4_mask, &addr->sin_addr);
		if(ioctl(sockfd, SIOCSIFNETMASK, &ifr) < 0)
		{
			perror("SIOCSIFNETMASK");
			close(sockfd);
			return -1;
		}
	}
	else
	{
		inet_pton(AF_INET, "0.0.0.0", &addr->sin_addr);
		if(ioctl(sockfd, SIOCSIFADDR, &ifr) < 0)
		{
			perror("SIOCSIFADDR");
			close(sockfd);
			return -1;
		}
	}
	
#if 0
	ioctl(sockfd, SIOCGIFFLAGS, &ifr);
	ifr.ifr_flags |= (IFF_UP | IFF_RUNNING);
	ioctl(sockfd, SIOCSIFFLAGS, &ifr);
#endif

	close(sockfd);
	return 0;
}

/*
 *  IPv4 add/del route item in route table
 */
int inet_setroute(int action, char **args)
{
	struct rtentry route;  /* route item struct */
	char target[128] = {0};
	char gateway[128] = {0};
	char netmask[128] = {0};

	struct sockaddr_in *addr;

	int skfd;

	/* clear route struct by 0 */
	memset((char *)&route, 0x00, sizeof(route));

	/* default target is net (host)*/
	route.rt_flags = RTF_UP ;

	while(args)
	{
		if(*args == NULL)
		{
			break;
		}
		if(!strcmp(*args, "-net"))
		{/* default is a network target */
			args++;
			strcpy(target, *args);
			addr = (struct sockaddr_in*) &route.rt_dst;
			addr->sin_family = AF_INET;
			addr->sin_addr.s_addr = inet_addr(target);
			args++;
			continue;
		}
		else if(!strcmp(*args, "-host"))
		{/* target is a host */
			args++;
			strcpy(target, *args);
			addr = (struct sockaddr_in*) &route.rt_dst;
			addr->sin_family = AF_INET;
			addr->sin_addr.s_addr = inet_addr(target);
			route.rt_flags |= RTF_HOST;
			args++;
			continue;
		}

		if(!strcmp(*args, "netmask"))
		{/* netmask setting */
			args++;
			strcpy(netmask, *args);
			addr = (struct sockaddr_in*) &route.rt_genmask;
			addr->sin_family = AF_INET;
			addr->sin_addr.s_addr = inet_addr(netmask);
			args++;
			continue;
		}
		if(!strcmp(*args, "gw") || !strcmp(*args, "gateway"))
		{/* gateway setting */
			args++;
			strcpy(gateway, *args);
			addr = (struct sockaddr_in*) &route.rt_gateway;
			addr->sin_family = AF_INET;
			addr->sin_addr.s_addr = inet_addr(gateway);
			route.rt_flags |= RTF_GATEWAY;
			args++;
			continue;
		}
		if(!strcmp(*args, "device") || !strcmp(*args, "dev"))
		{/* device setting */
			args++;
			route.rt_dev = *args;
			args++;
			continue;
		}
		if(!strcmp(*args, "mtu"))
		{/* mtu setting */
			args++;
			route.rt_flags |= RTF_MTU;
			route.rt_mtu = atoi(*args);
			args++;
			continue;
		}
		/* if you have other options, please put them in this place,
		like the options above. */
		break;
	}

	/* create a socket */
	skfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(skfd < 0)
	{
		perror("socket");
		cprintf_10114_3("socket\n");
		return -1;
	}

	/* tell the kernel to accept this route */
	if(action == RTACTION_DEL)
	{/* del a route item */
		if(ioctl(skfd, SIOCDELRT, &route) < 0)
		{
			cprintf_10114_3("*SIOCDELRT[%d][%s]\n", errno, strerror(errno));
			perror("SIOCDELRT");
			close(skfd);
			return -1;
		}
	}
	else
	{/* add a route item */
		if(ioctl(skfd, SIOCADDRT, &route) < 0)
		{
			cprintf_10114_3("*SIOCADDRT[%d][%s]\n", errno, strerror(errno));
			perror("SIOCADDRT");
			close(skfd);
			return -1;
		}
	}
	(void) close(skfd);
	return 0;
}

char *get_netmask_string(int32_t masklen)
{
	uint32_t mask=0;
	int32_t i=0;
	struct in_addr ip_addr;

	if(masklen < 0 || masklen > 32)
	{
		return NULL;
	}

	for(i=0;i<masklen;i++)
		mask |= 0x80000000>>i;

	ip_addr.s_addr = htonl(mask);

	return inet_ntoa(ip_addr);
}

