/*
 * FileName:       unix_socket.c       
 * Author:         Zhijian
 * Date:           2012-12-10
 * Description:    Save dns cache for tcp mode.
 * History:        
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "unix_socket.h"

int create_unix_socket_server()
{
    struct sockaddr_un servaddr;
	int s;

	s = socket(AF_UNIX, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror("socket");
		return s;
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sun_family = AF_UNIX;
	strcpy(servaddr.sun_path, DNSMASQ_SERVER_PATH);
	unlink(servaddr.sun_path);

	if(bind(s, (struct sockaddr *)&servaddr, sizeof(servaddr))<0)
	{
		perror("bind");
		close(s);
		return -1;
	}

	return s;
}

int create_unix_socket_client()
{
    int s;
    struct sockaddr_un servaddr;

	s = socket(AF_UNIX, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror("socket");
		return s;
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sun_family = AF_UNIX;
	strcpy(servaddr.sun_path, DNSMASQ_SERVER_PATH);

	if(connect(s, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
	{
		close(s);
		perror("connect");
		return -1;
	}

	return s;
}

int create_site_unix_socket_client()
{
	int s;
	struct sockaddr_un servaddr;

	s = socket(AF_UNIX, SOCK_DGRAM, 0);
	if(s < 0)
	{
		perror("socket");
		return s;
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sun_family = AF_UNIX;
	strcpy(servaddr.sun_path, SITEBLOCK_SERVER_PATH);

	if(connect(s, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
	{		
		close(s);
		perror("connect");
		return -1;
	}

	return s;
}

int read_unix_socket(int s, unsigned char * buf, int len)
{
	if(buf == NULL || len == 0)
	{
		return 0;
	}
	
	if(s < 0 || len < 0)
	{
		return -1;
	}

	len = recv(s, buf, len, 0);
	if(len < 0)
	{
		perror("recv");
		return -1;
	}

	return len;
}


int write_unix_socket(int s, const unsigned char * buf, int len)
{
	int ret;
    
	if(buf == NULL || len == 0)
	{
		return 0;
	}
	
	if(s < 0 || len < 0)
	{
		return -1;
	}

	ret = send(s, buf, len, 0);
	
	if(ret != len)
	{	
		perror("send");
		return -1;
	}

	return len;
}


void close_unix_socket(int s)
{
	if(s >= 0)
	{
		close(s);
	}
}

int send_to_site_server_via_unix_socket(const unsigned char * buf, int len)
{
	int s;
	int ret = -1;
	fd_set fdset;
	struct timeval tm;
	
	if(buf == NULL || len == 0)
	{
		return 0;
	}
	
	if(len < 0)
	{
		return -1;
	}

	s = create_site_unix_socket_client();
	
	if(s < 0)
	{
		return -1;
	}

	FD_ZERO(&fdset);
	FD_SET(s, &fdset);
	tm.tv_sec = 0;
	tm.tv_usec = 10000;
	if(select(s + 1, (fd_set *) NULL, &fdset, (fd_set *) NULL, &tm) <= 0) 
	{
		close_unix_socket(s);
		return -1;	
	}
	ret = write_unix_socket(s, (const unsigned char *)buf, len);
	
	close_unix_socket(s);
	
	return ret;
}

int send_to_server_via_unix_socket(const unsigned char * buf, int len)
{
	int s;
	int ret = -1;
    
	if(buf == NULL || len == 0)
	{
		return 0;
	}
	
	if(len < 0)
	{
		return -1;
	}

	s = create_unix_socket_client();
	if(s < 0)
	{
		return -1;
	}
	
	ret = write_unix_socket(s, (const unsigned char *)buf, len);
	
	close_unix_socket(s);
	
	return ret;
}
