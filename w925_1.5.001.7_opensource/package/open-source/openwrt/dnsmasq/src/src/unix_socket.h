/*
 * FileName:       unix_socket.h       
 * Author:         Zhijian
 * Date:           2012-12-10
 * Description:    Save dns cache for tcp mode.
 * History:        
 */

#ifndef _UNIX_SOCKET_H
#define _UNIX_SOCKET_H


#define DNSMASQ_SERVER_PATH	"/tmp/dnsmasq.server"
#define DNSMASQ_CLIENT_PATH	"/tmp/dnsmasq.client"
#define SITEBLOCK_SERVER_PATH	"/tmp/siteblock.server"

int create_unix_socket_server();
int create_unix_socket_client();
int create_site_unix_socket_client();
int read_unix_socket(int s, unsigned char * buf, int len);
int write_unix_socket(int s, const unsigned char * buf, int len);
void close_unix_socket(int s);
int send_to_site_server_via_unix_socket(const unsigned char * buf, int len);
int send_to_server_via_unix_socket(const unsigned char * buf, int len);
#endif
