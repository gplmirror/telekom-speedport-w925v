/* dnsmasq is Copyright (c) 2000-2011 Simon Kelley

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 dated June, 1991, or
   (at your option) version 3 dated 29 June, 2007.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
     
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dnsmasq.h"

static struct frec *lookup_frec(unsigned short id, unsigned int crc, union mysockaddr *addr);
static struct frec *lookup_frec_by_sender(unsigned short id,
					  union mysockaddr *addr,
					  unsigned int crc);
static struct frec *lookup_frec_by_id(unsigned short id, char *name);
static unsigned short get_id(unsigned int crc, union mysockaddr *addr);
static void free_frec(struct frec *f);
static struct randfd *allocate_rfd(int family);

#ifdef SELECT_TO_QUERY
int select_delay[MAX_DELAY_TIMES] = {300000, 300000, 300000, 300000, 300000, 300000};	/* default 300ms */
int dns_query_timeout = 0; /* default 0 */
int dns_queue_timeout = 10000000; /* default 10 sec */
#define	FW_INIT		0
#define	FW_PENDING	1
#define	FW_WAITING	2
#define	FW_FINISH	3
#endif

#if 1
#define DT_MARK_LAN2WAN         0x0001U 
#define DT_MARK_RESERVE_CONN    0x0002U 
#else
#include <arc_global.h>
#endif	//FIXME---

int g_elapse = 0;
int g_calibrate_usec = 0;

/* Add a flag
	value 0, forward query to both stack(ipv4 and ipv6).
	value 1, forward query to only the same stack as the source stack(ipv4 or ipv6). */
int forward_to_single_stack_flag =1;
int ipv6_server_flag = 0;

/* Send a UDP packet with its source address set as "source" 
   unless nowild is true, when we just send it with the kernel default */
static void send_from(int fd, int nowild, char *packet, size_t len, 
		      union mysockaddr *to, struct all_addr *source,
		      unsigned int iface)
{
  struct msghdr msg;
  struct iovec iov[1]; 
  union {
    struct cmsghdr align; /* this ensures alignment */
#if defined(HAVE_LINUX_NETWORK)
    char control[CMSG_SPACE(sizeof(struct in_pktinfo))];
#elif defined(IP_SENDSRCADDR)
    char control[CMSG_SPACE(sizeof(struct in_addr))];
#endif
#ifdef HAVE_IPV6
    char control6[CMSG_SPACE(sizeof(struct in6_pktinfo))];
#endif
  } control_u;
  
  iov[0].iov_base = packet;
  iov[0].iov_len = len;

  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  msg.msg_flags = 0;
  msg.msg_name = to;
  msg.msg_namelen = sa_len(to);
  msg.msg_iov = iov;
  msg.msg_iovlen = 1;
  	
  if (!nowild)
    {
      struct cmsghdr *cmptr;
      msg.msg_control = &control_u;
      msg.msg_controllen = sizeof(control_u);
      cmptr = CMSG_FIRSTHDR(&msg);

      if (to->sa.sa_family == AF_INET)
	{
#if defined(HAVE_LINUX_NETWORK)
	  struct in_pktinfo p;
	  p.ipi_ifindex = 0;
	  p.ipi_spec_dst = source->addr.addr4;
	  memcpy(CMSG_DATA(cmptr), &p, sizeof(p));
	  msg.msg_controllen = cmptr->cmsg_len = CMSG_LEN(sizeof(struct in_pktinfo));
	  cmptr->cmsg_level = SOL_IP;
	  cmptr->cmsg_type = IP_PKTINFO;
#elif defined(IP_SENDSRCADDR)
	  memcpy(CMSG_DATA(cmptr), &(source->addr.addr4), sizeof(source->addr.addr4));
	  msg.msg_controllen = cmptr->cmsg_len = CMSG_LEN(sizeof(struct in_addr));
	  cmptr->cmsg_level = IPPROTO_IP;
	  cmptr->cmsg_type = IP_SENDSRCADDR;
#endif
	}
      else
#ifdef HAVE_IPV6
	{
	  struct in6_pktinfo p;
	  p.ipi6_ifindex = iface; /* Need iface for IPv6 to handle link-local addrs */
	  p.ipi6_addr = source->addr.addr6;
	  memcpy(CMSG_DATA(cmptr), &p, sizeof(p));
	  msg.msg_controllen = cmptr->cmsg_len = CMSG_LEN(sizeof(struct in6_pktinfo));
	  cmptr->cmsg_type = daemon->v6pktinfo;
	  cmptr->cmsg_level = IPV6_LEVEL;
	}
#else
      iface = 0; /* eliminate warning */
#endif
    }
  
 retry:
  if (sendmsg(fd, &msg, 0) == -1)
    {
      /* certain Linux kernels seem to object to setting the source address in the IPv6 stack
	 by returning EINVAL from sendmsg. In that case, try again without setting the
	 source address, since it will nearly alway be correct anyway.  IPv6 stinks. */
      if (errno == EINVAL && msg.msg_controllen)
	{
	  msg.msg_controllen = 0;
	  goto retry;
	}
      if (retry_send())
	goto retry;
    }
}
          
static unsigned int search_servers(time_t now, struct all_addr **addrpp, 
				     unsigned int qtype, char *qdomain, int *type, char **domain, int *norebind)
			      
{
  /* If the query ends in the domain in one of our servers, set
     domain to point to that name. We find the largest match to allow both
     domain.org and sub.domain.org to exist. */
  
  unsigned int namelen = strlen(qdomain);
  unsigned int matchlen = 0;
  struct server *serv;
  unsigned int flags = 0;
  
  for (serv = daemon->servers; serv; serv=serv->next)
    /* domain matches take priority over NODOTS matches */
    if ((serv->flags & SERV_FOR_NODOTS) && *type != SERV_HAS_DOMAIN && !strchr(qdomain, '.') && namelen != 0)
      {
	unsigned int sflag = serv->addr.sa.sa_family == AF_INET ? F_IPV4 : F_IPV6; 
	*type = SERV_FOR_NODOTS;
	if (serv->flags & SERV_NO_ADDR)
	  flags = F_NXDOMAIN;
	else if (serv->flags & SERV_LITERAL_ADDRESS) 
	  { 
	    if (sflag & qtype)
	      {
		flags = sflag;
		if (serv->addr.sa.sa_family == AF_INET) 
		  *addrpp = (struct all_addr *)&serv->addr.in.sin_addr;
#ifdef HAVE_IPV6
		else
		  *addrpp = (struct all_addr *)&serv->addr.in6.sin6_addr;
#endif 
	      }
	    else if (!flags || (flags & F_NXDOMAIN))
	      flags = F_NOERR;
	  } 
      }
    else if (serv->flags & SERV_HAS_DOMAIN)
      {
	unsigned int domainlen = strlen(serv->domain);
	char *matchstart = qdomain + namelen - domainlen;
	if (namelen >= domainlen &&
	    hostname_isequal(matchstart, serv->domain) &&
	    (domainlen == 0 || namelen == domainlen || *(matchstart-1) == '.' ))
	  {
	    if (serv->flags & SERV_NO_REBIND)	
	      *norebind = 1;
	    else
	      {
		unsigned int sflag = serv->addr.sa.sa_family == AF_INET ? F_IPV4 : F_IPV6;
		/* implement priority rules for --address and --server for same domain.
		   --address wins if the address is for the correct AF
		   --server wins otherwise. */
		if (domainlen != 0 && domainlen == matchlen)
		  {
		    if ((serv->flags & SERV_LITERAL_ADDRESS))
		      {
			if (!(sflag & qtype) && flags == 0)
			  continue;
		      }
		    else
		      {
			if (flags & (F_IPV4 | F_IPV6))
			  continue;
		      }
		  }
		
		if (domainlen >= matchlen)
		  {
		    *type = serv->flags & (SERV_HAS_DOMAIN | SERV_USE_RESOLV | SERV_NO_REBIND);
		    *domain = serv->domain;
		    matchlen = domainlen;
		    if (serv->flags & SERV_NO_ADDR)
		      flags = F_NXDOMAIN;
		    else if (serv->flags & SERV_LITERAL_ADDRESS)
		      {
			if (sflag & qtype)
			  {
			    flags = sflag;
			    if (serv->addr.sa.sa_family == AF_INET) 
			      *addrpp = (struct all_addr *)&serv->addr.in.sin_addr;
#ifdef HAVE_IPV6
			    else
			      *addrpp = (struct all_addr *)&serv->addr.in6.sin6_addr;
#endif
			  }
			else if (!flags || (flags & F_NXDOMAIN))
			  flags = F_NOERR;
		      }
		    else
		      flags = 0;
		  } 
	      }
	  }
      }
  
  if (flags == 0 && !(qtype & F_QUERY) && 
      option_bool(OPT_NODOTS_LOCAL) && !strchr(qdomain, '.') && namelen != 0)
    /* don't forward A or AAAA queries for simple names, except the empty name */
    flags = F_NOERR;
  
  if (flags == F_NXDOMAIN && check_for_local_domain(qdomain, now))
    flags = F_NOERR;

  if (flags)
    {
      int logflags = 0;
      
      if (flags == F_NXDOMAIN || flags == F_NOERR)
	logflags = F_NEG | qtype;
  
      log_query(logflags | flags | F_CONFIG | F_FORWARD, qdomain, *addrpp, NULL);
    }
  else if ((*type) & SERV_USE_RESOLV)
    {
      *type = 0; /* use normal servers for this domain */
      *domain = NULL;
    }
  return  flags;
}

/* 
 *
 * Check if is IPv6 server
 */
int is_ipv6_dns(struct server *s)
{
#ifdef HAVE_IPV6
	if (s == NULL)
		return 0;	//return ipv6_server_flag;

	if (ipv6_server_flag && s->addr.sa.sa_family == AF_INET6)
		return 1;
#endif

	return 0;
}

/* 
 * print server IPv4/6 address
 */
void print_server(struct server *s, const char *func, const int line)
{
	char dbg_buf[128];

	if (s == NULL) {
		return;
	}

	memset(dbg_buf, 0, sizeof(dbg_buf));

#ifdef HAVE_IPV6
	if (s->addr.sa.sa_family == AF_INET6)
	{
		my_syslog(LOG_INFO, _("[%s/%d]: ==> IPv6 server:[%s]\n"), __FUNCTION__, __LINE__, inet_ntop(AF_INET6, &(s->addr.in6.sin6_addr), dbg_buf, sizeof(dbg_buf)));
	}
	else
#endif
	{
		my_syslog(LOG_INFO, _("[%s/%d]: ==> IPv4 server:[%s]\n"), __FUNCTION__, __LINE__,inet_ntoa(s->addr.in.sin_addr));
	}

	return;
}
/* end */


static int forward_query(int udpfd, union mysockaddr *udpaddr,
			 struct all_addr *dst_addr, unsigned int dst_iface,
			 struct dns_header *header, size_t plen, time_t now, struct frec *forward)
{
  char *domain = NULL;
  int type = 0, norebind = 0;
  struct all_addr *addrp = NULL;
  unsigned int crc = questions_crc(header, plen, daemon->namebuff);
  unsigned int flags = 0;
  unsigned int gotname = extract_request(header, plen, daemon->namebuff, NULL);
  struct server *start = NULL, *next = NULL;
#ifdef SELECT_TO_QUERY
  int sentcnt = 0;
#endif
  int query_from_lan = 0;
  unsigned int dt_mark = 0;  
  char buf[64];
  char name[PACKETSZ];
  int name_len = 0;
  unsigned char *p, *q;  
  unsigned short get_new_id = 0; 

  memset(name, 0, sizeof(name));

  if(udpaddr && udpaddr->sa.sa_family == AF_INET)
  {
  	query_from_lan = is_same_subnet_with_lan(udpaddr->in.sin_addr);
  }
  else if(udpaddr && udpaddr->sa.sa_family == AF_INET6)
  {	 
	query_from_lan = 1;
  }

  if(1 == query_from_lan)	//trigger PPP up if DNS from LAN and PPP is not up
  {

		if(access("/tmp/wan_up", R_OK) == -1)
		{
			if(access("/tmp/link_up", R_OK) == 0)
			{	
				cprintf(":%d: DNS query from LAN, bring up PPP\n", __LINE__);
				system("mapi_wan_cli trigger_start_wan");
			}
		}

  }

  /* RFC 4035: sect 4.6 para 2 */
  if (!option_bool(OPT_DNSSEC))
    header->hb4 &= ~HB4_AD;

  p = (unsigned char *)(header+1);
  encode_query_name(header, plen, &p, 4, name, &name_len);
	  
  /* may be no servers available. */
#if 0
  if (!daemon->servers)
  {
    forward = NULL;
    flags = F_NEG;
  }
  else
#endif
  if (forward || (forward = lookup_frec_by_id(ntohs(header->id), name)))
  {    	
#ifdef SELECT_TO_QUERY
	/* Update the start_pending_time for the packets in state FW_PENDING. */
	if (forward->state == FW_PENDING) 
		gettimeofday(&forward->start_pending_time, NULL);	

	if (forward->state == FW_WAITING ||forward->state == FW_PENDING || forward->state == FW_FINISH)
	{				
		check_forward_list();
		return 1;
	}
#else
	/* retry on existing query, send to all available servers  */
	domain = forward->sentto->domain;
	forward->sentto->failed_queries++;
	if (!option_bool(OPT_ORDER))
	{
		forward->forwardall = 1;
		daemon->last_server = NULL;
	}
	type = forward->sentto->flags & SERV_TYPE;
	if (!(start = forward->sentto->next))
		start = daemon->servers; /* at end of list, recycle */
#endif
  }
  else 
  {    
      if (gotname)
	flags = search_servers(now, &addrp, gotname, daemon->namebuff, &type, &domain, &norebind);
      
      if (!flags && !(forward = get_new_frec(now, NULL)))
	/* table full - server failure. */
	flags = F_NEG;
      
      if (forward)
	{
	  forward->state = FW_INIT;
	  forward->source = *udpaddr;
	  forward->dest = *dst_addr;
	  forward->iface = dst_iface;
	  forward->orig_id = ntohs(header->id);
	  forward->send_server_num = 1;
	  forward->fd = udpfd;
	  forward->crc = crc;
	  forward->forwardall = 0;
	  /* sanity check to avoid the malformed packet crashing our program */
	  if (plen >= sizeof(forward->packet))
	  	plen = sizeof(forward->packet)-1;

	  memcpy(forward->packet, (char *)header, plen);
	  forward->packet_len = plen;		
	 
	  /* sanity check to avoid the malformed packet crashing our program */
	  if (name_len >= sizeof(forward->original_name))
		  name_len = sizeof(forward->original_name)-1;

	  memcpy(forward->original_name, name, name_len);
	  forward->original_name_len = name_len;
	  
	  if (norebind)
	    forward->flags |= FREC_NOREBIND;
	  if (header->hb4 & HB4_CD)
	    forward->flags |= FREC_CHECKING_DISABLED;

	  //header->id = htons(forward->new_id);
	   
	  /* In strict_order mode, always try servers in the order 
	     specified in resolv.conf, if a domain is given 
	     always try all the available servers,
	     otherwise, use the one last known to work. */
	  
	  if (type == 0)
	    {
	      if (option_bool(OPT_ORDER))
		start = daemon->servers;
	      else if (!(start = daemon->last_server) ||
		       daemon->forwardcount++ > FORWARD_TEST ||
		       difftime(now, daemon->forwardtime) > FORWARD_TIME)
		{
		  start = daemon->servers;
		  forward->forwardall = 1;
		  daemon->forwardcount = 0;
		  daemon->forwardtime = now;
		}
	    }
	  else
	    {
	      start = daemon->servers;
	      if (!option_bool(OPT_ORDER))
		forward->forwardall = 1;
	    }
	}
  }

  for (start = daemon->servers; start; start = start->next)
  {
	if(serv_is_zero_addr(start))
		continue;
	else
		break;
  }

  /* check for send errors here (no route to host) 
     if we fail to send to all nameservers, send back an error
     packet straight away (helps modem users when offline)  */
     
  if (!start)
    {	    
		gettimeofday(&forward->start_pending_time, NULL);	
		forward->state = FW_PENDING;
    }
  else if (!flags && forward)
    {
      struct server *firstsentto = start;
      int forwarded = 0;
      
      if (udpaddr && option_bool(OPT_ADD_MAC))
	plen = add_mac(header, plen, ((char *) header) + PACKETSZ, udpaddr);

      while (1)
	{ 

	  /* only send to servers dealing with our domain.
	     domain may be NULL, in which case server->domain 
	     must be NULL also. */
	  if (type == (start->flags & SERV_TYPE) &&
	      (type != SERV_HAS_DOMAIN || hostname_isequal(domain, start->domain)) &&
	      !(start->flags & SERV_LITERAL_ADDRESS) &&
	      !serv_is_zero_addr(start))
	    {
	      int fd;

	      /* find server socket to use, may need to get random one. */
	      if (start->sfd) {
			  fd = start->sfd->fd;
		  }
	      else 
		{			
#ifdef HAVE_IPV6
			if (start->addr.in6.sin6_family == AF_INET6)
			{	
				/* if (!forward->rfd6 &&
				!(forward->rfd6 = allocate_rfd(AF_INET6)))
					break;
				*/
				if ( !(forward->rfd6 = allocate_rfd(AF_INET6)))
					break;
				daemon->rfd_save = forward->rfd6;
				fd = forward->rfd6->fd;
				save_fd_to_frec(forward, fd);	  
			}
			else
#endif
			{	
				/* if (!forward->rfd4 &&
				!(forward->rfd4 = allocate_rfd(AF_INET)))
				break;
				*/
				if ( !(forward->rfd4 = allocate_rfd(AF_INET)))
				break;

				daemon->rfd_save = forward->rfd4;
				fd = forward->rfd4->fd;			  
				save_fd_to_frec(forward, fd);	  
			}

#ifdef HAVE_CONNTRACK
		  /* Copy connection mark of incoming query to outgoing connection. */
		  if (option_bool(OPT_CONNTRACK))
		    {
		      unsigned int mark;
		      if (get_incoming_mark(udpaddr, dst_addr, 0, &mark))
			setsockopt(fd, SOL_SOCKET, SO_MARK, &mark, sizeof(unsigned int));
		    }
#endif
		}		

		if(query_from_lan)
		{			
			dt_mark = DT_MARK_LAN2WAN;			
			setsockopt(fd, SOL_SOCKET, SO_MARK, &dt_mark, sizeof(unsigned int));
		}
		else
		{			
			dt_mark = DT_MARK_RESERVE_CONN;			
			setsockopt(fd, SOL_SOCKET, SO_MARK, &dt_mark, sizeof(unsigned int));
		}
	      get_new_id = get_id(forward->crc, &(start->addr));		  
	      add_new_id_to_frec(forward, get_new_id, &(start->addr));
	      header->id = htons(get_new_id);
	      p = (unsigned char *)(header+1);
	      encode_query_name(header, plen, &p, 4, name, &name_len);
		  
	      if (sendto(fd, (char *)header, plen, 0,
			 &start->addr.sa,
			 sa_len(&start->addr)) == -1)
		{
			//cprintf("warning: [%s]: sendto failed: [%d][%s], ignore..\n", __func__, errno, strerror(errno));
			my_syslog(LOG_INFO, _("warning: [%s]: sendto failed: [%d][%s], ignore..\n"), __func__, errno, strerror(errno));
			if (retry_send())
				continue;
			else {
			}
		}

#ifdef SELECT_TO_QUERY
	      gettimeofday(&forward->last_querytime, NULL);
	      gettimeofday(&forward->first_querytime, NULL);
#endif

		  /* 
		   * If sendto failed, just remember this server and ignore the failure.
		   *
		   * Go through and do DT's retry in check_forward_list.
		   */
		{
		  /* Keep info in case we want to re-send this packet */
		  //If we keep info here, nl_routechange() in netlink.c will try to resend this DNS query
		  #if 0 //org
		  daemon->srv_save = start;
		  daemon->packet_len = plen;
		  #else
		  daemon->srv_save = NULL;
		  daemon->packet_len = 0;
		  #endif
		  
		  if (!gotname)
		    strcpy(daemon->namebuff, "query");
		  if (start->addr.sa.sa_family == AF_INET)
		    log_query(F_SERVER | F_IPV4 | F_FORWARD, daemon->namebuff, 
			      (struct all_addr *)&start->addr.in.sin_addr, NULL); 
#ifdef HAVE_IPV6
		  else
		    log_query(F_SERVER | F_IPV6 | F_FORWARD, daemon->namebuff, 
			      (struct all_addr *)&start->addr.in6.sin6_addr, NULL);
#endif 
		  start->queries++;
		  forwarded = 1;
		  forward->sentto = start;

#ifdef SELECT_TO_QUERY
		  forward->idx = 0;
		  forward->present_srv = start;
		  forward->last_srv = start;
		  memcpy(&(forward->last_srv_addr) , &(start->addr), sizeof(union mysockaddr)); 
		  forward->state = FW_WAITING;
		  sentcnt++;
		  /*break;*/
#endif
#ifndef SELECT_TO_QUERY
		  if (!forward->forwardall) 
		    break;
#endif
		  forward->forwardall++;
		}

	    }
	  
		update_random_seed();

		if(serv_is_zero_addr(start))			
			sentcnt++;
		
		/* 
		 * IPv4: Since we have tried the 1st server, update server to try next. 
		 *		 If next server also fail, go the next ipv4 _couple_;
		 * IPv6: After trying the 1st server, waiting. But do _not_ update *start.
		 */
#ifdef SELECT_TO_QUERY
		if(forward->send_server_num == 1)
		{
			my_syslog(LOG_INFO, _("++++%s line %d, forward->send_server_num is 1.\n"), __FUNCTION__, __LINE__);
			forward->send_server_num = 2;
			if(start->flags & SERV_DT_FLAG )
			{
				if (!(start = start->next)) 
				{
					forward->last_srv = NULL;
					memset(&(forward->last_srv_addr), 0, sizeof(union mysockaddr));
					break;
				}
			}
			else
				break;
		}
		else
		{
			my_syslog(LOG_INFO, _("++++%s line %d, forward->send_server_num is 2.\n"), __FUNCTION__, __LINE__);			
			forward->send_server_num = 1;
			for (next = start->next; next; next = next->next)
			{
				if(serv_is_zero_addr(next))
					continue;
				else
					break;
			}
			forward->last_srv = next;
			if(forward->last_srv)
				memcpy(&(forward->last_srv_addr) , &(next->addr), sizeof(union mysockaddr)); 
			else					
				memset(&(forward->last_srv_addr), 0, sizeof(union mysockaddr));

			if (sentcnt == 2)
			{
				break;
			}
		}
#else
		if (!(start = start->next)) {
			start = daemon->servers;
		}
#endif	
		if (start == firstsentto)
			break;
	}
      
	if (forwarded) 
	{
		return 1;
	}
      
#ifdef SELECT_TO_QUERY
	  /* linghong_tan 2012.11.30
	   * Will free this forward entry while:
	   * - All servers failed, send server fail and free in check_forward_list.
	   * - Received the server's response, and reply to client then free.
	   */
#else
      /* could not send on, prepare to return */ 
      header->id = htons(forward->orig_id);
      free_frec(forward); /* cancel */
#endif

    }	  
#ifdef SELECT_TO_QUERY
  /* linghong_tan 2012.11.30
   * Send server_fail instead of sending server_reject 
   */
#else
  /* could not send on, return empty answer or address if known for whole domain */
  if (udpfd != -1)
    {
		printf("%s: send reject to client\n", __func__);
      plen = setup_reply(header, plen, addrp, flags, daemon->local_ttl);
      send_from(udpfd, option_bool(OPT_NOWILD), (char *)header, plen, udpaddr, dst_addr, dst_iface);
    }
#endif

  return 0;
}

static size_t process_reply(struct dns_header *header, int packet_len, time_t now, 
			    struct server *server, size_t n, int check_rebind, int checking_disabled, char *original_name, int name_len, int is_child)
{
  unsigned char *pheader, *sizep;
  int munged = 0, is_sign;
  size_t plen, m;
  unsigned char *p;

  /* If upstream is advertising a larger UDP packet size
     than we allow, trim it so that we don't get overlarge
     requests for the client. We can't do this for signed packets. */

  if ((pheader = find_pseudoheader(header, n, &plen, &sizep, &is_sign)) && !is_sign)
    {
      unsigned short udpsz;
      unsigned char *psave = sizep;
      
      GETSHORT(udpsz, sizep);
      if (udpsz > daemon->edns_pktsz)
	PUTSHORT(daemon->edns_pktsz, psave);
    }

  /* RFC 4035 sect 4.6 para 3 */
  if (!is_sign && !option_bool(OPT_DNSSEC))
     header->hb4 &= ~HB4_AD;
  
   p = (unsigned char *)(header+1);
   restore_original_name(header, n, &p, 4,original_name, name_len);

  if (OPCODE(header) != QUERY || (RCODE(header) != NOERROR && RCODE(header) != NXDOMAIN))
    return n;
  
  /* Complain loudly if the upstream server is non-recursive. */
  if (!(header->hb4 & HB4_RA) && RCODE(header) == NOERROR && ntohs(header->ancount) == 0 &&
      server && !(server->flags & SERV_WARNED_RECURSIVE))
    {
      prettyprint_addr(&server->addr, daemon->namebuff);
      my_syslog(LOG_WARNING, _("nameserver %s refused to do a recursive query"), daemon->namebuff);
      if (!option_bool(OPT_LOG))
	server->flags |= SERV_WARNED_RECURSIVE;
    }
  
  if (daemon->bogus_addr && RCODE(header) != NXDOMAIN &&
      check_for_bogus_wildcard(header, n, daemon->namebuff, daemon->bogus_addr, now))
    {
      munged = 1;
      SET_RCODE(header, NXDOMAIN);
      header->hb3 &= ~HB3_AA;
    }
  else 
    {
      if (RCODE(header) == NXDOMAIN && 
	  extract_request(header, n, daemon->namebuff, NULL) &&
	  check_for_local_domain(daemon->namebuff, now))
	{
	  /* if we forwarded a query for a locally known name (because it was for 
	     an unknown type) and the answer is NXDOMAIN, convert that to NODATA,
	     since we know that the domain exists, even if upstream doesn't */
	  munged = 1;
	  header->hb3 |= HB3_AA;
	  SET_RCODE(header, NOERROR);
	}
      
      if (extract_addresses(header, n, daemon->namebuff, now, is_sign, check_rebind, checking_disabled))
	{
	  my_syslog(LOG_WARNING, _("possible DNS-rebind attack detected: %s"), daemon->namebuff);
	  munged = 1;
	}
    }
  
  /* do this after extract_addresses. Ensure NODATA reply and remove
     nameserver info. */
  
  if (munged)
    {
      header->ancount = htons(0);
      header->nscount = htons(0);
      header->arcount = htons(0);
    }
  
  /* the bogus-nxdomain stuff, doctor and NXDOMAIN->NODATA munging can all elide
     sections of the packet. Find the new length here and put back pseudoheader
     if it was removed. */
  //return resize_packet(header, n, pheader, plen);
  m = resize_packet(header, n, pheader, plen);

	if(is_child)
	{
		//try to send parameter of extract_addresses to parent process
		unsigned char buf[MAX_RX_BUF];
		buf_info_p binfo;
		addresses_info_p ainfo;
		int len;
		int offset;
		int i;

		len = BUF_INFO_SZ + ADDRESSES_INFO_SZ + m;
		if(len <= sizeof(buf))
		{
			binfo = (buf_info_p)buf;
			binfo->next_pid = 0;
			binfo->seq = 0;
			binfo->have_more = 0;
			ainfo = (addresses_info_p)binfo->buf;
			ainfo->qlen = n;
			memcpy(ainfo->name, daemon->namebuff, MAXDNAME);
			ainfo->now = now, 
			ainfo->is_sign = is_sign;
			ainfo->check_rebind = check_rebind;
			ainfo->checking_disabled = checking_disabled;
			memcpy(ainfo->header, header, m);
			send_to_server_via_unix_socket(buf, len);
		}		
	}

  return m;
}

/* sets new last_server */
void reply_query(int fd, int family, time_t now)
{
  /* packet from peer server, extract data for cache, and send to
     original requester */
  struct dns_header *header;
  union mysockaddr serveraddr;
  struct frec *forward;
  socklen_t addrlen = sizeof(serveraddr);
  ssize_t n = recvfrom(fd, daemon->packet, daemon->edns_pktsz, 0, &serveraddr.sa, &addrlen);
  size_t nn;
  struct server *server;
  unsigned char *p, *q;
  
  /* packet buffer overwritten */
  daemon->srv_save = NULL;
  
  /* Determine the address of the server replying  so that we can mark that as good */
  serveraddr.sa.sa_family = family;
#ifdef HAVE_IPV6
  if (serveraddr.sa.sa_family == AF_INET6)
    serveraddr.in6.sin6_flowinfo = 0;
#endif
  
  /* spoof check: answer must come from known server, */
  for (server = daemon->servers; server; server = server->next)
  {    
 	 if(serv_is_zero_addr(server))
	 	continue;
    if (!(server->flags & (SERV_LITERAL_ADDRESS | SERV_NO_ADDR)) &&
	sockaddr_isequal(&server->addr, &serveraddr))
      break;
  }
   
  header = (struct dns_header *)daemon->packet;
  
  if (!server ||
      n < (int)sizeof(struct dns_header) || !(header->hb3 & HB3_QR) ||
      !(forward = lookup_frec(ntohs(header->id), questions_crc(header, n, daemon->namebuff) , &serveraddr))
      )
    return;

  if(forward->state == FW_FINISH)
    return;

  server = forward->sentto;
#if 0
  if ((RCODE(header) == SERVFAIL || RCODE(header) == REFUSED || RCODE(header) == NOTIMP) &&
      !option_bool(OPT_ORDER) &&
      forward->forwardall == 0)
    /* for broken servers, attempt to send to another one. */
    {
      unsigned char *pheader;
      size_t plen;
      int is_sign;
      
      /* recreate query from reply */
      pheader = find_pseudoheader(header, (size_t)n, &plen, NULL, &is_sign);
      if (!is_sign)
	{
	  header->ancount = htons(0);
	  header->nscount = htons(0);
	  header->arcount = htons(0);
	  if ((nn = resize_packet(header, (size_t)n, pheader, plen)))
	    {
	      header->hb3 &= ~(HB3_QR | HB3_TC);
	      forward_query(-1, NULL, NULL, 0, header, nn, now, forward);
	      return;
	    }
	}
    }   
#endif
  if ((forward->sentto->flags & SERV_TYPE) == 0)
    {
      if (RCODE(header) == SERVFAIL || RCODE(header) == REFUSED || RCODE(header) == NOTIMP)
	server = NULL;
      else
	{
	  struct server *last_server;
	  
	  /* find good server by address if possible, otherwise assume the last one we sent to */ 
	  for (last_server = daemon->servers; last_server; last_server = last_server->next)
	    if (!(last_server->flags & (SERV_LITERAL_ADDRESS | SERV_HAS_DOMAIN | SERV_FOR_NODOTS | SERV_NO_ADDR)) &&
		sockaddr_isequal(&last_server->addr, &serveraddr))
	      {
		server = last_server;
		break;
	      }
	} 
      if (!option_bool(OPT_ALL_SERVERS))
	daemon->last_server = server;
    }
#if 0
  /* If the answer is an error, keep the forward record in place in case
     we get a good reply from another server. Kill it when we've
     had replies from all to avoid filling the forwarding table when
     everything is broken */
  if (forward->forwardall == 0 || --forward->forwardall == 1 || 
      (RCODE(header) != REFUSED && RCODE(header) != SERVFAIL && RCODE(header) != NOTIMP))
#endif
    {
      int check_rebind = !(forward->flags & FREC_NOREBIND);

      if (!option_bool(OPT_NO_REBIND))
	check_rebind = 0;
      
      /*Zhijian on 2012-12-10 16:15 Save dns cache for tcp mode.*/
      if ((nn = process_reply(header, daemon->packet_buff_sz, now, server, (size_t)n, check_rebind, forward->flags & FREC_CHECKING_DISABLED, forward->original_name, forward->original_name_len, 0)))
	{
	
	  header->id = htons(forward->orig_id);
	  header->hb4 |= HB4_RA; /* recursion if available */
	  send_from(forward->fd, option_bool(OPT_NOWILD), daemon->packet, nn, 
		    &forward->source, &forward->dest, forward->iface);
	}
      //free_frec(forward); /* cancel */
      forward->state = FW_FINISH;
    }
}


void receive_query(struct listener *listen, time_t now)
{
  struct dns_header *header = (struct dns_header *)daemon->packet;
  union mysockaddr source_addr;
  unsigned short type;
  struct all_addr dst_addr;
  struct in_addr netmask, dst_addr_4;
  size_t m;
  ssize_t n;
  int if_index = 0;
  struct iovec iov[1];
  struct msghdr msg;
  struct cmsghdr *cmptr;
  union {
    struct cmsghdr align; /* this ensures alignment */
#ifdef HAVE_IPV6
    char control6[CMSG_SPACE(sizeof(struct in6_pktinfo))];
#endif
#if defined(HAVE_LINUX_NETWORK)
    char control[CMSG_SPACE(sizeof(struct in_pktinfo))];
#elif defined(IP_RECVDSTADDR) && defined(HAVE_SOLARIS_NETWORK)
    char control[CMSG_SPACE(sizeof(struct in_addr)) +
		 CMSG_SPACE(sizeof(unsigned int))];
#elif defined(IP_RECVDSTADDR)
    char control[CMSG_SPACE(sizeof(struct in_addr)) +
		 CMSG_SPACE(sizeof(struct sockaddr_dl))];
#endif
  } control_u;
  char name[PACKETSZ];
  int name_len = 0;
  unsigned char *p, *q;

  memset(name, 0, sizeof(name));

  /* packet buffer overwritten */
  daemon->srv_save = NULL;
  
  if (listen->family == AF_INET && option_bool(OPT_NOWILD))
    {
      dst_addr_4 = listen->iface->addr.in.sin_addr;
      netmask = listen->iface->netmask;
    }
  else
    {
      dst_addr_4.s_addr = 0;
      netmask.s_addr = 0;
    }

  iov[0].iov_base = daemon->packet;
  iov[0].iov_len = daemon->edns_pktsz;
    
  msg.msg_control = control_u.control;
  msg.msg_controllen = sizeof(control_u);
  msg.msg_flags = 0;
  msg.msg_name = &source_addr;
  msg.msg_namelen = sizeof(source_addr);
  msg.msg_iov = iov;
  msg.msg_iovlen = 1;
  
  if ((n = recvmsg(listen->fd, &msg, 0)) == -1)
    return;
  
  if (n < (int)sizeof(struct dns_header) || 
      (msg.msg_flags & MSG_TRUNC) ||
      (header->hb3 & HB3_QR))
    return;
  
  source_addr.sa.sa_family = listen->family;
#ifdef HAVE_IPV6
  if (listen->family == AF_INET6)
    source_addr.in6.sin6_flowinfo = 0;
#endif

  if (!option_bool(OPT_NOWILD))
    {
      struct ifreq ifr;

      if (msg.msg_controllen < sizeof(struct cmsghdr))
	return;

#if defined(HAVE_LINUX_NETWORK)
      if (listen->family == AF_INET)
	for (cmptr = CMSG_FIRSTHDR(&msg); cmptr; cmptr = CMSG_NXTHDR(&msg, cmptr))
	  if (cmptr->cmsg_level == SOL_IP && cmptr->cmsg_type == IP_PKTINFO)
	    {
	      union {
		unsigned char *c;
		struct in_pktinfo *p;
	      } p;
	      p.c = CMSG_DATA(cmptr);
	      dst_addr_4 = dst_addr.addr.addr4 = p.p->ipi_spec_dst;
	      if_index = p.p->ipi_ifindex;
	    }
#elif defined(IP_RECVDSTADDR) && defined(IP_RECVIF)
      if (listen->family == AF_INET)
	{
	  for (cmptr = CMSG_FIRSTHDR(&msg); cmptr; cmptr = CMSG_NXTHDR(&msg, cmptr))
	    {
	      union {
		unsigned char *c;
		unsigned int *i;
		struct in_addr *a;
#ifndef HAVE_SOLARIS_NETWORK
		struct sockaddr_dl *s;
#endif
	      } p;
	       p.c = CMSG_DATA(cmptr);
	       if (cmptr->cmsg_level == IPPROTO_IP && cmptr->cmsg_type == IP_RECVDSTADDR)
		 dst_addr_4 = dst_addr.addr.addr4 = *(p.a);
	       else if (cmptr->cmsg_level == IPPROTO_IP && cmptr->cmsg_type == IP_RECVIF)
#ifdef HAVE_SOLARIS_NETWORK
		 if_index = *(p.i);
#else
  	         if_index = p.s->sdl_index;
#endif
	    }
	}
#endif
      
#ifdef HAVE_IPV6
      if (listen->family == AF_INET6)
	{
	  for (cmptr = CMSG_FIRSTHDR(&msg); cmptr; cmptr = CMSG_NXTHDR(&msg, cmptr))
	    if (cmptr->cmsg_level == IPV6_LEVEL && cmptr->cmsg_type == daemon->v6pktinfo)
	      {
		union {
		  unsigned char *c;
		  struct in6_pktinfo *p;
		} p;
		p.c = CMSG_DATA(cmptr);
		  
		dst_addr.addr.addr6 = p.p->ipi6_addr;
		if_index = p.p->ipi6_ifindex;
	      }
	}
#endif
      
      /* enforce available interface configuration */
      
      if (!indextoname(listen->fd, if_index, ifr.ifr_name) ||
	  !iface_check(listen->family, &dst_addr, ifr.ifr_name, &if_index))
	return;
      
      if (listen->family == AF_INET &&
	  option_bool(OPT_LOCALISE) &&
	  ioctl(listen->fd, SIOCGIFNETMASK, &ifr) == -1)
	return;
      
      netmask = ((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr;
    }
  
  if(g_sys_http_redirect)
  {
    p = (unsigned char *)(header+1);

    if (*(p+1) & 0x80)
    {
      *(p+1) &= 0x7F;
      header->hb4 |= HB4_AD;
    }
  }

  if (extract_request(header, (size_t)n, daemon->namebuff, &type))
    {
      char types[20];

      querystr(types, type);

      if (listen->family == AF_INET) 
	log_query(F_QUERY | F_IPV4 | F_FORWARD, daemon->namebuff, 
		  (struct all_addr *)&source_addr.in.sin_addr, types);
#ifdef HAVE_IPV6
      else
	log_query(F_QUERY | F_IPV6 | F_FORWARD, daemon->namebuff, 
		  (struct all_addr *)&source_addr.in6.sin6_addr, types);
#endif
    }  
	p = (unsigned char *)(header+1);
	get_original_query_name(header, n, &p, 4, name, &name_len);	
	
	m = answer_request (header, ((char *) header) + PACKETSZ, (size_t)n, 
		      dst_addr_4, netmask, now);   
  if (m >= 1)
    {   
      p = (unsigned char *)(header+1);
      restore_original_name(header, m, &p, 4,name, name_len);
		
      send_from(listen->fd, option_bool(OPT_NOWILD), (char *)header, 
		m, &source_addr, &dst_addr, if_index);
      daemon->local_answer++;
    }
  else if (forward_query(listen->fd, &source_addr, &dst_addr, if_index,
			 header, (size_t)n, now, NULL))
    daemon->queries_forwarded++;
  else
    daemon->local_answer++;
}

/* The daemon forks before calling this: it should deal with one connection,
   blocking as neccessary, and then return. Note, need to be a bit careful
   about resources for debug mode, when the fork is suppressed: that's
   done by the caller. */
unsigned char *tcp_request(int confd, time_t now,
			   union mysockaddr *local_addr, struct in_addr netmask, int is_child)
{
  size_t size = 0;
  int norebind = 0;
  int checking_disabled;
  size_t m;
  unsigned short qtype, gotname;
  unsigned char c1, c2;
  /* Max TCP packet + slop */
  int packet_len = 65536 + MAXDNAME + RRFIXEDSZ;
  unsigned char *packet = whine_malloc(packet_len);
  struct dns_header *header;
  struct server *last_server;
  struct in_addr dst_addr_4;
  union mysockaddr peer_addr;
  socklen_t peer_len = sizeof(union mysockaddr);
  char name[PACKETSZ];
  int name_len = 0;
  unsigned char *p;

  memset(name, 0, sizeof(name));
  
  if (getpeername(confd, (struct sockaddr *)&peer_addr, &peer_len) == -1)
    return packet;

  while (1)
    {
      if (!packet ||
	  !read_write(confd, &c1, 1, 1) || !read_write(confd, &c2, 1, 1) ||
	  !(size = c1 << 8 | c2) ||
	  !read_write(confd, packet, size, 1))
       	return packet; 
  
      if (size < (int)sizeof(struct dns_header))
	continue;
      
      header = (struct dns_header *)packet;

      /* save state of "cd" flag in query */
      checking_disabled = header->hb4 & HB4_CD;
       
      /* RFC 4035: sect 4.6 para 2 */
	  if (!option_bool(OPT_DNSSEC))
        header->hb4 &= ~HB4_AD;
      
      if ((gotname = extract_request(header, (unsigned int)size, daemon->namebuff, &qtype)))
	{
	  char types[20];
	  
	  p = (unsigned char *)(header+1);
	  get_original_query_name(header, size, &p, 4, name, &name_len);

	  querystr(types, qtype);
	  
	  if (peer_addr.sa.sa_family == AF_INET) 
	    log_query(F_QUERY | F_IPV4 | F_FORWARD, daemon->namebuff, 
		      (struct all_addr *)&peer_addr.in.sin_addr, types);
#ifdef HAVE_IPV6
	  else
	    log_query(F_QUERY | F_IPV6 | F_FORWARD, daemon->namebuff, 
		      (struct all_addr *)&peer_addr.in6.sin6_addr, types);
#endif
	}
      
      if (local_addr->sa.sa_family == AF_INET)
	dst_addr_4 = local_addr->in.sin_addr;
      else
	dst_addr_4.s_addr = 0;
      
      /* m > 0 if answered from cache */
      m = answer_request(header, ((char *) header) + 65536, (unsigned int)size, 
			 dst_addr_4, netmask, now);

      /* Do this by steam now we're not in the select() loop */
      check_log_writer(NULL); 
      
      if (m == 0)
	{
	  unsigned int flags = 0;
	  struct all_addr *addrp = NULL;
	  int type = 0;
	  char *domain = NULL;
	   
	  if (option_bool(OPT_ADD_MAC))
	    size = add_mac(header, size, ((char *) header) + 65536, &peer_addr);
	          
	  if (gotname)
	    flags = search_servers(now, &addrp, gotname, daemon->namebuff, &type, &domain, &norebind);
	  
#ifdef SELECT_TO_QUERY
	  if (type != 0  ||!daemon->last_server) //tcp fallback will not follow the strict server order after receive udp response from a server.
#else
	  if (type != 0  || option_bool(OPT_ORDER) || !daemon->last_server)
#endif
	    last_server = daemon->servers;
	  else
	    last_server = daemon->last_server;
      
	  if (!flags && last_server)
	    {
	      struct server *firstsendto = NULL;
	      unsigned int crc = questions_crc(header, (unsigned int)size, daemon->namebuff);

	      /* Loop round available servers until we succeed in connecting to one.
	         Note that this code subtley ensures that consecutive queries on this connection
	         which can go to the same server, do so. */
	      while (1) 
 		{
		  if (!firstsendto)
		    firstsendto = last_server;
		  else
		    {
		      if (!(last_server = last_server->next))
			last_server = daemon->servers;
		      
		      if (last_server == firstsendto)
			break;
		    }
	      
		  /* server for wrong domain */
		  if (type != (last_server->flags & SERV_TYPE) ||
		      (type == SERV_HAS_DOMAIN && !hostname_isequal(domain, last_server->domain)))
		    continue;

		  if (last_server->tcpfd == -1)
		    {
		      if ((last_server->tcpfd = socket(last_server->addr.sa.sa_family, SOCK_STREAM, 0)) == -1)
			continue;
		      
		      if ((!local_bind(last_server->tcpfd,  &last_server->source_addr, last_server->interface, 1) ||
			   connect(last_server->tcpfd, &last_server->addr.sa, sa_len(&last_server->addr)) == -1))
			{
			  close(last_server->tcpfd);
			  last_server->tcpfd = -1;
			  continue;
			}

#ifdef HAVE_CONNTRACK
		      /* Copy connection mark of incoming query to outgoing connection. */
		      if (option_bool(OPT_CONNTRACK))
			{
			  unsigned int mark;
			  struct all_addr local;
#ifdef HAVE_IPV6		      
			  if (local_addr->sa.sa_family == AF_INET6)
			    local.addr.addr6 = local_addr->in6.sin6_addr;
			  else
#endif
			    local.addr.addr4 = local_addr->in.sin_addr;
			  
			  if (get_incoming_mark(&peer_addr, &local, 1, &mark))
			    setsockopt(last_server->tcpfd, SOL_SOCKET, SO_MARK, &mark, sizeof(unsigned int));
			}
#endif	
		    }

		  c1 = size >> 8;
		  c2 = size;
		  
		  /*Start of Sean,20170427, encode query with 0x20 bit before forward dns query to WAN everytime.*/
		  p = (unsigned char *)(header+1);
		  encode_query_name(header, size, &p, 4, name, &name_len);
		  /*End of Sean,20170427, encode query with 0x20 bit before forward dns query to WAN everytime.*/

		  if (!read_write(last_server->tcpfd, &c1, 1, 0) ||
		      !read_write(last_server->tcpfd, &c2, 1, 0) ||
		      !read_write(last_server->tcpfd, packet, size, 0) ||
		      !read_write(last_server->tcpfd, &c1, 1, 1) ||
		      !read_write(last_server->tcpfd, &c2, 1, 1))
		    {
		      close(last_server->tcpfd);
		      last_server->tcpfd = -1;
		      continue;
		    } 
		  
		  m = (c1 << 8) | c2;
		  if (!read_write(last_server->tcpfd, packet, m, 1))
		    return packet;
		  
		  if (!gotname)
		    strcpy(daemon->namebuff, "query");
		  if (last_server->addr.sa.sa_family == AF_INET)
		    log_query(F_SERVER | F_IPV4 | F_FORWARD, daemon->namebuff, 
			      (struct all_addr *)&last_server->addr.in.sin_addr, NULL); 
#ifdef HAVE_IPV6
		  else
		    log_query(F_SERVER | F_IPV6 | F_FORWARD, daemon->namebuff, 
			      (struct all_addr *)&last_server->addr.in6.sin6_addr, NULL);
#endif 
		  
		  /* There's no point in updating the cache, since this process will exit and
		     lose the information after a few queries. We make this call for the alias and 
		     bogus-nxdomain side-effects. */
		  /* If the crc of the question section doesn't match the crc we sent, then
		     someone might be attempting to insert bogus values into the cache by 
		     sending replies containing questions and bogus answers. */
		  if (crc == questions_crc(header, (unsigned int)m, daemon->namebuff))
		    /*Start of Zhijian on 2012-12-10 16:17 Save dns cache for tcp mode.*/
		    m = process_reply(header, packet_len, now, last_server, (unsigned int)m, 
				      option_bool(OPT_NO_REBIND) && !norebind, checking_disabled, NULL, 0, is_child);
		    /*End of Zhijian on 2012-12-10 16:17 Save dns cache for tcp mode.*/
		  
		  break;
		}
	    }
	  
	  /* In case of local answer or no connections made. */
	  if (m == 0)
	    m = setup_reply(header, (unsigned int)size, addrp, flags, daemon->local_ttl);
	}

      check_log_writer(NULL);
      
      /*Start of Sean,20170427, encode query with 0x20 bit before forward dns query to WAN everytime.*/
      p = (unsigned char *)(header+1);
      restore_original_name(header, size, &p, 4, name, name_len);
      /*End of Sean,20170427, encode query with 0x20 bit before forward dns query to WAN everytime.*/

      c1 = m>>8;
      c2 = m;
      if (!read_write(confd, &c1, 1, 0) ||
	  !read_write(confd, &c2, 1, 0) || 
	  !read_write(confd, packet, m, 0))
	return packet;
    }
}

static struct frec *allocate_frec(time_t now)
{
	struct frec *f;
	int i = 0;

	if ((f = (struct frec *)whine_malloc(sizeof(struct frec))))
	{
#ifdef QUEUE_LIST
		f->next = NULL;
#else
		f->next = daemon->frec_list;
#endif
		f->time = now;
		f->sentto = NULL;
		f->rfd4 = NULL;
		f->flags = 0;
#ifdef HAVE_IPV6
		f->rfd6 = NULL;
#endif
		
#ifdef SELECT_TO_QUERY	
		f->idx = 0;
		f->orig_id = 0;
		f->send_server_num = 0;
		f->packet_len = 0;		
		f->original_name_len= 0;
		f->state = FW_INIT;		
		memset(f->packet, 0, sizeof(f->packet));	 
		memset(f->original_name, 0, sizeof(f->original_name));	 
		memset(&f->start_pending_time, 0, sizeof(struct timeval));	  	
		memset(&f->last_querytime, 0, sizeof(struct timeval));	  	
		memset(&f->first_querytime, 0, sizeof(struct timeval));
		f->present_srv = NULL;
		f->last_srv = NULL;
		memset(&(f->last_srv_addr), 0, sizeof(union mysockaddr));
		for(i = 0; i < MAX_SERVER_NUM; i++)
		{
			f->used_fd[i] = -1;			
			f->retry_list[i].flags = 0;		
			f->retry_list[i].new_id = 0;		
			f->retry_list[i].server = NULL;
		}
		f->syslog_add = 0;
#endif
#ifdef QUEUE_LIST
		if(daemon->frec_list == NULL)
		{
			daemon->frec_list_first = daemon->frec_list = f;
		}
		else
		{
			daemon->frec_list->next = f;
			daemon->frec_list = f;
		}
#else	//org
		daemon->frec_list = f;
#endif
	}
	return f;
}

static struct randfd *allocate_rfd(int family)
{
  static int finger = 0;
  int i;

  /* limit the number of sockets we have open to avoid starvation of 
     (eg) TFTP. Once we have a reasonable number, randomness should be OK */

  for (i = 0; i < RANDOM_SOCKS; i++)
    if (daemon->randomsocks[i].refcount == 0)
      {
	if ((daemon->randomsocks[i].fd = random_sock(family)) == -1)
	  break;
      
	daemon->randomsocks[i].refcount = 1;
	daemon->randomsocks[i].family = family;
	return &daemon->randomsocks[i];
      }

  /* No free ones or cannot get new socket, grab an existing one */
  for (i = 0; i < RANDOM_SOCKS; i++)
    {
      //int j = (i+finger) % RANDOM_SOCKS;
      int j = get_random(0, RANDOM_SOCKS-1);
      if (daemon->randomsocks[j].refcount != 0 &&
	  daemon->randomsocks[j].family == family && 
	  daemon->randomsocks[j].refcount != 0xffff)
	{
	  //finger = j;
	  daemon->randomsocks[j].refcount++;
	  return &daemon->randomsocks[j];
	}
    }

  return NULL; /* doom */
}

static void free_frec(struct frec *f)
{
	int i = 0;

	if(!f)
	{
		return;
	}
	clear_fd_of_frec(f);
#if 0 // move close action to clear_fd_of_frec
	if (f->rfd4 && --(f->rfd4->refcount) == 0)
		close(f->rfd4->fd);
#endif
	f->rfd4 = NULL;
	f->sentto = NULL;
	f->flags = 0;
#ifdef HAVE_IPV6
#if 0 // move close action to clear_fd_of_frec
	if (f->rfd6 && --(f->rfd6->refcount) == 0)
		close(f->rfd6->fd);
#endif 
	f->rfd6 = NULL;
#endif 

#ifdef SELECT_TO_QUERY	
	f->idx = 0;
	f->orig_id = 0;
	f->send_server_num = 0;
	f->packet_len = 0;		
	f->original_name_len= 0;
	f->state = FW_INIT;		
	memset(f->packet, 0, sizeof(f->packet));	 
	memset(f->original_name, 0, sizeof(f->original_name));	 
	memset(&f->start_pending_time, 0, sizeof(struct timeval));	  	
	memset(&f->last_querytime, 0, sizeof(struct timeval));	  	
	memset(&f->first_querytime, 0, sizeof(struct timeval));
	f->present_srv = NULL;
	f->last_srv = NULL;
	memset(&(f->last_srv_addr), 0, sizeof(union mysockaddr));
	for(i = 0; i < MAX_SERVER_NUM; i++)
	{
		f->used_fd[i] = -1;			
		f->retry_list[i].flags = 0;		
		f->retry_list[i].new_id = 0;		
		f->retry_list[i].server = NULL;
	}
	f->syslog_add = 0;
#endif
}

/* if wait==NULL return a free or older than TIMEOUT record.
   else return *wait zero if one available, or *wait is delay to
   when the oldest in-use record will expire. Impose an absolute
   limit of 4*TIMEOUT before we wipe things (for random sockets) */
struct frec *get_new_frec(time_t now, int *wait)
{
  struct frec *f, *oldest, *target;
  int count;
  
  if (wait)
    *wait = 0;

  for (f = daemon->frec_list, oldest = NULL, target =  NULL, count = 0; f; f = f->next, count++)
    if (f->state != FW_PENDING && !f->sentto)
      target = f;
    else 
      {
	if (difftime(now, f->time) >= 4*TIMEOUT)
	  {
	    free_frec(f);
	    target = f;
	  }
	
	if (!oldest || difftime(f->time, oldest->time) <= 0)
	  oldest = f;
      }

  if (target)
    {
      target->time = now;
      return target;
    }
  
  /* can't find empty one, use oldest if there is one
     and it's older than timeout */
  if (oldest && ((int)difftime(now, oldest->time)) >= TIMEOUT)
    { 
      /* keep stuff for twice timeout if we can by allocating a new
	 record instead */
      if (difftime(now, oldest->time) < 2*TIMEOUT && 
	  count <= daemon->ftabsize &&
	  (f = allocate_frec(now)))
	return f;

      if (!wait)
	{
	  free_frec(oldest);
	  oldest->time = now;
	}
      return oldest;
    }
  
  /* none available, calculate time 'till oldest record expires */
  if (count > daemon->ftabsize)
    {
      if (oldest && wait)
	*wait = oldest->time + (time_t)TIMEOUT - now;
      return NULL;
    }
  
  if (!(f = allocate_frec(now)) && wait)
    /* wait one second on malloc failure */
    *wait = 1;

  return f; /* OK if malloc fails and this is NULL */
}
 
/* crc is all-ones if not known. */
static struct frec *lookup_frec(unsigned short id, unsigned int crc, union mysockaddr *addr)
{
  struct frec *f;

  if(!addr)
	return NULL;

  for(f = daemon->frec_list; f; f = f->next)
    //if (f->sentto && f->new_id == id &&    
     if (f->sentto && id_is_equal(f, id, addr) && 
	(f->crc == crc || crc == 0xffffffff))
      return f;
      
  return NULL;
}

static struct frec *lookup_frec_by_sender(unsigned short id,
					  union mysockaddr *addr,
					  unsigned int crc)
{
  struct frec *f;
  
  for(f = daemon->frec_list; f; f = f->next)
    if ((f->state == FW_WAITING ||f->state == FW_PENDING || f->state == FW_FINISH) &&
	f->orig_id == id && 
	f->crc == crc &&
	sockaddr_isequal(&f->source, addr))
      return f;
   
  return NULL;
}

static struct frec *lookup_frec_by_id(unsigned short id, char *name)
{
  struct frec *f;
  
  for(f = daemon->frec_list; f; f = f->next)
    if ((f->state == FW_WAITING ||f->state == FW_PENDING || f->state == FW_FINISH) &&
	f->orig_id == id && 
	(hostname_isequal(f->original_name, name)))
      return f;
   
  return NULL;
}

/* A server record is going away, remove references to it */
void server_gone(struct server *server)
{
  struct frec *f;
  
  for (f = daemon->frec_list; f; f = f->next)
    if ((f->state == FW_WAITING || f->state == FW_FINISH)&& f->sentto && f->sentto == server)
	{ 
		f->last_srv = NULL;
		memset(&(f->last_srv_addr), 0, sizeof(union mysockaddr));
	}
  
  if (daemon->last_server == server)
    daemon->last_server = NULL;

  if (daemon->srv_save == server)
    daemon->srv_save = NULL;
}

void update_server_to_frec()
{
	struct frec *f;
	struct server *serv;
	int found;
	char buf[128];	
		
	for (f = daemon->frec_list; f; f = f->next)
	{		
		if (f->state != FW_WAITING && f->state != FW_FINISH)
			continue;
		
		found = 0;
		for (serv = daemon->servers; serv; serv = serv->next)
		{
			if(serv_is_zero_addr(serv))	  	
				continue;	
			if (sockaddr_isequal(&(f->last_srv_addr), &(serv->addr)))
			{ 				
				found = 1;
				f->last_srv = serv;
				memcpy(&(f->last_srv_addr) , &(serv->addr), sizeof(union mysockaddr)); 
				break;
			}
		}
		if(found == 0)
		{
			f->last_srv = NULL;
			memset(&(f->last_srv_addr), 0, sizeof(union mysockaddr));
		}
	}	
}

void flush_frec()
{
	struct frec *f = NULL;
	for (f = daemon->frec_list; f; f = f->next)
	{
		if (!f)
			break;
		free_frec(f);
	}
}

/* return unique random ids. */
static unsigned short get_id(unsigned int crc, union mysockaddr *addr)
{
  unsigned short ret = 0;
 
  if(!addr)
	return ret;
  do 
    ret = rand16();
  while (lookup_frec(ret, crc, addr));
  
  return ret;
}


#ifdef SELECT_TO_QUERY
void save_fd_to_frec(struct frec *f, int fd)
{
	int i;
	
	if(!f)
	{
		return;
	}
	
	for(i = 0; i < MAX_SERVER_NUM; i++)
	{
		if(f->used_fd[i] == -1)
		{
			f->used_fd[i] = fd;
			break;
		}			
	}
	return;
}

void clear_fd_of_frec(struct frec *f)
{
	int i, j;	
	
	if(!f)
	{
		return;
	}

	for(i = 0; i < MAX_SERVER_NUM; i++)
	{
		if (f->used_fd[i] == -1) 
			continue;
		for (j = 0; j < RANDOM_SOCKS; j++)
		{
			if (daemon->randomsocks[j].fd == f->used_fd[i] )
			{	
				daemon->randomsocks[j].refcount = 0;
				close(daemon->randomsocks[j].fd);		 
				break; //break for (j = 0; j < RANDOM_SOCKS; j++)
			}
		}
	}
	return;
}

void add_new_id_to_frec(struct frec *f, unsigned short id, union mysockaddr * addr)
{
	int i;
	
	if(!f ||!addr)
	{
		return;
	}

	for(i = 0; i < MAX_SERVER_NUM; i++)
	{	
		if(f->retry_list[i].flags == 0)
		{		
			f->retry_list[i].flags = 1;
			f->retry_list[i].new_id = id;			
			f->retry_list[i].server = addr;
			break;
		}			
	}
	return;
}

/* Compare the id with the f->new_id[i]. 
 * return value:
 * 1 equal
 * 0 unequal
*/
int id_is_equal(struct frec *f, unsigned short id, union mysockaddr *addr)
{
	int i;
	int ret = 0;
	
	if(!f ||!addr)
	{
		return ret;
	}

	for(i = 0; i < MAX_SERVER_NUM; i++)
	{		
		if(f->retry_list[i].new_id == id && sockaddr_isequal(f->retry_list[i].server, addr))
		{
			ret = 1;
			break;
		}			
	}
	return ret;
}

int send_serverfail(struct frec *f)
{
	int plen;
	unsigned char *p;
	struct dns_header *header;

	if (f == NULL)
		return -1;
	
	header = (struct dns_header *)f->packet;	
	header->id = htons(f->orig_id);
	
	plen = setup_reply((struct dns_header *)f->packet, f->packet_len, NULL, F_NEG, daemon->local_ttl);
	p = (unsigned char *)(header+1);
	restore_original_name(header, f->packet_len, &p, 4, f->original_name, f->original_name_len);
	send_from(f->fd, option_bool(OPT_NOWILD), (char *)f->packet, plen, &f->source, &f->dest, f->iface);

	return 0;
}

int get_sock(struct server *server, struct frec *forward)
{
	int fd;

	if (!server || !forward)
		return 0;

	/* find server socket to use, may need to get random one. */
	if (server->sfd)
		fd = server->sfd->fd;
	else 
	{
#ifdef HAVE_IPV6
		if (server->addr.sa.sa_family == AF_INET6)
		{
			/* if (!forward->rfd6 &&
					!(forward->rfd6 = allocate_rfd(AF_INET6)))
				return 0;
			*/
			if ( !(forward->rfd6 = allocate_rfd(AF_INET6)))
				return 0;
			daemon->rfd_save = forward->rfd6;
			fd = forward->rfd6->fd;
			save_fd_to_frec(forward, fd);	
		}
		else
#endif
		{
			/* if (!forward->rfd4 &&
					!(forward->rfd4 = allocate_rfd(AF_INET)))
				return 0;
			*/
			if ( !(forward->rfd4 = allocate_rfd(AF_INET)))
				return 0;

			daemon->rfd_save = forward->rfd4;
			fd = forward->rfd4->fd;
			save_fd_to_frec(forward, fd);	
		}
	}

	return fd;
}

double ms_difftime(struct timeval t1, struct timeval t0)
{
	return 1000000 * (t1.tv_sec-t0.tv_sec) + (t1.tv_usec-t0.tv_usec);
}

void check_forward_list()
{
	struct frec *f;
	struct server *s = NULL, *start = NULL, *next = NULL;
	struct dns_header *header;
	int idx, i=0;	
	int query_from_lan = 0;
	unsigned int dt_mark = 0;
	int sock = 0;
	char buf[128];	
	char name[PACKETSZ];
	int name_len = 0;
	unsigned char *p;
	unsigned short get_new_id = 0;
	int iTimePass = 0;	//for fine tune retry interval
	
	if(daemon->update_serv_flag) //updating dns server list, can not send packets at this time.
	{
		return;
	}
	
	for (start = daemon->servers; start; start = start->next)
	{
		if(serv_is_zero_addr(start))
			continue;
		else
			break;
	}

#ifdef QUEUE_LIST
	for (f = daemon->frec_list_first; f; f = f->next)
#else
	for (f = daemon->frec_list; f; f = f->next)
#endif
	{
		struct timeval now;
		static char srv_addr[INET6_ADDRSTRLEN] = "";
		int bAddDTLog = 0;

		if (f->state != FW_WAITING && f->state != FW_PENDING && f->state != FW_FINISH)
		{
			continue;
		}
		if (f->packet_len == 0) {
			printf("[warning]: A freq is waiting response, but no retry packet available!\n");
			continue;
		}
		
		gettimeofday(&now, NULL);
		
		if ((f->state == FW_PENDING) && (f->last_srv == NULL) 
			&& start && ms_difftime(now, f->start_pending_time) < dns_queue_timeout)
		{			
			f->idx = 0;
			f->send_server_num = 1;
			f->last_srv = start;
			memcpy(&(f->last_srv_addr) , &(start->addr), sizeof(union mysockaddr));
			f->sentto = start;
			gettimeofday(&f->last_querytime, NULL);     
			gettimeofday(&f->first_querytime, NULL);     
		}    

		idx = (f->idx)%MAX_DELAY_TIMES;
		g_elapse = ms_difftime(now, f->last_querytime);
		if ((f->state == FW_WAITING || f->state == FW_FINISH) && (g_elapse < select_delay[idx]))
		{
			if (g_elapse > SELECT_TIMEOUT_USEC_THRESH)
			{
				//count minimum of the frec_list.
				int i_calibrate_usec = select_delay[idx] - g_elapse;
				if(g_calibrate_usec == 0)
				{
					g_calibrate_usec = i_calibrate_usec;
				}
				else	//find mininum
				{
					if(iTimePass)	// other frec(s) have sent queries
						i_calibrate_usec -= iTimePass;

					if(i_calibrate_usec > 0)
					{
						if(i_calibrate_usec < g_calibrate_usec)
							g_calibrate_usec = i_calibrate_usec;
					}
					else	// other frec(s) has run out the SELECT_TIMEOUT_USEC
						g_calibrate_usec = SELECT_TIMEOUT_USEC_MIN;
				}
			}

			continue;
		}

		if(f->state == FW_PENDING && f->last_srv == NULL)
		{
			if(ms_difftime(now, f->start_pending_time) >=  dns_queue_timeout)
			{
				free_frec(f);
			}				
			continue;			
		}

		if ((f->state == FW_WAITING) && f->present_srv)
		{
			if ((f->present_srv->addr.sa.sa_family == AF_INET6) && (f->syslog_add==0))
			{			
				struct in6_addr inet6_addr = f->present_srv->addr.in6.sin6_addr;
				bAddDTLog = 1;
				inet_ntop(AF_INET6, &inet6_addr, srv_addr, sizeof(srv_addr));
			}
		}

		if (f->last_srv == NULL) /* already to the end of list */
		{	
			if ( (f->state == FW_WAITING ||f->state == FW_FINISH) && ms_difftime(now, f->first_querytime) >= dns_query_timeout)
			{
				printf("send serverfail to client, done\n");
				if(f->state == FW_WAITING)
					send_serverfail(f);
				free_frec(f);
			}			
			continue;
		}		
		f->idx++;

		if(f->state == FW_WAITING)
		{
			i = 0;
			/* send to interested servers */
			for (s=f->last_srv; s; s=s->next) {
				if (i++ == TRY_SERVER_NUMS) {
					printf("reach to MAX retry, try next server!\n");
					break;
				}
				if(serv_is_zero_addr(s))
					continue;
				sock = get_sock(s, f);
				if (sock == 0) {
					printf("No sock available!\n");
					continue;
				}
#if 0
#ifdef HAVE_IPV6
				if (s->addr.in6.sin6_family == AF_INET6)
					printf("send packet to IPv6 server:[%s]\n",	inet_ntop(AF_INET6, &(s->addr.in6.sin6_addr), buf, sizeof(buf)));
				else
#endif
					printf("send packet to IPv4 server:[%s]\n",	inet_ntoa(s->addr.in.sin_addr));
#endif
				if(f->source.sa.sa_family == AF_INET)
				{
					query_from_lan = is_same_subnet_with_lan(f->source.in.sin_addr);
				}
				else if(f->source.sa.sa_family == AF_INET6)
				{
					query_from_lan = 1;
				}

				if(query_from_lan)
				{
					dt_mark = DT_MARK_LAN2WAN;
					setsockopt(sock, SOL_SOCKET, SO_MARK, &dt_mark, sizeof(unsigned int));
				}
				else
				{
					dt_mark = DT_MARK_RESERVE_CONN;
					setsockopt(sock, SOL_SOCKET, SO_MARK, &dt_mark, sizeof(unsigned int));
				}
				/* do it */
				get_new_id = get_id(f->crc, &(s->addr));
				add_new_id_to_frec(f, get_new_id, &(s->addr));
				header = (struct dns_header *)f->packet;
				header->id = htons(get_new_id);
				p = (unsigned char *)(header+1);
				encode_query_name(header, f->packet_len, &p, 4, name, &name_len);
				sendto(sock, f->packet, f->packet_len, 0, &s->addr.sa, sa_len(&s->addr));
				iTimePass += SELECT_TIMEOUT_USEC_SEND;
				f->present_srv = s;
				if(f->send_server_num == 1)
				{
					if(s->flags & SERV_DT_FLAG )
						f->send_server_num = 2;
					else
						break;
				}
			}

			if(bAddDTLog)
			{
				dnsmasq_AddSyslog((void *)srv_addr, (void *)strlen(srv_addr), (void *)f->original_name, (void *)f->original_name_len);
				f->syslog_add = 1;
				iTimePass += SELECT_TIMEOUT_USEC_LOG;
			}
		}
		else if(f->state == FW_FINISH)
		{
			i = 0;
			/* send to interested servers */
			for (s=f->last_srv; s; s=s->next) {
				if (i++ == TRY_SERVER_NUMS) {
					printf("reach to MAX retry, try next server!\n");
					break;
				}
				if(serv_is_zero_addr(s))
					continue;
				sock = get_sock(s, f);
				if (sock == 0) {
					printf("No sock available!\n");
					continue;
				}
				if(f->send_server_num == 1)
				{
					if(s->flags & SERV_DT_FLAG )
						f->send_server_num = 2;
					else
						break;
				}
			}
		}

		if(f->send_server_num == 1)
		{
			f->send_server_num = 2;
		}
		else if(f->send_server_num == 2)
		{
			f->send_server_num = 1;
			for (next = s; next; next = next->next)
			{
				if(serv_is_zero_addr(next))
					continue;
				else
					break;
			}
           		f->last_srv = next;
			if(f->last_srv)
				memcpy(&(f->last_srv_addr) , &(next->addr), sizeof(union mysockaddr)); 
			else
				memset(&(f->last_srv_addr), 0, sizeof(union mysockaddr));
		}
		if(f->state == FW_PENDING)
		{
			f->state = FW_WAITING;
		}
		memcpy(&f->last_querytime, &now, sizeof(struct timeval));
	}

	// after sending query, count the g_calibrate_usec to check if there is query to send.
	if(iTimePass)
	{
		struct timeval now;
		int iInterval = 0, iMaxInterval = 0;

		if(g_calibrate_usec == 0)
			g_calibrate_usec = SELECT_TIMEOUT_USEC - iTimePass;

		gettimeofday(&now, NULL);

		for (f = daemon->frec_list; f; f = f->next)
		{
			if ((f->state == FW_WAITING || f->state == FW_FINISH))
			{
				iInterval = ms_difftime(now, f->last_querytime);
				if(iInterval > iMaxInterval)
					iMaxInterval = iInterval;
			}
		}

		if(iMaxInterval > SELECT_TIMEOUT_USEC_THRESH)
		{
			if(select_delay[0] < iMaxInterval)
				g_calibrate_usec = 1;
			else
				g_calibrate_usec = (select_delay[0] - iMaxInterval);
		}
	}

	return;
}
#endif


