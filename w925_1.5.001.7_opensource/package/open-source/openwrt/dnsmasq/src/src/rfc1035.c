/* dnsmasq is Copyright (c) 2000-2011 Simon Kelley

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 dated June, 1991, or
   (at your option) version 3 dated 29 June, 2007.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
     
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dnsmasq.h"

static int add_resource_record(struct dns_header *header, char *limit, int *truncp, 
			       unsigned int nameoffset, unsigned char **pp, 
			       unsigned long ttl, unsigned int *offset, unsigned short type, 
			       unsigned short class, char *format, ...);

#define CHECK_LEN(header, pp, plen, len) \
    ((size_t)((pp) - (unsigned char *)(header) + (len)) <= (plen))

#define ADD_RDLEN(header, pp, plen, len) \
    (!CHECK_LEN(header, pp, plen, len) ? 0 : (long)((pp) += (len)), 1)

struct iaddr {
    unsigned len; 
    unsigned char iabuf [16];
};

struct lease_t {
    unsigned char chaddr[16];
    struct iaddr yiaddr;
    u_int32_t expires;
    char hostname[64];
};

/* piaddr() turns an iaddr structure into a printable address. */
/* XXX: should use a const pointer rather than passing the structure */
const char *piaddr(const struct iaddr addr) 
{
        static char  pbuf[sizeof("ffff:ffff:ffff:ffff:ffff:ffff:255.255.255.255")]; /* "255.255.255.255" */

        /* INSIST((addr.len == 0) || (addr.len == 4) || (addr.len == 16)); */

        if (addr.len == 0) {
                return "<null address>";
        }
        if (addr.len == 4) {
                return inet_ntop(AF_INET, addr.iabuf, pbuf, sizeof(pbuf));
        }
        if (addr.len == 16) {
                return inet_ntop(AF_INET6, addr.iabuf, pbuf, sizeof(pbuf));
        }

        cprintf("%s:%d: dnsmasq Invalid address length %d.\n", __func__, __LINE__, addr.len);
        /* quell compiler warnings */
        return NULL;
}

/*
 * Convert Ethernet address binary data to string representation
 * @param   e   binary data
 * @param   a   string in xx:xx:xx:xx:xx:xx notation
 * @return  a
 */
char *
ether_etoa(const unsigned char *e, char *a) 
{
    char *c = a;
    int i;

    for (i = 0; i < ETHER_ADDR_LEN; i++) {
        if (i) 
            *c++ = ':';
        c += sprintf(c, "%02X", e[i] & 0xff);
    }   
    return a;
}

int extract_name(struct dns_header *header, size_t plen, unsigned char **pp, 
			char *name, int isExtract, int extrabytes)
{
  unsigned char *cp = (unsigned char *)name, *p = *pp, *p1 = NULL;
  unsigned int j, l, hops = 0;
  int retvalue = 1;
  
  if (isExtract)
    *cp = 0;

  while (1)
    { 
      unsigned int label_type;

      if (!CHECK_LEN(header, p, plen, 1))
	return 0;
      
      if ((l = *p++) == 0) 
	/* end marker */
	{
	  /* check that there are the correct no of bytes after the name */
	  if (!CHECK_LEN(header, p, plen, extrabytes))
	    return 0;
	  
	  if (isExtract)
	    {
	      if (cp != (unsigned char *)name)
		cp--;
	      *cp = 0; /* terminate: lose final period */
	    }
	  else if (*cp != 0)
	    retvalue = 2;
	  
	  if (p1) /* we jumped via compression */
	    *pp = p1;
	  else
	    *pp = p;
	  
	  return retvalue;
	}

      label_type = l & 0xc0;
      
      if (label_type == 0xc0) /* pointer */
	{ 
	  if (!CHECK_LEN(header, p, plen, 1))
	    return 0;
	      
	  /* get offset */
	  l = (l&0x3f) << 8;
	  l |= *p++;
	  
	  if (!p1) /* first jump, save location to go back to */
	    p1 = p;
	      
	  hops++; /* break malicious infinite loops */
	  if (hops > 255)
	    return 0;
	  
	  p = l + (unsigned char *)header;
	}
      else if (label_type == 0x80)
	return 0; /* reserved */
      else if (label_type == 0x40)
	{ /* ELT */
	  unsigned int count, digs;
	  
	  if ((l & 0x3f) != 1)
	    return 0; /* we only understand bitstrings */

	  if (!isExtract)
	    return 0; /* Cannot compare bitsrings */
	  
	  count = *p++;
	  if (count == 0)
	    count = 256;
	  digs = ((count-1)>>2)+1;
	  
	  /* output is \[x<hex>/siz]. which is digs+9 chars */
	  if (cp - (unsigned char *)name + digs + 9 >= MAXDNAME)
	    return 0;
	  if (!CHECK_LEN(header, p, plen, (count-1)>>3))
	    return 0;

	  *cp++ = '\\';
	  *cp++ = '[';
	  *cp++ = 'x';
	  for (j=0; j<digs; j++)
	    {
	      unsigned int dig;
	      if (j%2 == 0)
		dig = *p >> 4;
	      else
		dig = *p++ & 0x0f;
	      
	      *cp++ = dig < 10 ? dig + '0' : dig + 'A' - 10;
	    } 
	  cp += sprintf((char *)cp, "/%d]", count);
	  /* do this here to overwrite the zero char from sprintf */
	  *cp++ = '.';
	}
      else 
	{ /* label_type = 0 -> label. */
	  if (cp - (unsigned char *)name + l + 1 >= MAXDNAME)
	    return 0;
	  if (!CHECK_LEN(header, p, plen, l))
	    return 0;
	  
	  for(j=0; j<l; j++, p++)
	    if (isExtract)
	      {
		unsigned char c = *p;
		if (isascii(c) && !iscntrl(c) && c != '.')
		  *cp++ = *p;
		else
		  return 0;
	      }
	    else 
	      {
		unsigned char c1 = *cp, c2 = *p;
		
		if (c1 == 0)
		  retvalue = 2;
		else 
		  {
		    cp++;
		    if (c1 >= 'A' && c1 <= 'Z')
		      c1 += 'a' - 'A';
		    if (c2 >= 'A' && c2 <= 'Z')
		      c2 += 'a' - 'A';
		    
		    if (c1 != c2)
		      retvalue =  2;
		  }
	      }
	  
	  if (isExtract)
	    *cp++ = '.';
	  else if (*cp != 0 && *cp++ != '.')
	    retvalue = 2;
	}
    }
}

/* guang_zhao 20130119, support the extended PTR dns query about DNS-Based Service Discovery.
  * Need not limit MAXARPANAME.
  */
/* Max size of input string (for IPv6) is 75 chars.) */
//#define MAXARPANAME 75
static int in_arpa_name_2_addr(char *namein, struct all_addr *addrp)
{
  int j;
  //char name[MAXARPANAME+1], *cp1;  
  char name[MAXDNAME], *cp1;  
  unsigned char *addr = (unsigned char *)addrp;
  char *lastchunk = NULL, *penchunk = NULL;
  char *new_name = NULL;  
  int digit_field_num = 0;

  //if (strlen(namein) > MAXARPANAME)
  if (strlen(namein) > MAXDNAME)
    return 0;
    
  memset(addrp, 0, sizeof(struct all_addr)); 

  /* guang_zhao 20130119, support the extended PTR dns query about DNS-Based Service Discovery.    
    * http://www.dns-sd.org/
    * http://tools.ietf.org/html/draft-cheshire-dnsext-dns-sd-11
    */
    
  /* turn name into a series of asciiz strings */
  /* j counts no of labels */
  new_name = name;
  lastchunk = name;
  for(j = 1,cp1 = name; *namein; cp1++, namein++)
	if (*namein == '.')
	{
		penchunk = lastchunk;
		lastchunk = cp1 + 1;
		*cp1 = 0;	
		if(hostname_isequal(penchunk, "_udp") || hostname_isequal(penchunk, "_tcp"))
		{
			new_name = lastchunk;
		}
		j++;
	}
	else
		*cp1 = *namein;
  
  *cp1 = 0;

  if (j<3)
    return 0;

  if (hostname_isequal(lastchunk, "arpa") && hostname_isequal(penchunk, "in-addr"))
    {
      /* IP v4 */
      /* address arives as a name of the form
	  www.xxx.yyy.zzz.in-addr.arpa
	  some of the low order address octets might be missing
	  and should be set to zero. */
	  digit_field_num  = 0;
	  for (cp1 = new_name; cp1 != penchunk; cp1 += strlen(cp1)+1)
	  {
	  /* check for digits only (weeds out things like
	     50.0/24.67.28.64.in-addr.arpa which are used 
	     as CNAME targets according to RFC 2317 */
	  char *cp;
	  for (cp = cp1; *cp; cp++)
	    if (!isdigit((unsigned char)*cp))
	      return 0;

	/* RFC 1035, 3.5. IN-ADDR.ARPA domain,
	  * Domain names in the IN-ADDR.ARPA domain are defined to have up to four
	  * labels in addition to the IN-ADDR.ARPA suffix. 
	  *
	  * So drop the illogical PTR query(more than four labels), such as 2.0.2.168.192.in-addr.arpa
	  */
	  if (digit_field_num >= 4) //to drop the illogical PTR query, such as 2.0.2.168.192.in-addr.arpa
	      return 0;	  
	  
	  addr[3] = addr[2];
	  addr[2] = addr[1];
	  addr[1] = addr[0];
	  addr[0] = atoi(cp1);
	  digit_field_num++;
	  }

      return F_IPV4;
    }
#ifdef HAVE_IPV6
  else if (hostname_isequal(penchunk, "ip6") && 
	   (hostname_isequal(lastchunk, "int") || hostname_isequal(lastchunk, "arpa")))
    {
      /* IP v6:
         Address arrives as 0.1.2.3.4.5.6.7.8.9.a.b.c.d.e.f.ip6.[int|arpa]
    	 or \[xfedcba9876543210fedcba9876543210/128].ip6.[int|arpa]
      
	 Note that most of these the various reprentations are obsolete and 
	 left-over from the many DNS-for-IPv6 wars. We support all the formats
	 that we can since there is no reason not to.
      */
		
	  if (*new_name == '\\' && *(new_name+1) == '[' && 
	  (*(new_name+2) == 'x' || *(new_name+2) == 'X'))
	  {
		for (j = 0, cp1 = new_name+3; *cp1 && isxdigit((unsigned char) *cp1) && j < 32; cp1++, j++)
	    {
	      char xdig[2];
	      xdig[0] = *cp1;
	      xdig[1] = 0;
	      if (j%2)
			addr[j/2] |= strtol(xdig, NULL, 16);
	      else
			addr[j/2] = strtol(xdig, NULL, 16) << 4;
	    }
	  
		if (*cp1 == '/' && j == 32)
			return F_IPV6;
	  }
      else
	  {	
		for (cp1 = new_name; cp1 != penchunk; cp1 += strlen(cp1)+1)
	    {
	      if (*(cp1+1) || !isxdigit((unsigned char)*cp1))
			return 0;
	      
	      for (j = sizeof(struct all_addr)-1; j>0; j--)
			addr[j] = (addr[j] >> 4) | (addr[j-1] << 4);
	      addr[0] = (addr[0] >> 4) | (strtol(cp1, NULL, 16) << 4);
	    }
	  
		return F_IPV6;
	  }
    }
#endif
  
  return 0;
}

static unsigned char *skip_name(unsigned char *ansp, struct dns_header *header, size_t plen, int extrabytes)
{
  while(1)
    {
      unsigned int label_type;
      
      if (!CHECK_LEN(header, ansp, plen, 1))
	return NULL;
      
      label_type = (*ansp) & 0xc0;

      if (label_type == 0xc0)
	{
	  /* pointer for compression. */
	  ansp += 2;	
	  break;
	}
      else if (label_type == 0x80)
	return NULL; /* reserved */
      else if (label_type == 0x40)
	{
	  /* Extended label type */
	  unsigned int count;
	  
	  if (!CHECK_LEN(header, ansp, plen, 2))
	    return NULL;
	  
	  if (((*ansp++) & 0x3f) != 1)
	    return NULL; /* we only understand bitstrings */
	  
	  count = *(ansp++); /* Bits in bitstring */
	  
	  if (count == 0) /* count == 0 means 256 bits */
	    ansp += 32;
	  else
	    ansp += ((count-1)>>3)+1;
	}
      else
	{ /* label type == 0 Bottom six bits is length */
	  unsigned int len = (*ansp++) & 0x3f;
	  
	  if (!ADD_RDLEN(header, ansp, plen, len))
	    return NULL;

	  if (len == 0)
	    break; /* zero length label marks the end. */
	}
    }

  if (!CHECK_LEN(header, ansp, plen, extrabytes))
    return NULL;
  
  return ansp;
}

static unsigned char *skip_questions(struct dns_header *header, size_t plen)
{
  int q;
  unsigned char *ansp = (unsigned char *)(header+1);

  for (q = ntohs(header->qdcount); q != 0; q--)
    {
      if (!(ansp = skip_name(ansp, header, plen, 4)))
	return NULL;
      ansp += 4; /* class and type */
    }
  
  return ansp;
}

static unsigned char *skip_section(unsigned char *ansp, int count, struct dns_header *header, size_t plen)
{
  int i, rdlen;
  
  for (i = 0; i < count; i++)
    {
      if (!(ansp = skip_name(ansp, header, plen, 10)))
	return NULL; 
      ansp += 8; /* type, class, TTL */
      GETSHORT(rdlen, ansp);
      if (!ADD_RDLEN(header, ansp, plen, rdlen))
	return NULL;
    }

  return ansp;
}

/* CRC the question section. This is used to safely detect query 
   retransmision and to detect answers to questions we didn't ask, which 
   might be poisoning attacks. Note that we decode the name rather 
   than CRC the raw bytes, since replies might be compressed differently. 
   We ignore case in the names for the same reason. Return all-ones
   if there is not question section. */
unsigned int questions_crc(struct dns_header *header, size_t plen, char *name)
{
  int q;
  unsigned int crc = 0xffffffff;
  unsigned char *p1, *p = (unsigned char *)(header+1);

  for (q = ntohs(header->qdcount); q != 0; q--) 
    {
      if (!extract_name(header, plen, &p, name, 1, 4))
	return crc; /* bad packet */
      
      for (p1 = (unsigned char *)name; *p1; p1++)
	{
	  int i = 8;
	  char c = *p1;

	  if (c >= 'A' && c <= 'Z')
	    c += 'a' - 'A';

	  crc ^= c << 24;
	  while (i--)
	    crc = crc & 0x80000000 ? (crc << 1) ^ 0x04c11db7 : crc << 1;
	}
      
      /* CRC the class and type as well */
      for (p1 = p; p1 < p+4; p1++)
	{
	  int i = 8;
	  crc ^= *p1 << 24;
	  while (i--)
	    crc = crc & 0x80000000 ? (crc << 1) ^ 0x04c11db7 : crc << 1;
	}

      p += 4;
      if (!CHECK_LEN(header, p, plen, 0))
	return crc; /* bad packet */
    }

  return crc;
}


size_t resize_packet(struct dns_header *header, size_t plen, unsigned char *pheader, size_t hlen)
{
  unsigned char *ansp = skip_questions(header, plen);
    
  /* if packet is malformed, just return as-is. */
  if (!ansp)
    return plen;
  
  if (!(ansp = skip_section(ansp, ntohs(header->ancount) + ntohs(header->nscount) + ntohs(header->arcount),
			    header, plen)))
    return plen;
    
  /* restore pseudoheader */
  if (pheader && ntohs(header->arcount) == 0)
    {
      /* must use memmove, may overlap */
      memmove(ansp, pheader, hlen);
      header->arcount = htons(1);
      ansp += hlen;
    }

  return ansp - (unsigned char *)header;
}

unsigned char *find_pseudoheader(struct dns_header *header, size_t plen, size_t  *len, unsigned char **p, int *is_sign)
{
  /* See if packet has an RFC2671 pseudoheader, and if so return a pointer to it. 
     also return length of pseudoheader in *len and pointer to the UDP size in *p
     Finally, check to see if a packet is signed. If it is we cannot change a single bit before
     forwarding. We look for SIG and TSIG in the addition section, and TKEY queries (for GSS-TSIG) */
  
  int i, arcount = ntohs(header->arcount);
  unsigned char *ansp = (unsigned char *)(header+1);
  unsigned short rdlen, type, class;
  unsigned char *ret = NULL;

  if (is_sign)
    {
      *is_sign = 0;

      if (OPCODE(header) == QUERY)
	{
	  for (i = ntohs(header->qdcount); i != 0; i--)
	    {
	      if (!(ansp = skip_name(ansp, header, plen, 4)))
		return NULL;
	      
	      GETSHORT(type, ansp); 
	      GETSHORT(class, ansp);
	      
	      if (class == C_IN && type == T_TKEY)
		*is_sign = 1;
	    }
	}
    }
  else
    {
      if (!(ansp = skip_questions(header, plen)))
	return NULL;
    }
    
  if (arcount == 0)
    return NULL;
  
  if (!(ansp = skip_section(ansp, ntohs(header->ancount) + ntohs(header->nscount), header, plen)))
    return NULL; 
  
  for (i = 0; i < arcount; i++)
    {
      unsigned char *save, *start = ansp;
      if (!(ansp = skip_name(ansp, header, plen, 10)))
	return NULL; 

      GETSHORT(type, ansp);
      save = ansp;
      GETSHORT(class, ansp);
      ansp += 4; /* TTL */
      GETSHORT(rdlen, ansp);
      if (!ADD_RDLEN(header, ansp, plen, rdlen))
	return NULL;
      if (type == T_OPT)
	{
	  if (len)
	    *len = ansp - start;
	  if (p)
	    *p = save;
	  ret = start;
	}
      else if (is_sign && 
	       i == arcount - 1 && 
	       class == C_ANY && 
	       (type == T_SIG || type == T_TSIG))
	*is_sign = 1;
    }
  
  return ret;
}

struct macparm {
  unsigned char *limit;
  struct dns_header *header;
  size_t plen;
  union mysockaddr *l3;
};

static int filter_mac(int family, char *addrp, char *mac, size_t maclen, void *parmv)
{
  struct macparm *parm = parmv;
  int match = 0;
  unsigned short rdlen;
  struct dns_header *header = parm->header;
  unsigned char *lenp, *datap, *p;
  
  if (family == parm->l3->sa.sa_family)
    {
      if (family == AF_INET && memcmp (&parm->l3->in.sin_addr, addrp, INADDRSZ) == 0)
	match = 1;
#ifdef HAVE_IPV6
      else
	if (family == AF_INET6 && memcmp (&parm->l3->in6.sin6_addr, addrp, IN6ADDRSZ) == 0)
	  match = 1;
#endif
    }
 
  if (!match)
    return 1; /* continue */
  
  if (ntohs(header->arcount) == 0)
    {
      /* We are adding the pseudoheader */
      if (!(p = skip_questions(header, parm->plen)) ||
	  !(p = skip_section(p, 
			     ntohs(header->ancount) + ntohs(header->nscount), 
			     header, parm->plen)))
	return 0;
      *p++ = 0; /* empty name */
      PUTSHORT(T_OPT, p);
      PUTSHORT(PACKETSZ, p); /* max packet length - is 512 suitable default for non-EDNS0 resolvers? */
      PUTLONG(0, p);    /* extended RCODE */
      lenp = p;
      PUTSHORT(0, p);    /* RDLEN */
      rdlen = 0;
      if (((ssize_t)maclen) > (parm->limit - (p + 4)))
	return 0; /* Too big */
      header->arcount = htons(1);
      datap = p;
    }
  else
    {
      int i, is_sign;
      unsigned short code, len;
      
      if (ntohs(header->arcount) != 1 ||
	  !(p = find_pseudoheader(header, parm->plen, NULL, NULL, &is_sign)) ||
	  is_sign ||
	  (!(p = skip_name(p, header, parm->plen, 10))))
	return 0;
      
      p += 8; /* skip UDP length and RCODE */
      
      lenp = p;
      GETSHORT(rdlen, p);
      if (!CHECK_LEN(header, p, parm->plen, rdlen))
	return 0; /* bad packet */
      datap = p;

      /* check if option already there */
      for (i = 0; i + 4 < rdlen; i += len + 4)
	{
	  GETSHORT(code, p);
	  GETSHORT(len, p);
	  if (code == EDNS0_OPTION_MAC)
	    return 0;
	  p += len;
	}
      
      if (((ssize_t)maclen) > (parm->limit - (p + 4)))
	return 0; /* Too big */
    }
  
  PUTSHORT(EDNS0_OPTION_MAC, p);
  PUTSHORT(maclen, p);
  memcpy(p, mac, maclen);
  p += maclen;  

  PUTSHORT(p - datap, lenp);
  parm->plen = p - (unsigned char *)header;
  
  return 0; /* done */
}	      
     

size_t add_mac(struct dns_header *header, size_t plen, char *limit, union mysockaddr *l3)
{
  struct macparm parm;
     
/* Must have an existing pseudoheader as the only ar-record, 
   or have no ar-records. Must also not be signed */
   
  if (ntohs(header->arcount) > 1)
    return plen;

  parm.header = header;
  parm.limit = (unsigned char *)limit;
  parm.plen = plen;
  parm.l3 = l3;

  iface_enumerate(AF_UNSPEC, &parm, filter_mac);
  
  return parm.plen; 
}

/*same subnet with lan ip.*/
int is_same_subnet_with_lan(struct in_addr addr)
{
	int ret = 0;
	if(((daemon->if_ipaddr & daemon->if_netmask) == (addr.s_addr & daemon->if_netmask)) && (daemon->if_ipaddr != addr.s_addr))
	{
		ret = 1;
	}
	return ret;
}

/* is addr in the non-globally-routed IP space? */ 
static int private_net(struct in_addr addr, int ban_localhost) 
{
	int is_same_with_lan;

	is_same_with_lan = is_same_subnet_with_lan(addr);
	/*cprintf("is_same_with_lan=%d\n",is_same_with_lan);*/
 
	in_addr_t ip_addr = ntohl(addr.s_addr);

#if 1
  return
    (((ip_addr & 0xFF000000) == 0x7F000000) && ban_localhost)  /* 127.0.0.0/8    (loopback) */ || 
    (is_same_with_lan == 1)  /* is same subnet with lan */ ||
    ((ip_addr & 0xFFFF0000) == 0xC0A80000)  /* 192.168.0.0/16 (private)  */ ||
    ((ip_addr & 0xFF000000) == 0x0A000000)  /* 10.0.0.0/8     (private)  */ ||
    ((ip_addr & 0xFFF00000) == 0xAC100000)  /* 172.16.0.0/12  (private)  */ ||
    ((ip_addr & 0xFFFF0000) == 0xA9FE0000)  /* 169.254.0.0/16 (zeroconf) */ ;
#else
  return
    (((ip_addr & 0xFF000000) == 0x7F000000) && ban_localhost)  /* 127.0.0.0/8    (loopback) */ || 
    ((ip_addr & 0xFFFF0000) == 0xC0A80000)  /* 192.168.0.0/16 (private)  */ ||
    ((ip_addr & 0xFF000000) == 0x0A000000)  /* 10.0.0.0/8     (private)  */ ||
    ((ip_addr & 0xFFF00000) == 0xAC100000)  /* 172.16.0.0/12  (private)  */ ||
    ((ip_addr & 0xFFFF0000) == 0xA9FE0000)  /* 169.254.0.0/16 (zeroconf) */ ;
#endif
}

/* is addr in the non-globally-routed IP space? */ 
static int private_net6(struct in6_addr addr) 
{
	__be32 st = addr.s6_addr32[0];

	if ((st & htonl(0xFFC00000)) == htonl(0xFE800000)) //link-local address(FE80::/10)
	{
		return 1;
	}
	if ((st & htonl(0xFFC00000)) == htonl(0xFEC00000)) //site-local address(FEC0::/10)
	{
		return 1;
	}
	/* [RFC5156] 2.5.  Unique-Local
	
	   fc00::/7 are the unique-local addresses [RFC4193].  Addresses within
	   this block should not appear by default on the public Internet.
	   Procedures for advertising these addresses are further described in
	   [RFC4193].
	*/
	if ((st & htonl(0xFE000000)) == htonl(0xFC000000)) //unique local address
	{
		return 1;
	}

	return 0;
}

static unsigned char *do_doctor(unsigned char *p, int count, struct dns_header *header, size_t qlen, char *name)
{
  int i, qtype, qclass, rdlen;

  for (i = count; i != 0; i--)
    {
      if (name && option_bool(OPT_LOG))
	{
	  if (!extract_name(header, qlen, &p, name, 1, 10))
	    return 0;
	}
      else if (!(p = skip_name(p, header, qlen, 10)))
	return 0; /* bad packet */
      
      GETSHORT(qtype, p); 
      GETSHORT(qclass, p);
      p += 4; /* ttl */
      GETSHORT(rdlen, p);
      
      if (qclass == C_IN && qtype == T_A)
	{
	  struct doctor *doctor;
	  struct in_addr addr;
	  
	  if (!CHECK_LEN(header, p, qlen, INADDRSZ))
	    return 0;
	  
	  /* alignment */
	  memcpy(&addr, p, INADDRSZ);
	  
	  for (doctor = daemon->doctors; doctor; doctor = doctor->next)
	    {
	      if (doctor->end.s_addr == 0)
		{
		  if (!is_same_net(doctor->in, addr, doctor->mask))
		    continue;
		}
	      else if (ntohl(doctor->in.s_addr) > ntohl(addr.s_addr) || 
		       ntohl(doctor->end.s_addr) < ntohl(addr.s_addr))
		continue;
	      
	      addr.s_addr &= ~doctor->mask.s_addr;
	      addr.s_addr |= (doctor->out.s_addr & doctor->mask.s_addr);
	      /* Since we munged the data, the server it came from is no longer authoritative */
	      header->hb3 &= ~HB3_AA;
	      memcpy(p, &addr, INADDRSZ);
	      break;
	    }
	}
      else if (qtype == T_TXT && name && option_bool(OPT_LOG))
	{
	  unsigned char *p1 = p;
	  if (!CHECK_LEN(header, p1, qlen, rdlen))
	    return 0;
	  while ((p1 - p) < rdlen)
	    {
	      unsigned int i, len = *p1;
	      unsigned char *p2 = p1;
	      /* make counted string zero-term  and sanitise */
	      for (i = 0; i < len; i++)
		if (isprint(*(p2+1)))
		  {
		    *p2 = *(p2+1);
		    p2++;
		  }
	      *p2 = 0;
	      my_syslog(LOG_INFO, "reply %s is %s", name, p1);
	      /* restore */
	      memmove(p1 + 1, p1, len);
	      *p1 = len;
	      p1 += len+1;
	    }
	}		  
      
      if (!ADD_RDLEN(header, p, qlen, rdlen))
	 return 0; /* bad packet */
    }
  
  return p; 
}

static int find_soa(struct dns_header *header, size_t qlen, char *name)
{
  unsigned char *p;
  int qtype, qclass, rdlen;
  unsigned long ttl, minttl = ULONG_MAX;
  int i, found_soa = 0;
  
  /* first move to NS section and find TTL from any SOA section */
  if (!(p = skip_questions(header, qlen)) ||
      !(p = do_doctor(p, ntohs(header->ancount), header, qlen, name)))
    return 0;  /* bad packet */
  
  for (i = ntohs(header->nscount); i != 0; i--)
    {
      if (!(p = skip_name(p, header, qlen, 10)))
	return 0; /* bad packet */
      
      GETSHORT(qtype, p); 
      GETSHORT(qclass, p);
      GETLONG(ttl, p);
      GETSHORT(rdlen, p);
      
      if ((qclass == C_IN) && (qtype == T_SOA))
	{
	  found_soa = 1;
	  if (ttl < minttl)
	    minttl = ttl;

	  /* MNAME */
	  if (!(p = skip_name(p, header, qlen, 0)))
	    return 0;
	  /* RNAME */
	  if (!(p = skip_name(p, header, qlen, 20)))
	    return 0;
	  p += 16; /* SERIAL REFRESH RETRY EXPIRE */
	  
	  GETLONG(ttl, p); /* minTTL */
	  if (ttl < minttl)
	    minttl = ttl;
	}
      else if (!ADD_RDLEN(header, p, qlen, rdlen))
	return 0; /* bad packet */
    }
  
  /* rewrite addresses in additioal section too */
  if (!do_doctor(p, ntohs(header->arcount), header, qlen, NULL))
    return 0;
  
  if (!found_soa)
    minttl = daemon->neg_ttl;

  return minttl;
}

/* Note that the following code can create CNAME chains that don't point to a real record,
   either because of lack of memory, or lack of SOA records.  These are treated by the cache code as 
   expired and cleaned out that way. 
   Return 1 if we reject an address because it look like part of dns-rebinding attack. */
int extract_addresses(struct dns_header *header, size_t qlen, char *name, time_t now, 
		      int is_sign, int check_rebind, int checking_disabled)
{
  unsigned char *p, *p1, *endrr, *namep, *tmp;
  int i, j, qtype, qclass, aqtype, aqclass, ardlen, res, searched_soa = 0;
  unsigned long ttl = 0;
  struct all_addr addr;
  int flags = RCODE(header) == NXDOMAIN ? F_NXDOMAIN : 0;
  int found = 0, cname_count = 5, name_encoding, addrlen;
  //struct crec *cpp = NULL;
  unsigned long cttl = ULONG_MAX, attl;
  unsigned char buf[MAX_RX_BUF];
  buf_info_p binfo;
  site_info_t sinfo;
  unsigned char *pbuf;
  int length = 0, send_flag = 0;
  char addr_buf[64];  
  char name_orig[LEN_SITENAME];
  char mx_names[LEN_MX_NAMES];
  char word[LEN_SITENAME], *nxt;

	/* mark_chen 2012.11.26, skip the truncated message */
	/* Don't put stuff from a truncated packet into the cache,
	also don't cache replies where DNSSEC validation was turned off, either
	the upstream server told us so, or the original query specified it. */
	if ((header->hb3 & HB3_TC) || (header->hb4 & HB4_CD) || checking_disabled)
		return 0; /* no need to cache */
	/* end mark_chen 2012.11.26 */
	
	siteblock_dbg("line %d,  name[%s]\n", __LINE__, name);
	memset(&sinfo, 0, sizeof(sinfo));
	memset(&name_orig, 0, sizeof(name_orig));
	memset(&mx_names, 0, sizeof(mx_names));
	cache_start_insert();

	p = (unsigned char *)(header+1);
	
	/* find_soa is needed for dns_doctor and logging side-effects, so don't call it lazily if there are any. */
	if (daemon->doctors || option_bool(OPT_LOG))
	{
		searched_soa = 1;
		ttl = find_soa(header, qlen, name);
	}

	/* go through the questions. */
	// mark_chen 2012.12.08, try to cache all the answer
	for (i = ntohs(header->qdcount); i != 0; i--)
	{      
		namep = p;
		if (!extract_name(header, qlen, &p, name, 1, 4))
			return 0; /* bad packet */
           
		GETSHORT(qtype, p); 
		GETSHORT(qclass, p);
      
		if (qclass != C_IN)
			continue;

		/* For DT W724 to cache 'no such name' */
		#if 0
		if(flags & F_NXDOMAIN)
		{
			if (!searched_soa)
			{
				searched_soa = 1;
				ttl = find_soa(header, qlen, NULL);
				if(ttl)
					cache_insert(name, NULL, now, ttl, F_NXDOMAIN1 | F_FORWARD, NULL, NULL, NULL, NULL, qtype);
			}
		}
		#endif
		if (!(p1 = skip_questions(header, qlen)))
			return 0; /* bad packet */

		for (j = ntohs(header->ancount); j != 0; j--)
		{
			tmp = p1;
			if (!extract_name(header, qlen, &p1, name, 1, 0) ||
					!(res = extract_name(header, qlen, &tmp, name, 0, 10)))
				return 0; /* bad packet */

			GETSHORT(aqtype, p1); 
			GETSHORT(aqclass, p1);
			GETLONG(attl, p1);
			if((daemon->max_ttl != 0) && (attl > daemon->max_ttl) && !is_sign)
			{
				(p1) -= 4;
				PUTLONG(daemon->max_ttl, p1);
			}
			GETSHORT(ardlen, p1);
			endrr = p1+ardlen;

			// mark_chen 2012.12.08, no need to check the cache again
			if(i != ntohs(header->qdcount))
			{
				p1 = endrr;
				continue;
			}

			if(aqclass == C_IN && res != 2)
			{
				// mark_chen 2012.12.08, get the answer for query
				if(qtype == T_ANY || qtype == aqtype)
					found = 1;

				if(aqtype == T_PTR)
				{
#if 0
					name_encoding = in_arpa_name_2_addr(name, &addr);

					if (!name_encoding)
						continue;

					/* TTL of record is minimum of CNAMES and PTR */
					if (attl < cttl)
						cttl = attl;

					if (!extract_name(header, qlen, &p1, name, 1, 0))
						return 0; /* bad packet */

					cache_insert(name, &addr, now, cttl, name_encoding | F_REVERSE, NULL, NULL, NULL, NULL, 0);
#endif
				}
				else if(aqtype == T_CNAME)
				{
				  	/* mark_chen 2012.11.25, move the cache cache to the crec cache */
			  		struct cname cn;

					/* TTL of record is minimum of CNAMES and PTR */
					if (attl < cttl)
						cttl = attl;

					//if (!cname_count--)
					//	return 0;
					
					memset(&cn, 0, sizeof(cn));

					cn.target = strdup(name);

					if (!extract_name(header, qlen, &p1, name, 1, 0))
					{
						free_cname_record(&cn);
						return 0; /* bad packet */
					}

					cn.alias = strdup(name);
					
					siteblock_dbg("line %d, aqtype == T_CNAME,  cn.target[%s], cn.alias[%s]\n", __LINE__, cn.target, cn.alias);
					if(strlen(name_orig) == 0)
					{
						strcpy(name_orig, cn.target);						
						siteblock_dbg("line %d, aqtype == T_CNAME,  name_orig[%s]\n", __LINE__, name_orig);
					}

					cache_insert(cn.target, NULL, now, attl, flags | F_CNAME1 | F_FORWARD, NULL, NULL, NULL, &cn, 0);
				  	/* end mark_chen 2012.11.25 */
				}
				else if(aqtype == T_SRV)
				{
					struct mx_srv_record srv;

					memset(&srv, 0, sizeof(srv));

					GETSHORT(srv.priority, p1);
					GETSHORT(srv.weight, p1);
					GETSHORT(srv.srvport, p1);
		      
					srv.name = strdup(name);

					if (!extract_name(header, qlen, &p1, name, 1, 0))
					{
						free_mx_srv_record(&srv);
						return 0; /* bad packet */
					}
					srv.target = strdup(name);

					srv.offset = 0;
					srv.next = NULL;
					srv.issrv = 1;

					cache_insert(srv.name, NULL, now, attl, flags | F_SRV | F_FORWARD, &srv, NULL, NULL, NULL, 0);
				}
				else if(aqtype == T_MX)
				{
					struct mx_srv_record srv;

					memset(&srv, 0, sizeof(srv));

					GETSHORT(srv.weight, p1);
		      
					srv.name = strdup(name);

					if (!extract_name(header, qlen, &p1, name, 1, 0))
					{
						free_mx_srv_record(&srv);
						return 0; /* bad packet */
					}
					srv.target = strdup(name);

					srv.offset = 0;
					srv.next = NULL;
					srv.issrv = 0;
					
					if(strlen(mx_names) == 0)
						strcpy(mx_names, srv.target);
					else if((strlen(mx_names)+strlen(srv.target)) < LEN_MX_NAMES)
						sprintf(mx_names+strlen(mx_names), " %s", srv.target);
					siteblock_dbg("line %d, aqtype == T_MX,	mx.name[%s], mx.target[%s]\n", __LINE__, srv.name, srv.target);
					if(strlen(name_orig) == 0)
					{
						strcpy(name_orig, srv.name);						
						siteblock_dbg("line %d, aqtype == T_MX,	name_orig[%s]\n", __LINE__, name_orig);
					}

					cache_insert(srv.name, NULL, now, attl, flags | F_MX | F_FORWARD, &srv, NULL, NULL, NULL, 0);
				}
				else if(aqtype == T_TXT)
				{
					struct txt_record txt;

					if(ardlen > TXT_CACHE_MAX_SIZE) //do not cache invalid answer
						return 0;

					memset(&txt, 0, sizeof(txt));

					txt.name = strdup(name);
					txt.len = ardlen;
					txt.class = C_IN;
					txt.txt = (unsigned char *)opt_malloc(ardlen);
					memcpy(txt.txt, p1, ardlen);

					cache_insert(txt.name, NULL, now, attl, flags | F_TXT | F_FORWARD, NULL, NULL, &txt, NULL, 0);
				}
				else if(aqtype == T_NAPTR)
				{
					struct naptr naptr;
					int len;

					memset(&naptr, 0, sizeof(naptr));
		      
					GETSHORT(naptr.order, p1);
					GETSHORT(naptr.pref, p1);
      
					naptr.name = strdup(name);
    
					#define getstring(dest) \
					len = *p1; \
					dest = malloc(len + 1); \
					memcpy(dest, p1+1, len); \
					dest[len] = '\0'; \
					p1 += len + 1;

					getstring(naptr.flags);
					getstring(naptr.services);
					getstring(naptr.regexp);
					#undef getstring

					if (!extract_name(header, qlen, &p1, name, 1, 0))
					{
						free_naptr(&naptr);
						return 0; /* bad packet */
					}
					naptr.replace = strdup(name);

					cache_insert(naptr.name, NULL, now, attl, flags | F_NAPTR | F_FORWARD, NULL, &naptr, NULL, NULL, 0);
				}
				else
				{ /* other type answer */
					if (aqtype == T_A)
					{
						addrlen = INADDRSZ;
						flags |= F_IPV4;
					}
#ifdef HAVE_IPV6
					else if (aqtype == T_AAAA)
					{
						addrlen = IN6ADDRSZ;
						flags |= F_IPV6;
					}
#endif
					else
					{
						p1 = endrr;
						continue;
					}

					/* copy address into aligned storage */
					if (!CHECK_LEN(header, p1, qlen, addrlen))
						return 0; /* bad packet */
					memcpy(&addr, p1, addrlen);

					/* check for returned address in private space */
					if (check_rebind && (flags & F_IPV4) &&
							private_net(addr.addr.addr4, !option_bool(OPT_LOCAL_REBIND)))
						return 1;
					if (check_rebind && (flags & F_IPV6) &&
							private_net6(addr.addr.addr6))
						return 1;

					cache_insert(name, &addr, now, attl, flags | F_FORWARD, NULL, NULL, NULL, NULL, 0);
					
					/* Prepare the domain and address info for arc_site_blocking process. */
					send_flag = 1;
					if(strlen(name_orig) == 0)
					{
						strcpy(name_orig, name);						
						siteblock_dbg("line %d, aqtype == A/AAAA,  name_orig[%s]\n", __LINE__, name_orig);
					}
					if(addrlen == IN6ADDRSZ)
					{
						inet_ntop(AF_INET6, &(addr.addr.addr6), addr_buf, sizeof(addr_buf));
						siteblock_dbg("line %d,  name[%s] IPv6 addr:[%s]\n", __LINE__, name, addr_buf);
						if(strlen(sinfo.ipv6_address) == 0)
							strcpy(sinfo.ipv6_address, addr_buf);
						else if((strlen(sinfo.ipv6_address)+strlen(addr_buf)) < LEN_SITEADDR)
							sprintf(sinfo.ipv6_address+strlen(sinfo.ipv6_address), " %s", addr_buf);
					}
					else
					{
						inet_ntop(AF_INET, &(addr.addr.addr4), addr_buf, sizeof(addr_buf));
						siteblock_dbg("line %d,  name[%s] IPv4 addr:[%s]\n", __LINE__, name, addr_buf);
						if(strlen(sinfo.ipv4_address) == 0)
							strcpy(sinfo.ipv4_address, addr_buf);
						else if((strlen(sinfo.ipv4_address)+strlen(addr_buf)) < LEN_SITEADDR)
							sprintf(sinfo.ipv4_address+strlen(sinfo.ipv4_address), " %s", addr_buf);
					}
				}
			}
		  
			p1 = endrr;
			if (!CHECK_LEN(header, p1, qlen, 0))
				return 0; /* bad packet */
		}
			
		if (!found && !option_bool(OPT_NO_NEG))
		{
			if (!searched_soa)
			{
				searched_soa = 1;
				ttl = find_soa(header, qlen, NULL);
			}
			/* If there's no SOA to get the TTL from, inherit its TTL */
			if (ttl)
			{
				// mark_chen 2012.12.08, cache the record with no answer
				if(qtype == T_PTR)
				{
#if 0
					name_encoding = in_arpa_name_2_addr(name, &addr);

					if (!name_encoding)
						continue;
					cache_insert(NULL, &addr, now, ttl, name_encoding | F_REVERSE | F_NEG | flags, NULL, NULL, NULL, NULL, 0);
#endif
				}
				else
				{
					if(qtype == T_CNAME)
						flags |= F_CNAME1;
					else if(qtype == T_SRV)
						flags |= F_SRV;
					else if(qtype == T_TXT)
						flags |= F_TXT;
					else if(qtype == T_MX)
						flags |= F_MX;
					else if(qtype == T_NAPTR)
						flags |= F_NAPTR;
					else if(qtype == T_A)
						flags |= F_IPV4;
					else if(qtype == T_AAAA)
						flags |= F_IPV6;
					else
						flags |= F_Other;
						
					cache_insert(name, NULL, now, ttl ? ttl : cttl, F_FORWARD | F_NEG | flags, NULL, NULL, NULL, NULL, qtype);
				}
			}
		}
    	}

    	/* mark_chen 2012.11.20, cache the additional records */
    	if(ntohs(header->arcount) > 0)
    	{
    		if(ntohs(header->nscount) > 0)
    			p1 = skip_section(p1, ntohs(header->nscount), header, qlen);

    		for (j = ntohs(header->arcount); j != 0; j--) 
    		{
    			flags = 0;
    			tmp = p1;
    			if (!extract_name(header, qlen, &p1, name, 1, 0) ||
    					!(res = extract_name(header, qlen, &tmp, name, 0, 10)))
    				return 0; /* bad packet */

    			GETSHORT(aqtype, p1); 
    			GETSHORT(aqclass, p1);
    			GETLONG(attl, p1);
    			GETSHORT(ardlen, p1);
    			endrr = p1+ardlen;

    			if (aqtype == T_A)
    			{
    				addrlen = INADDRSZ;
    				flags |= F_IPV4;
    			}
#ifdef HAVE_IPV6
    			else if (aqtype == T_AAAA)
    			{
    				addrlen = IN6ADDRSZ;
    				flags |= F_IPV6;
    			}
#endif
    			else
    			{
    				p1 = endrr;
    				continue;
    			}


    			if (aqclass == C_IN && res != 2)
    			{			  
    				/* copy address into aligned storage */
    				if (!CHECK_LEN(header, p1, qlen, addrlen))
    					return 0; /* bad packet */
    				memcpy(&addr, p1, addrlen);
			  
    				/* check for returned address in private space */
    				if (check_rebind && (flags & F_IPV4) &&
    						private_net(addr.addr.addr4, !option_bool(OPT_LOCAL_REBIND)))
    					return 1;
    				if (check_rebind && (flags & F_IPV6) &&
    						private_net6(addr.addr.addr6))
    					return 1;
    				// mark_chen 2012.11.25, for moving the cname & txt cache to the crec cache
    				cache_insert(name, &addr, now, attl, flags | F_FORWARD, NULL, NULL, NULL, NULL, 0);
    				foreach(word, mx_names, nxt)
    				{
    					if(strcmp(word, name) == 0)
    					{
    						send_flag = 1;
    						if(addrlen == IN6ADDRSZ)
    						{
    							inet_ntop(AF_INET6, &(addr.addr.addr6), addr_buf, sizeof(addr_buf));
    							siteblock_dbg("line %d,  name[%s] IPv6 addr:[%s]\n", __LINE__, name, addr_buf);
    							if(strlen(sinfo.ipv6_address) == 0)
    								strcpy(sinfo.ipv6_address, addr_buf);
    							else if((strlen(sinfo.ipv6_address)+strlen(addr_buf)) < LEN_SITEADDR)
    								sprintf(sinfo.ipv6_address+strlen(sinfo.ipv6_address), " %s", addr_buf);
    						}
    						else
    						{
    							inet_ntop(AF_INET, &(addr.addr.addr4), addr_buf, sizeof(addr_buf));
    							siteblock_dbg("line %d,  name[%s] IPv4 addr:[%s]\n", __LINE__, name, addr_buf);
    							if(strlen(sinfo.ipv4_address) == 0)
    								strcpy(sinfo.ipv4_address, addr_buf);
    							else if((strlen(sinfo.ipv4_address)+strlen(addr_buf)) < LEN_SITEADDR)
    								sprintf(sinfo.ipv4_address+strlen(sinfo.ipv4_address), " %s", addr_buf);
    						}
    						break;						
    					}
    				}
    			}

    			p1 = endrr;
    			if (!CHECK_LEN(header, p1, qlen, 0))
    				return 0; /* bad packet */
    		}
    	}
    	/* end mark_chen 2012.11.20 */

	/* Send the domain and address info to arc_site_blocking process. */
	if(send_flag)
	{
		if(dnsmasq_AbuseRule_lookup(daemon->AbuseRule_list, name_orig) >= 0)
		{
			binfo = (buf_info_p)buf;
			binfo->next_pid = 0;
			binfo->seq = 0;
			binfo->have_more = 0;
			pbuf = (addresses_info_p)binfo->buf;

			strcpy(pbuf, name_orig);
			if(strlen(sinfo.ipv4_address))
				sprintf(pbuf+strlen(pbuf), " 4 %d %s", strlen(sinfo.ipv4_address), sinfo.ipv4_address);
			if(strlen(sinfo.ipv6_address))
				sprintf(pbuf+strlen(pbuf), " 6 %d %s", strlen(sinfo.ipv6_address), sinfo.ipv6_address);
			length = BUF_INFO_SZ + strlen(pbuf);
			siteblock_dbg("line %d, length[%d], pbuf[%s].\n", __LINE__, length, pbuf);
			send_to_site_server_via_unix_socket(buf, length);
			memset(&buf, 0, length);
		}
	}

  /* Don't put stuff from a truncated packet into the cache,
     also don't cache replies where DNSSEC validation was turned off, either
     the upstream server told us so, or the original query specified it. */
  if (!(header->hb3 & HB3_TC) && !(header->hb4 & HB4_CD) && !checking_disabled)
    cache_end_insert();

  return 0;
}

/* If the packet holds exactly one query
   return F_IPV4 or F_IPV6  and leave the name from the query in name */

unsigned int extract_request(struct dns_header *header, size_t qlen, char *name, unsigned short *typep)
{
  unsigned char *p = (unsigned char *)(header+1);
  int qtype, qclass;

  if (typep)
    *typep = 0;

  if (ntohs(header->qdcount) != 1 || OPCODE(header) != QUERY)
    return 0; /* must be exactly one query. */
  
  if (!extract_name(header, qlen, &p, name, 1, 4))
    return 0; /* bad packet */
   
  GETSHORT(qtype, p); 
  GETSHORT(qclass, p);

  if (typep)
    *typep = qtype;

  if (qclass == C_IN)
    {
      if (qtype == T_A)
	return F_IPV4;
      if (qtype == T_AAAA)
	return F_IPV6;
      if (qtype == T_ANY)
	return  F_IPV4 | F_IPV6;
    }
  
  return F_QUERY;
}


size_t setup_reply(struct dns_header *header, size_t qlen,
		struct all_addr *addrp, unsigned int flags, unsigned long ttl)
{
  unsigned char *p;

  if (!(p = skip_questions(header, qlen)))
    return 0;
  
  /* clear authoritative and truncated flags, set QR flag */
  header->hb3 = (header->hb3 & ~(HB3_AA | HB3_TC)) | HB3_QR;
  /* set RA flag */
  header->hb4 |= HB4_RA;

  header->nscount = htons(0);
  header->arcount = htons(0);
  header->ancount = htons(0); /* no answers unless changed below */
  if (flags == F_NEG)
    SET_RCODE(header, SERVFAIL); /* couldn't get memory */
  else if (flags == F_NOERR)
    SET_RCODE(header, NOERROR); /* empty domain */
  else if (flags == F_NXDOMAIN)
    SET_RCODE(header, NXDOMAIN);
  else if (flags == F_IPV4)
    { /* we know the address */
      SET_RCODE(header, NOERROR);
      header->ancount = htons(1);
      header->hb3 |= HB3_AA;
      add_resource_record(header, NULL, NULL, sizeof(struct dns_header), &p, ttl, NULL, T_A, C_IN, "4", addrp);
    }
#ifdef HAVE_IPV6
  else if (flags == F_IPV6)
    {
      SET_RCODE(header, NOERROR);
      header->ancount = htons(1);
      header->hb3 |= HB3_AA;
      add_resource_record(header, NULL, NULL, sizeof(struct dns_header), &p, ttl, NULL, T_AAAA, C_IN, "6", addrp);
    }
#endif
  else /* nowhere to forward to */
    SET_RCODE(header, REFUSED);
 
  return p - (unsigned char *)header;
}

/* check if name matches local names ie from /etc/hosts or DHCP or local mx names. */
int check_for_local_domain(char *name, time_t now)
{
  struct crec *crecp;
  struct mx_srv_record *mx;
  struct txt_record *txt;
  struct interface_name *intr;
  struct ptr_record *ptr;
  struct naptr *naptr;

  if ((crecp = cache_find_by_name(NULL, name, now, F_IPV4 | F_IPV6 | F_CNAME)) &&
      (crecp->flags & (F_HOSTS | F_DHCP)))
    return 1;
  
  for (naptr = daemon->naptr; naptr; naptr = naptr->next)
     if (hostname_isequal(name, naptr->name))
      return 1;

   for (mx = daemon->mxnames; mx; mx = mx->next)
    if (hostname_isequal(name, mx->name))
      return 1;

  for (txt = daemon->txt; txt; txt = txt->next)
    if (hostname_isequal(name, txt->name))
      return 1;

  for (intr = daemon->int_names; intr; intr = intr->next)
    if (hostname_isequal(name, intr->name))
      return 1;

  for (ptr = daemon->ptr; ptr; ptr = ptr->next)
    if (hostname_isequal(name, ptr->name))
      return 1;
 
  return 0;
}

/* Is the packet a reply with the answer address equal to addr?
   If so mung is into an NXDOMAIN reply and also put that information
   in the cache. */
int check_for_bogus_wildcard(struct dns_header *header, size_t qlen, char *name, 
			     struct bogus_addr *baddr, time_t now)
{
  unsigned char *p;
  int i, qtype, qclass, rdlen;
  unsigned long ttl;
  struct bogus_addr *baddrp;

  /* skip over questions */
  if (!(p = skip_questions(header, qlen)))
    return 0; /* bad packet */

  for (i = ntohs(header->ancount); i != 0; i--)
    {
      if (!extract_name(header, qlen, &p, name, 1, 10))
	return 0; /* bad packet */
  
      GETSHORT(qtype, p); 
      GETSHORT(qclass, p);
      GETLONG(ttl, p);
      GETSHORT(rdlen, p);
      
      if (qclass == C_IN && qtype == T_A)
	{
	  if (!CHECK_LEN(header, p, qlen, INADDRSZ))
	    return 0;
	  
	  for (baddrp = baddr; baddrp; baddrp = baddrp->next)
	    if (memcmp(&baddrp->addr, p, INADDRSZ) == 0)
	      {
		/* Found a bogus address. Insert that info here, since there no SOA record
		   to get the ttl from in the normal processing */
		cache_start_insert();
		// mark_chen 2012.11.25, for moving the cname & txt cache to the crec cache
		cache_insert(name, NULL, now, ttl, F_IPV4 | F_FORWARD | F_NEG | F_NXDOMAIN | F_CONFIG, NULL, NULL, NULL, NULL, 0);
		cache_end_insert();
		
		return 1;
	      }
	}
      
      if (!ADD_RDLEN(header, p, qlen, rdlen))
	return 0;
    }
  
  return 0;
}

static int add_resource_record(struct dns_header *header, char *limit, int *truncp, unsigned int nameoffset, unsigned char **pp, 
			       unsigned long ttl, unsigned int *offset, unsigned short type, unsigned short class, char *format, ...)
{
  va_list ap;
  unsigned char *sav, *p = *pp;
  int j;
  unsigned short usval;
  long lval;
  char *sval;

  if (truncp && *truncp)
    return 0;

  PUTSHORT(nameoffset | 0xc000, p);
  PUTSHORT(type, p);
  PUTSHORT(class, p);
  PUTLONG(ttl, p);      /* TTL */

  sav = p;              /* Save pointer to RDLength field */
  PUTSHORT(0, p);       /* Placeholder RDLength */

  va_start(ap, format);   /* make ap point to 1st unamed argument */
  
  for (; *format; format++)
    switch (*format)
      {
#ifdef HAVE_IPV6
      case '6':
	sval = va_arg(ap, char *); 
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p -IN6ADDRSZ) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	memcpy(p, sval, IN6ADDRSZ);
	p += IN6ADDRSZ;
	break;
#endif
	
      case '4':
	sval = va_arg(ap, char *); 
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p -INADDRSZ) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	memcpy(p, sval, INADDRSZ);
	p += INADDRSZ;
	break;
	
      case 's':
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p -2) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	usval = va_arg(ap, int);
	PUTSHORT(usval, p);
	break;
	
      case 'l':
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p -4) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	lval = va_arg(ap, long);
	PUTLONG(lval, p);
	break;
	
      case 'd':
	/* get domain-name answer arg and store it in RDATA field */
	if (offset)
	  *offset = p - (unsigned char *)header;
	sval = va_arg(ap, char *);
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p - strlen(sval) - 1) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	p = do_rfc1035_name(p, sval);
	*p++ = 0;
	break;
	
      case 't':
	usval = va_arg(ap, int);
	sval = va_arg(ap, char *);
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p -usval) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	memcpy(p, sval, usval);
	p += usval;
	break;

      case 'z':
	sval = va_arg(ap, char *);
	usval = sval ? strlen(sval) : 0;
	if (usval > 255)
	  usval = 255;
	*p++ = (unsigned char)usval;
	/* check for overflow of buffer before adding data */
	  if (limit && ((unsigned char *)limit - p -usval) < 0)
	    {
	      if (truncp)
		*truncp = 1;
	      return 0;
	    }
	memcpy(p, sval, usval);
	p += usval;
	break;
      }

  va_end(ap);	/* clean up variable argument pointer */
  
  j = p - sav - 2;
  PUTSHORT(j, sav);     /* Now, store real RDLength */
  
  /* check for overflow of buffer */
  if (limit && ((unsigned char *)limit - p) < 0)
    {
      if (truncp)
	*truncp = 1;
      return 0;
    }
  
  *pp = p;
  return 1;
}

static unsigned long crec_ttl(struct crec *crecp, time_t now)
{
  /* Return 0 ttl for DHCP entries, which might change
     before the lease expires. */

  if  (crecp->flags & (F_IMMORTAL | F_DHCP))
    return daemon->local_ttl;
  
  /* Return the Max TTL value if it is lower then the actual TTL */
  if (daemon->max_ttl == 0 || ((unsigned)(crecp->ttd - now) < daemon->max_ttl))
    return crecp->ttd - now;
  else
    return daemon->max_ttl;
}
  
#define MAC_LEN 18
#define IPV6_IP6_LEN 46
#define MAC2V6LLADDR_FILE "/proc/net/mac2v6lladdr"
static int dnsmasq_get_ipv6_by_mac(const char *pMAC, char *pIP6)
{
	char mac[MAC_LEN] = "", ip6addr[IPV6_IP6_LEN];
	unsigned int jiffer;
	FILE *fp = NULL;
	int found = 0;

	memset(pIP6, 0, IPV6_IP6_LEN);

	fp = fopen(MAC2V6LLADDR_FILE, "r");

	if (fp == NULL)
		return -1;

	while(fscanf(fp, "%s %s %u\n", mac, ip6addr, &jiffer) != EOF)
	{
		if(strncmp(pMAC, mac, MAC_LEN) == 0)
		{
			strncpy(pIP6, ip6addr, IPV6_IP6_LEN);
			found = 1;
			break;
		}
	}

	fclose(fp);

	if(found)
	{
		return 0; // success
	}
	else
	{
		return -1; // fail
	}
}

/* return zero if we can't answer from cache, or packet size if we can */
size_t answer_request(struct dns_header *header, char *limit, size_t qlen,  
		      struct in_addr local_addr, struct in_addr local_netmask, time_t now) 
{
  char *name = daemon->namebuff;
  char *end_hostname = NULL, *lan_ipv4_addr;
  char lan_ipv6_addr[INET6_ADDRSTRLEN], lan_mac[20];  
  struct all_addr lan_addr;
  struct lease_t lease, tmp_lease;
  unsigned long lan_expires;
  int found_hostname = 0;
  int name_with_domain = 0, ret_ipv6_by_mac = 0;
  unsigned char *p, *ansp, *pheader;
  int qtype, qclass;
  struct all_addr addr;
  unsigned int nameoffset, offset;
  //unsigned short flag;
  unsigned int flag = 0; // mark_chen 2012.11.28, extend the flags to 4 bytes 
  int q, ans, anscount = 0, addncount = 0;
  int dryrun = 0, sec_reqd = 0, pseudo = 0, found = 0;
  int is_sign;
  int i = 0;
  struct crec *crecp = NULL, *cname_crecp[daemon->cachesize + 3];
  int nxdomain = 0, auth = 1, trunc = 0, localise = 0;
  //int cname_count = 5;
  int cname_count = daemon->cachesize, cnamei = 0, cnamej = 0, cname_loop = 0;
  struct mx_srv_record *rec;
  FILE *fp;
 
  /* If there is an RFC2671 pseudoheader then it will be overwritten by
     partial replies, so we have to do a dry run to see if we can answer
     the query. We check to see if the do bit is set, if so we always
     forward rather than answering from the cache, which doesn't include
     security information. */

  if (find_pseudoheader(header, qlen, NULL, &pheader, &is_sign))
    { 
      unsigned short udpsz, flags;
      unsigned char *psave = pheader;

      GETSHORT(udpsz, pheader);
      pheader += 2; /* ext_rcode */
      GETSHORT(flags, pheader);
      
      sec_reqd = flags & 0x8000; /* do bit */ 

      /* If our client is advertising a larger UDP packet size
	 than we allow, trim it so that we don't get an overlarge
	 response from upstream */

      if (!is_sign && (udpsz > daemon->edns_pktsz))
	PUTSHORT(daemon->edns_pktsz, psave); 

	  /* mark_chen 2012.12.06, support edns reply to client.
	  	if it's udp dns query, needs to check the buffer size;
	  	if tcp, no need. */
	  if((limit - (char *) header) < daemon->edns_pktsz)
	  {
	  	if(udpsz > daemon->edns_pktsz)
	  		limit = (char *) header + daemon->edns_pktsz;
		  else
		  	limit = (char *) header + udpsz;
	  }
	  
	  pseudo = 1;
	  /* end mark_chen 2012.12.06 */

      dryrun = 1;
    }

  if (ntohs(header->qdcount) == 0 || OPCODE(header) != QUERY )
    return 0;
  
  for (rec = daemon->mxnames; rec; rec = rec->next)
    rec->offset = 0;
  
 rerun:
  /* determine end of question section (we put answers there) */
  if (!(ansp = skip_questions(header, qlen)))
    return 0; /* bad packet */
   
  /* now process each question, answers go in RRs after the question */
  p = (unsigned char *)(header+1);

  for (q = ntohs(header->qdcount); q != 0; q--)
    {
      /* save pointer to name for copying into answers */
      nameoffset = p - (unsigned char *)header;

      /* now extract name as .-concatenated string into name */
      if (!extract_name(header, qlen, &p, name, 1, 4))
	return 0; /* bad packet */
            
      GETSHORT(qtype, p); 
      GETSHORT(qclass, p);

      ans = 0; /* have we answered this question */

	  /* guang_zhao 20121205, Filter the name contains "speedport.ip" 
	   * and dt_dhcpd_domain such as ".Speedport_W_724V_01011601_00_303",
	   * If the filtered name can be found in lan dhcp table, will do mini dns, 
	   * If not found, will reply nxdomain directly. 
	   */
	 /* Sean 20161227, W925 domain name is "speedport.ip" and dt_dhcpd_domain is "speedport.ip".
	  *  The filtered name should not use 'break'. It shouldl be looked in the lan dhcp table.
	  */
	   
	if(strcasecmp(name, LOCAL_DNS_DOMAIN_NAME_PREFIX) == 0) //response "speedport.ip" directly.
	{
		if(qtype == T_A || qtype == T_AAAA || qtype == T_ANY)
		{			
			ans = 1;
			if(qtype == T_A || qtype == T_ANY)
			{							
				inet_aton(lan_ip4addr, &lan_addr.addr.addr4);					
				lan_addr.addr.addr4 = ntohl(lan_addr.addr.addr4);
				if(add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
					0, NULL, T_A, C_IN, "4", &lan_addr))
					anscount++;
			}				
			if(qtype == T_AAAA || qtype == T_ANY)
			{				
				inet_pton(AF_INET6, LOCAL_IPV6_ADDR, &lan_addr.addr.addr6);
				if(add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
					0, NULL, T_AAAA, C_IN, "6", &lan_addr))
					anscount++;
			}
		}
		dryrun = 0;			
		break;
	}
#ifdef SUPPORT_LOCAL_MCJP
	else if(strcasecmp(name, daemon->mcjp_domain_name) == 0)
	{
		if(qtype == T_A || qtype == T_AAAA || qtype == T_ANY)
		{
			ans = 1;
			if(qtype == T_A || qtype == T_ANY)
			{
				inet_aton(daemon->mcjp_igmp_group, &lan_addr.addr.addr4);
				lan_addr.addr.addr4 = ntohl(lan_addr.addr.addr4);
				if(add_resource_record(header, limit, &trunc, nameoffset, &ansp,
					0, NULL, T_A, C_IN, "4", &lan_addr))
					anscount++;
			}
			if(qtype == T_AAAA || qtype == T_ANY)
			{
				inet_pton(AF_INET6, daemon->mcjp_mld_group, &lan_addr.addr.addr6);
				if(add_resource_record(header, limit, &trunc, nameoffset, &ansp,
					0, NULL, T_AAAA, C_IN, "6", &lan_addr))
					anscount++;
			}
		}
		dryrun = 0;
		break;
	}
#endif
	else
	{
		//if(strcasestr(name, LOCAL_DNS_DOMAIN_NAME_PREFIX) || strcasestr(name, SYSTEM_HOST_NAME_PREFIX))
		if(strcasestr(name, LOCAL_DNS_DOMAIN_NAME_PREFIX))
		{
			nxdomain = 1;		
			dryrun = 0;		
			//break;	//Sean 20161227, 'break' would not search in lan dhcp table!
			if(qtype!=T_A && qtype!=T_AAAA)
				break;
		}
	}

	/* guang_zhao 20160908, modify mini dns support to fix the speedhome.bridge issue.*/
	if(qtype == T_A || qtype == T_AAAA || qtype == T_ANY)
	{
		end_hostname = NULL;
		if(strchr(name, '.'))
		{
			name_with_domain = 1;
			if(daemon->dt_dhcpd_domain)
			{
				end_hostname = strcasestr(name, daemon->dt_dhcpd_domain);
				if(end_hostname) //PC name with the domain suffix.
				{
					*end_hostname = 0; // truncate the name without domain suffix
					name_with_domain = 0;
				}
			}
		}
		else
			name_with_domain = 0;
	
		memset(&lan_ipv4_addr, 0, sizeof(lan_ipv4_addr));	
		memset(&lan_ipv6_addr, 0, sizeof(lan_ipv6_addr)); 
		memset(&lan_mac, 0, sizeof(lan_mac)); 
		memset(&lease, 0, sizeof(lease)); 
		memset(&tmp_lease, 0, sizeof(tmp_lease));

		if((fp = fopen("/tmp/udhcpd.leases", "r"))) 
		{
			/* guang_zhao 20130105, search and find the lan pc name from dhcpd table and ipv6 neigh table.*/
			while(fread(&tmp_lease, sizeof(tmp_lease), 1, fp))
			{				
				ether_etoa(tmp_lease.chaddr, lan_mac);
				if(!strcmp(lan_mac,"00:00:00:00:00:00")) 
					continue;	

				lan_expires = ntohl(tmp_lease.expires);
				if(lan_expires == 0)
					continue;

				if(!strcasecmp(name, tmp_lease.hostname))
				{
					found_hostname++;
					memcpy(&lease, &tmp_lease, sizeof(tmp_lease));
					break;
				}
			}
			fclose(fp);
			if(found_hostname)
			{
				ans = 1;
				if(qtype == T_A || qtype == T_ANY)
				{
					lan_ipv4_addr = piaddr(lease.yiaddr);
					inet_aton(lan_ipv4_addr, &lan_addr.addr.addr4);
					lan_addr.addr.addr4 = ntohl(lan_addr.addr.addr4);
					if(add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
						lan_expires, NULL, T_A, C_IN, "4", &lan_addr))
						anscount++;
				}
				
				if(qtype == T_AAAA || qtype == T_ANY)
				{
					for(i=0; lan_mac[i]; i++) // change the mac address to all lowercase.
					{
						if(lan_mac[i] >= 'A' && lan_mac[i] <= 'Z')
							lan_mac[i] += 32;
					}

					ret_ipv6_by_mac = dnsmasq_get_ipv6_by_mac(lan_mac, lan_ipv6_addr);	//get LLA ipv6 address by mac address.
					inet_pton(AF_INET6, lan_ipv6_addr, &lan_addr.addr.addr6);
					if(ret_ipv6_by_mac == 0)
					{
						if(add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
							lan_expires, NULL, T_AAAA, C_IN, "6", &lan_addr))
							anscount++;
					}
				}
			}
		}

		/* guang_zhao 20130128, 
		  * Reference to RFC 1034, 3.1. Name space specifications and terminology.
		  * A character string that represents the starting labels of a domain name
		  * which is incomplete, and should be completed by local software using
		  * knowledge of the local domain (often called "relative").
		  * So if name_with_domain is 0, this qurey should not send to WAN.
		*/
		if(name_with_domain == 0)
		{
			if(!ans)
				nxdomain = 1;
			
			dryrun = 0;			
			break;
		}
		else
		{
			if(ans)
			{
				dryrun = 0;
				break;
			}
		}
	}
	/* end of mini dns support.*/

      if (qclass == C_IN)
	{
	  if (qtype == T_PTR || qtype == T_ANY)
	    {
	      /* see if it's w.z.y.z.in-addr.arpa format */
	      int is_arpa = in_arpa_name_2_addr(name, &addr);
	      struct ptr_record *ptr;
	      struct interface_name* intr = NULL;

	      for (ptr = daemon->ptr; ptr; ptr = ptr->next)
		if (hostname_isequal(name, ptr->name))
		  break;

	      if (is_arpa == F_IPV4)
		for (intr = daemon->int_names; intr; intr = intr->next)
		  {
		    if (addr.addr.addr4.s_addr == get_ifaddr(intr->intr).s_addr)
		      break;
		    else
		      while (intr->next && strcmp(intr->intr, intr->next->intr) == 0)
			intr = intr->next;
		  }
	      
	      if (intr)
		{
		  ans = 1;
		  if (!dryrun)
		    {
		      log_query(F_IPV4 | F_REVERSE | F_CONFIG, intr->name, &addr, NULL);
		      if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
					      daemon->local_ttl, NULL,
					      T_PTR, C_IN, "d", intr->name))
			anscount++;
		    }
		}
	      else if (ptr)
		{
		  ans = 1;
		  if (!dryrun)
		    {
		      log_query(F_CONFIG | F_RRNAME, name, NULL, "<PTR>");
		      for (ptr = daemon->ptr; ptr; ptr = ptr->next)
			if (hostname_isequal(name, ptr->name) &&
			    add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
						daemon->local_ttl, NULL,
						T_PTR, C_IN, "d", ptr->ptr))
			  anscount++;
			 
		    }
		}
	      else if ((crecp = cache_find_by_addr(NULL, &addr, now, is_arpa)))
		do 
		  { 
		    /* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
		    if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
		      continue;
		    
		    if (crecp->flags & F_NEG)
		      {
			ans = 1;
			auth = 0;
			if (crecp->flags & F_NXDOMAIN)
			  nxdomain = 1;
			if (!dryrun)
			  log_query(crecp->flags & ~F_FORWARD, name, &addr, NULL);
		      }
		    else if ((crecp->flags & (F_HOSTS | F_DHCP)) || !sec_reqd)
		      {
			ans = 1;
			if (!(crecp->flags & (F_HOSTS | F_DHCP)))
			  auth = 0;
			if (!dryrun)
			  {
			    log_query(crecp->flags & ~F_FORWARD, cache_get_name(crecp), &addr, 
				      record_source(crecp->uid));
			    
			    if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
						    crec_ttl(crecp, now), NULL,
						    T_PTR, C_IN, "d", cache_get_name(crecp)))
			      anscount++;
			  }
		      }
		  } while ((crecp = cache_find_by_addr(crecp, &addr, now, is_arpa)));
	      else 
		{
			if (is_arpa == F_IPV4 && 
			       option_bool(OPT_BOGUSPRIV) && 
			       private_net(addr.addr.addr4, 1))
			{
			  /* if not in cache, enabled and private IPV4 address, return NXDOMAIN */
			  ans = 1;
			  nxdomain = 1;
			  if (!dryrun)
			    log_query(F_CONFIG | F_REVERSE | F_IPV4 | F_NEG | F_NXDOMAIN, 
				      name, &addr, NULL);
			}
			else if (is_arpa == F_IPV6 && 
			       option_bool(OPT_BOGUSPRIV) && 
			       private_net6(addr.addr.addr6))
			{
			  /* if not in cache, enabled and private IPV6 address, return NXDOMAIN */
			  ans = 1;
			  nxdomain = 1;
			  if (!dryrun)
			    log_query(F_CONFIG | F_REVERSE | F_IPV6 | F_NEG | F_NXDOMAIN, 
				      name, &addr, NULL);
			}
	      }
        }

		cnamei = 0;
		/* mark_chen 2012.12.08, get cname first */
		cname_restart:
		if ((crecp = cache_find_by_name(NULL, name, now, flag | F_CNAME1)))
		{
			/* See if a putative address is on the network from which we recieved
				the query, is so we'll filter other answers. */
			if (local_addr.s_addr != 0 && option_bool(OPT_LOCALISE) && flag == F_IPV4)
			{
				struct crec *save = crecp;

				do {
					if ((crecp->flags & F_HOSTS) &&
						is_same_net(*((struct in_addr *)&crecp->addr), local_addr, local_netmask))
					{
						localise = 1;
						break;
					} 
				} while ((crecp = cache_find_by_name(crecp, name, now, flag | F_CNAME1)));
				crecp = save;
			}

			do
			{ 
				/* don't answer wildcard queries with data not from /etc/hosts
					or DHCP leases */
				if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
					break;
				
				if (crecp->flags & F_NEG)
				{
					if(qtype == T_CNAME)
						ans = 1;
					auth = 0;
					if (crecp->flags & F_NXDOMAIN)
						nxdomain = 1;
					if (!dryrun)
						log_query(crecp->flags, name, NULL, NULL);
					break;
				}

				struct cname* cn = &crecp->addr.cn;

				cname_crecp[cnamei] = NULL;

				if(cnamei > 0)
				{
					for(cnamej = 0; cnamej < cnamei; cnamej++)
					{
						if(cname_crecp[cnamej])
						{
							if(hostname_isequal(cache_get_name(cname_crecp[cnamej]), cn->alias))
							{
								cname_loop = 1;
								break; //break for loop
							}
						}
						else
							break; //break for loop
					}
				}

				// mark_chen 2012.12.08, get the answer for cname/ptr query
				if(qtype == T_CNAME || qtype == T_PTR)
					ans = 1;

				if (!dryrun)
				{
					if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
							 crec_ttl(crecp, now), &nameoffset,
							 T_CNAME, C_IN, "d", cn->alias))
					{
						anscount++;
					}
					else
						break; // mark_chen 2012.12.27,  break while loop if needs more data buffer  than expect 
				}
				
				strcpy(name, cn->alias);
				
				if(!cname_loop)
					cname_crecp[cnamei++] = crecp;
				else
					break; //break while loop, but there is cname loop, no soa found before
				
				if(!cname_count--) // mark_chen 2012.12.27,  force to break while loop
					return 0;

				if(qtype == T_CNAME) // mark_chen 2012.12.08, no need more for cname query
					break; //break while loop
				else
					goto cname_restart;
			} while ((crecp = cache_find_by_name(crecp, name, now, flag | F_CNAME1)));
		}

		if (qtype == T_TXT || qtype == T_ANY)
		{
			struct txt_record *t; // mark_chen 2012.11.25, roll back to the original
			found = 0; // mark_chen 2012.11.25, move the txt cache to the crec cache

			for(t = daemon->txt; t ; t = t->next)
			{
				if (t->class == qclass && hostname_isequal(name, t->name))
				{
					ans = 1;
					found = 1; // mark_chen 2012.11.25, move the txt cache to the crec cache
					if (!dryrun)
					{
						log_query(F_CONFIG | F_RRNAME, name, NULL, "<TXT>");
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
								daemon->local_ttl, NULL,
								T_TXT, t->class, "t", t->len, t->txt))
							anscount++;
					}
				}
			}
			/* mark_chen 2012.11.25, move the txt cache to the crec cache */
			if (!found)
			{
				crecp = NULL;
				while (1)
				{
					crecp = cache_find_by_name(crecp, name, now, flag | F_TXT);
					if (!crecp)
						break;

					/* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
					if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
						continue;

					if (crecp->flags & F_NEG)
					{
						ans = 1;
						auth = 0;
						if (crecp->flags & F_NXDOMAIN)
							nxdomain = 1;

						if (!dryrun)
							log_query(crecp->flags, name, NULL, NULL);

						break;
					}

					struct txt_record* txt = &crecp->addr.txt;
			  
					ans = 1;
					found = 1;
			  
					if (!dryrun)
					{
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
								crec_ttl(crecp, now), NULL,
								T_TXT, C_IN, "t", txt->len, txt->txt))
						{
							anscount++;
							log_query(F_CONFIG | F_RRNAME, name, NULL, "<TXT>");
						}
					}
				}
			}
		 /* end mark_chen 2012.11.25 */
		}

		if (qtype == T_MX || qtype == T_ANY)
		{
			found = 0;
			for (rec = daemon->mxnames; rec; rec = rec->next)
				if (!rec->issrv && hostname_isequal(name, rec->name))
				{
					ans = found = 1;
					if (!dryrun)
					{
						log_query(F_CONFIG | F_RRNAME, name, NULL, "<MX>");
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, daemon->local_ttl,
								&offset, T_MX, C_IN, "sd", rec->weight, rec->target))
						{
							anscount++;
							if (rec->target)
								rec->offset = offset;
						}
					}
				}
				
			if (!found && (option_bool(OPT_SELFMX) || option_bool(OPT_LOCALMX)) && 
					cache_find_by_name(NULL, name, now, F_HOSTS | F_DHCP))
			{ 
				ans = 1;
				if (!dryrun)
				{
					log_query(F_CONFIG | F_RRNAME, name, NULL, "<MX>");
					if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, daemon->local_ttl, NULL, 
							T_MX, C_IN, "sd", 1, option_bool(OPT_SELFMX) ? name : daemon->mxtarget))
						anscount++;
				}
			}

			/* mark_chen 2012.12.01, search the mx cache in crecp */
			if (!found)
			{
				crecp = NULL;
				while (1)
				{
					crecp = cache_find_by_name(crecp, name, now, flag | F_MX);
					if (!crecp)
						break;

					/* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
					if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
						continue;

					if (crecp->flags & F_NEG)
					{
						ans = 1;
						auth = 0;
						if (crecp->flags & F_NXDOMAIN)
							nxdomain = 1;

						if (!dryrun)
							log_query(crecp->flags, name, NULL, NULL);

						break;
					}

					struct mx_srv_record* srv = &crecp->addr.srv;
			  
					ans = 1;
					found = 1;
			  
					if (!dryrun)
					{
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, crec_ttl(crecp, now),
								//NULL, T_MX, C_IN, "sd", srv->weight, srv->target))
								&offset, T_MX, C_IN, "sd", srv->weight, srv->target))
						{
							srv->offset = offset;
							anscount++;
							log_query((crecp->flags & ~F_FORWARD) | F_RRNAME,
									cache_get_name(crecp), NULL, srv->target);
						}
					}
				}
			}
		}
	  	  
		if (qtype == T_SRV || qtype == T_ANY)
		{
			struct mx_srv_record *move = NULL, **up = &daemon->mxnames;
			found = 0;

			for (rec = daemon->mxnames; rec; rec = rec->next)
				if (rec->issrv && hostname_isequal(name, rec->name))
				{
					found = ans = 1;
					if (!dryrun)
					{
						log_query(F_CONFIG | F_RRNAME, name, NULL, "<SRV>");
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, daemon->local_ttl, 
								&offset, T_SRV, C_IN, "sssd", 
								rec->priority, rec->weight, rec->srvport, rec->target))
						{
							anscount++;
							if (rec->target)
								rec->offset = offset;
						}
					}
		    
					/* unlink first SRV record found */
					if (!move)
					{
						move = rec;
						*up = rec->next;
					}
					else
						up = &rec->next;      
				}
				else
					up = &rec->next;

			/* put first SRV record back at the end. */
			if (move)
			{
				*up = move;
				move->next = NULL;
			}

			if (!found)
			{
				crecp = NULL;
				while (1)
				{
					crecp = cache_find_by_name(crecp, name, now, flag | F_SRV);
					if (!crecp)
						break;

					/* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
					if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
						continue;

					if (crecp->flags & F_NEG)
					{
						ans = 1;
						auth = 0;
						if (crecp->flags & F_NXDOMAIN)
							nxdomain = 1;

						if (!dryrun)
							log_query(crecp->flags, name, NULL, NULL);

						break;
					}

					struct mx_srv_record* srv = &crecp->addr.srv;
					ans = 1;
					found = 1;
					if (!dryrun)
					{
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, crec_ttl(crecp, now),
								//&nameoffset, T_SRV, C_IN, "sssd",
								&offset, T_SRV, C_IN, "sssd", //mark_chen 2012.11.20, fix the 2nd srv cache error bug
								srv->priority, srv->weight, srv->srvport, srv->target))
						{
							srv->offset = offset;
							anscount++;
							log_query((crecp->flags & ~F_FORWARD) | F_RRNAME,
									cache_get_name(crecp), NULL, srv->target);
						}
					}
				}
			}
	      
			if (!found && option_bool(OPT_FILTER) && (qtype == T_SRV || (qtype == T_ANY && strchr(name, '_'))))
			{
				ans = 1;
				if (!dryrun)
				log_query(F_CONFIG | F_NEG, name, NULL, NULL);
			}
		}

		if (qtype == T_NAPTR || qtype == T_ANY)
		{
			struct naptr *na;

			for (na = daemon->naptr; na; na = na->next)
				if (hostname_isequal(name, na->name))
				{
					ans = 1;
					if (!dryrun)
					{
						log_query(F_CONFIG | F_RRNAME, name, NULL, "<NAPTR>");
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, daemon->local_ttl, 
								NULL, T_NAPTR, C_IN, "sszzzd", 
								na->order, na->pref, na->flags, na->services, na->regexp, na->replace))
							anscount++;
					}
				}
	      
			if (!ans)
			{
				crecp = NULL;
				while (1)
				{
					crecp = cache_find_by_name(crecp, name, now, flag | F_NAPTR);
					if (!crecp)
						break;

					/* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
					if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
						continue;

					if (crecp->flags & F_NEG)
					{
						ans = 1;
						auth = 0;
						if (crecp->flags & F_NXDOMAIN)
							nxdomain = 1;

						if (!dryrun)
							log_query(crecp->flags, name, NULL, NULL);

						break;
					}

					struct naptr* na = &crecp->addr.naptr;

					ans = 1;
			  
					if (!dryrun)
					{
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, crec_ttl(crecp, now),
								NULL, T_NAPTR, C_IN, "sszzzd",
								na->order, na->pref, na->flags, na->services, na->regexp, na->replace))
						{
							anscount++;
							log_query((crecp->flags & ~F_FORWARD) | F_RRNAME,
									cache_get_name(crecp), NULL, na->regexp);
						}
					}
				}
			}
		}
	  
		for (flag = F_IPV4; flag; flag = (flag == F_IPV4) ? F_IPV6 : 0)
		{
			unsigned short type = T_A;
	      
			if (flag == F_IPV6)
#ifdef HAVE_IPV6
				type = T_AAAA;
#else
				break;
#endif
	      
			if (qtype != type && qtype != T_ANY)
				continue;
	      
			/* Check for "A for A"  queries; be rather conservative 
				about what looks like dotted-quad.  */
			if (qtype == T_A)
			{
				char *cp;
				unsigned int i, a;
				int x;

				for (cp = name, i = 0, a = 0; *cp; i++)
				{
					if (!isdigit((unsigned char)*cp) || (x = strtol(cp, &cp, 10)) > 255) 
					{
						i = 5;
						break;
					}
		      
					a = (a << 8) + x;
		      
					if (*cp == '.') 
					cp++;
				}
		  
				if (i == 4)
				{
					ans = 1;
					if (!dryrun)
					{
						addr.addr.addr4.s_addr = htonl(a);
						log_query(F_FORWARD | F_CONFIG | F_IPV4, name, &addr, NULL);
						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
								daemon->local_ttl, NULL, type, C_IN, "4", &addr))
						anscount++;
					}
					continue;
				}

				/* interface name stuff */
				struct interface_name *intr;

				for (intr = daemon->int_names; intr; intr = intr->next)
					if (hostname_isequal(name, intr->name))
						break;
		  
				if (intr)
				{
					ans = 1;
					if (!dryrun)
					{
						if ((addr.addr.addr4 = get_ifaddr(intr->intr)).s_addr == (in_addr_t) -1)
							log_query(F_FORWARD | F_CONFIG | F_IPV4 | F_NEG, name, NULL, NULL);
						else
						{
							log_query(F_FORWARD | F_CONFIG | F_IPV4, name, &addr, NULL);
							if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
									daemon->local_ttl, NULL, type, C_IN, "4", &addr))
								anscount++;
						}
					}
					continue;
				}
			}

			crecp = NULL;

			while ((crecp = cache_find_by_name(crecp, name, now, flag)))
			{ 
				if ((crecp->flags & (F_HOSTS | F_DHCP)) || !sec_reqd)
				{
					/* If we are returning local answers depending on network,
						filter here. */
					if (localise && (crecp->flags & F_HOSTS) &&
							!is_same_net(*((struct in_addr *)&crecp->addr), local_addr, local_netmask))
						continue;
       
					if (!(crecp->flags & (F_HOSTS | F_DHCP)))
						auth = 0;

					/* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
					if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
						continue;

					if (crecp->flags & F_NEG)
					{
						ans = 1;
						auth = 0;
						if (crecp->flags & F_NXDOMAIN)
							nxdomain = 1;
						if (!dryrun)
							log_query(crecp->flags, name, NULL, NULL);
						break;
					}
					
					ans = 1;
					
					if (!dryrun)
					{
						log_query(crecp->flags & ~F_REVERSE, name, &crecp->addr.addr,
								record_source(crecp->uid));

						if (add_resource_record(header, limit, &trunc, nameoffset, &ansp, 
								crec_ttl(crecp, now), NULL, type, C_IN, type == T_A ? "4" : "6", &crecp->addr))
							anscount++;
					}
				}
			}
		}
		
		if (qtype == T_MAILB)
			ans = 1, nxdomain = 1;

		if (qtype == T_SOA && option_bool(OPT_FILTER))
		{
			ans = 1; 
			if (!dryrun)
				log_query(F_CONFIG | F_NEG, name, &addr, NULL);
		}

		/* Check if it's in no such name cache */
		if (!ans)
		{
			crecp = NULL;

			while (1)
			{
				int flag=0;
					if(qtype == T_CNAME)
						flag |= F_CNAME1;
					else if(qtype == T_SRV)
						flag |= F_SRV;
					else if(qtype == T_TXT)
						flag |= F_TXT;
					else if(qtype == T_MX)
						flag |= F_MX;
					else if(qtype == T_NAPTR)
						flag |= F_NAPTR;
					else if(qtype == T_A)
						flag |= F_IPV4;
					else if(qtype == T_AAAA)
						flag |= F_IPV6;
					else
						flag |= F_Other;
					
				crecp = cache_find_by_name(crecp, name, now, flag | F_NXDOMAIN);
				if (!crecp)
					break;

				ans = 1;
				if (!dryrun)
				{
					log_query(F_CONFIG | F_NXDOMAIN, name, NULL, "<NXDOMAIN>");
					anscount = 0;
					nxdomain = 1;
					break;
				}
			}
		}

		// mark_chen 2012.12.08, for other type with no record for this query
		if(qtype != T_PTR && qtype != T_CNAME && qtype != T_TXT && qtype != T_SRV
				&& qtype != T_NAPTR && qtype != T_A && qtype != T_AAAA)
		{
			crecp = NULL;

			while (1)
			{
				crecp = cache_find_by_name(crecp, name, now, F_Other);
				if (!crecp)
					break;

				/* don't answer wildcard queries with data not from /etc/hosts or dhcp leases */
				if (qtype == T_ANY && !(crecp->flags & (F_HOSTS | F_DHCP)))
					continue;

				if (qtype == crecp->addr.nxtype)
				{
					if (crecp->flags & F_NEG)
					{
						ans = 1;
						auth = 0;
						if (crecp->flags & F_NXDOMAIN)
							nxdomain = 1;
						if (!dryrun)
							log_query(crecp->flags, name, NULL, NULL);
					}
					
					break;
				}
			}
		}
		/* end mark_chen 2012.12.08 */
	}

      if (!ans)
        return 0; /* failed to answer a question */
  	}
  
  if (dryrun)
    {
      dryrun = 0;
      goto rerun;
    }
  
  /* create an additional data section, for stuff in SRV and MX record replies. */
  for (rec = daemon->mxnames; rec; rec = rec->next)
    if (rec->offset != 0)
      {
	/* squash dupes */
	struct mx_srv_record *tmp;
	for (tmp = rec->next; tmp; tmp = tmp->next)
	  if (tmp->offset != 0 && hostname_isequal(rec->target, tmp->target))
	    tmp->offset = 0;
	
	crecp = NULL;
	while ((crecp = cache_find_by_name(crecp, rec->target, now, F_IPV4 | F_IPV6)))
	  {
#ifdef HAVE_IPV6
	    int type =  crecp->flags & F_IPV4 ? T_A : T_AAAA;
#else
	    int type = T_A;
#endif
	    if (crecp->flags & F_NEG)
	      continue;

	    if (add_resource_record(header, limit, NULL, rec->offset, &ansp, 
				    crec_ttl(crecp, now), NULL, type, C_IN, 
				    crecp->flags & F_IPV4 ? "4" : "6", &crecp->addr))
	      addncount++;
	  }
      }

    /* mark_chen 2012.11.20, send the additional records cache for srv query */
    if ((qtype == T_SRV || qtype == T_MX) && found)
    {
        crecp = NULL;
        while (1)
        {
            struct crec *arcrecp = NULL;
            if(qtype == T_SRV)
	            crecp = cache_find_by_name(crecp, name, now, flag | F_SRV);
		else
			crecp = cache_find_by_name(crecp, name, now, flag | F_MX);
            if (!crecp)
                break;

            struct mx_srv_record* srv = &crecp->addr.srv;

            while ((arcrecp = cache_find_by_name(arcrecp, srv->target, now, F_IPV4 | F_IPV6)))
            {
#ifdef HAVE_IPV6
                int type =  arcrecp->flags & F_IPV4 ? T_A : T_AAAA;
#else
                int type = T_A;
#endif
                if (arcrecp->flags & F_NEG)
                    continue;
                if (add_resource_record(header, limit, NULL, srv->offset, &ansp,
                    crec_ttl(arcrecp, now), NULL, type, C_IN, 
                    arcrecp->flags & F_IPV4 ? "4" : "6", &arcrecp->addr))
                    addncount++;
            }
        }
    }
    /* end mark_chen 2012.11.20 */

    /* mark_chen 2012.12.07, reply with pseudoheader when receiving it */
	if(pseudo && (limit - (char *)ansp) > RRFIXEDSZ)
	{
		*ansp++ = 0; /* empty name */
		PUTSHORT(T_OPT, ansp);
		PUTSHORT(daemon->edns_pktsz, ansp); /* max packet length for EDNS0 */
		PUTLONG(0, ansp);    /* extended RCODE */
		PUTSHORT(0, ansp);    /* RDLEN */
		addncount++;
	}
    /* end mark_chen 2012.12.07 */

  /* done all questions, set up header and return length of result */
  /* clear authoritative and truncated flags, set QR flag */
  header->hb3 = (header->hb3 & ~(HB3_AA | HB3_TC)) | HB3_QR;
  /* set RA flag */
  header->hb4 |= HB4_RA;
   
  /* authoritive - only hosts and DHCP derived names. */
  if (auth)
    header->hb3 |= HB3_AA;
  
  /* truncation */
  if (trunc)
    header->hb3 |= HB3_TC;

  if (anscount == 0 && nxdomain)
    SET_RCODE(header, NXDOMAIN);
  else
    SET_RCODE(header, NOERROR); /* no error */

  header->ancount = htons(anscount);
  header->nscount = htons(0);
  header->arcount = htons(addncount);
  return ansp - (unsigned char *)header;
}





