#!/bin/sh

. /lib/functions.sh
. ../netifd-proto.sh

init_proto "$@"

echo -e "\n\n~~~~~~~ $0 [$$] ~~~~~~~~~" > /dev/console
echo ">> $@" > /dev/console
echo -e "\n\n" > /dev/console

proto_bridge_init_config() {
	proto_config_add_string "bridgeifname"
}

proto_bridge_setup() {
	local config="$1"
	local wan_ifname="$2"

	json_get_vars bridgeifname
	echo -e "\n~~~ proto_bridge_setup [$$] ~~~ ($0) $@, bridgeifname = $bridgeifname, wan_ifname = $wan_ifname\n\n" > /dev/console

	/usr/sbin/brctl addif $bridgeifname $wan_ifname > /dev/console 2>&1
	echo brctl return $? > /dev/console
	brctl show > /dev/console

	while true; do
		sleep 9999
	done

	echo -e "\n~~~ proto_bridge_setup [$$] DONE ~~~\n\n" > /dev/console
}

proto_bridge_teardown() {
	local interface="$1"
	local wan_ifname="$2"

	json_get_vars bridgeifname

	echo -e "\n~~~ proto_bridge_teardown [$$] ~~~ ($0) $@, bridgeifname = $bridgeifname\n\n" > /dev/console

	/usr/sbin/brctl delif $bridgeifname $wan_ifname > /dev/console 2>&1
	echo brctl return $? > /dev/console
	brctl show > /dev/console
}

add_protocol bridge
