#!/bin/sh

. /lib/functions.sh
. ../netifd-proto.sh
init_proto "$@"

proto_dhcp_init_config() {
	renew_handler=1

	proto_config_add_string 'ipaddr:ipaddr'
	proto_config_add_string 'hostname:hostname'
	proto_config_add_string clientid
	proto_config_add_string vendorid
	proto_config_add_boolean 'broadcast:bool'
	proto_config_add_string 'reqopts:list(string)'
	proto_config_add_string iface6rd
	proto_config_add_string sendopts
	proto_config_add_boolean delegate
	proto_config_add_string zone6rd
	proto_config_add_string zone
	proto_config_add_string mtu6rd
	proto_config_add_string customroutes
	proto_config_add_string 'domainname:domainname'
	proto_config_add_string 'netmask:netmask'
	proto_config_add_string macaddr
	proto_config_add_boolean 'use_dhclient:bool'
}

DHCLIENT_CFG_FILE="/tmp/dhclient.conf"
DHCLIENT_LEASE_FILE="/tmp/dhclient.leases"
#DHCLIENT_DFT_FILE="/usr/sbin/arc_dhcp4c_update"
DHCLIENT_DFT_FILE="/usr/sbin/dhclient.script"

proto_dhcp_setup() {
	local config="$1"
	local iface="$2"


	local ipaddr hostname clientid vendorid broadcast reqopts iface6rd sendopts delegate zone6rd zone mtu6rd customroutes domainname netmask macaddr use_dhclient
	use_dhclient=0
	json_get_vars ipaddr hostname clientid vendorid broadcast reqopts iface6rd sendopts delegate zone6rd zone mtu6rd customroutes domainname netmask macaddr use_dhclient

	local opt dhcpopts

	#echo "[proto_dhcp_setup] reqopts:$reqopts" > /dev/console
	for opt in $reqopts; do
		append dhcpopts "-O $opt"
	done

	#echo "[proto_dhcp_setup] sendopts:$sendopts" > /dev/console
	for opt in $sendopts; do
		append dhcpopts "-x $opt"
	done

	[ "$broadcast" = 1 ] && broadcast="-B" || broadcast=
	[ -n "$clientid" ] && clientid="-x 0x3d:${clientid//:/}" || clientid="-C"
	[ -n "$iface6rd" ] && proto_export "IFACE6RD=$iface6rd"
	[ "$iface6rd" != 0 -a -f /lib/netifd/proto/6rd.sh ] && append dhcpopts "-O 212"
	[ -n "$zone6rd" ] && proto_export "ZONE6RD=$zone6rd"
	[ -n "$zone" ] && proto_export "ZONE=$zone"
	[ -n "$mtu6rd" ] && proto_export "MTU6RD=$mtu6rd"
	[ -n "$customroutes" ] && proto_export "CUSTOMROUTES=$customroutes"
	[ "$delegate" = "0" ] && proto_export "IFACE6RD_DELEGATE=0"

	proto_export "INTERFACE=$config"

	#echo "[proto_dhcp_setup] _EXPORT_VAR:$_EXPORT_VAR" > /dev/console
	#echo "[proto_dhcp_setup] _EXPORT_VARS:$_EXPORT_VARS" > /dev/console

	echo "send vendor-class-identifier \"$vendorid\";" > $DHCLIENT_CFG_FILE
	echo "send host-name \"$hostname\";" >> $DHCLIENT_CFG_FILE
	echo "send domain-name \"$domainname\";" >> $DHCLIENT_CFG_FILE
	echo "send dhcp-client-identifier 1:$macaddr;" >> $DHCLIENT_CFG_FILE
	echo "request subnet-mask, classless-static-routes, routers, domain-name, domain-name-servers;" >> $DHCLIENT_CFG_FILE
	echo "require subnet-mask;" >> $DHCLIENT_CFG_FILE
	echo "dhcp-lease-time, dhcp-server-identifier;" >> $DHCLIENT_CFG_FILE
	echo "alias {" >> $DHCLIENT_CFG_FILE
	echo "	interface \"$iface\";" >> $DHCLIENT_CFG_FILE
	echo "	fixed-address $ipaddr;" >> $DHCLIENT_CFG_FILE
	echo "	option subnet-mask $netmask;" >> $DHCLIENT_CFG_FILE
	echo "}" >> $DHCLIENT_CFG_FILE

	rm $DHCLIENT_LEASE_FILE

	#chriskuo+wait QoS setup to finish
	qos_enable=`ccfg_cli get enable@qos_glb`
	if [ "x$qos_enable" = "x1" ]; then
		#while [ -f "/tmp/.arclinkup.pid" ]; do
		while [ ! -f "/tmp/qos_setup.done" ]; do
			sleep 1
		done
	fi
	#chriskuo+WAN interface down & up
	# calling parameters: dhcp.sh wan1 ptm0.8
	qos_enable=`ccfg_cli get enable@qos_glb`
	if [ "x$qos_enable" == "x1" ]; then
		/usr/sbin/qos/qos_recfg_1p_map_vconfig.sh $iface
	fi
	#chriskuo+e

	proto_run_command "$config" /usr/sbin/dhclient -4 -d -cf $DHCLIENT_CFG_FILE -sf $DHCLIENT_DFT_FILE -lf $DHCLIENT_LEASE_FILE -pf /var/run/dhclient-$iface.pid -li br-lan $iface
#	proto_run_command "$config" udhcpc \
#		-p /var/run/udhcpc-$iface.pid \
#		-s /lib/netifd/dhcp.script \
#		-f -t 0 -i "$iface" \
#		${ipaddr:+-r $ipaddr} \
#		${hostname:+-H $hostname} \
#		${vendorid:+-V $vendorid} \
#		$clientid $broadcast $dhcpopts
}

proto_dhcp_renew() {
	local interface="$1"
	# SIGUSR1 forces udhcpc to renew its lease
	local sigusr1="$(kill -l SIGUSR1)"
	[ -n "$sigusr1" ] && proto_kill_command "$interface" $sigusr1
}

proto_dhcp_teardown() {
	local interface="$1"
	proto_kill_command "$interface"
}

add_protocol dhcp
