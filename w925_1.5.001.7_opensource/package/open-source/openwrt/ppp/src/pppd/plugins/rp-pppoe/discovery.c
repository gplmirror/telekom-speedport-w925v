/***********************************************************************
*
* discovery.c
*
* Perform PPPoE discovery
*
* Copyright (C) 1999 by Roaring Penguin Software Inc.
*
***********************************************************************/

static char const RCSID[] =
"$Id: discovery.c,v 1.6 2008/06/15 04:35:50 paulus Exp $";

#define _GNU_SOURCE 1
#include "pppoe.h"
#include "pppd/pppd.h"

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/wait.h>

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifdef HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef USE_LINUX_PACKET
#include <sys/ioctl.h>
#include <fcntl.h>
#endif

#include <signal.h>
#include <sys/sysinfo.h>
#include <linux/if_vlan.h>
#include <linux/sockios.h>

#define VLAN_TAG_DETECT_DELAY  60
static int tagging = 1;
static int PADI_count = 0;
static char untag_iface[16] = "";
static char tag_iface[16] = "";
static char first_iface[16] = "";
static char second_iface[16] = "";
static char tmpBuf[128];
static char wan_ifname[16];
static char wan_vlan_id[16] = "";
static int vlan_detect = 1;
static int vlan_tagged = 1;
static int vlan_id = 0;
int pppoe_reconnect_flag = 0;
char mac_addr_get[6]; // run time interface address
char wan_mac_addr[6]; // PPP WAN interface mac address
char wan_mac_addr_str[20]; // PPP WAN interface mac address string
extern int gWan_Type;
extern int gWan_Idx;

unsigned long PADIR_timestamp=0;
unsigned long PADIR_LastTimestamp=0;
int PADIR_sent=0;
int chk_needLogMsg()
{
	int needLogMsg;
	
	needLogMsg=0;
	if (PADIR_sent==0) 
	{
		time(&PADIR_timestamp);
		PADIR_LastTimestamp=PADIR_timestamp;
		needLogMsg=1;
	}
	else 
	{
		unsigned long t;
		time(&t);
		if ((t>PADIR_timestamp && t-PADIR_timestamp<=90)
			||(t<PADIR_timestamp && (0xFFFFFFFF-PADIR_timestamp+t)<=90))
		{
			PADIR_LastTimestamp=t;
			needLogMsg=1;
		}
		else
		{
			if ((t>PADIR_LastTimestamp && t-PADIR_LastTimestamp>=300)
				||(t<PADIR_LastTimestamp && (0xFFFFFFFF-PADIR_LastTimestamp+t)>=300))
			{
				PADIR_LastTimestamp=t;
				needLogMsg=1;
			}
		}
	}
	PADIR_sent++;
	return needLogMsg;
}
void clr_needLogFlag()
{
	PADIR_timestamp=0;
	PADIR_LastTimestamp=0;
	PADIR_sent=0;
}
/**
 *      get_tmp_long:	forrest_fei 2012.12.24
 *      @tmp_path[in]:	the file to read from
 *      return:		the value read from tmp_path with int format
 *      Read from @tmp_path and return value with int format
 */
 int get_tmp_long(const char *tmp_path)
{
	FILE *fp = NULL;
	int value = 0;

	fp = fopen(tmp_path, "r");
	if(fp)
	{
		fscanf(fp, "%d", &value);
		fclose(fp);
	}
	return value;
}

int get_tmp_value(const char *tmp_path, char *value)
{
	FILE *fp = NULL;

	fp = fopen(tmp_path, "r");
	if(fp)
	{
		fscanf(fp, "%s", value);
		fclose(fp);
	}
	else
	{
		sprintf(value, "0");
	}
	return 1;
}

void printMacAddr(char *mac)
{
	int i;

	arc_dprintf("mac: %02x:%02x:%02x:%02x:%02x:%02x\n", mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
}

int _SetHWaddr(char *ifname, char *mac_addr_str)
{
	struct ifreq ifr;
	int s = -1;

	s = socket(AF_INET, SOCK_DGRAM, 0);
	if(s == -1) goto _SetHWaddr_exit;

	strcpy(ifr.ifr_name, ifname);
	sscanf(mac_addr_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
        &ifr.ifr_hwaddr.sa_data[0],
        &ifr.ifr_hwaddr.sa_data[1],
        &ifr.ifr_hwaddr.sa_data[2],
        &ifr.ifr_hwaddr.sa_data[3],
        &ifr.ifr_hwaddr.sa_data[4],
        &ifr.ifr_hwaddr.sa_data[5]
        );
	ifr.ifr_hwaddr.sa_family = ARPHRD_ETHER;

	if(ioctl(s, SIOCSIFHWADDR, &ifr) == -1)
		goto _SetHWaddr_exit;

	if(s >= 0)
		close(s);

	return 0;

_SetHWaddr_exit:
	if(s >= 0)
		close(s);
	return -1;
}

int _CreateVlan(char *if_name, int vid)
{
	int fd, ret = 0;
	struct vlan_ioctl_args if_request;

	memset(&if_request, 0, sizeof(struct vlan_ioctl_args));
	strcpy(if_request.device1, if_name);
	if_request.u.VID = vid;

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		return 2;
	}

	if_request.cmd = ADD_VLAN_CMD;
	if (ioctl(fd, SIOCSIFVLAN, &if_request) < 0) {
		ret = 3;
	}
End_l:
	if (fd != -1) close(fd);
	return ret;
}


/**********************************************************************
*%FUNCTION: parseForHostUniq
*%ARGUMENTS:
* type -- tag type
* len -- tag length
* data -- tag data.
* extra -- user-supplied pointer.  This is assumed to be a pointer to int.
*%RETURNS:
* Nothing
*%DESCRIPTION:
* If a HostUnique tag is found which matches our PID, sets *extra to 1.
***********************************************************************/
static void
parseForHostUniq(UINT16_t type, UINT16_t len, unsigned char *data,
		 void *extra)
{
    int *val = (int *) extra;
    if (type == TAG_HOST_UNIQ && len == sizeof(pid_t)) {
	pid_t tmp;
	memcpy(&tmp, data, len);
	if (tmp == getpid()) {
	    *val = 1;
	}
    }
}

/**********************************************************************
*%FUNCTION: parseForHostUniq_new
*%ARGUMENTS:
* type -- tag type
* len -- tag length
* data -- tag data.
* extra -- user-supplied pointer.  This is assumed to be a pointer to int.
*%RETURNS:
* Nothing
*%DESCRIPTION:
* If a HostUnique tag is found which matches our MAC address, sets *extra to 1.
***********************************************************************/
void
parseForHostUniq_new(PPPoEPacket *packet, UINT16_t type, UINT16_t len, unsigned char *data,
		 void *extra)
{
    int *val = (int *) extra;
    if (type == TAG_HOST_UNIQ && len == ETH_ALEN) {
	if ( !strncmp(data, packet->ethHdr.h_dest, ETH_ALEN)) {
	    *val = 1;
	}
    }
}

int set_sock_nonblock(int fd)
{
    int oldflag;

    oldflag = fcntl(fd, F_GETFL, 0);
    if(oldflag == -1) {
        fatal("Get disc_sock flag error!\n");
        return (0);
    }

    oldflag |= O_NONBLOCK;
    if(fcntl(fd, F_SETFL, oldflag)==-1){
        fatal("Set disc_sock nonblock error!\n");
	return (0);
    }
    return (1);
}

/**********************************************************************
*%FUNCTION: packetIsForMe
*%ARGUMENTS:
* conn -- PPPoE connection info
* packet -- a received PPPoE packet
*%RETURNS:
* 1 if packet is for this PPPoE daemon; 0 otherwise.
*%DESCRIPTION:
* If we are using the Host-Unique tag, verifies that packet contains
* our unique identifier.
***********************************************************************/
static int
packetIsForMe(PPPoEConnection *conn, PPPoEPacket *packet)
{
    int forMe = 0;

    /* If packet is not directed to our MAC address, forget it */
    if (memcmp(packet->ethHdr.h_dest, conn->myEth, ETH_ALEN)) return 0;

    /* If we're not using the Host-Unique tag, then accept the packet */
    if (!conn->useHostUniq) return 1;

    //parsePacket(packet, parseForHostUniq, &forMe);

    parsePacket(packet, parseForHostUniq_new, &forMe);
    return forMe;
}

char acname_buf[64] = {'\0'};
/**********************************************************************
*%FUNCTION: parsePADOTags
*%ARGUMENTS:
* type -- tag type
* len -- tag length
* data -- tag data
* extra -- extra user data.  Should point to a PacketCriteria structure
*          which gets filled in according to selected AC name and service
*          name.
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Picks interesting tags out of a PADO packet
***********************************************************************/
static void
parsePADOTags(PPPoEPacket *packet, UINT16_t type, UINT16_t len, unsigned char *data,
	      void *extra)
{
    struct PacketCriteria *pc = (struct PacketCriteria *) extra;
    PPPoEConnection *conn = pc->conn;
#ifdef MINI_JUMBO_FRAME
    UINT16_t mru;
#endif
    int i;
    char message_str[200];

    switch(type) {
    case TAG_AC_NAME:
	pc->seenACName = 1;
	if (conn->printACNames) {
	    info("Access-Concentrator: %.*s", (int) len, data);
	}
	//chriskuo+ARCADYAN for Zafaco
	{
		int acn_len = (len < sizeof(acname_buf))? len: sizeof(acname_buf);
		strncpy(acname_buf, data, acn_len);
		acname_buf[acn_len] = '\0';
	}
	if (conn->acName && len == strlen(conn->acName) &&
	    !strncmp((char *) data, conn->acName, len)) {
	    pc->acNameOK = 1;
	}
	break;
    case TAG_SERVICE_NAME:
	pc->seenServiceName = 1;
	if (conn->serviceName && len == strlen(conn->serviceName) &&
	    !strncmp((char *) data, conn->serviceName, len)) {
	    pc->serviceNameOK = 1;
	}
	conn->rec_serviceName.type = htons(type);
	conn->rec_serviceName.length = htons(len);
	memcpy(conn->rec_serviceName.payload, data, len);
	break;
    case TAG_AC_COOKIE:
	conn->cookie.type = htons(type);
	conn->cookie.length = htons(len);
	memcpy(conn->cookie.payload, data, len);
	break;
    case TAG_RELAY_SESSION_ID:
	conn->relayId.type = htons(type);
	conn->relayId.length = htons(len);
	memcpy(conn->relayId.payload, data, len);
	break;
#ifdef MINI_JUMBO_FRAME
   case TAG_PPP_MAX_PAYLOAD:
         if (len == sizeof(mru)) {

            memcpy(&mru, data, sizeof(mru));
            mru = ntohs(mru);

            if (mru >= ETH_PPPOE_MTU) {
              conn->mru = mru;
              conn->seenMaxPayload = 1;
            }

         }
         break;
#endif
    case TAG_SERVICE_NAME_ERROR:
	if (conn->printACNames) {
		sprintf(message_str, "Got a Service-Name-Error tag: %.*s\n", (int) len, data);
	} else {
		sprintf(message_str, "PADO: Service-Name-Error: %.*s", (int) len, data);
	}
{ 
	char cmdbuf[100];
	sprintf(cmdbuf,"umng_syslog_cli addErrorCode -1 R015 \"s\"", message_str);
	system(cmdbuf);
}
	if (conn->printACNames) {
	    printf("Got a Service-Name-Error tag: %.*s\n", (int) len, data);
	} else {
		error("PADO: Service-Name-Error: %.*s", (int) len, data);
		conn->error = 1;
	}
	break;
    case TAG_AC_SYSTEM_ERROR:
	if (conn->printACNames) {
	    printf("Got a System-Error tag: %.*s\n", (int) len, data);
	} else {
		error("PADO: System-Error: %.*s", (int) len, data);
		conn->error = 1;
	}
	break;
    case TAG_GENERIC_ERROR:
	if (conn->printACNames) {
	    printf("Got a Generic-Error tag: %.*s\n", (int) len, data);
	} else {
		error("PADO: Generic-Error: %.*s", (int) len, data);
		conn->error = 1;
	}
	break;
    }
}

/**********************************************************************
*%FUNCTION: parsePADSTags
*%ARGUMENTS:
* type -- tag type
* len -- tag length
* data -- tag data
* extra -- extra user data (pointer to PPPoEConnection structure)
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Picks interesting tags out of a PADS packet
***********************************************************************/
static void
parsePADSTags(PPPoEPacket *packet, UINT16_t type, UINT16_t len, unsigned char *data,
	      void *extra)
{
    PPPoEConnection *conn = (PPPoEConnection *) extra;
#ifdef MINI_JUMBO_FRAME
    UINT16_t mru;
#endif
    switch(type) {
    case TAG_SERVICE_NAME:
	dbglog("PADS: Service-Name: '%.*s'", (int) len, data);
	break;
#ifdef MINI_JUMBO_FRAME
    case TAG_PPP_MAX_PAYLOAD:
      if (len == sizeof(mru)) {
          memcpy(&mru, data, sizeof(mru));
          mru = ntohs(mru);
          if (mru >= ETH_PPPOE_MTU) {
            conn->mru = mru;
            conn->seenMaxPayload = 1;
          }
      }
    break;
#endif
    case TAG_SERVICE_NAME_ERROR:
	error("PADS: Service-Name-Error: %.*s", (int) len, data);
	conn->error = 1;
	break;
    case TAG_AC_SYSTEM_ERROR:
#if 0 // Morgan, not to exit pppd to prevent next connection trigger from failing.
	error("PADS: System-Error: %.*s", (int) len, data);
#else
	conn->error = 1;
#endif
	break;
    case TAG_GENERIC_ERROR:
	error("PADS: Generic-Error: %.*s", (int) len, data);
	conn->error = 1;
	break;
    case TAG_RELAY_SESSION_ID:
	conn->relayId.type = htons(type);
	conn->relayId.length = htons(len);
	memcpy(conn->relayId.payload, data, len);
	break;
    }
}

static void init_connect_status()
{
	char cmdbuf[100];

	sprintf(cmdbuf,"abscfg set ARC_WAN_xy_TMP_Connect_Status %d %d \"%s\"", gWan_Type, gWan_Idx, "1"); //connect_status : 1 connecting
	system(cmdbuf);
	if(gWan_Idx == gDefaultWan_Idx)
	{
		sprintf(cmdbuf,"abscfg set ARC_WAN_TMP_DefaultRoute_IPv4_WAN_Ready 2");
		system(cmdbuf);
		sprintf(cmdbuf,"abscfg set ARC_WAN_TMP_DefaultRoute_IPv6_WAN_Ready 2");
		system(cmdbuf);
	}
	sprintf(cmdbuf,"abscfg set ARC_WAN_xy_TMP_PPP_Auth %d %d \"%s\"", gWan_Type, gWan_Idx, "0"); //connect_status : 1 connecting
	system(cmdbuf);
	sprintf(cmdbuf,"abscfg set ARC_WAN_xy_TMP_PPP_Error_Reason %d %d \"%s\"", gWan_Type, gWan_Idx, ""); //connect_status : 1 connecting
	system(cmdbuf);

}

/***********************************************************************
*%FUNCTION: sendPADI
*%ARGUMENTS:
* conn -- PPPoEConnection structure
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Sends a PADI packet
***********************************************************************/
static void
sendPADI(PPPoEConnection *conn)
{
    PPPoEPacket packet;
    unsigned char *cursor = packet.payload;
    PPPoETag *svc = (PPPoETag *) (&packet.payload);
    UINT16_t namelen = 0;
    UINT16_t plen;
    int omit_service_name = 0;
	char cmd_str[200];

    if (conn->serviceName) {
	namelen = (UINT16_t) strlen(conn->serviceName);
	if (!strcmp(conn->serviceName, "NO-SERVICE-NAME-NON-RFC-COMPLIANT")) {
	    omit_service_name = 1;
	}
    }

    /* Set destination to Ethernet broadcast address */
    memset(packet.ethHdr.h_dest, 0xFF, ETH_ALEN);
    memcpy(packet.ethHdr.h_source, conn->myEth, ETH_ALEN);

    packet.ethHdr.h_proto = htons(Eth_PPPOE_Discovery);
    packet.vertype = PPPOE_VER_TYPE(1, 1);
    packet.code = CODE_PADI;
    packet.session = 0;

    if (!omit_service_name) {
	plen = TAG_HDR_SIZE + namelen;
	CHECK_ROOM(cursor, packet.payload, plen);

	svc->type = TAG_SERVICE_NAME;
	svc->length = htons(namelen);

	if (conn->serviceName) {
	    memcpy(svc->payload, conn->serviceName, strlen(conn->serviceName));
	}
	cursor += namelen + TAG_HDR_SIZE;
    } else {
	plen = 0;
    }

    /* If we're using Host-Uniq, copy it over */
    if (conn->useHostUniq) {
	PPPoETag hostUniq;
#if 1 // bitonic
	hostUniq.type = htons(TAG_HOST_UNIQ);
	hostUniq.length = htons(ETH_ALEN);
	memcpy(hostUniq.payload, conn->myEth, ETH_ALEN);
	CHECK_ROOM(cursor, packet.payload, ETH_ALEN  + TAG_HDR_SIZE);
	memcpy(cursor, &hostUniq, ETH_ALEN + TAG_HDR_SIZE);
	cursor += ETH_ALEN + TAG_HDR_SIZE;
	plen += ETH_ALEN + TAG_HDR_SIZE;
#else
	pid_t pid = getpid();
	hostUniq.type = htons(TAG_HOST_UNIQ);
	hostUniq.length = htons(sizeof(pid));
	memcpy(hostUniq.payload, &pid, sizeof(pid));
	CHECK_ROOM(cursor, packet.payload, sizeof(pid) + TAG_HDR_SIZE);
	memcpy(cursor, &hostUniq, sizeof(pid) + TAG_HDR_SIZE);
	cursor += sizeof(pid) + TAG_HDR_SIZE;
	plen += sizeof(pid) + TAG_HDR_SIZE;
#endif
    }
#ifdef MINI_JUMBO_FRAME
    /* Add our maximum MTU/MRU */
    if( conn->mru >  ETH_PPPOE_MTU ) {

      PPPoETag maxPayload;
      UINT16_t mru = htons(conn->mru);
      maxPayload.type = htons(TAG_PPP_MAX_PAYLOAD);
      maxPayload.length = htons(sizeof(mru));
      memcpy(maxPayload.payload, &mru, sizeof(mru));
      CHECK_ROOM(cursor, packet.payload, sizeof(mru) + TAG_HDR_SIZE);
      memcpy(cursor, &maxPayload, sizeof(mru) + TAG_HDR_SIZE);
      cursor += sizeof(mru) + TAG_HDR_SIZE;
      plen += sizeof(mru) + TAG_HDR_SIZE;
    }
#endif
    packet.length = htons(plen);

	if (chk_needLogMsg()==1)
	{
		sprintf(cmd_str, "umng_syslog_cli addEventCode R101");
		osSystem(cmd_str);
	}
    sendPacket(conn, conn->discoverySocket, &packet, (int) (plen + HDR_SIZE));
}

/**********************************************************************
*%FUNCTION: waitForPADO
*%ARGUMENTS:
* conn -- PPPoEConnection structure
* timeout -- how long to wait (in seconds)
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Waits for a PADO packet and copies useful information
***********************************************************************/
void
waitForPADO(PPPoEConnection *conn, int timeout)
{
    fd_set readable;
    int r;
    struct timeval tv;
    PPPoEPacket packet;
    int len;
    int ret;
	char cmd_str[200];

    struct PacketCriteria pc;
    pc.conn          = conn;
    pc.acNameOK      = (conn->acName)      ? 0 : 1;
    pc.serviceNameOK = (conn->serviceName) ? 0 : 1;
    pc.seenACName    = 0;
    pc.seenServiceName = 0;
    conn->error = 0;
#ifdef MINI_JUMBO_FRAME
    conn->seenMaxPayload = 0;
#endif

    do {
	if (BPF_BUFFER_IS_EMPTY) {
	    tv.tv_sec = timeout;
	    tv.tv_usec = 0;

	    FD_ZERO(&readable);
	    FD_SET(conn->discoverySocket, &readable);

	    while(1) {
		r = select(conn->discoverySocket+1, &readable, NULL, NULL, &tv);
		if (r >= 0 || errno != EINTR) break;
	    }
	    if (r < 0) {
		error("select (waitForPADO): %m");
		return;
	    }
	    if (r == 0)
	    {
	    	arc_pppd_dbg("select timeout\n");
	    	return;        /* Timed out */
	    }
	}

	/* Get the packet */
	ret = receivePacket(conn->discoverySocket, &packet, &len);

	/* Check length */
	if (ntohs(packet.length) + HDR_SIZE > len) {
	    error("Bogus PPPoE length field (%u)",
		   (unsigned int) ntohs(packet.length));
	    continue;
	}

#ifdef USE_BPF
	/* If it's not a Discovery packet, loop again */
	if (etherType(&packet) != Eth_PPPOE_Discovery) continue;
#endif

	/* If it's not for us, loop again */
	if (!packetIsForMe(conn, &packet)) continue;

	if (packet.code == CODE_PADO) {
	    if (BROADCAST(packet.ethHdr.h_source)) {
		error("Ignoring PADO packet from broadcast MAC address");
		continue;
	    }
	    if (conn->req_peer
		&& memcmp(packet.ethHdr.h_source, conn->req_peer_mac, ETH_ALEN) != 0) {
		warn("Ignoring PADO packet from wrong MAC address");
		continue;
	    }
	    if (parsePacket(&packet, parsePADOTags, &pc) < 0)
			return;
	    if (conn->error)
			return;
	    if (!pc.seenACName) {
		error("Ignoring PADO packet with no AC-Name tag");
		continue;
	    }
	    if (!pc.seenServiceName) {
		error("Ignoring PADO packet with no Service-Name tag");
		continue;
	    }
	    conn->numPADOs++;
	    if (pc.acNameOK && pc.serviceNameOK) {
		memcpy(conn->peerEth, packet.ethHdr.h_source, ETH_ALEN);
		conn->discoveryState = STATE_RECEIVED_PADO;

		clr_needLogFlag();
		sprintf(cmd_str, "umng_syslog_cli addEventCode R102");
		osSystem(cmd_str);
		break;
	    }
	}
    } while (conn->discoveryState != STATE_RECEIVED_PADO);

#if 0
	if (strlen(acname_buf) > 0)
	{
		char buf[128];
		printf("Access-Concentrator: %s\n", acname_buf);
		sprintf(buf, "echo %s > /tmp/ppp_acname.txt", acname_buf);
		system(buf);
		sprintf(buf, "/usr/sbin/qos/qos_wan_up_2.sh %s &", acname_buf);
		system(buf);
	} else {
		system("/usr/sbin/qos/qos_wan_up_2.sh na&");
	}
#endif
#define MAX_MSG_LENGTH	128
	{
    	FILE *f;
		f = fopen("/tmp/ppp_acname", "w");
		if (f) {
			fputs(acname_buf, f);	// even if it's 0 length, to clear previous session info
			fclose(f); 
		}
	}
	
	arc_pppd_dbg("PPPoE receive PADO.\n");
}

/***********************************************************************
*%FUNCTION: sendPADR
*%ARGUMENTS:
* conn -- PPPoE connection structur
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Sends a PADR packet
***********************************************************************/
static void
sendPADR(PPPoEConnection *conn)
{
    PPPoEPacket packet;
    PPPoETag *svc = (PPPoETag *) packet.payload;
    unsigned char *cursor = packet.payload;
	char cmd_str[200];

    UINT16_t namelen = 0;
    UINT16_t plen;

    if (conn->serviceName) {
	namelen = (UINT16_t) strlen(conn->serviceName);
    }
    else
    {
	namelen =  ntohs(conn->rec_serviceName.length);
    }
    plen = TAG_HDR_SIZE + namelen;
    CHECK_ROOM(cursor, packet.payload, plen);

    memcpy(packet.ethHdr.h_dest, conn->peerEth, ETH_ALEN);
    memcpy(packet.ethHdr.h_source, conn->myEth, ETH_ALEN);

    packet.ethHdr.h_proto = htons(Eth_PPPOE_Discovery);
    packet.vertype = PPPOE_VER_TYPE(1, 1);
    packet.code = CODE_PADR;
    packet.session = 0;

    svc->type = TAG_SERVICE_NAME;
    svc->length = htons(namelen);
    if (conn->serviceName) {
	memcpy(svc->payload, conn->serviceName, namelen);
    }
    else
    {
	memcpy(svc->payload, conn->rec_serviceName.payload, namelen);
    }
    cursor += namelen + TAG_HDR_SIZE;

    /* If we're using Host-Uniq, copy it over */
    if (conn->useHostUniq) {
	PPPoETag hostUniq;
#if 1 // bitonic
	hostUniq.type = htons(TAG_HOST_UNIQ);
	hostUniq.length = htons(ETH_ALEN);
	memcpy(hostUniq.payload, conn->myEth, ETH_ALEN);
	CHECK_ROOM(cursor, packet.payload, ETH_ALEN  + TAG_HDR_SIZE);
	memcpy(cursor, &hostUniq, ETH_ALEN + TAG_HDR_SIZE);
	cursor += ETH_ALEN + TAG_HDR_SIZE;
	plen += ETH_ALEN + TAG_HDR_SIZE;
#else
	pid_t pid = getpid();
	hostUniq.type = htons(TAG_HOST_UNIQ);
	hostUniq.length = htons(sizeof(pid));
	memcpy(hostUniq.payload, &pid, sizeof(pid));
	CHECK_ROOM(cursor, packet.payload, sizeof(pid)+TAG_HDR_SIZE);
	memcpy(cursor, &hostUniq, sizeof(pid) + TAG_HDR_SIZE);
	cursor += sizeof(pid) + TAG_HDR_SIZE;
	plen += sizeof(pid) + TAG_HDR_SIZE;
#endif
    }
#ifdef MINI_JUMBO_FRAME
    /* Add our maximum MTU/MRU */
    if( conn->seenMaxPayload == 1 ) {

      PPPoETag maxPayload;
      UINT16_t mru = htons(conn->mru);
      maxPayload.type = htons(TAG_PPP_MAX_PAYLOAD);
      maxPayload.length = htons(sizeof(mru));
      memcpy(maxPayload.payload, &mru, sizeof(mru));
      CHECK_ROOM(cursor, packet.payload, sizeof(mru) + TAG_HDR_SIZE);
      memcpy(cursor, &maxPayload, sizeof(mru) + TAG_HDR_SIZE);
      cursor += sizeof(mru) + TAG_HDR_SIZE;
      plen += sizeof(mru) + TAG_HDR_SIZE;
    }
#endif
    /* Copy cookie and relay-ID if needed */
    if (conn->cookie.type) {
	CHECK_ROOM(cursor, packet.payload,
		   ntohs(conn->cookie.length) + TAG_HDR_SIZE);
	memcpy(cursor, &conn->cookie, ntohs(conn->cookie.length) + TAG_HDR_SIZE);
	cursor += ntohs(conn->cookie.length) + TAG_HDR_SIZE;
	plen += ntohs(conn->cookie.length) + TAG_HDR_SIZE;
    }

    if (conn->relayId.type) {
	CHECK_ROOM(cursor, packet.payload,
		   ntohs(conn->relayId.length) + TAG_HDR_SIZE);
	memcpy(cursor, &conn->relayId, ntohs(conn->relayId.length) + TAG_HDR_SIZE);
	cursor += ntohs(conn->relayId.length) + TAG_HDR_SIZE;
	plen += ntohs(conn->relayId.length) + TAG_HDR_SIZE;
    }

	if (chk_needLogMsg()==1) 
	{
		sprintf(cmd_str, "umng_syslog_cli addEventCode R103");
		osSystem(cmd_str);
	}
    packet.length = htons(plen);
    sendPacket(conn, conn->discoverySocket, &packet, (int) (plen + HDR_SIZE));
}

/**********************************************************************
*%FUNCTION: waitForPADS
*%ARGUMENTS:
* conn -- PPPoE connection info
* timeout -- how long to wait (in seconds)
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Waits for a PADS packet and copies useful information
***********************************************************************/
static void
waitForPADS(PPPoEConnection *conn, int timeout)
{
    fd_set readable;
    int r;
    struct timeval tv;
    PPPoEPacket packet;
    int len;
	char cmd_str[200];

    conn->error = 0;
    do {
	if (BPF_BUFFER_IS_EMPTY) {
	    tv.tv_sec = timeout;
	    tv.tv_usec = 0;

	    FD_ZERO(&readable);
	    FD_SET(conn->discoverySocket, &readable);

	    while(1) {
		r = select(conn->discoverySocket+1, &readable, NULL, NULL, &tv);
		if (r >= 0 || errno != EINTR) break;
	    }
	    if (r < 0) {
		error("select (waitForPADS): %m");
		return;
	    }
	    if (r == 0) return;
	}

	/* Get the packet */
	receivePacket(conn->discoverySocket, &packet, &len);

	/* Check length */
	if (ntohs(packet.length) + HDR_SIZE > len) {
	    error("Bogus PPPoE length field (%u)",
		   (unsigned int) ntohs(packet.length));
	    continue;
	}

#ifdef USE_BPF
	/* If it's not a Discovery packet, loop again */
	if (etherType(&packet) != Eth_PPPOE_Discovery) continue;
#endif

	/* If it's not from the AC, it's not for me */
	if (memcmp(packet.ethHdr.h_source, conn->peerEth, ETH_ALEN)) continue;

	/* If it's not for us, loop again */
	if (!packetIsForMe(conn, &packet)) continue;

	/* Is it PADS?  */
	if (packet.code == CODE_PADS) {
	    /* Parse for goodies */
	    if (parsePacket(&packet, parsePADSTags, conn) < 0)
		return;
	    if (conn->error)
		return;
	    conn->discoveryState = STATE_SESSION;

		clr_needLogFlag();
		sprintf(cmd_str, "umng_syslog_cli addEventCode R104");
		osSystem(cmd_str);

	    break;
	}
    } while (conn->discoveryState != STATE_SESSION);

    /* Don't bother with ntohs; we'll just end up converting it back... */
    conn->session = packet.session;

    info("[waitForPADS] PPP session is %d", (int) ntohs(conn->session));

	arc_pppd_dbg("PPPoE receive PADS, pppoe session is 0x%x.\n", conn->session);

    /* RFC 2516 says session id MUST NOT be zero or 0xFFFF */
    if (ntohs(conn->session) == 0 || ntohs(conn->session) == 0xFFFF) {
	error("Access concentrator used a session value of %x -- the AC is violating RFC 2516", (unsigned int) ntohs(conn->session));
    }
}

static void pre_vlan_tag_detect()
{
	void *tid = NULL;
	int values[6];
	int i;
	char cmd[200];

	memset(tmpBuf, 0, sizeof(tmpBuf));
	memset(wan_ifname, 0, sizeof(wan_ifname));
	memset(wan_vlan_id, 0, sizeof(wan_vlan_id));


	sprintf(cmd,"abscfg set ARC_WAN_xy_TMP_Connect_Start_Time %d %d \"%s\"",gWan_Type, gWan_Idx, "0");
	system(cmd);

	sprintf(cmd,"abscfg set ARC_WAN_xy_BASE_Ifname %d %d \"%s\" %d",gWan_Type, gWan_Idx, wan_ifname, sizeof(wan_ifname));
	system(cmd);

	sprintf(cmd,"abscfg get ARC_WAN_xy_VLAN_Id %d %d",gWan_Type, gWan_Idx);
	osSystem_GetOutput(cmd, wan_ifname, sizeof(wan_ifname) );

	vlan_id = atoi(wan_vlan_id);

	strncpy(untag_iface, wan_ifname, 4);
	sprintf(tag_iface, "%s.%s", untag_iface, wan_vlan_id);

	sprintf(cmd,"abscfg get ARC_WAN_xy_VLAN_DETECT %d %d",gWan_Type, gWan_Idx);
	osSystem_GetOutput(cmd, tmpBuf, sizeof(tmpBuf) );

	vlan_detect = atoi(tmpBuf);

	sprintf(cmd,"abscfg get ARC_WAN_xy_VLAN_Tagging %d %d",gWan_Type, gWan_Idx);
	osSystem_GetOutput(cmd, tmpBuf, sizeof(tmpBuf) );
	vlan_tagged = atoi(tmpBuf);
	if(gWan_Type == 0)
		vlan_tagged = 1;

	// use ifconfig to get base ifname mac address
	sprintf(cmd,"abscfg get ARC_WAN_xy_MACaddr %d %d",gWan_Type, gWan_Idx);
	memset(wan_mac_addr_str, 0, sizeof(wan_mac_addr_str));
	osSystem_GetOutput(cmd, wan_mac_addr_str, sizeof(wan_mac_addr_str) );

	if( 6 == sscanf( wan_mac_addr_str, "%x:%x:%x:%x:%x:%x%c",
    	&values[0], &values[1], &values[2],
    	&values[3], &values[4], &values[5] ) )
	{
    	/* convert to uint8_t */
    	for( i = 0; i < 6; ++i )
        	wan_mac_addr[i] = (uint8_t) values[i];
	}
	else
	{
    	/* invalid mac */
	}
	_SetHWaddr(untag_iface, wan_mac_addr_str);

	_CreateVlan(wan_ifname, vlan_id);

	_SetHWaddr(tag_iface, wan_mac_addr_str);

	if(vlan_tagged == 0)
	{
		strcpy(first_iface, untag_iface);
		strcpy(second_iface, tag_iface);
	}
	else
	{
		strcpy(first_iface, tag_iface);
		strcpy(second_iface, untag_iface);
	}
	sprintf(cmd,"ifconfig %s up", first_iface);
	arc_dprintf("[pre_vlan_tag_detect] cmd : %s, line %d\n", cmd, __LINE__);
	system(cmd);
	sprintf(cmd,"ifconfig %s up", second_iface);
	arc_dprintf("[pre_vlan_tag_detect] cmd : %s, line %d\n", cmd, __LINE__);
	system(cmd);

	sprintf(cmd,"abscfg set ARC_WAN_xy_TMP_Connect_Status %d %d \"%s\"",gWan_Type, gWan_Idx, "1");
	system(cmd);
	sprintf(cmd,"abscfg set ARC_WAN_xy_TMP_PPP_Auth %d %d \"%s\"",gWan_Type, gWan_Idx, "0");
	system(cmd);
	sprintf(cmd,"abscfg set ARC_WAN_xy_TMP_PPP_Error_Reason %d %d \"%s\"",gWan_Type, gWan_Idx, "");
	system(cmd);

}
/**********************************************************************
*%FUNCTION: discovery
*%ARGUMENTS:
* conn -- PPPoE connection info structure
*%RETURNS:
* Nothing
*%DESCRIPTION:
* Performs the PPPoE discovery phase
***********************************************************************/
void
discovery(PPPoEConnection *conn)
{
    int padiAttempts = 0;
    int padrAttempts = 0;
    int timeout = conn->discoveryTimeout;

    char message_str[200], cmd[200];
    int pid = 0;

	conn->error = 0;

    pre_vlan_tag_detect();

detect_retry:
    if(vlan_detect == 1)
    {
		if(tagging == 0)
		{
			strcpy(conn->ifName, second_iface);
			tagging = 1;
		}
		else
		{
			strcpy(conn->ifName, first_iface);
			tagging = 0;

			if(PADI_count >0 && PADI_count < 4)
			{
				PADI_count <<= 1;
			}
			else
			{
				PADI_count = 1;
			}
		}

		_SetHWaddr(first_iface, wan_mac_addr_str);

		_SetHWaddr(second_iface, wan_mac_addr_str);

		sprintf(cmd,"ifconfig %s up", first_iface);
		arc_dprintf("[discovery] cmd : %s, line %d\n", cmd, __LINE__);
		system(cmd);
		sprintf(cmd,"ifconfig %s up", second_iface);
		arc_dprintf("[discovery] cmd : %s, line %d\n", cmd, __LINE__);
		system(cmd);
    }
    else
    {
		strcpy(conn->ifName, first_iface);
		PADI_count = MAX_PADI_ATTEMPTS;
    }

    conn->discoverySocket =
	openInterface(conn->ifName, Eth_PPPOE_Discovery, conn->myEth);

    if(!set_sock_nonblock(conn->discoverySocket)){
		return;
    }

    padiAttempts = 0;
    timeout = PADI_TIMEOUT;

    init_connect_status();

    do {
		padiAttempts++;
		if (padiAttempts > PADI_count) {
			if(vlan_detect == 1)
			{
				warn("Timeout waiting for PADO packets");
				close(conn->discoverySocket);
				conn->discoverySocket = -1;
				goto detect_retry; 
			}
		}

		arc_pppd_dbg("PPPoE send PADI.\n");
		sendPADI(conn);
		conn->discoveryState = STATE_SENT_PADI;
		conn->numPADOs = 0;

		if(vlan_detect == 1 && padiAttempts == 4 && strcmp(conn->ifName, second_iface) == 0)
		{
			timeout = VLAN_TAG_DETECT_DELAY;
			sprintf(message_str, "\"Without successful PADO response.\"");
			sprintf(cmd,"umng_syslog_cli addErrorCode -1 R020 \"%s\"",message_str);
			system(cmd);
		}
		waitForPADO(conn, timeout);

#if 0
		timeout *= 2;
#endif
		if (vlan_detect == 1 && conn->numPADOs)
		{
			tagging = 1;
			PADI_count = 0;
			{
				if( strcmp(conn->ifName, untag_iface) == 0 && vlan_tagged != 0)
				{
					sprintf(cmd, "abscfg set ARC_WAN_xy_VLAN_Tagging %d %d 0", gWan_Type, gWan_Idx);
					system(cmd);
					sprintf(cmd, "interface-agent wan %d", gWan_Idx);
					system(cmd);
					arc_pppd_dbg("detect non-vlan\n");
				}
				else if( strcmp(conn->ifName, tag_iface) == 0 && vlan_tagged != 1)
				{
					sprintf(cmd, "abscfg set ARC_WAN_xy_VLAN_Tagging %d %d 1", gWan_Type, gWan_Idx);
					system(cmd);
					sprintf(cmd, "interface-agent wan %d", gWan_Idx);
					system(cmd);
					arc_pppd_dbg("detect vlan\n");
				}

			}
		}
    } while (conn->discoveryState == STATE_SENT_PADI);

    timeout = conn->discoveryTimeout;
    do {
		padrAttempts++;
		if (padrAttempts > MAX_PADI_ATTEMPTS ||pppoe_reconnect_flag) {
		    warn("Timeout waiting for PADS packets");
		    close(conn->discoverySocket);
		    conn->discoverySocket = -1;
		    pppoe_reconnect_flag = 0;
		    return;
		}

		arc_pppd_dbg("PPPoE send PADR.\n");
		sendPADR(conn);
		conn->discoveryState = STATE_SENT_PADR;
		waitForPADS(conn, timeout);
		if (conn->error) {
			return;
		}
#if 0
		timeout *= 2;
#endif
    } while (conn->discoveryState == STATE_SENT_PADR);

#ifdef MINI_JUMBO_FRAME
    info("discovery:: Negotiated MRU=%d. MRU should be adapted by caller\n",conn->mru);
#endif

	//guang_zhao 20130410, send signal to dnsmasq for clear dns cache when pppoe starts again.
	pid = get_tmp_long(RESOLV_PID);
	if(pid > 0)
		kill(pid, SIGHUP);

    /* We're done. */
    conn->discoveryState = STATE_SESSION;
    return;
}
