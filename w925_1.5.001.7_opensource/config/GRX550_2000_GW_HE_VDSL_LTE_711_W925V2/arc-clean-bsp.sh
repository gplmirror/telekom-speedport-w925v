#!/bin/bash

BUILD_DIR=`pwd`
BSP_DIR=lantiq-bsp
#PRJ_PROFILE=""
SCRIPTS_DIR=$BUILD_DIR/scripts
EXTERN_DIR=$BUILD_DIR/extern

. $SCRIPTS_DIR/arc-config.sh

#[ -f "$BUILD_DIR/active_config" ] && {
#        PRJ_PROFILE=`sed -n 's/..config\/\([^ ]*\).*/\1/p' $BUILD_DIR/active_config`
#        echo "PRJ_PROFILE=$PRJ_PROFILE"
#}
make -C $EXTERN_DIR/$BSP_DIR PROFILE=${CONFIG_BSP_IB_PROFILE} clean
# make -C $EXTERN_DIR/$BSP_DIR/cfe/build/broadcom/bcm63xx_rom PROFILE=VRV9510RWAC34-1-B-ASU clean
