#!/bin/sh

BASEDIR="$PWD"
CONFIGNAME=$1

. $BASEDIR/scripts/arc-config.sh
. $BASEDIR/scripts/arc-common.sh

BSPNAME="$CONFIG_BSP_CPU_MODEL"

if [ "$CONFIG_BSP_BUILD_SOURCE" = "0" ]; then
    echo "Check pre-built BSP."
    check_bsp_ftp_update
else
    echo "Check remote BSP."

    if ! check_bsp_source_tree; then
	echo "BSP tree is not existing."
	return 0
    fi

    check_git_update "$BASEDIR/extern/${CONFIG_BSP_NAME}/${BSPNAME}" $CONFIG_BSP_SRC_BRANCH
    if [ $? -eq 1 ]; then
	$BASEDIR/update_bsp.sh
    fi
fi
