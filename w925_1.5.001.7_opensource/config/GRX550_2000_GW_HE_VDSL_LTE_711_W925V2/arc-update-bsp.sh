#!/bin/sh
#
# Prepare below folders
# 1. Toolchain.
#   ${BASEDIR}/extern/toolchain
# 2. Imagebuilder.
#   ${BASEDIR}/extern/imagebuilder
# 3. Linux headers for ko module.
#   ${BASEDIR}/extern/linux
# 4. Staging rootfs
#   ${BASEDIR}/${CONFIG_BSP_IB_STAGING_ROOTPATH}
# 5. Host utilities
#   ${BASEDIR}/staging_dir/host

BASEDIR="$PWD"
CONFIGNAME=$1

. $BASEDIR/scripts/arc-config.sh
. $BASEDIR/scripts/arc-common.sh

BSPNAME="$CONFIG_BSP_CPU_MODEL"

if [ "$CONFIG_BSP_BUILD_SOURCE" = "0" ]; then
    extract_bsp_archive
else
    if ! check_bsp_source_tree; then
	echo "Check BSP source tree failed!"
	exit 1
    fi

    echo "Re-Build BSP..."
    if ! build_bsp_source_tree; then
	echo "Re-Build BSP failed!"
	exit 1
    fi

    echo -e "Remove extern/{toolchain,imagebuilder,linux}"
    rm -rf $BASEDIR/extern/{toolchain,imagebuilder,linux}
    # Terry 20160923, workaround to fix some build server can't remove symbolic links well
    (cd $BASEDIR/extern && rm -rf toolchain imagebuilder linux)

    # Make symbolic link to BSP
    if [ ! -d $BASEDIR/extern/toolchain ] || \
	   [ ! -d $BASEDIR/extern/imagebuilder ] || \
	   [ ! -d $BASEDIR/extern/linux ] || \
	   [ ! -d $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH} ]; then
	if ! link_bsp_path; then
	    echo "Prepare BSP failed!"
	    exit 1
	fi
    fi
fi

BSPCONFIGNAME=$(echo $CONFIG_BSP_IB_CONFIGNAME | awk '{print toupper($0)}')
echo -e "\033[34;01m################################################################################"
echo -e "# Environment is ready for: ${CONFIGNAME}"
echo -e "# BSP config name:          ${BSPCONFIGNAME}"
echo -e "# Command to build firmware image:"
echo -e "#   make V=99"
echo -e "# Command to clean the codebase:"
echo -e "#   make clean"
echo -e "# Command to clean everything: (Re-execute './scripts/arc-change-project.sh' is required)"
echo -e "#   make dirclean"
echo -e "# Change configuration:"
echo -e "#   make menuconfig"
echo -e "# Location of official firmware images:"
echo -e "#   $BASEDIR/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}"
echo -e "################################################################################\033[00;00m"
