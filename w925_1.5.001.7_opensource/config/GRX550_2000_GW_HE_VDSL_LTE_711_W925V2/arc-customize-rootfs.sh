#!/bin/bash

ROOTFS_DIR=$1

usage() {
  echo "usage: $0 <rootfs directory>"
}

dbg() {
  echo $1
}

if [ "x$ROOTFS_DIR" = "x" ] ; then
  usage
  exit -1
fi

if [ ! -d $ROOTFS_DIR ] ; then
  echo "$ROOTFS_DIR does not exist!"
  exit -1
fi

#copy /etc/dbus-1 to /usr/share/config/dbus-1
mkdir -p $ROOTFS_DIR/usr/share/config
#cp -af $ROOTFS_DIR/etc/dbus-1 $ROOTFS_DIR/usr/share/config/
#cp -af $ROOTFS_DIR/etc/config/glbcfg.* $ROOTFS_DIR/usr/share/config/
#cp -af $ROOTFS_DIR/etc/config/.glbcfg $ROOTFS_DIR/usr/share/config/
#cp -af $ROOTFS_DIR/etc/passwd  $ROOTFS_DIR/usr/share/config/
#cp -af $ROOTFS_DIR/etc/config/arc-middle $ROOTFS_DIR/usr/share/config/
#cp -af $ROOTFS_DIR/etc/services $ROOTFS_DIR/usr/share/services
#cp -af $ROOTFS_DIR/etc/l7-protocols $ROOTFS_DIR/usr/share/l7-protocols
# cp -af $ROOTFS_DIR/etc/ $ROOTFS_DIR/usr/share/config/
# symbolic link /etc
# rm -rf $ROOTFS_DIR/etc
# ln -sf /tmp/etc $ROOTFS_DIR/etc
###tar czvf $ROOTFS_DIR/etc/dftconfig.tgz -C $ROOTFS_DIR/etc ./config
#rm -rf $ROOTFS_DIR/etc/config
#ln -sf /tmp/etc/config $ROOTFS_DIR/etc/config

# symbolic link /var
#rm -rf $ROOTFS_DIR/var
#ln -sf /tmp/var $ROOTFS_DIR/var
rm -rf $ROOTFS_DIR/var
ln -sf /ramdisk/var $ROOTFS_DIR/var

# symbolic link /etc
#rm -rf $ROOTFS_DIR/mnt
#ln -sf /tmp/media $ROOTFS_DIR/mnt

# symbolic link /etc/udev
rm -rf $ROOTFS_DIR/etc/udev
ln -sf /tmp/etc/udev $ROOTFS_DIR/etc/udev

# dbus message /usr/share/msg
mkdir -p $ROOTFS_DIR/usr/share/msg

# symbolic link for mtlk firmware
(cd $ROOTFS_DIR; for f in root/mtlk/images/*; do ln -sf /tmp/mtlk_images/$(basename $f) lib/firmware/; done)

# Remove unused files
rm -rf $ROOTFS_DIR/{init,stamp,CONTROL}
find $ROOTFS_DIR/lib -name *.a -delete

# PPP script
ln -sf /usr/scripts/ip-up $ROOTFS_DIR/etc/ppp/ip-up
ln -sf /usr/scripts/ip-down $ROOTFS_DIR/etc/ppp/ip-down
ln -sf /usr/scripts/ipv6-up $ROOTFS_DIR/etc/ppp/ipv6-up
ln -sf /usr/scripts/ipv6-down $ROOTFS_DIR/etc/ppp/ipv6-down

# DNS proxy
(cd $ROOTFS_DIR/etc; ln -sf /tmp/resolv.conf resolv.conf)

# Samba /Pure-FTP
mkdir -p $ROOTFS_DIR/ramdisk_copy/etc
(cd $ROOTFS_DIR/etc; ln -sf ../ramdisk/etc/shadow shadow)
(cd $ROOTFS_DIR/etc; ln -sf ../ramdisk/etc/samba samba)
(cd $ROOTFS_DIR/etc; cp passwd ../ramdisk_copy/etc/passwd)
(cd $ROOTFS_DIR/etc; cp group ../ramdisk_copy/etc/group)
#(cd $ROOTFS_DIR/etc; ln -sf ../ramdisk/etc/passwd passwd)
(cd $ROOTFS_DIR/etc; ln -sf ../ramdisk/etc/group group)

# Twonky DLNA server
mkdir -p $ROOTFS_DIR/ramdisk_copy/tmp/
cp $ROOTFS_DIR/usr/share/twonkyserver/resources/devicedescription-custom-settings.txt $ROOTFS_DIR/ramdisk_copy/tmp/
ln -sf /tmp/devicedescription-custom-settings.txt $ROOTFS_DIR/usr/share/twonkyserver/resources/devicedescription-custom-settings.txt
# cp $ROOTFS_DIR/usr/share/twonkyserver/twonkyserver-default.ini $ROOTFS_DIR/ramdisk_copy/tmp/
# ln -sf /tmp/twonkyserver-default.ini $ROOTFS_DIR/usr/share/twonkyserver/twonkyserver-default.ini

# Redirect /tmp/rw_fs to /etc/rw_fs
mkdir -p $ROOTFS_DIR/etc/rw_fs/primary
mkdir -p $ROOTFS_DIR/ramdisk_copy/tmp/primary
(cd $ROOTFS_DIR/ramdisk_copy/tmp; ln -sf /etc/rw_fs rw_fs)

# Remove unused service lib.
find $ROOTFS_DIR/opt/lantiq/servd/lib/ ! -name 'libdslsl.so' ! -name 'libethsl.so' ! -name 'libsyssl.so' -delete
#find $ROOTFS_DIR/opt/lantiq/servd/lib/ ! -name 'libethsl.so' ! -name 'libsyssl.so' -delete
#find $ROOTFS_DIR/usr/agents/ ! -name 'arca-autowandetect-agent.so' ! -name 'arca-dsl-agent.so' ! -name 'arca-httpd-agent.so' -delete
find $ROOTFS_DIR/usr/agents/ -name 'arca-wanppp-agent.so' -delete

# For WiFi 2.4G calibration
# (cd $ROOTFS_DIR/lib/firmware; ln -sf /tmp/mtlk_images/cal_wlan0.bin cal_wlan0.bin)

# Remove files of unused packages
unused_pack="iotivity iotivity-cpp iotivity-hanfun ltq-voice-tapidemo libcellwan libwebsockets lighttpd lighttpd-mod-access lighttpd-mod-accesslog lighttpd-mod-alias lighttpd-mod-auth lighttpd-mod-cgi lighttpd-mod-compress lighttpd-mod-fastcgi lighttpd-mod-setenv lighttpd-mod-usertrack lighttpd-mod-websocket libmultiwan luci luci-app-firewall luci-base luci-lib-ip luci-lib-nixio luci-mod-admin-full luci-proto-ipv6luci-proto-ipv6 luci-proto-ppp luci-theme-bootstrap mwan3 umbim vsftpd webcgi websockets wss_gateway-common wss_gateway-hue_plugin wss_gateway-mini_plugin_manager wwan"
for i in $unused_pack
do
	[ ! -f $ROOTFS_DIR/usr/lib/opkg/info/$i.list ] && continue;

	for j in $(cat $ROOTFS_DIR/usr/lib/opkg/info/$i.list)
	do
		case $j in
			/etc/init.d/*) #might have link file in /etc/rc.d
				Name=$(echo $j | awk -F/ '{print $4}')
				Snum=$(grep 'START=' $ROOTFS_DIR/$j | awk -F= '{print $2}')
				[ -n "$Snum" ] && rm -f $ROOTFS_DIR/etc/rc.d/S$Snum$Name
				Knum=$(grep 'STOP=' $ROOTFS_DIR/$j | awk -F= '{print $2}')
				[ -n "$Knum" ] && rm -f $ROOTFS_DIR/etc/rc.d/K$Knum$Name
				;;
		esac
		rm -f $ROOTFS_DIR/$j
		fdir=$(dirname $ROOTFS_DIR/$j)
		while [ -z "$(ls -A $fdir 2>/dev/null)" ] # true if fdir is empty directory
		do
			rmdir $fdir 2>/dev/null
			fdir=$(dirname $fdir)
		done
	done
	rm -f $ROOTFS_DIR/usr/lib/opkg/info/{$i.list,$i.control,$i.conffiles}
done
rm -f $ROOTFS_DIR/usr/sbin/tapidemo
rm -f $ROOTFS_DIR/usr/sbin/ifxsip
rm -f $ROOTFS_DIR/opt/lantiq/bin/ifxsip
#rm -f $ROOTFS_DIR/opt/lantiq/www

# Make symbolic link for scripts in /etc/init.d
( \
    cd $ROOTFS_DIR; \
    for script in ./etc/init.d/*; do \
	grep '#!/bin/sh /etc/rc.common' $script >/dev/null || continue; \
	IPKG_INSTROOT=$ROOTFS_DIR $(which bash) ./etc/rc.common $script enable; \
    done || true \
)
