#!/bin/sh
#
#
BASEDIR="$PWD"
IMAGEBUILDER_DIR="${BASEDIR}/extern/imagebuilder"
DEST_DIR="$1"

. $BASEDIR/scripts/arc-config.sh

make -C ${IMAGEBUILDER_DIR} image PROFILE=${CONFIG_BSP_IB_PROFILE}

# Copy arc-utility rootfs in staging_dir
(cd ${BASEDIR}/staging_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq-stripped; \
		tar cf - . | (cd ${IMAGEBUILDER_DIR}/staging_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq && tar xf -))

# Copy arc-utility rootfs in build_dir
(cd ${BASEDIR}/build_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq; \
		tar cf - . | (cd ${IMAGEBUILDER_DIR}/build_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq && tar xf -)) 

# Execute mklibs together with BSP's rootfs
make mklibs PROFILE=${CONFIG_BSP_IB_PROFILE} \
		STAGING_DIR_ROOT=${IMAGEBUILDER_DIR}/staging_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq \
		TARGET_DIR=${IMAGEBUILDER_DIR}/build_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq 

mkdir -p ${DEST_DIR}

# Customize rootfs
${BASEDIR}/scripts/arc-customize-rootfs.sh ${IMAGEBUILDER_DIR}/build_dir/${CONFIG_BSP_IB_TARGETNAME}/root-lantiq 

make -C ${IMAGEBUILDER_DIR} build_image USER_PROFILE=${CONFIG_BSP_IB_PROFILE}

cp -Rp ${BASEDIR}/${CONFIG_BSP_IB_FW_SRC} ${DEST_DIR}

echo -e "\033[34;01m################################################################################"
echo -e "# The location of firmware images:"
echo -e "#   1. arc-utility:"
echo -e "#      ${DEST_DIR}"
echo -e "#   2. official: "
echo -e "#      ${BASEDIR}/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}"
echo -e "################################################################################\033[00;00m"
date
