#!/bin/sh
#
# Prepare below folders
# 1. Toolchain.
#   ${BASEDIR}/extern/toolchain
# 2. Imagebuilder.
#   ${BASEDIR}/extern/imagebuilder
# 3. Linux headers for ko module.
#   ${BASEDIR}/extern/linux
# 4. Staging rootfs
#   ${BASEDIR}/${CONFIG_BSP_IB_STAGING_ROOTPATH}
# 5. Host utilities
#   ${BASEDIR}/staging_dir/host

BASEDIR="$PWD"
CONFIGNAME=$1

. $BASEDIR/scripts/arc-config.sh
. $BASEDIR/scripts/arc-common.sh

BSPNAME="$CONFIG_BSP_CPU_MODEL"
DLBSP=0
UDBSP=0

if [ -n "$CONFIG_BSP_SRC_BRANCH" ]; then
	BSPBRANCH="$CONFIG_BSP_SRC_BRANCH"
else
	BSPBRANCH="master"
fi 

ask_bool() {
	local DEFAULT="$1"; shift
	local def defstr val
	case "$DEFAULT" in
		1) def=0; defstr="Y/n";;
		0) def=1; defstr="y/N";;
		*) def=;  defstr="y/n";;
	esac
	while [ -z "$val" ]; do
		local VAL

		echo -en "$* ($defstr): "
		# printf "$* ($defstr): "
		read VAL
		case "$VAL" in
			y*|Y*) val=0;;
			n*|N*) val=1;;
			*) val="$def";;
		esac
	done
	return "$val"
}

if [ "$CONFIG_BSP_BUILD_SOURCE" = "1" ]; then
		if [ ! -d $BASEDIR/extern/$CONFIG_BSP_NAME/${BSPNAME} ]; then
			mkdir -p $BASEDIR/extern/${CONFIG_BSP_NAME}
			cd $BASEDIR/extern/${CONFIG_BSP_NAME}
			echo "Clone BSP $CONFIG_BSP_SRC_URL"
			git clone -q -b $BSPBRANCH $CONFIG_BSP_SRC_URL ${BSPNAME} 2>/dev/null
			if [ "$?" = "0" ]; then
 				echo "Build BSP..."
 				if ! build_bsp_source_tree; then
 					echo "Build BSP failed!"
					exit 1
 				fi
 				UDBSP=1
			else
 				DLBSP=1
			fi
		else
 			echo "Build BSP..."
 			if ! build_bsp_source_tree; then
 				echo "Build BSP failed!"
 				exit 1
 			fi
			UDBSP=1
		fi
else
	DLBSP=1
fi

if [ "$CONFIG_BSP_DISABLE_DOWNLOAD" = "1" ]; then
        echo -e "Disable BSP check and download."
        UDBSP=1
        DLBSP=0
fi

if [ $DLBSP = 1 ]; then
	BSP_VERSION=""
	BSP_SERVER_VERSION=""
	if [ -f $BASEDIR/extern/${CONFIG_BSP_NAME}/.bsp_version ]; then
		BSP_VERSION=$(cat $BASEDIR/extern/${CONFIG_BSP_NAME}/.bsp_version)
	fi

	BSP_SERVER_VERSION=$(curl -s ${CONFIG_BSP_URL}/ | sort -nr -k 9 | head -n 1 | awk '{print $9}')
	
	echo -e "\033[1mLocal BSP version:  ${BSP_VERSION}"
	echo -e "Server BSP version: ${BSP_SERVER_VERSION}\033[00;00m"
	
	if [ "${BSP_VERSION}" != "${BSP_SERVER_VERSION}" ] && [ "${BSP_SERVER_VERSION}" != "" ]; then
		if ask_bool 1 "\033[1mNew BSP version detected. Do you want to upgrade local BSP?\n\033[5m(Local: ${BSP_VERSION}, Server: ${BSP_SERVER_VERSION})\033[00;00m"; then
			echo "Download Toolchain and Imagebuilder."
			UDBSP=1
			rm -rf $BASEDIR/extern/${CONFIG_BSP_NAME}/${CONFIG_BSP_TC_FILENAME} \
					$BASEDIR/extern/${CONFIG_BSP_NAME}/${CONFIG_BSP_IB_FILENAME} \
					$BASEDIR/extern/${CONFIG_BSP_NAME}/.bsp_version
			mkdir -p $BASEDIR/extern/${CONFIG_BSP_NAME}
			(cd $BASEDIR/extern/${CONFIG_BSP_NAME} && \
					wget ${CONFIG_BSP_URL}/${BSP_SERVER_VERSION}/${CONFIG_BSP_TC_FILENAME} && \
					wget ${CONFIG_BSP_URL}/${BSP_SERVER_VERSION}/${CONFIG_BSP_IB_FILENAME})
			echo ${BSP_SERVER_VERSION} > $BASEDIR/extern/${CONFIG_BSP_NAME}/.bsp_version;
		fi
	fi
fi

if [ $UDBSP = 1 ]; then
	echo -e "Remove extern/{toolchain,imagebuilder,linux}"
	rm -rf $BASEDIR/extern/{toolchain,imagebuilder,linux}
	# Terry 20160923, workaround to fix some build server can't remove symbolic links well
	(cd $BASEDIR/extern && rm -rf toolchain imagebuilder linux)
fi

# Extract archive files
if [ ! -d $BASEDIR/extern/toolchain ] || \
		[ ! -d $BASEDIR/extern/imagebuilder ] || \
		[ ! -d $BASEDIR/extern/linux ] || \
		[ ! -d $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH} ]; then
	if [ $DLBSP = 1 ]; then
 		if ! extract_bsp_archive; then
 			echo "Extract BSP failed!"
  			exit 1
		fi
	else
 		if ! link_bsp_path; then
 			echo "Prepare BSP failed!"
 			exit 1
 		fi
	fi
fi

BSPCONFIGNAME=$(echo $CONFIG_BSP_IB_CONFIGNAME | awk '{print toupper($0)}')
echo -e "\033[34;01m################################################################################"
echo -e "# Environment is ready for: ${CONFIGNAME}"
echo -e "# BSP config name:          ${BSPCONFIGNAME}"
echo -e "# Command to build firmware image:"
echo -e "#   make V=99"
echo -e "# Command to clean the codebase:"
echo -e "#   make clean"
echo -e "# Command to clean everything: (Re-execute './scripts/arc-change-project.sh' is required)"
echo -e "#   make dirclean"
echo -e "# Change configuration:"
echo -e "#   make menuconfig"
echo -e "# Location of official firmware images:"
echo -e "#   $BASEDIR/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}"
echo -e "################################################################################\033[00;00m"
