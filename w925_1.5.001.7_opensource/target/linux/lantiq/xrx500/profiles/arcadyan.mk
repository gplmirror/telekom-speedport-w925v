define Profile/VRL9519WWAC34_B_IR
  NAME:=Arcadyan VRL9519WWAC34_B_IR VDSL Hybrid Board (GRX350)
endef
$(eval $(call Profile,VRL9519WWAC34_B_IR))

define Profile/VRV951BXWAC44_B_23
  NAME:=Arcadyan VRV951BXWAC44_B_23 VDSL Board (GRX350)
endef
$(eval $(call Profile,VRV951BXWAC44_B_23))

define Profile/NRASA9517015J
  NAME:=Arcadyan NRASA9517015J Board (GRX350)
endef
$(eval $(call Profile,NRASA9517015J))

define Profile/VRV951BXWAC44_B_23_R0Ai
  NAME:=Arcadyan VRV951BXWAC44_B_23 R0Ai VDSL Board (GRX550)
endef
$(eval $(call Profile,VRV951BXWAC44_B_23_R0Ai))

define Profile/VRV951BXWAC44_B_23_R0A2
  NAME:=Arcadyan VRV951BXWAC44_B_23 R0A2 VDSL Board (GRX550)
endef
$(eval $(call Profile,VRV951BXWAC44_B_23_R0A2))

define Profile/W925V2
  NAME:=Arcadyan W925V2 VDSL Board (GRX550)
endef
$(eval $(call Profile,W925V2))
