# Lantiq SoC xRX330 Family/Reference Boards

define Profile/EASY389_BOND
  NAME:=EASY389 BOND Reference Board (600MHz)
  PACKAGES:=
endef

$(eval $(call Profile,EASY389_BOND))

define Profile/EASY330
 NAME:=EASY330 Singleline DSL with Power Monitoring INA219
endef

$(eval $(call Profile,EASY330))

define Profile/EASY300_BOND
  NAME:=EASY300 VDSL BOND Board (600MHz)
  PACKAGES:=
endef

$(eval $(call Profile,EASY300_BOND))

define Profile/EASY330_GFAST
  NAME:=EASY330 G.Fast Board (720MHz)
  PACKAGES:=
endef

$(eval $(call Profile,EASY330_GFAST))

define Profile/EASY330_BOND
  NAME:=EASY330 VDSL BOND Board (720MHz)
  PACKAGES:=
endef

$(eval $(call Profile,EASY330_BOND))

define Profile/VRV9518SWAC24_B_49
  NAME:=Arcadyan VRV9518SWAC24-B-49 VDSL IAD Board (600MHz)
  PACKAGES:=
endef

$(eval $(call Profile,VRV9518SWAC24_B_49))
