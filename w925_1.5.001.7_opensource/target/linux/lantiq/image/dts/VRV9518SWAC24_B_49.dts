/dts-v1/;


/include/ "EASY330_BOND.dtsi"

/ {
	sram@1F000000 {
		#address-cells = <1>;
		#size-cells = <1>;
		compatible = "lantiq,sram", "simple-bus";
		reg = <0x1F000000 0x800000>;
		ranges = <0x0 0x1F000000 0x7FFFFF>;

		eiu0: eiu@101000 {
			#interrupt-cells = <1>;
			interrupt-controller;
			compatible = "lantiq,eiu-xway";
			reg = <0x101000 0x1000>;
			interrupt-parent = <&icu0>;
			interrupts = <166 135 66 40 41 42>;
		};

		pmu0: pmu@102000 {
			compatible = "lantiq,pmu-xway";
			reg = <0x102000 0x1000>;
		};

		cgu0: cgu@103000 {
			compatible = "lantiq,cgu-xway";
			reg = <0x103000 0x1000>;
		};

		dcdc@106a00 {
			compatible = "lantiq,dcdc-xrx200";
			reg = <0x106a00 0x200>;
		};

		ts: ts@106F00 {
			compatible = "lantiq,ts-grx390";
			reg = <0x106F00 0x10>;
			interrupt-parent = <&icu0>;
			interrupts = <143>;
			lantiq,numofsensors = <0x1>;
		}; 

		rcu0: reset-controller@203000 {
			compatible = "lantiq,rcu-xway";
			reg = <0x203000 0x1000>;
			#reset-cells = <1>; 
			/* irq for thermal sensor */
			interrupt-parent = <&icu0>;
			interrupts = <115>;
		};

		mps@107000 {
			compatible = "lantiq,mps-xrx100";
			reg = <0x107000 0x400>;
			interrupt-parent = <&icu0>;
			interrupts = <154 155>;
			slic-reset = <&gpio 25 0>;
			lantiq,mbx = <&mpsmbx>;
		};

		mpsmbx: mpsmbx@200000 {
			reg = <0x200000 0x200>;
		};
	};
	
	fpi@10000000 {
		localbus@0 {
			ranges = <0 0 0x4000000 0x3ffffff>;
			nand-parts@0 {
				compatible = "gen_nand", "lantiq,nand-xway";
				lantiq,cs = <1>;
				bank-width = <2>;
				reg = <0 0x0 0x2000000>;
				#address-cells = <1>;
				#size-cells = <1>;

				partition@0 {
					label = "uboot";
					reg = <0x000000 0x100000>;
				};

				partition@100000 {
					label = "ubootconfigA";
					reg = <0x100000 0x40000>;
				};

				partition@140000 {
					label = "ubootconfigB";
					reg = <0x140000 0x40000>;
				};

				partition@180000 {
					label = "gphyfirmware";
					reg = <0x180000 0x40000>;
				};

				partition@1c0000 {
					label = "system_sw";
					reg = <0x1c0000 0xf800000>;
				};

				partition@f9c0000 {
					label = "calibration";
					reg = <0xf9c0000 0x100000>;
				};

				partition@fac0000 {
					label = "glbcfg";
					reg = <0xfac0000 0x200000>;
				};

				partition@fcc0000 {
					label = "board_data";
					reg = <0xfcc0000 0x100000>;
				};
				
				partition@fdc0000 {
					label = "res";
					reg = <0xfdc0000 0x240000>;
				};
			};
		};

		spi@E100800 {
			compatible = "lantiq,spi-xway-broken", "lantiq,spi-ssc";
			reg = <0xE100800 0x100>;
			interrupt-parent = <&icu0>;
			interrupts = <22 23 24>;
			#address-cells = <1>;
			#size-cells = <1>;
			
			m25p80 {
				/* Remove */
			};
		};
		
		gpio: pinmux@E100B10 {
			compatible = "lantiq,pinctrl-ar10";
			pinctrl-names = "default";
			pinctrl-0 = <&state_default>;

			interrupt-parent = <&icu0>;
			interrupts = <166 135 66 40 41 42 38>;

			#gpio-cells = <2>;
			gpio-controller;
			reg = <0xE100B10 0xA0>;

			state_default: pinmux {
				exin3 {
					lantiq,groups = "exin3";
					lantiq,function = "exin";
				};
				exin5 {
					lantiq,groups = "exin5";
					lantiq,function = "exin";
				};
				stp {
					lantiq,groups = "stp";
					lantiq,function = "stp";
				};
				spi {
					lantiq,groups = "spi", "spi_cs1";
					lantiq,function = "spi";
				};
				nand {
					lantiq,groups = "nand cle", "nand ale",
							"nand rd", "nand rdy";
					lantiq,function = "ebu";
				};
				mdio {
					lantiq,groups = "mdio";
					lantiq,function = "mdio";
				};
				conf_out {
					lantiq,pins = "io24", "io13", "io49", /* nand cle, ale and rd */
							"io4", "io5", "io6", /* stp */
							"io15", "io17", "io18", /* spi */
							"io42", "io43"; /* USB1/USB2 power */
					lantiq,open-drain = <0>;
					lantiq,pull = <0>;
					lantiq,output = <1>;
				};
				conf_in {
					lantiq,pins = "io39", /* exin3 */
							"io9", /* exin5 */
							"io48", /* nand rdy */
							"io16", /* spi in */
							"io3", /* DECT button */
							"io14"; /* WiFi button */
					lantiq,pull = <2>;
				};
			};
		};
		
		stp: stp@E100BB0 {
			compatible = "lantiq,gpio-stp-xway";
			reg = <0xE100BB0 0x40>;
			#gpio-cells = <2>;
			gpio-controller;

			lantiq,shadow = <0xffff>;
			lantiq,groups = <0x1>;
			lantiq,dsl = <0x0>;
			lantiq,phy1 = <0x0>;
			lantiq,phy2 = <0x0>;
			lantiq,phy3 = <0x0>;
			lantiq,phy4 = <0x0>;
			/* lantiq,rising; */
		};

		usb@E101000 {
			status = "okay";
			vbus = <&gpio 42 0> , <&gpio 43 0>;
			oc_off     = <1 1>;
		};

		/* From xRX330.dtsi */
		pcie1:pcie@9000000 {
			compatible = "lantiq,pcie-xrx200";
			interrupt-parent = <&icu0>;
			interrupts = <57 17 18 19 20 49 50 51 52>;
			/*pcie-reset = <&stp 12 0>;*/
			/*GPIO pin id is 186 */
			pcie-reset = <&stp 0 0>;
		};
	};

	ltq_swreset {
		compatible = "lantiq,ltq_swreset";
		swreset_pin = <&gpio 61 0>;
		swreset_bit = <1>;
		status = "okay";
	};

	dect {
		compatible = "lantiq,ltqdect";
		interrupt-parent = <&eiu0>; /*External Interrupt*/
		interrupts = <42>; /* External interrupt connected to IM1_IRL2; (IRQBase=8)+(IM1=32)+(IRL2=2)=42*/
		lantiq,dect-cs = <&gpio 15 0>; /*DECT chip select using GPIO APIs*/
		gpio-reset = <&gpio 19 0>; /*COSIC Reset PIN connected to Shift Reg interface but uses GPIO APIs*/
		status = "okay";
	};

	dect-page {
		compatible = "lantiq,ltqdect-page";
		lantiq,pagepin= <&gpio 3 0>; /*DECT Page Pin connected to GPIO 3*/
		lantiq,pagebit= <1>; /*DECT Page Bit: Bit Info meant for DECT*/
		status = "okay";
	};
	
	gphy-xrx200 {
		compatible = "lantiq,phy-xrx200";
		firmware = "lantiq/ltq_fw_PHY11G_IP_1v5_xRx3xx_A21_R8434.bin";
		/* phys = [ 00 01 02 03 ]; */
		phys = [ 00 01 02 ];
	};

	gphy-fw {
		compatible = "lantiq,xway-phy-fw";
		fw-mode = "11G-FW"; /*"11G-FW"*/ /*22F-FW*/
		no_of_phys = <3>;
	};

	gpio-leds {
		compatible = "gpio-leds";

		/* WiFi */
		wlan0 {
			label = "wlan0:green";
			gpios = <&stp 1 1>;
		};
		/* Internet red */
		lte_led {
			label = "lte_led";
			gpios = <&stp 4 1>;
		};
		/* VoIP */
		voip_led {
			label = "voip_led";
			gpios = <&gpio 10 1>;
		};
		/* LAN */
		wlan1 {
			label = "wlan1:green";
			gpios = <&stp 2 1>;
		};
		/* DSL */
		broadband_led {
			label = "broadband_led";
			gpios = <&stp 5 1>;
		};
		/* Info green */
		broadband_led1 {
			label = "broadband_led1";
			gpios = <&stp 6 1>;
		};
		/* Internet green */
		internet_led {
			label = "internet_led";
			gpios = <&stp 3 1>;
		};
		/* DECT */
		dect_led {
			label = "dect_led";
			gpios = <&gpio 11 1>;
		};
		/* O2 board additional */
		/* Info red */
		info_unpin {
			label = "info_unpin";
			gpios = <&stp 7 1>;
		};
		power_led {
			label = "power_led";
			gpios = <&gpio 26 1>;
			default-state = "on";
		};
		power_update {
			label = "power_update";
			gpios = <&gpio 27 1>;
			default-state = "off";
		};
		qtn_reset {
			label = "qtn_reset";
			gpios = <&gpio 58 1>;
			default-state = "off";
		};
	};
};
