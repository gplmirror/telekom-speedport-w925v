#!/bin/sh

if [ -z "$CONFIG_BSP_NAME" ]; then
    echo -e "Please include arc-config.sh first!"
    exit 1
fi

ask_bool() {
	local DEFAULT="$1"; shift
	local def defstr val
	case "$DEFAULT" in
		1) def=0; defstr="Y/n";;
		0) def=1; defstr="y/N";;
		*) def=;  defstr="y/n";;
	esac
	while [ -z "$val" ]; do
		local VAL

		echo -en "$* ($defstr): "
		read VAL
		case "$VAL" in
			y*|Y*) val=0;;
			n*|N*) val=1;;
			*) val="$def";;
		esac
	done
	return "$val"
}

check_bsp_source_tree()
{
    if [ ! -d $BASEDIR/extern/${CONFIG_BSP_NAME}/$BSPNAME ]; then
	echo -e "There is no BSP source tree existence in: $BASEDIR/extern/${CONFIG_BSP_NAME}/$BSPNAME"
	return 1
    fi
}

build_bsp_source_tree()
{
    (cd $BASEDIR/extern/${CONFIG_BSP_NAME}/$BSPNAME && eval $CONFIG_BSP_SRC_MAKE) || \
    {
	echo -E "Build BSP source tree failed!"
	return 1
    }
}

extract_bsp_archive()
{
    printf "\033[34;01mExtract files:\n\t${CONFIG_BSP_TC_FILENAME}\n\t${CONFIG_BSP_IB_FILENAME}\n"
    printf "\033[34;05mPlease wait...\n\033[00;00m"
    BSP_TC_ROOTNAME=$(echo ${CONFIG_BSP_TC_ROOTPATH} | awk -F/ '{ print $1; }')
    BSP_IB_ROOTNAME=$(echo ${CONFIG_BSP_IB_ROOTPATH} | awk -F/ '{ print $1; }')
    if [ ! -f $BASEDIR/extern/${CONFIG_BSP_NAME}/${CONFIG_BSP_TC_FILENAME} ]; then
	echo -e "Can't find ToolChain archive: $BASEDIR/extern/${CONFIG_BSP_NAME}/${CONFIG_BSP_TC_FILENAME}"
	return 1
    fi
    if [ ! -f $BASEDIR/extern/${CONFIG_BSP_NAME}/${CONFIG_BSP_IB_FILENAME} ]; then
	echo -e "Can't find ToolChain archive: $BASEDIR/extern/${CONFIG_BSP_NAME}/${CONFIG_BSP_IB_FILENAME}"
	return 1
    fi
    
    (cd $BASEDIR/extern/${CONFIG_BSP_NAME} && \
	    (pv ${CONFIG_BSP_TC_FILENAME} | tar jxf -) && \
	    mkdir -p $BASEDIR/{build_dir,staging_dir} && \
	    rm -rf $BASEDIR/extern/toolchain && \
	    mkdir -p $BASEDIR/extern/toolchain && \
	    (cd ${CONFIG_BSP_TC_ROOTPATH}; tar cf - . | (cd $BASEDIR/extern/toolchain && tar xf -)) && \
	    rm -rf ${BSP_TC_ROOTNAME} && \
	    (pv ${CONFIG_BSP_IB_FILENAME} | tar jxf -) && \
	    echo -e "Preparing build environment. Please wait..." && \
	    rm -rf $BASEDIR/extern/imagebuilder && \
	    mkdir -p $BASEDIR/extern/imagebuilder && \
	    (cd ${CONFIG_BSP_IB_ROOTPATH}; tar cf - . | (cd $BASEDIR/extern/imagebuilder && tar xf -)) && \
	    rm -rf $BASEDIR/extern/linux && \
	    mkdir -p $BASEDIR/extern/linux && \
	    (cd ${CONFIG_BSP_IB_ROOTPATH}/${CONFIG_LINUX_DIR}; tar cf - . | (cd $BASEDIR/extern/linux && tar xf -)) && \
	    rm -rf ${BSP_IB_ROOTNAME} && \
	    (if [ -L $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH} ]; then rm -f $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH}; fi) && \
	    mkdir -p $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH} && \
	    (cd $BASEDIR/extern/imagebuilder/${CONFIG_BSP_IB_STAGING_ROOTPATH} && rsync -avu . --exclude 'root-lantiq' --exclude 'root-lantiq*' $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH}) && \
	    (if [ -L $BASEDIR/staging_dir/host ]; then rm -f $BASEDIR/staging_dir/host; fi) && \
	    mkdir -p $BASEDIR/staging_dir/host && \
	    (cd $BASEDIR/extern/imagebuilder/staging_dir/host && rsync -avu . --exclude 'bin/bison' --exclude 'bin/flex' $BASEDIR/staging_dir/host) && \
	    rm -rf $BASEDIR/${CONFIG_LINUX_DIR} && \
	    mkdir -p $(dirname $BASEDIR/${CONFIG_LINUX_DIR}) && \
	    ln -fs $BASEDIR/extern/linux $BASEDIR/${CONFIG_LINUX_DIR}) || \
    {
	echo -e "Extract BSP archives failed!"
	return 1
    }
    
    # Backup official image
    mkdir -p $BASEDIR/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}
    cp -Rp $BASEDIR/${CONFIG_BSP_IB_FW_SRC} $BASEDIR/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}
    return 0
}

link_bsp_path()
{
    if [ ! -d $BASEDIR/extern/${CONFIG_BSP_NAME} ]; then
	echo -e "Missing $BASEDIR/extern/${CONFIG_BSP_NAME}"
	return 1;
    fi
    
    printf "\033[34;05mPlease wait ...\n\033[00;00m"
    
    (cd $BASEDIR/extern/${CONFIG_BSP_NAME} && \
	    echo -e "Preparing build environment. Please wait..." && \
	    mkdir -p $BASEDIR/{build_dir,staging_dir} && \
	    rm -rf $BASEDIR/extern/toolchain && \
	    ln -fs $CONFIG_BSP_TC_BSP_PATH $BASEDIR/extern/toolchain && \
	    rm -rf $BASEDIR/extern/imagebuilder && \
	    ln -fs $CONFIG_BSP_IB_BSP_PATH $BASEDIR/extern/imagebuilder && \
	    rm -rf $BASEDIR/extern/linux && \
	    ln -fs ${CONFIG_BSP_IB_BSP_PATH}/${CONFIG_LINUX_DIR} $BASEDIR/extern/linux && \
	    rm -rf ${BSP_IB_ROOTNAME} && \
	    (if [ -L $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH} ]; then rm -f $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH}; fi) && \
	    mkdir -p $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH} && \
	    (cd $BASEDIR/extern/imagebuilder/${CONFIG_BSP_IB_STAGING_ROOTPATH} && rsync -avu . --exclude 'root-lantiq' --exclude 'root-lantiq*' $BASEDIR/${CONFIG_BSP_IB_STAGING_ROOTPATH}) && \
	    (if [ -L $BASEDIR/staging_dir/host ]; then rm -f $BASEDIR/staging_dir/host; fi) && \
	    mkdir -p $BASEDIR/staging_dir/host && \
	    (cd $BASEDIR/extern/imagebuilder/staging_dir/host && rsync -avu . --exclude 'bin/bison' --exclude 'bin/flex' $BASEDIR/staging_dir/host) && \
	    rm -rf $BASEDIR/${CONFIG_LINUX_DIR} && \
	    mkdir -p $(dirname $BASEDIR/${CONFIG_LINUX_DIR}) && \
	    ln -fs $BASEDIR/extern/linux $BASEDIR/${CONFIG_LINUX_DIR}) || \
    {
	echo -e "Make symbolic link for BSP failed!"
	return 1
    }

    # Backup official image
    mkdir -p $BASEDIR/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}
    cp -Rp $BASEDIR/${CONFIG_BSP_IB_FW_SRC} $BASEDIR/bin-official/lantiq/${CONFIG_BSP_IB_CONFIGNAME}

    return 0
}

check_git_update()
{
    DIR=$1

    if [ -n "$2" ]; then
	BRANCH="$2"
    else
	BRANCH="master"
    fi

    [ ! -d $DIR ] && return 0
    (cd $DIR && git fetch &> /dev/null) || \
    {
	echo "Can't get remote repository."
	return 0
    }
    HEAD_REV=$(cd $DIR && git rev-parse HEAD)
    REMOTE_REV=$(cd $DIR && git rev-parse origin/$BRANCH)
    [ "$HEAD_REV" = "$REMOTE_REV" ] && { echo "$DIR is up-to-date" ; return 0; }
    echo -e "\033[1mWarning! $DIR branch:$BRANCH is out-of-date\033[00;00m"
    if ask_bool 1 "\033[1mDo you want to continue?\033[00;00m"; then
	return 0
    else
	echo "*******************************************************"
	echo "* Please use below command to update the codebase:"
	echo "* (cd $DIR && git pull --rebase)"
	echo "*"
	echo "* Please use below command to re-build the updated BSP."
	echo "* ./update_bsp.sh"
	echo "*******************************************************"
	exit 1
    fi
}

check_bsp_ftp_update()
{
    if [ "$CONFIG_BSP_BUILD_SOURCE" != "0" ]; then
	return 0
    fi
    
    BSP_VERSION=""
    BSP_SERVER_VERSION=""
    if [ -f $BASEDIR/extern/${CONFIG_BSP_NAME}/.bsp_version ]; then
	BSP_VERSION=$(cat $BASEDIR/extern/${CONFIG_BSP_NAME}/.bsp_version)
    fi

    BSP_SERVER_VERSION=$(curl -s ${CONFIG_BSP_URL}/ | sort -nr -k 9 | head -n 1 | awk '{print $9}')
    
    echo -e "\033[1mLocal BSP version:  ${BSP_VERSION}"
    echo -e "Server BSP version: ${BSP_SERVER_VERSION}\033[00;00m"
    
    if [ "${BSP_VERSION}" != "${BSP_SERVER_VERSION}" ] && [ "${BSP_SERVER_VERSION}" != "" ]; then
	echo -e "\033[1mWarning! Prebuilt BSP is out-of-date\033[00;00m"
	if ask_bool 1 "\033[1mDo you want to continue?\033[00;00m"; then
	    return 0
	else
	    echo "*******************************************************"
	    echo "* Please use below command to update pre-built BSP:"
	    echo "* (echo Y|./scripts/arc-prepare-bsp.sh)"
	    echo "*"
	    echo "*******************************************************"
	    exit 1
	fi
    fi
}
